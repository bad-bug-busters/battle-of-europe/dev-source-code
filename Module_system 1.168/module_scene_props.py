# -*- coding: cp1252 -*-
from header_common import *
from header_scene_props import *
from header_operations import *
from header_triggers import *
from header_sounds import *
from module_constants import *
from header_items import * #MOD
import string

####################################################################################################################
#  Each scene prop record contains the following fields:
#  1) Scene prop id: used for referencing scene props in other files. The prefix spr_ is automatically added before each scene prop id.
#  2) Scene prop flags. See header_scene_props.py for a list of available flags
#  3) Hit points #MOD
#  4) Mesh name: Name of the mesh.
#  5) Physics object name:
#  6) Triggers: Simple triggers that are associated with the scene prop
####################################################################################################################

#MOD begin
set_material_earth = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_earth)])
set_material_water = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_water)])
set_material_stone = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_stone)])
set_material_wood = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_wood)])
set_material_metal = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_metal)])
set_material_ceramic = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_ceramic)])
set_material_glass = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_glass)])
set_material_bone = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_bone)])
set_material_leather = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_leather)])
set_material_fabric = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_fabric)])
set_material_food = (ti_on_init_scene_prop,[(call_script, "script_init_scene_prop", material_food)])

set_negative_slot_values = (ti_on_init_scene_prop,
[
	(store_trigger_param_1, ":instance_id"),
	(try_for_range, ":cur_scene_prop_slot", negative_prop_slots_begin, negative_prop_slots_end),
		(scene_prop_set_slot, ":instance_id", ":cur_scene_prop_slot", -1),
	(try_end),
])

parent_prop_spawn = (ti_on_init_scene_prop,
[
	(this_or_next|multiplayer_is_server),
	(neg|game_in_multiplayer_mode),
	
  (store_trigger_param_1, ":parent_instance_id"),

  (prop_instance_get_position, pos0, ":parent_instance_id"),
  
	(set_fixed_point_multiplier, 100),
	(position_get_x, ":pos_x", pos0),
	(position_get_y, ":pos_y", pos0),
	(position_get_z, ":pos_z", pos0),
	(position_get_rotation_around_x, ":rot_x", pos0),
	(position_get_rotation_around_y, ":rot_y", pos0),
	(position_get_rotation_around_z, ":rot_z", pos0),
	
	(val_mul, ":rot_x", -1),
	(val_mul, ":rot_y", -1),
	  
	(scene_prop_set_slot, ":parent_instance_id", scene_prop_default_pos_x, ":pos_x"),
	(scene_prop_set_slot, ":parent_instance_id", scene_prop_default_pos_y, ":pos_y"),
	(scene_prop_set_slot, ":parent_instance_id", scene_prop_default_pos_z, ":pos_z"),
	(scene_prop_set_slot, ":parent_instance_id", scene_prop_default_rot_x, ":rot_x"),
	(scene_prop_set_slot, ":parent_instance_id", scene_prop_default_rot_y, ":rot_y"),
	(scene_prop_set_slot, ":parent_instance_id", scene_prop_default_rot_z, ":rot_z"),
	
	# (assign, reg0, ":pos_x"),
	# (assign, reg1, ":pos_y"),
	# (assign, reg2, ":pos_z"),
	# (assign, reg3, ":rot_x"),
	# (assign, reg4, ":rot_y"),
	# (assign, reg5, ":rot_z"),
	# (display_message, "@X:{reg0} Y:{reg1} Z:{reg2} x:{reg3} y:{reg4} z:{reg5}"),
])
	
child_prop_spawn = (ti_on_init_scene_prop,
[
	(this_or_next|multiplayer_is_server),
	(neg|game_in_multiplayer_mode),
	
  (store_trigger_param_1, ":child_instance_id"),

	(set_fixed_point_multiplier, 100),
  (prop_instance_get_scene_prop_kind, ":child_instance_kind", ":child_instance_id"),
  (try_begin),
    (this_or_next|eq, ":child_instance_kind", "spr_cannon_barrel_1_32lb"),
    (eq, ":child_instance_kind", "spr_cannon_wheel_1"),
    (try_begin),
      (eq, ":child_instance_kind", "spr_cannon_barrel_1_32lb"),
      (try_begin),
        (scene_prop_slot_eq, "$g_last_parent_prop", scene_prop_child_1, -1),
        (scene_prop_set_slot, "$g_last_parent_prop", scene_prop_child_1, ":child_instance_id"),
        (scene_prop_set_slot, ":child_instance_id", scene_prop_parent, "$g_last_parent_prop"),
      (try_end),
			(try_begin),
				(eq, "$g_quick_artillery", 1),
				(scene_prop_set_slot, ":child_instance_id", scene_prop_usage_state, 2),
			(try_end),
    (else_try),
      (eq, ":child_instance_kind", "spr_cannon_wheel_1"),
      (assign, ":last_prop_slot", scene_prop_child_6),
      (try_for_range, ":cur_prop_slot", scene_prop_child_2, ":last_prop_slot"),
        (scene_prop_slot_eq, "$g_last_parent_prop", ":cur_prop_slot", -1),
        (scene_prop_set_slot, "$g_last_parent_prop", ":cur_prop_slot", ":child_instance_id"),
        (scene_prop_set_slot, ":child_instance_id", scene_prop_parent, "$g_last_parent_prop"),
        (assign, ":last_prop_slot", -1),#stop loop
				#(prop_instance_get_position, pos7, ":child_instance_id"),
        (store_random_in_range, ":rot_x", 0, 36000),#randomize wheel rotation
        (position_rotate_x_floating, pos_spawn, ":rot_x"),
        (prop_instance_set_position, ":child_instance_id", pos_spawn, 1),
        (scene_prop_set_slot, ":child_instance_id", scene_prop_rot_x, ":rot_x"),
				(scene_prop_set_slot, ":child_instance_id", scene_prop_default_rot_x, ":rot_x"),
      (try_end),
    (try_end),
  (try_end),
])

shield_prop_spawn = (ti_on_init_scene_prop,
[
	(store_trigger_param_1, ":instance_id"),

	(prop_instance_get_scene_prop_kind, ":instance_kind", ":instance_id"),
	(try_begin),
		(this_or_next|eq, ":instance_kind", "spr_pavise_prop_b"),
		(this_or_next|eq, ":instance_kind", "spr_pavise_prop_b_+1"),
		(this_or_next|eq, ":instance_kind", "spr_pavise_prop_b_+2"),
		(eq, ":instance_kind", "spr_pavise_prop_b_+3"),
		(assign, ":tableau", "tableau_pavise_shield_2"),
		(try_begin),
			(eq, ":instance_kind", "spr_pavise_prop_b"),
			(assign, ":item_id", itm_tab_shield_pavise_b),
		(else_try),
			(eq, ":instance_kind", "spr_pavise_prop_b_+1"),
			(assign, ":item_id", itm_tab_shield_pavise_b +loom_1_item),
		(else_try),
			(eq, ":instance_kind", "spr_pavise_prop_b_+2"),
			(assign, ":item_id", itm_tab_shield_pavise_b +loom_2_item),
		(else_try),
			(assign, ":item_id", itm_tab_shield_pavise_b +loom_3_item),
		(try_end),
		
	(else_try),
		(this_or_next|eq, ":instance_kind", "spr_pavise_prop_c"),
		(this_or_next|eq, ":instance_kind", "spr_pavise_prop_c_+1"),
		(this_or_next|eq, ":instance_kind", "spr_pavise_prop_c_+2"),
		(eq, ":instance_kind", "spr_pavise_prop_c_+3"),
		(assign, ":tableau", "tableau_pavise_shield_1"),
		(try_begin),
			(eq, ":instance_kind", "spr_pavise_prop_c"),
			(assign, ":item_id", itm_tab_shield_pavise_c),
		(else_try),
			(eq, ":instance_kind", "spr_pavise_prop_c_+1"),
			(assign, ":item_id", itm_tab_shield_pavise_c +loom_1_item),
		(else_try),
			(eq, ":instance_kind", "spr_pavise_prop_c_+2"),
			(assign, ":item_id", itm_tab_shield_pavise_c +loom_2_item),
		(else_try),
			(assign, ":item_id", itm_tab_shield_pavise_c +loom_3_item),
		(try_end),
	(try_end),
	  
	(item_get_hit_points, ":hit_points", ":item_id"),
	(item_get_body_armor, ":resistance", ":item_id"),
	(scene_prop_set_hit_points, ":instance_id", ":hit_points"),
	(scene_prop_set_slot, ":instance_id", scene_prop_resistance, ":resistance"),

	(assign, ":value", -1),
	(try_begin),
		(gt, "$agent_deployed_shield", -1),
		(agent_is_active, "$agent_deployed_shield"),
		(agent_get_troop_id, ":troop_no", "$agent_deployed_shield"),
		(agent_get_team, ":agent_team", "$agent_deployed_shield"),
		(team_get_faction, ":team_faction_no", ":agent_team"),
		(faction_get_slot, ":value", ":team_faction_no", slot_faction_flag),
		(val_mul, ":value", 1000),
		(call_script, "script_agent_troop_get_banner_mesh", "$agent_deployed_shield", ":troop_no"),
		(val_add, ":value", reg0),
	(try_end),
	(cur_scene_prop_set_tableau_material, ":tableau", ":value"),
	  
	#dealing damage to prop wont work in this trigger, so do it later
	(try_begin),
		(this_or_next|multiplayer_is_server),
		(neg|game_in_multiplayer_mode),
		(gt, "$next_spawn_prop_hp", -1),
		(assign, "$spawn_prop_different_hp", ":instance_id"),
	(try_end),
])
	
construct_prop_spawn = (ti_on_init_scene_prop,
[
	(store_trigger_param_1, ":instance_id"),
	#dealing damage to prop wont work in this trigger, so do it later
	(try_begin),
		(this_or_next|multiplayer_is_server),
		(neg|game_in_multiplayer_mode),
		(gt, "$next_spawn_prop_hp", -1),
		(assign, "$spawn_prop_different_hp", ":instance_id"),
	(try_end),
])	
	
ammo_box_prop_spawn = (ti_on_init_scene_prop,
[
	(store_trigger_param_1, ":instance_id"),

	(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
	(prop_instance_get_scene_prop_kind, ":instance_kind", ":instance_id"),
	(try_begin),
		(eq, ":instance_kind", "spr_cannon_ammo_box_32lb_1"),
		(scene_prop_set_slot, ":instance_id", scene_prop_item_1, itm_round_shot_32lb),
		(scene_prop_set_slot, ":instance_id", scene_prop_item_1_max_ammo, 24),
		(scene_prop_set_slot, ":instance_id", scene_prop_item_1_cur_ammo, 24),
		(scene_prop_set_slot, ":instance_id", scene_prop_item_2, itm_canister_shot_32lb),
		(scene_prop_set_slot, ":instance_id", scene_prop_item_2_max_ammo, 12),
		(scene_prop_set_slot, ":instance_id", scene_prop_item_2_cur_ammo, 12),
	(try_end),
])	

# Note that on client-side in multiplayer missions only trigger param 1 and position registers are available. Trigger params 2..7 are only available to server or in singleplayer.
# trigger param 1 = prop instance_id reference
# trigger param 2 = amount of damage delivered to the scene prop (not available on client side in multiplayer)
# trigger param 3 = attacker agent_id (not available on client side in multiplayer)
# trigger param 4 = item_id of the weapon used to attack
# trigger param 5 = weapon item modifier
# trigger param 6 = item_id of missile used to attack (if attack was by ranged weapon)
# trigger param 7 = missile item modifier
# pos1            = position of the hit area
# pos2            = X field holds attacker's agent_id reference
check_prop_hit_trigger = (ti_on_scene_prop_hit,
[
  (store_trigger_param_1, ":instance_no"),
	(store_trigger_param_2, ":damage"),
	(store_trigger_param, ":item_id", 4),
	(store_trigger_param, ":missile_item_no", 6),
	
	(set_fixed_point_multiplier, 1),
	(position_get_x, ":agent_id", pos2),
	(set_fixed_point_multiplier, 100),

	# (scene_prop_get_max_hit_points, reg20, ":instance_no"),
	# (display_message, "@{reg20}"),

	(prop_instance_get_scene_prop_kind, ":prop_kind", ":instance_no"),
	(is_between, ":prop_kind", destructible_props_begin, destructible_props_end),

	(scene_prop_get_max_hit_points, ":max_hit_points", ":instance_no"),
	(scene_prop_get_hit_points, ":hit_points", ":instance_no"),
	(scene_prop_get_slot, ":resistance", ":instance_no", scene_prop_resistance),

	(copy_position, pos_hit, pos1),
	(call_script, "script_calc_attack_penetration", ":item_id", ":damage", ":resistance", ":agent_id", ":missile_item_no"),
	(assign, ":final_damage", reg0),

	(assign, ":usage_value", 0),
	(assign, ":repair_value", 0),

	(try_begin),
		(eq, ":prop_kind", "spr_gunpowder_barrel"),
		(item_slot_eq, ":missile_item_no", slot_item_is_alt_mode, 1),
		(call_script, "script_item_missile_hit", ":agent_id", 2, missile_bomb),
		(call_script, "script_move_scene_prop_under_ground", ":instance_no", 0),
	(try_end),
	
	(try_begin),
		(troop_slot_eq, "trp_prop_destructible", ":prop_kind", 1),
		(try_begin),
			(neg|multiplayer_is_dedicated_server),#clientside
			(copy_position, pos_calc, pos1),
			(try_begin),
				(eq, ":prop_kind", "spr_construct_earthwork"),
				(try_begin),
					(eq, ":item_id", "itm_engineer_shovel"),
					(call_script, "script_reserve_sound_channel", "snd_digging"),
				(try_end),
			(else_try),
				(store_sub, ":hp_result", ":hit_points", ":final_damage"),
				(try_begin),
					(gt, ":hp_result", 0),
					(call_script, "script_reserve_sound_channel", "snd_dummy_hit"),
				(else_try),
					(call_script, "script_reserve_sound_channel", "snd_dummy_destroyed"),
				(try_end),
				(particle_system_burst_no_sync, "psys_bullet_hit_objects", pos1, 2),
			(try_end),
		(try_end),
		
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			
			(agent_is_active,":agent_id"),
			(agent_is_alive, ":agent_id"),

			(try_begin),
				(eq, ":prop_kind", "spr_construct_earthwork"),
					
				(assign, ":max_movement", 100),
				(store_mul, ":dmg_move_x1000", ":max_movement", 1000),
				(val_div, ":dmg_move_x1000", ":max_hit_points"),
					
				(store_mul, ":cur_z_pos", ":hit_points", ":dmg_move_x1000"),
				(val_div, ":cur_z_pos", 1000),
				
				(val_add, ":max_hit_points", 1),
					
				(agent_get_action_dir, ":action_dir", ":agent_id"),
				(try_begin),
					(eq, ":item_id", "itm_engineer_shovel"),
					(eq, ":action_dir", 0),
					(assign, ":repair_value", ":final_damage"),
					(assign, ":final_damage", 0),
					(val_add, ":hit_points", ":repair_value"),
					(val_clamp, ":hit_points", 0, ":max_hit_points"),#no invalid values
					(scene_prop_set_cur_hit_points, ":instance_no", ":hit_points"),
				(else_try),
					(try_begin),
						(neq, ":item_id", "itm_engineer_shovel"),
						(val_div, ":final_damage", 10),
					(try_end),
					(val_sub, ":hit_points", ":final_damage"),
					(val_clamp, ":hit_points", 0, ":max_hit_points"),#no invalid values
				(try_end),
					
				(store_mul, ":next_z_pos", ":hit_points", ":dmg_move_x1000"),
				(val_div, ":next_z_pos", 1000),
					
				(store_sub, ":move_z", ":next_z_pos", ":cur_z_pos"),
				(try_begin),
					(neq, ":move_z", 0),
					(prop_instance_get_position, pos1, ":instance_no"),
					(position_move_z, pos1, ":move_z"),
					(prop_instance_set_position, ":instance_no", pos1, 0),
						# (assign, reg1, ":move_z"),
						# (assign, reg2, ":cur_z_pos"),
						# (assign, reg3, ":next_z_pos"),
						# (display_message, "@move: {reg1} cur: {reg2} next: {reg3}"),
				(try_end),

			(else_try),
				(eq, ":item_id", "itm_engineer_hammer"),
				(assign, ":repair_value", 50),
				(assign, ":final_damage", 0),
				(val_add, ":hit_points", ":repair_value"),
				(val_add, ":max_hit_points", 1),
				(val_clamp, ":hit_points", 0, ":max_hit_points"),#no invalid values
				(val_sub, ":max_hit_points", 1),
				(scene_prop_set_cur_hit_points, ":instance_no", ":hit_points"),
			(try_end),
			
			(try_begin),
				(gt, ":repair_value", ":final_damage"),
				(assign, ":usage_value", ":repair_value"),
			(else_try),
				(assign, ":usage_value", ":final_damage"),
			(try_end),
		(try_end),
		
	# (else_try),
		# (this_or_next|eq, ":missile_item_no", "itm_grenade"),
		# (this_or_next|eq, ":missile_item_no", "itm_spam_grenade"),
		# (eq, ":missile_item_no", "itm_round_shot_32lb_missile"),
		
		# (val_sub, ":final_damage", ":resistance"),
		# (val_max, ":final_damage", 0),
		# (assign, ":usage_value", ":final_damage"),
		
		# (copy_position, pos_hit, pos1),
		# (call_script, "script_server_missile_hit_scene_prop", ":instance_no", ":final_damage"),
	(try_end),
	
	(this_or_next|multiplayer_is_server),
	(neg|game_in_multiplayer_mode),
	(try_begin),
		(gt, ":usage_value", 0),
		(multiplayer_is_server),
		(neg|agent_is_non_player, ":agent_id"),
		
		#upkeep
		(try_begin),
			(multiplayer_is_dedicated_server),
			(gt, ":item_id", -1),
			(item_get_type, ":item_type", ":item_id"),
			(this_or_next|eq, ":item_type", itp_type_one_handed_wpn),
			(this_or_next|eq, ":item_type", itp_type_two_handed_wpn),
			(eq, ":item_type", itp_type_polearm),
			(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":agent_id", ":item_id", ":usage_value"),
		(try_end),
		
		#send damage report to dealer player
		(multiplayer_is_server),
		(agent_is_active, ":agent_id"),
		(agent_get_player_id, ":dealer_player", ":agent_id"),
		(player_is_active, ":dealer_player"),
		(assign, reg0, ":final_damage"),
		(try_begin),
			(player_slot_eq, ":dealer_player", slot_player_show_damage_report, 1),
			(str_store_string, s0, "str_delivered_damage_to_object"),
			(call_script, "script_multiplayer_send_message_to_player", ":dealer_player", color_dealer_damage),
		(try_end),
		
		(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
		(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
		(try_begin),
			(gt, ":repair_value", ":final_damage"),
			(store_sub, ":cur_score", ":max_hit_points", ":hit_points"),
			(try_begin),
				(lt, ":repair_value", ":cur_score"),
				(assign, ":cur_score", ":repair_value"),
			(try_end),
		(else_try),
			(assign, ":cur_score", ":hit_points"),
			(try_begin),
				(lt, ":final_damage", ":cur_score"),
				(assign, ":cur_score", ":final_damage"),
			(try_end),
			(val_add, ":cur_score", ":resistance"),
		(try_end),
		
		(try_begin),
			(this_or_next|eq, ":missile_item_no", "itm_grenade"),
			(this_or_next|eq, ":missile_item_no", "itm_spam_grenade"),
			(eq, ":missile_item_no", "itm_round_shot_32lb_missile"),
			(val_div, ":cur_score", score_artillery_divider),
		(else_try),
			(val_div, ":cur_score", score_prop_divider),
		(try_end),
		
		(gt, ":cur_score", 0),
		(player_get_slot, ":player_score", ":dealer_player", slot_player_score),
		(val_add, ":player_score", ":cur_score"),
		(player_set_slot, ":dealer_player", slot_player_score, ":player_score"),
		
		(get_max_players, ":num_players"),
		(try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
			(player_is_active, ":player_no"),
			(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_score_count, ":dealer_player", ":player_score"),
		(try_end),
	(try_end),

	#set trigger result
	(set_trigger_result, ":final_damage"),
])

check_prop_destroy_trigger = (ti_on_scene_prop_destroy,
[
	# (display_message, "@ti_on_scene_prop_destroy", 0xCCCCCC),
	(store_trigger_param_1, ":instance_no"),
	(call_script, "script_move_scene_prop_under_ground", ":instance_no", 0),
])
	
check_item_start_use_trigger = (ti_on_scene_prop_start_use,#only server
[
	(store_trigger_param_1, ":agent_id"),
	(store_trigger_param_2, ":instance_id"),

	#(display_message, "@start use called"),
	(call_script, "script_start_use_item", ":instance_id", ":agent_id"),
	#for only server itself-----------------------------------------------------------------------------------------------
	(multiplayer_is_server),
	(get_max_players, ":num_players"),                               
	(try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
		(player_is_active, ":player_no"),
		(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_start_use_item, ":instance_id", ":agent_id"),
	(try_end),
])
#MOD end


check_item_use_trigger = (ti_on_scene_prop_use,#only server
    [
      (store_trigger_param_1, ":agent_id"),
      (store_trigger_param_2, ":instance_id"),

      #(display_message, "@use called"),
      (call_script, "script_use_item", ":instance_id", ":agent_id"),
      #for only server itself-----------------------------------------------------------------------------------------------
      (multiplayer_is_server),#MOD edit
      (get_max_players, ":num_players"),                               
      (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
        (player_is_active, ":player_no"),
        (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_use_item, ":instance_id", ":agent_id"),
      (try_end),
    ])

check_sally_door_use_trigger_double = (ti_on_scene_prop_use,
    [
      (store_trigger_param_1, ":agent_id"),
      (store_trigger_param_2, ":instance_id"),

      (agent_get_position, pos1, ":agent_id"),
      (prop_instance_get_starting_position, pos2, ":instance_id"),
      
      (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),

      (try_begin),
        #out doors like castle sally door can be opened only from inside, if door coordinate is behind your coordinate. Also it can be closed from both sides.
        
        (prop_instance_get_scene_prop_kind, ":scene_prop_id", ":instance_id"),
        
        (assign, ":can_open_door", 0),
        (try_begin),
          (neg|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),
          (neg|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),
          (neg|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),
          (neg|eq, ":scene_prop_id", "spr_earth_sally_gate_left"),
          
          (position_is_behind_position, pos1, pos2),
          (assign, ":can_open_door", 1),
        (else_try),  
          (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_right"),
          (this_or_next|eq, ":scene_prop_id", "spr_viking_keep_destroy_sally_door_left"),
          (this_or_next|eq, ":scene_prop_id", "spr_earth_sally_gate_right"),
          (eq, ":scene_prop_id", "spr_earth_sally_gate_left"),

          (neg|position_is_behind_position, pos1, pos2),
          (assign, ":can_open_door", 1),
        (try_end),
        
        (this_or_next|eq, ":can_open_door", 1),
        (eq, ":opened_or_closed", 1),
      
        (try_begin),
          #for only server itself-----------------------------------------------------------------------------------------------
          (call_script, "script_use_item", ":instance_id", ":agent_id"),
          #for only server itself-----------------------------------------------------------------------------------------------
          (multiplayer_is_server),#MOD edit
          (get_max_players, ":num_players"),                               
          (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
            (player_is_active, ":player_no"),
            (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_use_item, ":instance_id", ":agent_id"),
          (try_end),
        (try_end),
      (try_end),
    ])

check_sally_door_use_trigger = (ti_on_scene_prop_use,
    [
      (store_trigger_param_1, ":agent_id"),
      (store_trigger_param_2, ":instance_id"),

      (agent_get_position, pos1, ":agent_id"),
      (prop_instance_get_starting_position, pos2, ":instance_id"),
      
      (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),

      (try_begin),
        #out doors like castle sally door can be opened only from inside, if door coordinate is behind your coordinate. Also it can be closed from both sides.
        (this_or_next|position_is_behind_position, pos1, pos2),
        (eq, ":opened_or_closed", 1),
      
        (try_begin),
          #for only server itself-----------------------------------------------------------------------------------------------
          (call_script, "script_use_item", ":instance_id", ":agent_id"),
          #for only server itself-----------------------------------------------------------------------------------------------
          (multiplayer_is_server),#MOD edit
          (get_max_players, ":num_players"),                               
          (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
            (player_is_active, ":player_no"),
            (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_use_item, ":instance_id", ":agent_id"),
          (try_end),
        (try_end),
      (try_end),
    ])

check_castle_door_use_trigger = (ti_on_scene_prop_use,
    [
      (store_trigger_param_1, ":agent_id"),
      (store_trigger_param_2, ":instance_id"),

 #     (agent_get_position, pos1, ":agent_id"),
 #     (prop_instance_get_starting_position, pos2, ":instance_id"),#MOD disable
      
      (scene_prop_get_slot, ":opened_or_closed", ":instance_id", scene_prop_open_or_close_slot),

      (try_begin),
        (ge, ":agent_id", 0),
        (agent_get_team, ":agent_team", ":agent_id"),

        #in doors like castle room doors can be opened from both sides, but only defenders can open these doors. Also it can be closed from both sides.
        (this_or_next|eq, ":agent_team", 0),
        (eq, ":opened_or_closed", 1),
      
        (try_begin),
          #for only server itself-----------------------------------------------------------------------------------------------
          (call_script, "script_use_item", ":instance_id", ":agent_id"),
          #for only server itself-----------------------------------------------------------------------------------------------
          (multiplayer_is_server),#MOD edit
          (get_max_players, ":num_players"),                               
          (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
            (player_is_active, ":player_no"),
            (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_use_item, ":instance_id", ":agent_id"),
          (try_end),
        (try_end),
      (try_end),
    ])

check_ladder_animate_trigger = (ti_on_scene_prop_is_animating,
    [      
      (store_trigger_param_1, ":instance_id"),
      (store_trigger_param_2, ":remaining_time"),

      (call_script, "script_check_creating_ladder_dust_effect", ":instance_id", ":remaining_time"),
      ])

check_ladder_animation_finish_trigger = (ti_on_scene_prop_animation_finished,
    [
      (store_trigger_param_1, ":instance_id"),

      (prop_instance_enable_physics, ":instance_id", 1),
      ])

scene_props = [
#MOD begin
  ("scene_props_begin", 0, 0, 0, []),
	("clouds", 0, "cloud_layer", 0, []),
	("?_ambient_sounds", 0, 0, 0, []),
#MOD end
  # ("invalid_object",0,"question_mark","0", []),
  ("inventory",sokf_type_container|sokf_place_at_origin,"package","bobaggage", []),
  ("empty", 0, "0", "0", []),
  ("chest_a",sokf_type_container,"chest_gothic","bochest_gothic", []),
  ("container_small_chest",sokf_type_container,"package","bobaggage", []),
  ("container_chest_b",sokf_type_container,"chest_b","bo_chest_b", []),
  ("container_chest_c",sokf_type_container,"chest_c","bo_chest_c", []),
  ("player_chest",sokf_type_container,"player_chest","bo_player_chest", []),
  ("locked_player_chest",0,"player_chest","bo_player_chest", []),
	
	("fire_xs",0,"0","0",
	[set_negative_slot_values,
		(ti_on_init_scene_prop,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
		]),
	]),
	
	("fire_s",0,"0","0",
	[set_negative_slot_values,
		(ti_on_init_scene_prop,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
		]),
	]),
	
	("fire_m",0,"0","0",
	[set_negative_slot_values,
		(ti_on_init_scene_prop,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
		]),
	]),
	
	("fire_l",0,"0","0",
	[set_negative_slot_values,
		(ti_on_init_scene_prop,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
		]),
	]),
	
	("fire_xl",0,"0","0",
	[set_negative_slot_values,
		(ti_on_init_scene_prop,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
		]),
	]),
	
	("grenade",0,"grenade","0",
	[set_negative_slot_values,
		(ti_on_init_scene_prop,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_is_used, 1),
			(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
		]),
	]),

  ("barrier_20m",sokf_invisible|sokf_type_barrier,"barrier_20m","bo_barrier_20m", []),
  ("barrier_16m",sokf_invisible|sokf_type_barrier,"barrier_16m","bo_barrier_16m", []),
  ("barrier_8m" ,sokf_invisible|sokf_type_barrier,"barrier_8m" ,"bo_barrier_8m" , []),
  ("barrier_4m" ,sokf_invisible|sokf_type_barrier,"barrier_4m" ,"bo_barrier_4m" , []),
  ("barrier_2m" ,sokf_invisible|sokf_type_barrier,"barrier_2m" ,"bo_barrier_2m" , []),
  
  ("exit_4m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_4m" ,"bo_barrier_4m" , []),
  ("exit_8m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_8m" ,"bo_barrier_8m" , []),
  ("exit_16m" ,sokf_invisible|sokf_type_barrier_leave,"barrier_16m" ,"bo_barrier_16m" , []),

  ("ai_limiter_2m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_2m" ,"bo_barrier_2m" , []),
  ("ai_limiter_4m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_4m" ,"bo_barrier_4m" , []),
  ("ai_limiter_8m" ,sokf_invisible|sokf_type_ai_limiter,"barrier_8m" ,"bo_barrier_8m" , []),
  ("ai_limiter_16m",sokf_invisible|sokf_type_ai_limiter,"barrier_16m","bo_barrier_16m", []),
	
  ("Shield",sokf_dynamic,"0","boshield", []),

  ("shadow_circle_1",0,"shadow_circle_1","0", []),
  ("shadow_circle_2",0,"shadow_circle_2","0", []),
  ("shadow_square_1",0,"shadow_square_1","0", []),
  ("shadow_square_2",0,"shadow_square_2","0", []),
	
	("glow_a", 0, "glow_a", "0", []),
	("glow_b", 0, "glow_b", "0", []),
 
  ("pointer_arrow", 0, "pointer_arrow", "0", []),
  ("box_a_dynamic",sokf_static_movement|sokf_dynamic_physics,"box_a","bo_box_a", []),

  ("barrier_box",sokf_invisible|sokf_type_barrier3d,"barrier_box","bo_barrier_box", []),
  ("barrier_capsule",sokf_invisible|sokf_type_barrier3d,"barrier_capsule","bo_barrier_capsule", []),
  ("barrier_cone" ,sokf_invisible|sokf_type_barrier3d,"barrier_cone" ,"bo_barrier_cone" , []),
  ("barrier_sphere" ,sokf_invisible|sokf_type_barrier3d,"barrier_sphere" ,"bo_barrier_sphere" , []),

	("headquarters_flag_gray_code_only",sokf_static_movement|sokf_face_player,"mp_flag_white","0", []),  
	("headquarters_pole_code_only",sokf_static_movement,"mp_flag_pole","bo_mp_flag_pole", [set_material_wood,check_prop_hit_trigger]),#MOD edit

	("headquarters_flag_kingdom",sokf_static_movement|sokf_face_player,"ctf_flag_kingdom","0", []),#MOD
	("headquarters_flag_rebel",sokf_static_movement|sokf_face_player,"ctf_flag_kingdom","0", []),#MOD edit
  
#MOD begin
 ("grid_10x2", 0, "grid_10x2", 0, []),

#keep
	("full_keep_b",0,"full_keep_b_mod","bo_full_keep_b_mod", [set_material_stone,check_prop_hit_trigger]),# MOD edit #removed sokf_type_ladder
  ("square_keep_a",0,"square_keep_a","bo_square_keep_a", [set_material_stone,check_prop_hit_trigger]),
  ("square_keep_b",0,"square_keep_b","bo_square_keep_b", [set_material_stone,check_prop_hit_trigger]),
  ("square_keep_c",0,"square_keep_c","bo_square_keep_c", [set_material_stone,check_prop_hit_trigger]),
  ("square_keep_d",0,"square_keep_d","bo_square_keep_d", [set_material_stone,check_prop_hit_trigger]),
  ("square_keep_e",0,"square_keep_e","bo_square_keep_e", [set_material_stone,check_prop_hit_trigger]),
  ("square_keep_f",0,"square_keep_f","bo_square_keep_f", [set_material_stone,check_prop_hit_trigger]),
	("castle_square_keep_a",0,"castle_square_keep_a","bo_castle_square_keep_a", [set_material_stone,check_prop_hit_trigger]),
	("castle_e_keep_a",0,"castle_e_keep_a","bo_castle_e_keep_a", [set_material_stone,check_prop_hit_trigger]),
	("castle_f_keep_a",0,"castle_f_keep_a","bo_castle_f_keep_a", [set_material_stone,check_prop_hit_trigger]),
	("castle_g_square_keep_a",0,"castle_g_square_keep_a","bo_castle_g_square_keep_a", [set_material_stone,check_prop_hit_trigger]),
	("castle_h_keep_a",0,"castle_h_keep_a","bo_castle_h_keep_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_keep_b",0,"castle_h_keep_b","bo_castle_h_keep_b", [set_material_stone,check_prop_hit_trigger]),
	("castle_i_square_keep_a",0,"castle_i_square_keep_a","bo_castle_i_square_keep_a", [set_material_stone,check_prop_hit_trigger]),
  ("earth_square_keep_a",0,"earth_square_keep_a","bo_earth_square_keep_a", [set_material_wood,check_prop_hit_trigger]),
 	("arabian_square_keep_a",0,"arabian_square_keep_a","bo_arabian_square_keep_a", [set_material_stone,check_prop_hit_trigger]), 
	("arabian_castle_keep_a",0,"arabian_castle_keep_a","bo_arabian_castle_keep_a", [set_material_stone,check_prop_hit_trigger]),	
	# ("snowy_castle_square_keep_a",0,"snowy_castle_square_keep_a","bo_snowy_castle_square_keep_a", [set_material_stone,check_prop_hit_trigger]),

#tower
	("castle_gaillard",0,"castle_gaillard","bo_castle_gaillard", [set_material_stone,check_prop_hit_trigger]),
	("castle_courtyard_house_extension_a",0,"castle_courtyard_house_extension_a","bo_castle_courtyard_house_extension_a", [set_material_stone,check_prop_hit_trigger]),
	("castle_courtyard_house_extension_b",0,"castle_courtyard_house_extension_b","bo_castle_courtyard_house_extension_b", [set_material_stone,check_prop_hit_trigger]),
  ("round_tower_a",0,"round_tower_a","bo_round_tower_a", [set_material_stone,check_prop_hit_trigger]),
  ("small_round_tower_a",0,"small_round_tower_a","bo_small_round_tower_a", [set_material_stone,check_prop_hit_trigger]),
  ("small_round_tower_roof_a",0,"small_round_tower_roof_a","bo_small_round_tower_roof_a", [set_material_wood,check_prop_hit_trigger]),
  ("castle_round_tower_a",0,"castle_round_tower_a","bo_castle_round_tower_a", [set_material_stone,check_prop_hit_trigger]),
  ("square_tower_roof_a",0,"square_tower_roof_a","0", [set_material_wood,check_prop_hit_trigger]),
  ("castle_tower_a",0,"castle_tower_a","bo_castle_tower_a", [set_material_stone,check_prop_hit_trigger]),
	("castle_e_tower",0,"castle_e_tower","bo_castle_e_tower", [set_material_stone,check_prop_hit_trigger]),
	("castle_e_corner",0,"castle_e_corner","bo_castle_e_corner", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_corner_b",0,"castle_e_corner_b","bo_castle_e_corner_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_corner_c",0,"castle_e_corner_c","bo_castle_e_corner_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_tower_a",0,"castle_f_tower_a","bo_castle_f_tower_a", [set_material_stone,check_prop_hit_trigger]),# MOD edit #removed sokf_type_ladder
  ("castle_g_tower_a",0,"castle_g_tower_a","bo_castle_g_tower_a", [set_material_stone,check_prop_hit_trigger]),# MOD edit #removed sokf_type_ladder
  ("castle_g_corner_a",0,"castle_g_corner_a","bo_castle_g_corner_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_g_corner_c",0,"castle_g_corner_c","bo_castle_g_corner_c", [set_material_stone,check_prop_hit_trigger]),  
	("castle_h_corner_a",0,"castle_h_corner_a","bo_castle_h_corner_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_corner_c",0,"castle_h_corner_c","bo_castle_h_corner_c", [set_material_stone,check_prop_hit_trigger]),
	("castle_i_tower_a",0,"castle_i_tower_a","bo_castle_i_tower_a", [set_material_stone,check_prop_hit_trigger]),# MOD edit #removed sokf_type_ladder
  ("castle_i_corner_a",0,"castle_i_corner_a","bo_castle_i_corner_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_i_corner_c",0,"castle_i_corner_c","bo_castle_i_corner_c", [set_material_stone,check_prop_hit_trigger]),  
	("earth_tower_a",0,"earth_tower_a","bo_earth_tower_a", [set_material_wood,check_prop_hit_trigger]),
  ("earth_tower_small_a",0,"earth_tower_small_a","bo_earth_tower_small_a", [set_material_wood,check_prop_hit_trigger]),
	("earth_tower_small_b",0,"earth_tower_small_b","bo_earth_tower_small_b", [set_material_wood,check_prop_hit_trigger]),
	("arabian_castle_corner_a",0,"arabian_castle_corner_a","bo_arabian_castle_corner_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_corner_b",0,"arabian_castle_corner_b","bo_arabian_castle_corner_b", [set_material_stone,check_prop_hit_trigger]),
  # ("snowy_castle_tower_a",0,"snowy_castle_tower_a","bo_snowy_castle_tower_a", [set_material_stone,check_prop_hit_trigger]),
  # ("snowy_castle_round_tower_a",0,"snowy_castle_round_tower_a","bo_snowy_castle_round_tower_a", [set_material_stone,check_prop_hit_trigger]),


#gatehouse
  ("gatehouse_new_a",0,"gatehouse_new_a","bo_gatehouse_new_a", [set_material_stone,check_prop_hit_trigger]),# MOD edit #removed sokf_type_ladder
  ("gatehouse_new_b",0,"gatehouse_new_b","bo_gatehouse_new_b", [set_material_stone,check_prop_hit_trigger]),# MOD edit #removed sokf_type_ladder
	("gatehouse_b",0,"gatehouse_b","bo_gatehouse_b", [set_material_stone,check_prop_hit_trigger]),
	("castle_e_gate_house_a",0,"castle_e_gate_house_a","bo_castle_e_gate_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_gatehouse_a",0,"castle_f_gatehouse_a","bo_castle_f_gatehouse_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_g_gate_house",0,"castle_g_gate_house","bo_castle_g_gate_house", [set_material_stone,check_prop_hit_trigger]),
	("castle_h_gatehouse_a",0,"castle_h_gatehouse_a","bo_castle_h_gatehouse_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_i_gate_house",0,"castle_i_gate_house","bo_castle_i_gate_house", [set_material_stone,check_prop_hit_trigger]),
	("gate_house_a",0,"gate_house_a","bo_gate_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("gate_house_b",0,"gate_house_b","bo_gate_house_b", [set_material_stone,check_prop_hit_trigger]),
	("castle_gate_house_a",0,"castle_gate_house_a","bo_castle_gate_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("earth_gate_house_a",0,"earth_gate_house_a","bo_earth_gate_house_a", [set_material_wood,check_prop_hit_trigger]),
	("earth_gate_house_b",0,"earth_gate_house_b","bo_earth_gate_house_b", [set_material_wood,check_prop_hit_trigger]),
	("arabian_castle_gate_house_a",0,"arabian_castle_gate_house_a","bo_arabian_castle_gate_house_a", [set_material_stone,check_prop_hit_trigger]),	
	# ("gatehouse_new_snowy_a",0,"gatehouse_new_snowy_a","bo_gatehouse_new_b", [set_material_stone,check_prop_hit_trigger]),
	# ("snowy_castle_gate_house_a",0,"snowy_castle_gate_house_a","bo_snowy_castle_gate_house_a", [set_material_stone,check_prop_hit_trigger]),

#wall
  ("village_wall_a",0,"village_wall_a","bo_village_wall_a", [set_material_stone,check_prop_hit_trigger]),
  ("village_wall_b",0,"village_wall_b","bo_village_wall_b", [set_material_stone,check_prop_hit_trigger]),
	("battlement_a",0,"battlement_a","bo_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("battlement_a_destroyed",0,"battlement_a_destroyed","bo_battlement_a_destroyed", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_a",0,"castle_battlement_a","bo_castle_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_b",0,"castle_battlement_b","bo_castle_battlement_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_c",0,"castle_battlement_c","bo_castle_battlement_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_a_destroyed",0,"castle_battlement_a_destroyed","bo_castle_battlement_a_destroyed", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_b_destroyed",0,"castle_battlement_b_destroyed","bo_castle_battlement_b_destroyed", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_corner_a",0,"castle_battlement_corner_a","bo_castle_battlement_corner_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_corner_b",0,"castle_battlement_corner_b","bo_castle_battlement_corner_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_corner_c",0,"castle_battlement_corner_c","bo_castle_battlement_corner_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_stairs_a",0,"castle_battlement_stairs_a","bo_castle_battlement_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_battlement_stairs_b",0,"castle_battlement_stairs_b","bo_castle_battlement_stairs_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_battlement_a",0,"castle_e_battlement_a","bo_castle_e_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_battlement_c",0,"castle_e_battlement_c","bo_castle_e_battlement_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_battlement_a_destroyed",0,"castle_e_battlement_a_destroyed","bo_castle_e_battlement_a_destroyed", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_battlement_a",0,"castle_h_battlement_a","bo_castle_h_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_battlement_b",0,"castle_h_battlement_b","bo_castle_h_battlement_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_battlement_c",0,"castle_h_battlement_c","bo_castle_h_battlement_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_battlement_a2",0,"castle_h_battlement_a2","bo_castle_h_battlement_a2", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_battlement_b2",0,"castle_h_battlement_b2","bo_castle_h_battlement_b2", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_battlement_barrier",0,"castle_h_battlement_barrier","bo_castle_h_battlement_barrier", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_a",0,"castle_f_battlement_a","bo_castle_f_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_a_destroyed",0,"castle_f_battlement_a_destroyed","bo_castle_f_battlement_a_destroyed", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_b",0,"castle_f_battlement_b","bo_castle_f_battlement_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_c",0,"castle_f_battlement_c","bo_castle_f_battlement_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_d",0,"castle_f_battlement_d","bo_castle_f_battlement_d", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_e",0,"castle_f_battlement_e","bo_castle_f_battlement_e", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_corner_a",0,"castle_f_battlement_corner_a","bo_castle_f_battlement_corner_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_corner_b",0,"castle_f_battlement_corner_b","bo_castle_f_battlement_corner_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_battlement_corner_c",0,"castle_f_battlement_corner_c","bo_castle_f_battlement_corner_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_g_battlement_a",0,"castle_g_battlement_a","bo_castle_g_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_g_battlement_a1",0,"castle_g_battlement_a1","bo_castle_g_battlement_a1", [set_material_stone,check_prop_hit_trigger]),
	("castle_g_battlement_b",0,"castle_g_battlement_b","bo_castle_g_battlement_b", [set_material_stone,check_prop_hit_trigger]),
	("castle_g_battlement_c",0,"castle_g_battlement_c","bo_castle_g_battlement_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_i_battlement_a",0,"castle_i_battlement_a","bo_castle_i_battlement_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_i_battlement_a1",0,"castle_i_battlement_a1","bo_castle_i_battlement_a1", [set_material_stone,check_prop_hit_trigger]),
  ("castle_i_battlement_c",0,"castle_i_battlement_c","bo_castle_i_battlement_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_wall_stairs_a",sokf_type_ladder,"castle_f_wall_stairs_a","bo_castle_f_wall_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_wall_stairs_b",sokf_type_ladder,"castle_f_wall_stairs_b","bo_castle_f_wall_stairs_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_wall_way_a",0,"castle_f_wall_way_a","bo_castle_f_wall_way_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_wall_way_b",0,"castle_f_wall_way_b","bo_castle_f_wall_way_b", [set_material_stone,check_prop_hit_trigger]),
  ("earth_wall_a",0,"earth_wall_a","bo_earth_wall_a", [set_material_wood,check_prop_hit_trigger]),
  ("earth_wall_a2",0,"earth_wall_a2","bo_earth_wall_a2", [set_material_wood,check_prop_hit_trigger]),
  ("earth_wall_b",0,"earth_wall_b","bo_earth_wall_b", [set_material_wood,check_prop_hit_trigger]),
  ("earth_wall_b2",0,"earth_wall_b2","bo_earth_wall_b2", [set_material_wood,check_prop_hit_trigger]),
	("arabian_castle_battlement_a",0,"arabian_castle_battlement_a","bo_arabian_castle_battlement_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_battlement_b_destroyed",0,"arabian_castle_battlement_b_destroyed","bo_arabian_castle_battlement_b_destroyed", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_battlement_c",0,"arabian_castle_battlement_c","bo_arabian_castle_battlement_c", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_battlement_d",0,"arabian_castle_battlement_d","bo_arabian_castle_battlement_d", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_battlement_section_a",0,"arabian_castle_battlement_section_a","bo_arabian_castle_battlement_section_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_wall_a",0,"arabian_wall_a","bo_arabian_wall_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_wall_b",0,"arabian_wall_b","bo_arabian_wall_b", [set_material_stone,check_prop_hit_trigger]),
	("small_wall_connect_a",0,"small_wall_connect_a","bo_small_wall_connect_a", [set_material_stone,check_prop_hit_trigger]),

	("small_wall_a",0,"small_wall_a","bo_small_wall_a", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_b",0,"small_wall_b","bo_small_wall_b", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_c",0,"small_wall_c","bo_small_wall_c", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_c_destroy",0,"small_wall_c_destroy","bo_small_wall_c_destroy", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_d",0,"small_wall_d","bo_small_wall_d", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_e",0,"small_wall_e","bo_small_wall_d", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_f",0,"small_wall_f","bo_small_wall_f", [set_material_stone,check_prop_hit_trigger]),
  ("small_wall_f2",0,"small_wall_f2","bo_small_wall_f2", [set_material_stone,check_prop_hit_trigger]),

#stair
	("castle_f_sally_port_elevation",0,"castle_f_sally_port_elevation","bo_castle_f_sally_port_elevation", [set_material_stone,check_prop_hit_trigger]),
	("stairs_a",sokf_type_ladder,"stairs_a","bo_stairs_a", [set_material_stone,check_prop_hit_trigger]),
	("stone_stairs_a",sokf_type_ladder,"stone_stairs_a","bo_stone_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("stone_stairs_b",sokf_type_ladder,"stone_stairs_b","bo_stone_stairs_b", [set_material_stone,check_prop_hit_trigger]),
	("stairs_arch_a",sokf_type_ladder,"stairs_arch_a","bo_stairs_arch_a", [set_material_stone,check_prop_hit_trigger]),
  ("square_stairs_a",0,"square_stairs_a","bo_square_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_stairs_a",sokf_type_ladder,"castle_stairs_a","bo_castle_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_stairs_a",0,"castle_e_stairs_a","bo_castle_e_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_stairs_a",sokf_type_ladder,"castle_h_stairs_a","bo_castle_h_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_stairs_b",0,"castle_h_stairs_b","bo_castle_h_stairs_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_f_stairs_a",sokf_type_ladder,"castle_f_stairs_a","bo_castle_f_stairs_a", [set_material_wood,check_prop_hit_trigger]),
  ("earth_stairs_a",sokf_type_ladder,"earth_stairs_a","bo_earth_stairs_a", [set_material_wood,check_prop_hit_trigger]),
  ("earth_stairs_b",sokf_type_ladder,"earth_stairs_b","bo_earth_stairs_b", [set_material_wood,check_prop_hit_trigger]),
	("earth_stairs_c",0,"earth_stairs_c","bo_earth_stairs_c", [set_material_wood,check_prop_hit_trigger]),
	("arabian_castle_stairs",sokf_type_ladder,"arabian_castle_stairs","bo_arabian_castle_stairs", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_stairs_b",sokf_type_ladder,"arabian_castle_stairs_b","bo_arabian_castle_stairs_b", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_stairs_c",sokf_type_ladder,"arabian_castle_stairs_c","bo_arabian_castle_stairs_c", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_stairs",sokf_type_ladder,"arabian_village_stairs","bo_arabian_village_stairs", [set_material_stone,check_prop_hit_trigger]),
	
#houses
  ("castle_e_house_a",0,"castle_e_house_a","bo_castle_e_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_e_house_b",0,"castle_e_house_b","bo_castle_e_house_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_courtyard_house_a",0,"castle_courtyard_house_a","bo_castle_courtyard_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_courtyard_house_b",0,"castle_courtyard_house_b","bo_castle_courtyard_house_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_courtyard_house_c",0,"castle_courtyard_house_c","bo_castle_courtyard_house_c", [set_material_stone,check_prop_hit_trigger]),
  ("castle_courtyard_a",0,"castle_courtyard_a","bo_castle_courtyard_a", [set_material_stone,check_prop_hit_trigger]),	
  ("castle_h_house_a",0,"castle_h_house_a","bo_castle_h_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_house_b",0,"castle_h_house_b","bo_castle_h_house_b", [set_material_stone,check_prop_hit_trigger]),
  ("castle_h_house_c",0,"castle_h_house_c","bo_castle_h_house_b", [set_material_stone,check_prop_hit_trigger]),	

	("rhodok_houses_a",0,"rhodok_houses_a","bo_rhodok_houses_a", [set_material_stone,check_prop_hit_trigger]),
	("rhodok_houses_b",0,"rhodok_houses_b","bo_rhodok_houses_b", [set_material_stone,check_prop_hit_trigger]),
	("rhodok_houses_c",0,"rhodok_houses_c","bo_rhodok_houses_c", [set_material_stone,check_prop_hit_trigger]),
	("rhodok_houses_d",0,"rhodok_houses_d","bo_rhodok_houses_d", [set_material_stone,check_prop_hit_trigger]),
	("rhodok_houses_e",0,"rhodok_houses_e","bo_rhodok_houses_e", [set_material_stone,check_prop_hit_trigger]),
	("rhodok_house_passage_a",0,"rhodok_house_passage_a","bo_rhodok_house_passage_a", [set_material_stone,check_prop_hit_trigger]),

  ("stone_house_a",0,"stone_house_a","bo_stone_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("stone_house_b",0,"stone_house_b","bo_stone_house_b", [set_material_stone,check_prop_hit_trigger]),
  ("stone_house_c",0,"stone_house_c","bo_stone_house_c", [set_material_stone,check_prop_hit_trigger]),
  ("stone_house_d",0,"stone_house_d","bo_stone_house_d", [set_material_stone,check_prop_hit_trigger]),
  ("stone_house_e",0,"stone_house_e","bo_stone_house_e", [set_material_stone,check_prop_hit_trigger]),
  ("stone_house_f",0,"stone_house_f","bo_stone_house_f", [set_material_stone,check_prop_hit_trigger]),
	
	("arabian_house_a",0,"arabian_house_a","bo_arabian_house_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_b",0,"arabian_house_b","bo_arabian_house_b", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_c",0,"arabian_house_c","bo_arabian_house_c", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_d",0,"arabian_house_d","bo_arabian_house_d", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_e",0,"arabian_house_e","bo_arabian_house_e", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_f",0,"arabian_house_f","bo_arabian_house_f", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_g",0,"arabian_house_g","bo_arabian_house_g", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_h",0,"arabian_house_h","bo_arabian_house_h", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_i",0,"arabian_house_i","bo_arabian_house_i", [set_material_stone,check_prop_hit_trigger]),
	("arabian_house_a2",0,"arabian_house_a2","bo_arabian_house_a2", [set_material_stone,check_prop_hit_trigger]),
	("arabian_passage_house_a",0,"arabian_passage_house_a","bo_arabian_passage_house_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_house_a",0,"arabian_village_house_a","bo_arabian_village_house_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_house_b",0,"arabian_village_house_b","bo_arabian_village_house_b", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_house_c",0,"arabian_village_house_c","bo_arabian_village_house_c", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_house_d",0,"arabian_village_house_d","bo_arabian_village_house_d", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_stable",0,"arabian_village_stable","bo_arabian_village_stable", [set_material_stone,check_prop_hit_trigger]),
	("arabian_village_hut",0,"arabian_village_hut","bo_arabian_village_hut", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_house_a",0,"arabian_castle_house_a","bo_arabian_castle_house_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_castle_house_b",0,"arabian_castle_house_b","bo_arabian_castle_house_b", [set_material_stone,check_prop_hit_trigger]),

	("arabian_lighthouse_a",0,"arabian_lighthouse_a","bo_arabian_lighthouse_a", [set_material_stone,check_prop_hit_trigger]),
	
	("granary_a",0,"granary_a","bo_granary_a", [set_material_stone,check_prop_hit_trigger]),
  ("mosque_a",0,"mosque_a","bo_mosque_a", [set_material_stone,check_prop_hit_trigger]),
  ("stone_minaret_a",0,"stone_minaret_a","bo_stone_minaret_a", [set_material_stone,check_prop_hit_trigger]),	
	
  ("farm_house_a",0,"farm_house_a","bo_farm_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("farm_house_b",0,"farm_house_b","bo_farm_house_b", [set_material_wood,check_prop_hit_trigger]),
  ("farm_house_c",0,"farm_house_c","bo_farm_house_c", [set_material_stone,check_prop_hit_trigger]),
  ("mountain_house_a",0,"mountain_house_a","bo_mountain_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("mountain_house_b",0,"mountain_house_b","bo_mountain_house_b", [set_material_wood,check_prop_hit_trigger]),
	
	("village_hut_a",0,"village_hut_a","bo_village_hut_a", [set_material_wood,check_prop_hit_trigger]),
	
	("village_house_a",0,"village_house_a","bo_village_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("village_house_b",0,"village_house_b","bo_village_house_b", [set_material_stone,check_prop_hit_trigger]),
  ("village_house_c",0,"village_house_c","bo_village_house_c", [set_material_stone,check_prop_hit_trigger]),
  ("village_house_d",0,"village_house_d","bo_village_house_d", [set_material_stone,check_prop_hit_trigger]),
  ("village_house_e",0,"village_house_e","bo_village_house_e", [set_material_wood,check_prop_hit_trigger]),
  ("village_house_f",0,"village_house_f","bo_village_house_f", [set_material_wood,check_prop_hit_trigger]),
  ("village_house_g",0,"village_house_g","bo_village_house_g", [set_material_stone,check_prop_hit_trigger]),
  ("village_house_h",0,"village_house_h","bo_village_house_h", [set_material_stone,check_prop_hit_trigger]),
  ("village_house_i",0,"village_house_i","bo_village_house_i", [set_material_stone,check_prop_hit_trigger]),
  ("village_house_j",0,"village_house_j","bo_village_house_j", [set_material_stone,check_prop_hit_trigger]),
	
  ("village_steppe_a",0,"village_steppe_a","bo_village_steppe_a", [set_material_stone,check_prop_hit_trigger]),
  ("village_steppe_b",0,"village_steppe_b","bo_village_steppe_b", [set_material_stone,check_prop_hit_trigger]),
  ("village_steppe_c",0,"village_steppe_c","bo_village_steppe_c", [set_material_stone,check_prop_hit_trigger]),
  ("village_steppe_d",0,"village_steppe_d","bo_village_steppe_d", [set_material_stone,check_prop_hit_trigger]),
  ("village_steppe_e",0,"village_steppe_e","bo_village_steppe_e", [set_material_stone,check_prop_hit_trigger]),
  ("village_steppe_f",0,"village_steppe_f","bo_village_steppe_f", [set_material_stone,check_prop_hit_trigger]),
	
  ("town_house_steppe_a",0,"town_house_steppe_a","bo_town_house_steppe_a", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_b",0,"town_house_steppe_b","bo_town_house_steppe_b", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_c",0,"town_house_steppe_c","bo_town_house_steppe_c", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_d",0,"town_house_steppe_d","bo_town_house_steppe_d", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_e",0,"town_house_steppe_e","bo_town_house_steppe_e", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_f",0,"town_house_steppe_f","bo_town_house_steppe_f", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_g",0,"town_house_steppe_g","bo_town_house_steppe_g", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_h",0,"town_house_steppe_h","bo_town_house_steppe_h", [set_material_stone,check_prop_hit_trigger]),
  ("town_house_steppe_i",0,"town_house_steppe_i","bo_town_house_steppe_i", [set_material_stone,check_prop_hit_trigger]),
	
	("town_house_aa",0,"town_house_aa","bo_town_house_aa", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_a",0,"town_house_a","bo_town_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_b",0,"town_house_b","bo_town_house_b", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_c",0,"town_house_c","bo_town_house_c", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_d",0,"town_house_d","bo_town_house_d", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_e",0,"town_house_e","bo_town_house_e", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_f",0,"town_house_f","bo_town_house_f", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_g",0,"town_house_g","bo_town_house_g", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_h",0,"town_house_h","bo_town_house_h", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_i",0,"town_house_i","bo_town_house_i", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_j",0,"town_house_j","bo_town_house_j", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_l",0,"town_house_l","bo_town_house_l", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_m",0,"town_house_m","bo_town_house_m", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_n",0,"town_house_n","bo_town_house_n", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_o",0,"town_house_o","bo_town_house_o", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_p",0,"town_house_p","bo_town_house_p", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_q",0,"town_house_q","bo_town_house_q", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_r",0,"town_house_r","bo_town_house_r", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_s",0,"town_house_s","bo_town_house_s", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_t",0,"town_house_t","bo_town_house_t", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_u",0,"town_house_u","bo_town_house_u", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_v",0,"town_house_v","bo_town_house_v", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_w",0,"town_house_w","bo_town_house_w", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_y",0,"town_house_y","bo_town_house_y", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_z",0,"town_house_z","bo_town_house_z", [set_material_wood,check_prop_hit_trigger]),
  ("town_house_za",0,"town_house_za","bo_town_house_za", [set_material_wood,check_prop_hit_trigger]),
	
	("city_swad_01" ,0,"city_swad_01" ,"bo_city_swad_01" , [set_material_wood,check_prop_hit_trigger]),
	("city_swad_02" ,0,"city_swad_02" ,"bo_city_swad_02" , [set_material_wood,check_prop_hit_trigger]),
	("city_swad_03" ,0,"city_swad_03" ,"bo_city_swad_03" , [set_material_wood,check_prop_hit_trigger]),
	("city_swad_04" ,0,"city_swad_04" ,"bo_city_swad_04" , [set_material_wood,check_prop_hit_trigger]),
	("city_swad_05" ,0,"city_swad_05" ,"bo_city_swad_05" , [set_material_wood,check_prop_hit_trigger]),
	("city_swad_passage_01" ,0,"city_swad_passage_01" ,"bo_city_swad_passage_01" , [set_material_wood,check_prop_hit_trigger]),
  
  ("passage_house_a",0,"passage_house_a","bo_passage_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("passage_house_b",0,"passage_house_b","bo_passage_house_b", [set_material_wood,check_prop_hit_trigger]),
  ("passage_house_c",0,"passage_house_c","bo_passage_house_c", [set_material_stone,check_prop_hit_trigger]),
  ("passage_house_d",0,"passage_house_d","bo_passage_house_d", [set_material_stone,check_prop_hit_trigger]),
	
  ("small_timber_frame_house_a",0,"small_timber_frame_house_a","bo_small_timber_frame_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("timber_frame_house_b",0,"tf_house_b","bo_tf_house_b", [set_material_wood,check_prop_hit_trigger]),
  ("timber_frame_house_c",0,"tf_house_c","bo_tf_house_c", [set_material_wood,check_prop_hit_trigger]),
  ("timber_frame_extension_a",0,"timber_frame_extension_a","bo_timber_frame_extension_a", [set_material_wood,check_prop_hit_trigger]),
  ("timber_frame_extension_b",0,"timber_frame_extension_b","bo_timber_frame_extension_b", [set_material_wood,check_prop_hit_trigger]),
	("square_extension_a",0,"square_extension_a","bo_square_extension_a", [set_material_wood,check_prop_hit_trigger]),
	
	("side_building_a",0,"side_building_a","bo_side_building_a", [set_material_stone,check_prop_hit_trigger]),
  
  ("earth_house_a",0,"earth_house_a","bo_earth_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("earth_house_b",0,"earth_house_b","bo_earth_house_b", [set_material_wood,check_prop_hit_trigger]),
  ("earth_house_c",0,"earth_house_c","bo_earth_house_c", [set_material_wood,check_prop_hit_trigger]),
  ("earth_house_d",0,"earth_house_d","bo_earth_house_d", [set_material_wood,check_prop_hit_trigger]),

  ("house_extension_a",0,"house_extension_a","bo_house_extension_a", [set_material_wood,check_prop_hit_trigger]),
  ("house_extension_b",0,"house_extension_b","bo_house_extension_b", [set_material_wood,check_prop_hit_trigger]),
  ("house_extension_c",0,"house_extension_c","bo_house_extension_a", [set_material_wood,check_prop_hit_trigger]),#reuse 
  ("house_extension_d",0,"house_extension_d","bo_house_extension_d", [set_material_wood,check_prop_hit_trigger]),
  ("house_extension_e",0,"house_extension_e","bo_house_extension_e", [set_material_wood,check_prop_hit_trigger]),
  ("house_extension_f",0,"house_extension_f","bo_house_extension_f", [set_material_stone,check_prop_hit_trigger]),
  ("house_extension_f2",0,"house_extension_f2","bo_house_extension_f", [set_material_stone,check_prop_hit_trigger]),
  ("house_extension_g",0,"house_extension_g","bo_house_extension_g", [set_material_stone,check_prop_hit_trigger]),
  ("house_extension_g2",0,"house_extension_g2","bo_house_extension_g", [set_material_stone,check_prop_hit_trigger]),
  ("house_extension_h",0,"house_extension_h","bo_house_extension_h", [set_material_stone,check_prop_hit_trigger]),
  ("house_extension_i",0,"house_extension_i","bo_house_extension_i", [set_material_stone,check_prop_hit_trigger]),

  ("village_straw_house_a",0,"village_straw_house_a","bo_village_straw_house_a", [set_material_wood,check_prop_hit_trigger]),
  ("village_stable_a",0,"village_stable_a","bo_village_stable_a", [set_material_wood,check_prop_hit_trigger]),
  ("village_shed_a",0,"village_shed_a","bo_village_shed_a", [set_material_wood,check_prop_hit_trigger]),
  ("village_shed_b",0,"village_shed_b","bo_village_shed_b", [set_material_stone,check_prop_hit_trigger]),
	
	("full_stable_a",0,"full_stable_a","bo_full_stable_a", [set_material_wood,check_prop_hit_trigger]),
	("full_stable_b",0,"full_stable_b","bo_full_stable_b", [set_material_stone,check_prop_hit_trigger]),
	("full_stable_c",0,"full_stable_c","bo_full_stable_c", [set_material_stone,check_prop_hit_trigger]),
	
	("church_a",0,"church_a","bo_church_a", [set_material_stone,check_prop_hit_trigger]),
  ("church_tower_a",0,"church_tower_a","bo_church_tower_a", [set_material_stone,check_prop_hit_trigger]),
	
  ("windmill",0,"windmill","bo_windmill", [set_material_stone,check_prop_hit_trigger]),
  ("windmill_fan_turning",sokf_moveable,"windmill_fan_turning",0,[set_material_wood]),
  ("windmill_fan",0,"windmill_fan","bo_windmill_fan", [set_material_wood,check_prop_hit_trigger]),

#arena
  ("arena_block_a",0,"arena_block_a","bo_arena_block_ab", [set_material_stone,check_prop_hit_trigger]),
  ("arena_block_b",0,"arena_block_b","bo_arena_block_ab", [set_material_stone,check_prop_hit_trigger]),
  ("arena_block_c",0,"arena_block_c","bo_arena_block_c", [set_material_wood,check_prop_hit_trigger]),
  ("arena_block_d",0,"arena_block_d","bo_arena_block_def", [set_material_wood,check_prop_hit_trigger]),
  ("arena_block_e",0,"arena_block_e","bo_arena_block_def", [set_material_wood,check_prop_hit_trigger]),
  ("arena_block_f",0,"arena_block_f","bo_arena_block_def", [set_material_wood,check_prop_hit_trigger]),
  ("arena_block_g",0,"arena_block_g","bo_arena_block_ghi", [set_material_stone,check_prop_hit_trigger]),
  ("arena_block_h",0,"arena_block_h","bo_arena_block_ghi", [set_material_stone,check_prop_hit_trigger]),
  ("arena_block_i",0,"arena_block_i","bo_arena_block_ghi", [set_material_wood,check_prop_hit_trigger]),
  ("arena_block_j",0,"arena_block_j","bo_arena_block_j", [set_material_stone,check_prop_hit_trigger]),
  ("arena_block_j_awning",0,"arena_block_j_awning","bo_arena_block_j_awning", [set_material_wood,check_prop_hit_trigger]),
  ("arena_palisade_a",0,"arena_palisade_a","bo_arena_palisade_a", [set_material_wood,check_prop_hit_trigger]),
  ("arena_wall_a",0,"arena_wall_a","bo_arena_wall_ab", [set_material_stone,check_prop_hit_trigger]),
  ("arena_wall_b",0,"arena_wall_b","bo_arena_wall_ab", [set_material_stone,check_prop_hit_trigger]),
  ("arena_barrier_a",0,"arena_barrier_a","bo_arena_barrier_a", [set_material_wood,check_prop_hit_trigger]),
  ("arena_barrier_b",0,"arena_barrier_b","bo_arena_barrier_bc", [set_material_wood,check_prop_hit_trigger]),
  ("arena_barrier_c",0,"arena_barrier_c","bo_arena_barrier_bc", [set_material_wood,check_prop_hit_trigger]),
  ("arena_block_j_a",0,"arena_block_j_a","bo_arena_block_j_a", [set_material_stone,check_prop_hit_trigger]),
  ("arena_underway_a",0,"arena_underway_a","bo_arena_underway_a", [set_material_stone,check_prop_hit_trigger]),
  ("arena_circle_a",0,"arena_circle_a","bo_arena_circle_a", [set_material_stone,check_prop_hit_trigger]),
	
#ruins
  ("destroy_castle_a",0,"destroy_castle_a","bo_destroy_castle_a", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_castle_b",0,"destroy_castle_b","bo_destroy_castle_b", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_castle_c",0,"destroy_castle_c","bo_destroy_castle_c", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_castle_d",0,"destroy_castle_d","bo_destroy_castle_d", [set_material_stone,check_prop_hit_trigger]),
	("destroy_house_a",0,"destroy_house_a","bo_destroy_house_a", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_house_b",0,"destroy_house_b","bo_destroy_house_b", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_house_c",0,"destroy_house_c","bo_destroy_house_c", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_windmill",0,"destroy_windmill","bo_destroy_windmill", [set_material_stone,check_prop_hit_trigger]),
	("destroy_heap",0,"destroy_heap","bo_destroy_heap", [set_material_stone,check_prop_hit_trigger]),

#bridges
	("bridge_b",0,"bridge_b","bo_bridge_b", [set_material_stone,check_prop_hit_trigger]),
	("destroy_bridge_a",0,"destroy_bridge_a","bo_destroy_bridge_a", [set_material_stone,check_prop_hit_trigger]),
  ("destroy_bridge_b",0,"destroy_bridge_b","bo_destroy_bridge_b", [set_material_stone,check_prop_hit_trigger]),
  ("bridge_modular_a",0,"bridge_modular_a","bo_bridge_modular_a", [set_material_stone,check_prop_hit_trigger]),
  ("bridge_modular_b",0,"bridge_modular_b","bo_bridge_modular_b", [set_material_stone,check_prop_hit_trigger]),
	("rock_bridge_a",0,"rock_bridge_a","bo_rock_bridge_a", [set_material_stone,check_prop_hit_trigger]),
	("harbour_a",0,"harbour_a","bo_harbour_a", [set_material_wood,check_prop_hit_trigger]),
	("rope_bridge_15m",0,"rope_bridge_15m","bo_rope_bridge_15m", [set_material_wood,check_prop_hit_trigger]),
	
#ladders
	("siege_ladder_move_6m",sokf_type_ladder|sokf_static_movement|spr_use_time(2),"siege_ladder_move_6m","bo_siege_ladder_move_6m",[set_material_wood,check_item_use_trigger,check_ladder_animate_trigger,check_ladder_animation_finish_trigger,check_prop_hit_trigger]),  
  ("siege_ladder_move_8m",sokf_type_ladder|sokf_static_movement|spr_use_time(2),"siege_ladder_move_8m","bo_siege_ladder_move_8m",[set_material_wood,check_item_use_trigger,check_ladder_animate_trigger,check_ladder_animation_finish_trigger,check_prop_hit_trigger]),  
  ("siege_ladder_move_10m",sokf_type_ladder|sokf_static_movement|spr_use_time(3),"siege_ladder_move_10m","bo_siege_ladder_move_10m",[set_material_wood,check_item_use_trigger,check_ladder_animate_trigger,check_ladder_animation_finish_trigger,check_prop_hit_trigger]),  
  ("siege_ladder_move_12m",sokf_type_ladder|sokf_static_movement|spr_use_time(3),"siege_ladder_move_12m","bo_siege_ladder_move_12m",[set_material_wood,check_item_use_trigger,check_ladder_animate_trigger,check_ladder_animation_finish_trigger,check_prop_hit_trigger]),  
  ("siege_ladder_move_14m",sokf_type_ladder|sokf_static_movement|spr_use_time(4),"siege_ladder_move_14m","bo_siege_ladder_move_14m",[set_material_wood,check_item_use_trigger,check_ladder_animate_trigger,check_ladder_animation_finish_trigger,check_prop_hit_trigger]), 

#interiors
	("interior_dungeon_a",0,"dungeon_a_new","bo_dungeon_a_new", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_stairs_a",sokf_type_ladder,"dungeon_stairs_a","bo_dungeon_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_stairs_b",sokf_type_ladder,"dungeon_stairs_b","bo_dungeon_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_torture_room_a",0,"dungeon_torture_room_a","bo_dungeon_torture_room_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_entry_a",0,"dungeon_entry_a","bo_dungeon_entry_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_entry_b",0,"dungeon_entry_b","bo_dungeon_entry_b", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_entry_c",0,"dungeon_entry_c","bo_dungeon_entry_c", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_cell_a",0,"dungeon_cell_a","bo_dungeon_cell_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_cell_b",0,"dungeon_cell_b","bo_dungeon_cell_b", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_cell_c",0,"dungeon_cell_c","bo_dungeon_cell_c", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_corridor_a",0,"dungeon_corridor_a","bo_dungeon_corridor_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_corridor_b",0,"dungeon_corridor_b","bo_dungeon_corridor_b", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_corridor_c",0,"dungeon_corridor_c","bo_dungeon_corridor_b", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_corridor_d",0,"dungeon_corridor_d","bo_dungeon_corridor_b", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_direction_a",0,"dungeon_direction_a","bo_dungeon_direction_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_direction_b",0,"dungeon_direction_b","bo_dungeon_direction_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_room_a",0,"dungeon_room_a","bo_dungeon_room_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_tower_stairs_a",sokf_type_ladder,"dungeon_tower_stairs_a","bo_dungeon_tower_stairs_a", [set_material_stone,check_prop_hit_trigger]),
  ("interior_dungeon_tower_cell_a",0,"dungeon_tower_cell_a","bo_dungeon_tower_cell_a", [set_material_stone,check_prop_hit_trigger]),
	
  ("interior_prison_b",0,"interior_prison_b","bo_interior_prison_b", [set_material_stone,check_prop_hit_trigger]),
	("interior_prison_d",0,"interior_prison_d","bo_interior_prison_d", [set_material_stone,check_prop_hit_trigger]),
	("interior_prison_e",0,"interior_prison_e","bo_interior_prison_e", [set_material_stone,check_prop_hit_trigger]),
	("interior_prison_cell_a",0,"interior_prison_cell_a","bo_interior_prison_cell_a", [set_material_stone,check_prop_hit_trigger]),
	
#rock/ground
	("water_river",0,"water_plane","0", [set_material_water]),
	("desert_field",0,"desert_field","bo_desert_field", [set_material_earth,check_prop_hit_trigger]),
	("arabian_ground_a",0,"arabian_ground_a","bo_arabian_ground_a", [set_material_stone,check_prop_hit_trigger]),
	("arabian_parterre_a",0,"arabian_parterre_a","bo_arabian_parterre_a", [set_material_earth,check_prop_hit_trigger]),
	("grave_a",0,"grave_a","bo_grave_a", [set_material_earth,check_prop_hit_trigger]),
	("mine_a",0,"mine_a","bo_mine_a", [set_material_stone,check_prop_hit_trigger]),
	("cave_entrance_1",0,"cave_entrance_1","bo_cave_entrance_1", [set_material_stone,check_prop_hit_trigger]),
	("tunnel_a",0,"tunnel_a","bo_tunnel_a", [set_material_stone,check_prop_hit_trigger]),
  ("tunnel_salt",0,"tunnel_salt","bo_tunnel_salt", [set_material_stone,check_prop_hit_trigger]),
  ("salt_a",0,"salt_a","bo_salt_a", [set_material_earth,check_prop_hit_trigger]),
	
	("rock_acantilado-05",0,"acantilado-05","bo_acantilado-05", [set_material_stone,check_prop_hit_trigger]),
	("rock_acantilado-06-p2",0,"acantilado-06-p2","bo_acantilado-06-p2", [set_material_stone,check_prop_hit_trigger]),
	("rock_acantilado-07",0,"acantilado-07","bo_acantilado-07", [set_material_stone,check_prop_hit_trigger]),
	("rock_acantilado-07-p2",0,"acantilado-07-p2","bo_acantilado-07-p2", [set_material_stone,check_prop_hit_trigger]),
	("rock_acantilado-07-p3",0,"acantilado-07-p3","bo_acantilado-07-p3", [set_material_stone,check_prop_hit_trigger]),
	("rock_barandilla-01",0,"barandilla-01","bo_barandilla-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_cave-frame-01",0,"cave-frame-01","bo_cave-frame-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_cave-frame-02",0,"cave-frame-02","bo_cave-frame-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_cave-p01",0,"cave-p01","bo_cave-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_cave-p02",0,"cave-p02","bo_cave-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_cave-p03",0,"cave-p03","bo_cave-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_cave_entrance_1_open",0,"cave_entrance_1_open","bo_cave_entrance_1_open", [set_material_stone,check_prop_hit_trigger]),
	("rock_columna-01",0,"columna-01","bo_columna-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_columna-02",0,"columna-02","bo_columna-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_cornisa-01",0,"cornisa-01","bo_cornisa-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_cornisa-02",0,"cornisa-02","bo_cornisa-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_estalactita-01",0,"estalactita-01","bo_estalactita-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mo-wall-01",0,"mo-wall-01","bo_mo-wall-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mo-wall-02",0,"mo-wall-02","bo_mo-wall-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01",0,"mont-01","bo_mont-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big",0,"mont-01-big","bo_mont-01-big", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-cave",0,"mont-01-big-cave","bo_mont-01-big-cave", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-cave-p01",0,"mont-01-big-cave-p01","bo_mont-01-big-cave-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-cave-p02",0,"mont-01-big-cave-p02","bo_mont-01-big-cave-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-cave-p03",0,"mont-01-big-cave-p03","bo_mont-01-big-cave-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-cave-p04",0,"mont-01-big-cave-p04","bo_mont-01-big-cave-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-p01",0,"mont-01-big-p01","bo_mont-01-big-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-p02",0,"mont-01-big-p02","bo_mont-01-big-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-p03",0,"mont-01-big-p03","bo_mont-01-big-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-big-p04",0,"mont-01-big-p04","bo_mont-01-big-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-p1",0,"mont-01-p1","bo_mont-01-p1", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-01-p2",0,"mont-01-p2","bo_mont-01-p2", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02",0,"mont-02","bo_mont-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big",0,"mont-02-big","bo_mont-02-big", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-cave-p01",0,"mont-02-big-cave-p01","bo_mont-02-big-cave-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-cave-p02",0,"mont-02-big-cave-p02","bo_mont-02-big-cave-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-cave-p03",0,"mont-02-big-cave-p03","bo_mont-02-big-cave-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-cave-p04",0,"mont-02-big-cave-p04","bo_mont-02-big-cave-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-p01",0,"mont-02-big-p01","bo_mont-02-big-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-p02",0,"mont-02-big-p02","bo_mont-02-big-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-p03",0,"mont-02-big-p03","bo_mont-02-big-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-big-p04",0,"mont-02-big-p04","bo_mont-02-big-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-cave-p01",0,"mont-02-cave-p01","bo_mont-02-cave-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-cave-p02",0,"mont-02-cave-p02","bo_mont-02-cave-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-cave-p03",0,"mont-02-cave-p03","bo_mont-02-cave-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-cave-p04",0,"mont-02-cave-p04","bo_mont-02-cave-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-p01",0,"mont-02-p01","bo_mont-02-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-p02",0,"mont-02-p02","bo_mont-02-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-p03",0,"mont-02-p03","bo_mont-02-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-02-p04",0,"mont-02-p04","bo_mont-02-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-03-p3",0,"mont-03-p3","bo_mont-03-p3", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big",0,"mont-04-big","bo_mont-04-big", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-cave-p01",0,"mont-04-big-cave-p01","bo_mont-04-big-cave-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-cave-p02",0,"mont-04-big-cave-p02","bo_mont-04-big-cave-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-cave-p03",0,"mont-04-big-cave-p03","bo_mont-04-big-cave-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-cave-p04",0,"mont-04-big-cave-p04","bo_mont-04-big-cave-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-p01",0,"mont-04-big-p01","bo_mont-04-big-p01", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-p02",0,"mont-04-big-p02","bo_mont-04-big-p02", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-p03",0,"mont-04-big-p03","bo_mont-04-big-p03", [set_material_stone,check_prop_hit_trigger]),
	("rock_mont-04-big-p04",0,"mont-04-big-p04","bo_mont-04-big-p04", [set_material_stone,check_prop_hit_trigger]),
	("rock_pasarela-01",0,"pasarela-01","bo_pasarela-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_pasarela-02",0,"pasarela-02","bo_pasarela-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_pasarela-03",0,"pasarela-03","bo_pasarela-03", [set_material_stone,check_prop_hit_trigger]),
	("rock_pasarela-05",0,"pasarela-05","bo_pasarela-05", [set_material_stone,check_prop_hit_trigger]),
	("rock_rock-01",0,"rock-01","bo_rock-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_rock-02",0,"rock-02","bo_rock-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_rock-03",0,"rock-03","bo_rock-03", [set_material_stone,check_prop_hit_trigger]),
	("rock_rock-04",0,"rock-04","bo_rock-04", [set_material_stone,check_prop_hit_trigger]),
	("rock_rock-05",0,"rock-05","bo_rock-05", [set_material_stone,check_prop_hit_trigger]),
	("rock_rock-06",0,"rock-06","bo_rock-06", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel-01",0,"tunnel-01","bo_tunnel-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel-02",0,"tunnel-02","bo_tunnel-02", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel-03",0,"tunnel-03","bo_tunnel-03", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_crossing",0,"tunnel_crossing","bo_tunnel_crossing", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_crossing_supports",0,"tunnel_crossing_supports","bo_tunnel_crossing_supports", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_curved",0,"tunnel_curved","bo_tunnel_curved", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_curved_supports",0,"tunnel_curved_supports","bo_tunnel_curved_supports", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_short",0,"tunnel_short","bo_tunnel_short", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_short_supports",0,"tunnel_short_supports","bo_tunnel_short_supports", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_sloped",0,"tunnel_sloped","bo_tunnel_sloped", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_sloped_supports",0,"tunnel_sloped_supports","bo_tunnel_sloped_supports", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_split",0,"tunnel_split","bo_tunnel_split", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_split_supports",0,"tunnel_split_supports","bo_tunnel_split_supports", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_straight",0,"tunnel_straight","bo_tunnel_straight", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_straight_supports",0,"tunnel_straight_supports","bo_tunnel_straight_supports", [set_material_stone,check_prop_hit_trigger]),
	("rock_tunnel_support",0,"tunnel_support","bo_tunnel_support", [set_material_stone,check_prop_hit_trigger]),
	("rock_way-01",0,"way-01","bo_way-01", [set_material_stone,check_prop_hit_trigger]),
	("rock_way-01-bank-01",0,"way-01-bank-01","bo_way-01-bank-01", [set_material_stone,check_prop_hit_trigger]),
	
#other
	("straw_a",0,"straw_a","0", [set_material_earth]),
  ("straw_b",0,"straw_b","0", [set_material_earth]),
  ("straw_c",0,"straw_c","0", [set_material_earth]),
	
	("fireplace_a",0,"fireplace_a","bo_fireplace_a", [set_material_stone,check_prop_hit_trigger]),
  ("fireplace_b",0,"fireplace_b","bo_fireplace_b", [set_material_stone,check_prop_hit_trigger]),
  ("fireplace_c",0,"fireplace_c","bo_fireplace_c", [set_material_stone,check_prop_hit_trigger]),
	("fireplace_d_interior",0,"fireplace_d","bo_fireplace_d", [set_material_stone,check_prop_hit_trigger]),
	
	("fountain",0, "fountain", "bo_fountain", [set_material_stone,check_prop_hit_trigger]),
	
	("horse_mill",0,"horse_mill","bo_horse_mill", [set_material_stone,check_prop_hit_trigger]),
	("horse_mill_collar",0,"horse_mill_collar","bo_horse_mill_collar",[set_material_wood,check_prop_hit_trigger]),
	
	("destroy_a",0,"destroy_a","0", [set_material_stone]),
  ("destroy_b",0,"destroy_b","0", [set_material_stone]),
	
	("tutorial_flag_yellow",sokf_static_movement|sokf_face_player,"tutorial_flag_yellow","0", []),
  ("tutorial_flag_red",sokf_static_movement|sokf_face_player,"tutorial_flag_red","0", []),
  ("tutorial_flag_blue",sokf_static_movement|sokf_face_player,"tutorial_flag_blue","0", []),
	
	("band_a",0,"band_a","0", []),
	
	("ship_b",0,"ship_b","bo_ship_b", [set_material_wood,check_prop_hit_trigger]),
  ("ship_c",0,"ship_c","bo_ship_c", [set_material_wood,check_prop_hit_trigger]),
	("ship_c_sail_off",0,"ship_c_sail_off","bo_ship_c_sail_off", [set_material_wood,check_prop_hit_trigger]),
  ("ship_d",0,"ship_d","bo_ship_d", [set_material_wood,check_prop_hit_trigger]),
	
	("belfry_a",0,"belfry_a","bo_belfry_a", [set_material_wood,check_prop_hit_trigger]),

  ("belfry_b",0,"belfry_b","bo_belfry_b", [set_material_wood,check_prop_hit_trigger]),
  ("belfry_b_platform_a",0,"belfry_b_platform_a","bo_belfry_b_platform_a", [set_material_wood,check_prop_hit_trigger]),

  ("belfry_old",0,"belfry_a","bo_belfry_a", [set_material_wood,check_prop_hit_trigger]),
  ("belfry_platform_a",0,"belfry_platform_a","bo_belfry_platform_a", [set_material_wood,check_prop_hit_trigger]),
  ("belfry_platform_b",0,"belfry_platform_b","bo_belfry_platform_b", [set_material_wood,check_prop_hit_trigger]),
  ("belfry_platform_old",0,"belfry_platform_b","bo_belfry_platform_b", [set_material_wood,check_prop_hit_trigger]),
  ("belfry_wheel",0,"belfry_wheel",0, [set_material_wood,check_prop_hit_trigger]),
  ("belfry_wheel_old",0,"belfry_wheel",0, [set_material_wood,check_prop_hit_trigger]),

	("destructible_props_begin", 0, 0, 0, []),
#environment
	("bridge_wooden",sokf_static_movement,"bridge_wooden","bo_bridge_wooden",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("suspension_bridge_a",sokf_static_movement,"suspension_bridge_a","bo_suspension_bridge_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("ramp_small_a",sokf_static_movement,"ramp_small_a","bo_ramp_small_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("barrel",sokf_static_movement,"barrel","bobarrel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("gunpowder_barrel",sokf_static_movement,"barrel","bobarrel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("bench_tavern",sokf_static_movement,"bench_tavern","bobench_tavern",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bench_tavern_b",sokf_static_movement,"bench_tavern_b","bo_bench_tavern_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("bowl_wood",sokf_static_movement,"bowl_wood","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chandelier_table",sokf_static_movement,"chandelier_table","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chandelier_tavern",sokf_static_movement,"chandelier_tavern","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("chest_gothic",sokf_static_movement,"chest_gothic","bochest_gothic",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chest_b",sokf_static_movement,"chest_b","bo_chest_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chest_c",sokf_static_movement,"chest_c","bo_chest_c",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("counter_tavern",sokf_static_movement,"counter_tavern","bocounter_tavern",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 0)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("cup",sokf_static_movement,"cup","0", [set_material_ceramic,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dish_metal",sokf_static_movement,"dish_metal","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("gothic_chair",sokf_static_movement,"gothic_chair","bogothic_chair",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("gothic_stool",sokf_static_movement,"gothic_stool","bogothic_stool",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("grate",sokf_static_movement,"grate","bograte",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("jug",sokf_static_movement,"jug","0", [set_material_ceramic,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("potlamp",sokf_static_movement,"potlamp","0", [set_material_ceramic,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("weapon_rack",sokf_static_movement,"weapon_rack","boweapon_rack",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("weapon_rack_big",sokf_static_movement,"weapon_rack_big","boweapon_rack_big",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("tavern_barrel",sokf_static_movement,"barrel","bobarrel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tavern_barrel_b",sokf_static_movement,"tavern_barrel_b","bo_tavern_barrel_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("merchant_sign",sokf_static_movement,"merchant_sign","bo_tavern_sign",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tavern_sign",sokf_static_movement,"tavern_sign","bo_tavern_sign",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("sack",sokf_static_movement,"sack","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skull_a",sokf_static_movement,"skull_a","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skull_b",sokf_static_movement,"skull_b","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skull_c",sokf_static_movement,"skull_c","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skull_d",sokf_static_movement,"skull_d","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skeleton_cow",sokf_static_movement,"skeleton_cow","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("cupboard_a",sokf_static_movement,"cupboard_a","bo_cupboard_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("box_a",sokf_static_movement,"box_a","bo_box_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("bucket_a",sokf_static_movement,"bucket_a","bo_bucket_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 50)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("cloth_a",sokf_static_movement,"cloth_a","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("cloth_b",sokf_static_movement,"cloth_b","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("mat_a",sokf_static_movement,"mat_a","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("mat_b",sokf_static_movement,"mat_b","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("mat_c",sokf_static_movement,"mat_c","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("mat_d",sokf_static_movement,"mat_d","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),

  ("wood_a",sokf_static_movement,"wood_a","bo_wood_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("wood_b",sokf_static_movement,"wood_b","bo_wood_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 50)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("wood_heap",sokf_static_movement,"wood_heap_a","bo_wood_heap_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("wood_heap_b",sokf_static_movement,"wood_heap_b","bo_wood_heap_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("water_well_a",sokf_static_movement,"water_well_a","bo_water_well_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 15000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("net_a",sokf_static_movement,"net_a","bo_net_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("net_b",sokf_static_movement,"net_b","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("meat_hook",sokf_static_movement,"meat_hook","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("cooking_pole",sokf_static_movement,"cooking_pole","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bowl_a",sokf_static_movement,"bowl_a","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bucket_b",sokf_static_movement,"bucket_b","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("washtub_a",sokf_static_movement,"washtub_a","bo_washtub_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("washtub_b",sokf_static_movement,"washtub_b","bo_washtub_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("table_trunk_a",sokf_static_movement,"table_trunk_a","bo_table_trunk_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("table_trestle_long",sokf_static_movement,"table_trestle_long","bo_table_trestle_long",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("table_trestle_small",sokf_static_movement,"table_trestle_small","bo_table_trestle_small",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("table_small",sokf_static_movement,"table_small","bo_table_small",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("table_small_b",sokf_static_movement,"table_small_b","bo_table_small_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("chair_trunk_a",sokf_static_movement,"chair_trunk_a","bo_chair_trunk_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chair_trunk_b",sokf_static_movement,"chair_trunk_b","bo_chair_trunk_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chair_trunk_c",sokf_static_movement,"chair_trunk_c","bo_chair_trunk_c",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chair_trestle",sokf_static_movement,"chair_trestle","bo_chair_trestle",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("wheel",sokf_static_movement,"wheel","bo_wheel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("ladder",sokf_type_ladder|sokf_static_movement,"ladder","boladder",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

	("ladder_move_5m",sokf_type_ladder|sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2),"ladder_move_5m","bo_ladder_move_5m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200),]),set_material_wood,check_item_use_trigger,check_prop_hit_trigger,check_prop_destroy_trigger]),	
		
	("railing_a",sokf_static_movement,"railing_a","bo_railing_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  
  ("village_stand",sokf_static_movement,"village_stand","bovillage_stand",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("wooden_stand",sokf_static_movement,"wooden_stand","bowooden_stand",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("stand_thatched",sokf_static_movement,"stand_thatched","bo_stand_thatched",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),	
  ("stand_cloth",sokf_static_movement,"stand_cloth","bo_stand_cloth",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("arabian_stable",sokf_static_movement,"arabian_stable","bo_arabian_stable",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("arabian_tent",sokf_static_movement,"arabian_tent","bo_arabian_tent",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("arabian_tent_b",sokf_static_movement,"arabian_tent_b","bo_arabian_tent_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

  ("crude_fence",sokf_static_movement,"fence","bo_fence",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("crude_fence_small",sokf_static_movement,"crude_fence_small","bo_crude_fence_small",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("crude_fence_small_b",sokf_static_movement,"crude_fence_small_b","bo_crude_fence_small_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("ramp_12m",sokf_static_movement,"ramp_12m","bo_ramp_12m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("ramp_14m",sokf_static_movement,"ramp_14m","bo_ramp_14m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("chain_1m",sokf_static_movement,"chain_1m","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chain_2m",sokf_static_movement,"chain_2m","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chain_5m",sokf_static_movement,"chain_5m","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chain_10m",sokf_static_movement,"chain_10m","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("stone_step_a",sokf_static_movement,"floor_stone_a","bo_floor_stone_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 10000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("stone_step_b",sokf_static_movement,"stone_step_b","0", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 10000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("stone_step_c",sokf_static_movement,"stone_step_c","0", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 10000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("stone_heap",sokf_static_movement,"stone_heap","bo_stone_heap",[set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("stone_heap_b",sokf_static_movement,"stone_heap_b","bo_stone_heap",[set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("destroy_tree_a",sokf_static_movement,"destroy_tree_a","bo_destroy_tree_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("destroy_tree_b",sokf_static_movement,"destroy_tree_b","bo_destroy_tree_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

  ("awning_a",sokf_static_movement,"awning_a","bo_awning",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("awning_b",sokf_static_movement,"awning_b","bo_awning",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("awning_c",sokf_static_movement,"awning_c","bo_awning",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("awning_long",sokf_static_movement,"awning_long","bo_awning_long",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("awning_long_b",sokf_static_movement,"awning_long_b","bo_awning_long",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("awning_d",sokf_static_movement,"awning_d","bo_awning_d",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

  ("skeleton_head",sokf_static_movement,"skeleton_head","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skeleton_bone",sokf_static_movement,"skeleton_bone","0", [set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("skeleton_a",sokf_static_movement,"skeleton_a","bo_skeleton_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 50)]),set_material_bone,check_prop_hit_trigger,check_prop_destroy_trigger]),

  ("open_stable_a",sokf_static_movement,"open_stable_a","bo_open_stable_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("open_stable_b",sokf_static_movement,"open_stable_b","bo_open_stable_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

	("arabian_ramp_a",sokf_static_movement,"arabian_ramp_a","bo_arabian_ramp_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("arabian_ramp_b",sokf_static_movement,"arabian_ramp_b","bo_arabian_ramp_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

  ("tree_house_a",sokf_static_movement,"tree_house_a","bo_tree_house_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tree_house_guard_a",sokf_static_movement,"tree_house_guard_a","bo_tree_house_guard_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),	
  ("tree_house_guard_b",sokf_static_movement,"tree_house_guard_b","bo_tree_house_guard_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tree_shelter_a",sokf_static_movement,"tree_shelter_a","bo_tree_shelter_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
#ships/boats
	("boat_destroy",sokf_static_movement,"boat_destroy","bo_boat_destroy",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

#gates/doors
  ("castle_drawbridge_open",sokf_static_movement,"castle_drawbridges_open","bo_castle_drawbridges_open",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("castle_drawbridge_closed",sokf_static_movement,"castle_drawbridges_closed","bo_castle_drawbridges_closed",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("drawbridge",sokf_static_movement,"drawbridge","bo_drawbridge",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("draw_bridge_a",sokf_static_movement,"draw_bridge_a","bo_draw_bridge_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("gatehouse_door_left",sokf_static_movement,"gatehouse_door_left","bo_gatehouse_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("gatehouse_door_right",sokf_static_movement,"gatehouse_door_right","bo_gatehouse_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("castle_g_gate_house_door_a",sokf_static_movement,"castle_g_gate_house_door_a","bo_castle_g_gate_house_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("castle_g_gate_house_door_b",sokf_static_movement,"castle_g_gate_house_door_b","bo_castle_g_gate_house_door_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("castle_i_gate_house_door_a",sokf_static_movement,"castle_i_gate_house_door_a","bo_castle_i_gate_house_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("castle_i_gate_house_door_b",sokf_static_movement,"castle_i_gate_house_door_b","bo_castle_i_gate_house_door_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("earth_gate_a",sokf_static_movement,"earth_gate_a","bo_earth_gate_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("courtyard_gate_a",sokf_static_movement,"courtyard_entry_a","bo_courtyard_entry_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("courtyard_gate_b",sokf_static_movement,"courtyard_entry_b","bo_courtyard_entry_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("courtyard_gate_c",sokf_static_movement,"courtyard_entry_c","bo_courtyard_entry_c",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

	("portcullis",sokf_static_movement,"portcullis_a_mod","bo_portcullis_a_mod",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("door_screen",sokf_static_movement,"door_screen","0",[set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("door_extension_a",sokf_static_movement,"door_extension_a","bo_door_extension_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("dungeon_door_cell_a",sokf_static_movement,"dungeon_door_cell_a","bo_dungeon_door_cell_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_cell_b",sokf_static_movement,"dungeon_door_cell_b","bo_dungeon_door_cell_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_entry_a",sokf_static_movement,"dungeon_door_entry_a","bo_dungeon_door_entry_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_entry_b",sokf_static_movement,"dungeon_door_entry_b","bo_dungeon_door_entry_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_entry_c",sokf_static_movement,"dungeon_door_entry_c","bo_dungeon_door_entry_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_direction_a",sokf_static_movement,"dungeon_door_direction_a","bo_dungeon_door_direction_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_direction_b",sokf_static_movement,"dungeon_door_direction_b","bo_dungeon_door_direction_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_stairs_a",sokf_static_movement,"dungeon_door_stairs_a","bo_dungeon_door_stairs_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_door_stairs_b",sokf_static_movement,"dungeon_door_stairs_b","bo_dungeon_door_stairs_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tutorial_door_b",sokf_static_movement,"tutorial_door_b","bo_tutorial_door_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("castle_f_doors_top_a",sokf_static_movement,"castle_f_doors_top_a","bo_castle_f_doors_top_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("house_roof_door",sokf_static_movement,"house_roof_door","bo_house_roof_door",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("towngate_door_left",sokf_static_movement,"door_g_left","bo_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("towngate_door_right",sokf_static_movement,"door_g_right","bo_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("towngate_rectangle_door_left",sokf_static_movement,"towngate_rectangle_door_left","bo_towngate_rectangle_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("towngate_rectangle_door_right",sokf_static_movement,"towngate_rectangle_door_right","bo_towngate_rectangle_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_a",sokf_static_movement,"door_a","bo_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_b",sokf_static_movement,"door_b","bo_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_c",sokf_static_movement,"door_c","bo_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_d",sokf_static_movement,"door_d","bo_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tavern_door_a",sokf_static_movement,"tavern_door_a","bo_tavern_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tavern_door_b",sokf_static_movement,"tavern_door_b","bo_tavern_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_e_left",sokf_static_movement,"door_e_left","bo_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_e_right",sokf_static_movement,"door_e_right","bo_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_f_left",sokf_static_movement,"door_f_left","bo_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_f_right",sokf_static_movement,"door_f_right","bo_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_h_left",sokf_static_movement,"door_g_left","bo_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("door_h_right",sokf_static_movement,"door_g_right","bo_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("passage_house_c_door",sokf_static_movement,"passage_house_c_door","bo_passage_house_c_door",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("panel_door_a",sokf_static_movement,"house_door_a","bo_house_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("panel_door_b",sokf_static_movement,"house_door_b","bo_house_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
	("viking_keep_destroy_door",sokf_static_movement,"viking_keep_destroy_door","bo_viking_keep_destroy_door", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 3000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("viking_keep_destroy_sally_door_right",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(0),"viking_keep_destroy_sally_door_right","bo_viking_keep_destroy_sally_door_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 6000)]),set_material_wood, check_sally_door_use_trigger_double, check_prop_hit_trigger, check_prop_destroy_trigger]),
  ("viking_keep_destroy_sally_door_left",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(0),"viking_keep_destroy_sally_door_left","bo_viking_keep_destroy_sally_door_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 6000)]),set_material_wood, check_sally_door_use_trigger_double, check_prop_hit_trigger, check_prop_destroy_trigger]),
  ("castle_f_door_b",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(0),"castle_e_sally_door_a","bo_castle_e_sally_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood, check_castle_door_use_trigger, check_prop_hit_trigger, check_prop_destroy_trigger]),
  ("earth_sally_gate_left",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2),"earth_sally_gate_left","bo_earth_sally_gate_left",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood, check_sally_door_use_trigger_double, check_prop_hit_trigger, check_prop_destroy_trigger]),
  ("earth_sally_gate_right",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2),"earth_sally_gate_right","bo_earth_sally_gate_right",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood, check_sally_door_use_trigger_double, check_prop_hit_trigger, check_prop_destroy_trigger]),
  ("castle_e_sally_door_a",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(0),"castle_e_sally_door_a","bo_castle_e_sally_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 6000)]),set_material_wood, check_sally_door_use_trigger, check_prop_hit_trigger, check_prop_destroy_trigger]),
  ("castle_f_sally_door_a",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(0),"castle_f_sally_door_a","bo_castle_f_sally_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood, check_sally_door_use_trigger, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("door_destructible",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2),"tutorial_door_a","bo_tutorial_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood, check_item_use_trigger, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("castle_f_door_a",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(0),"castle_f_door_a","bo_castle_f_door_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 2000)]),set_material_wood, check_castle_door_use_trigger, check_prop_hit_trigger, check_prop_destroy_trigger]),
	
  ("winch",sokf_static_movement,"winch","bo_winch",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winch_b",sokf_static_movement|spr_use_time(5),"winch_b","bo_winch", 
	[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger,
   (ti_on_scene_prop_use,
    [
      (store_trigger_param_1, ":agent_id"),
      (store_trigger_param_2, ":instance_id"),

      #for only server itself-----------------------------------------------------------------------------------------------
      (call_script, "script_use_item", ":instance_id", ":agent_id"),
      #for only server itself-----------------------------------------------------------------------------------------------
      (get_max_players, ":num_players"),                               
      (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
        (player_is_active, ":player_no"),
        (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_use_item, ":instance_id", ":agent_id"),
      (try_end),
    ]),
  ]),
	("winch_stabilizer_a",sokf_static_movement,"winch_stabilizer_a","bo_winch_stabilizer_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 4000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),

#siege
	("siege_wall_a",sokf_static_movement,"siege_wall_a","bo_siege_wall_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),	
	("siege_large_shield_a",sokf_static_movement,"siege_large_shield_a","bo_siege_large_shield_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("spike_group_a",sokf_static_movement,"spike_group_a","bo_spike_group_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("spike_a",sokf_static_movement,"spike_a","bo_spike_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("siege_ladder_6m",sokf_type_ladder|sokf_static_movement,"siege_ladder_move_6m","bo_siege_ladder_move_6m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 600)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("siege_ladder_8m",sokf_type_ladder|sokf_static_movement,"siege_ladder_move_8m","bo_siege_ladder_move_8m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 800)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("siege_ladder_10m",sokf_type_ladder|sokf_static_movement,"siege_ladder_move_10m","bo_siege_ladder_move_10m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("siege_ladder_12m",sokf_type_ladder|sokf_static_movement,"siege_ladder_move_12m","bo_siege_ladder_move_12m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("siege_ladder_14m",sokf_type_ladder|sokf_static_movement,"siege_ladder_move_14m","bo_siege_ladder_move_14m",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("gourd",sokf_static_movement,"gourd","bo_gourd",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("gourd_spike",sokf_static_movement,"gourd_spike","bo_gourd_spike",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("arena_archery_target_a",sokf_static_movement|sokf_destructible,"arena_archery_target_a","bo_arena_archery_target_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("archery_butt_a",sokf_static_movement,"archery_butt","bo_archery_butt",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_earth,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("archery_target_with_hit_a",sokf_static_movement|sokf_destructible,"arena_archery_target_a","bo_arena_archery_target_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("dummy_a",sokf_destructible|sokf_static_movement|sokf_destructible,"arena_archery_target_b","bo_arena_archery_target_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
#tools
	("wheelbarrow",sokf_static_movement,"wheelbarrow","bo_wheelbarrow",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("broom",sokf_static_movement,"broom","0",[set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("torture_tool_a",sokf_static_movement,"torture_tool_a","bo_torture_tool_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("torture_tool_b",sokf_static_movement,"torture_tool_b","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("torture_tool_c",sokf_static_movement,"torture_tool_c","bo_torture_tool_c",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("knife_eating",sokf_static_movement,"knife_eating","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("colander",sokf_static_movement,"colander","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("ladle",sokf_static_movement,"ladle","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("spoon",sokf_static_movement,"spoon","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("skewer",sokf_static_movement,"skewer","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("bowl",sokf_static_movement,"bowl_big","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bowl_small",sokf_static_movement,"bowl_small","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("cauldron_a",sokf_static_movement,"cauldron_a","bo_cauldron_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 50)]),set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("fry_pan_a",sokf_static_movement,"fry_pan_a","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tripod_cauldron_a",sokf_static_movement,"tripod_cauldron_a","bo_tripod_cauldron_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tripod_cauldron_b",sokf_static_movement,"tripod_cauldron_b","bo_tripod_cauldron_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("basket",sokf_static_movement,"basket_small","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("basket_big",sokf_static_movement,"basket_large","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("basket_big_green",sokf_static_movement,"basket_big","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("leatherwork_frame",sokf_static_movement,"leatherwork_frame","0", [set_material_leather,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("basket_a",sokf_static_movement,"basket_a","bo_basket_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("feeding_trough_a",sokf_static_movement,"feeding_trough_a","bo_feeding_trough_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("hook_a",sokf_static_movement,"hook_a","0", [set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("village_oven",sokf_static_movement,"village_oven","bo_village_oven",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 10000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("cart",sokf_static_movement,"cart","bo_cart",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("winery_wine_cart_small_loaded",sokf_static_movement,"winery_wine_cart_small_loaded","bo_winery_wine_cart_small_loaded",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_wine_cart_small_empty",sokf_static_movement,"winery_wine_cart_small_empty","bo_winery_wine_cart_small_empty",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_wine_cart_empty",sokf_static_movement,"winery_wine_cart_empty","bo_winery_wine_cart_empty",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_wine_cart_loaded",sokf_static_movement,"winery_wine_cart_loaded","bo_winery_wine_cart_loaded",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("weavery_loom_a",sokf_static_movement,"weavery_loom_a","bo_weavery_loom_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("weavery_spinning_wheel",sokf_static_movement,"weavery_spinning_wheel","bo_weavery_spinning_wheel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("smithy_grindstone_wheel",sokf_static_movement,"smithy_grindstone_wheel","bo_smithy_grindstone_wheel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("smithy_forge_bellows",sokf_static_movement,"smithy_forge_bellows","bo_smithy_forge_bellows",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("smithy_forge",sokf_static_movement,"smithy_forge","bo_smithy_forge",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 20000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("smithy_anvil",sokf_static_movement,"smithy_anvil","bo_smithy_anvil",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("tannery_hide_a",sokf_static_movement,"tannery_hide_a","bo_tannery_hide_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_leather,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tannery_hide_b",sokf_static_movement,"tannery_hide_b","bo_tannery_hide_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_leather,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tannery_pools_a",sokf_static_movement,"tannery_pools_a","bo_tannery_pools_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tannery_pools_b",sokf_static_movement,"tannery_pools_b","bo_tannery_pools_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("weavery_dye_pool_r",sokf_static_movement,"weavery_dye_pool_r","bo_weavery_dye_pool",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("weavery_dye_pool_y",sokf_static_movement,"weavery_dye_pool_y","bo_weavery_dye_pool",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("weavery_dye_pool_b",sokf_static_movement,"weavery_dye_pool_b","bo_weavery_dye_pool",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("weavery_dye_pool_p",sokf_static_movement,"weavery_dye_pool_p","bo_weavery_dye_pool",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("weavery_dye_pool_g",sokf_static_movement,"weavery_dye_pool_g","bo_weavery_dye_pool",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
#furniture
  ("winery_barrel_shelf",sokf_static_movement,"winery_barrel_shelf","bo_winery_barrel_shelf",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_wall_shelf",sokf_static_movement,"winery_wall_shelf","bo_winery_wall_shelf",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_huge_barrel",sokf_static_movement,"winery_huge_barrel","bo_winery_huge_barrel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_wine_press",sokf_static_movement,"winery_wine_press","bo_winery_wine_press",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 1000)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("winery_middle_barrel",sokf_static_movement,"winery_middle_barrel","bo_winery_middle_barrel",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("mill_flour_sack",sokf_static_movement,"mill_flour_sack","bo_mill_flour_sack",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 50)]),set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),	
  ("mill_flour_sack_desk_a",sokf_static_movement,"mill_flour_sack_desk_a","bo_mill_flour_sack_desk_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),	
  ("mill_flour_sack_desk_b",sokf_static_movement,"mill_flour_sack_desk_b","bo_mill_flour_sack_desk_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 400)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("brewery_big_bucket",sokf_static_movement,"brewery_big_bucket","bo_brewery_big_bucket",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("brewery_bucket_platform_a",sokf_static_movement,"brewery_bucket_platform_a","bo_brewery_bucket_platform_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("plate_a",sokf_static_movement,"plate_a","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("plate_b",sokf_static_movement,"plate_b","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("plate_c",sokf_static_movement,"plate_c","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("hanger",sokf_static_movement,"hanger","0", [set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("shelves",sokf_static_movement,"shelves","boshelves",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("table_tavern",sokf_static_movement,"table_tavern","botable_tavern",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("table_castle_a",sokf_static_movement,"table_castle_a","bo_table_castle_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("table_round_a",sokf_static_movement,"table_round_a","bo_table_round_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("table_round_b",sokf_static_movement,"table_round_b","bo_table_round_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("end_table_a",sokf_static_movement,"end_table_a","bo_end_table_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("tavern_table_a",sokf_static_movement,"tavern_table_a","bo_tavern_table_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tavern_table_b",sokf_static_movement,"tavern_table_b","bo_tavern_table_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("chair_castle_a",sokf_static_movement,"chair_castle_a","bo_chair_castle_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("tavern_chair_a",sokf_static_movement,"tavern_chair_a","bo_tavern_chair_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("tavern_chair_b",sokf_static_movement,"tavern_chair_b","bo_tavern_chair_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("pillow_a",sokf_static_movement,"pillow_a","bo_pillow",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("pillow_b",sokf_static_movement,"pillow_b","bo_pillow",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("pillow_c",sokf_static_movement,"pillow_c","0",[set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("carpet_with_pillows_a",sokf_static_movement,"carpet_with_pillows_a","bo_carpet_with_pillows",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("carpet_with_pillows_b",sokf_static_movement,"carpet_with_pillows_b","bo_carpet_with_pillows",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
		
  ("carpet_a",sokf_static_movement,"carpet_a","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("carpet_b",sokf_static_movement,"carpet_b","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("carpet_c",sokf_static_movement,"carpet_c","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("carpet_d",sokf_static_movement,"carpet_d","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("carpet_e",sokf_static_movement,"carpet_e","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("carpet_f",sokf_static_movement,"carpet_f","0", [set_material_fabric,check_prop_hit_trigger,check_prop_destroy_trigger]),

	("sofa_a",sokf_static_movement,"sofa_a","bo_sofa",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("sofa_b",sokf_static_movement,"sofa_b","bo_sofa",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("dungeon_bed_a",sokf_static_movement,"dungeon_bed_a","0",[set_material_earth,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dungeon_bed_b",sokf_static_movement,"dungeon_bed_b","bo_dungeon_bed_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_earth,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
  ("bed_a",sokf_static_movement,"bed_a","bo_bed_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bed_b",sokf_static_movement,"bed_b","bo_bed_b",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bed_c",sokf_static_movement,"bed_c","bo_bed_c",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bed_d",sokf_static_movement,"bed_d","bo_bed_d",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bed_e",sokf_static_movement,"bed_e","bo_bed_e",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bed_f",sokf_static_movement,"bed_f","bo_bed_f",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
	("ewer_a",sokf_static_movement,"ewer_a","bo_ewer_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("brazier_with_fire",sokf_static_movement,"brazier","bo_brazier",[set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("cooking_fire",sokf_static_movement,"fire_floor","0",[set_material_stone,check_prop_hit_trigger,check_prop_destroy_trigger]),	
	("candle_a",sokf_static_movement,"candle_a","0",[set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("candle_b",sokf_static_movement,"candle_b","0",[set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]), 
  ("candle_c",sokf_static_movement,"candle_c","0",[set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("lamp_a",sokf_static_movement,"lamp_a","0",[set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("lamp_b",sokf_static_movement,"lamp_b","0",[set_material_metal,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("torch",sokf_static_movement,"torch_a","0",[set_material_wood,check_prop_hit_trigger,check_prop_destroy_trigger]),
	
#food
	("fried_pig",sokf_static_movement,"pork","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("fried_pig_b",sokf_static_movement,"fried_pig","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("garlic",sokf_static_movement,"garlic","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("garlic_b",sokf_static_movement,"garlic_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("lettuce",sokf_static_movement,"lettuce","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("grape_a",sokf_static_movement,"grape_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("grape_b",sokf_static_movement,"grape_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("apple_a",sokf_static_movement,"apple_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("apple_b",sokf_static_movement,"apple_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("maize_a",sokf_static_movement,"maize_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("maize_b",sokf_static_movement,"maize_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("cabbage",sokf_static_movement,"cabbage","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("cabbage_b",sokf_static_movement,"cabbage_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
	("bean",sokf_static_movement,"bean","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("flax_bundle",sokf_static_movement,"raw_flax","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("olive_plane",sokf_static_movement,"olive_plane","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("grapes_plane",sokf_static_movement,"grapes_plane","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("date_fruit_plane",sokf_static_movement,"date_fruit_plane","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dye_blue",sokf_static_movement,"raw_dye_blue","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dye_red",sokf_static_movement,"raw_dye_red","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("dye_yellow",sokf_static_movement,"raw_dye_yellow","0",[set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("marrow_a",sokf_static_movement,"marrow_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("marrow_b",sokf_static_movement,"marrow_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("squash_plant",sokf_static_movement,"marrow_c","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("cheese_a",sokf_static_movement,"cheese_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("cheese_b",sokf_static_movement,"cheese_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("cheese_slice_a",sokf_static_movement,"cheese_slice_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bread_a",sokf_static_movement,"bread_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bread_b",sokf_static_movement,"bread_b","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("bread_slice_a",sokf_static_movement,"bread_slice_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("fish_a",sokf_static_movement,"fish_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("fish_roasted_a",sokf_static_movement,"fish_roasted_a","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
  ("chicken_roasted",sokf_static_movement,"chicken","0", [set_material_food,check_prop_hit_trigger,check_prop_destroy_trigger]),
#MOD end
 
#MOD begin
	("cannon_1", 0, "cannon_1", 0, []),
	("cannon_base_1", sokf_static_movement, "cannon_base_1", "bo_cannon_base_1", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 10000)]),set_material_wood, set_negative_slot_values, parent_prop_spawn, check_prop_hit_trigger]),
	("cannon_barrel_1_32lb", sokf_static_movement|spr_use_time(3), "cannon_barrel_1_32lb", "bo_cannon_barrel_1_32lb",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 5000)]),set_material_metal, set_negative_slot_values, child_prop_spawn, check_item_start_use_trigger, check_item_use_trigger,check_prop_hit_trigger]),
	("cannon_wheel_1", sokf_static_movement, "cannon_wheel_1", 0, [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood, set_negative_slot_values, child_prop_spawn, check_prop_hit_trigger]),
	("cannon_ammo_box_32lb_1", sokf_static_movement|sokf_show_hit_point_bar|spr_use_time(1), "ammo_box_1", "bo_ammo_box_1",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 3000)]),set_material_wood, set_negative_slot_values, ammo_box_prop_spawn, check_item_use_trigger, check_prop_hit_trigger]),

	("round_shot_missile", sokf_moveable, "round_shot_32lb", 0, [set_material_metal, set_negative_slot_values,
		(ti_on_scene_prop_init,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_weight, 145),# 0.1 kg steps
			(scene_prop_set_slot, ":instance_id", scene_prop_radius, 8),# cm
			(scene_prop_set_slot, ":instance_id", scene_prop_friction, 1),# mm
		]),
	]),

	("arrow_missile", sokf_moveable, "arrow_bodkin_flying", 0, [set_material_wood, set_negative_slot_values,
		(ti_on_scene_prop_init,
		[
			(store_trigger_param_1, ":instance_id"),
			(scene_prop_set_slot, ":instance_id", scene_prop_friction, 2),# mm
			
		]),
	]),

	("cannons_end", 0, 0, 0, []),

	("pavise_prop_b", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_2", "bo_tableau_shield_pavise_prop_2", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),
	("pavise_prop_c", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_1", "bo_tableau_shield_pavise_prop_1", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),

	("pavise_prop_b_+1", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_2", "bo_tableau_shield_pavise_prop_2", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),
	("pavise_prop_c_+1", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_1", "bo_tableau_shield_pavise_prop_1", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),

	("pavise_prop_b_+2", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_2", "bo_tableau_shield_pavise_prop_2", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),
	("pavise_prop_c_+2", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_1", "bo_tableau_shield_pavise_prop_1", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),

	("pavise_prop_b_+3", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_2", "bo_tableau_shield_pavise_prop_2", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),
	("pavise_prop_c_+3", sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible|spr_use_time(2), "tableau_shield_pavise_prop_1", "bo_tableau_shield_pavise_prop_1", [set_material_wood, shield_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger, check_item_use_trigger]),

	("shields_end", 0, 0, 0, []),
	
	("construct_box_a",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible,"box_a","bo_box_a", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 200)]),set_material_wood, construct_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("construct_siege_large_shield_a",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible,"siege_large_shield_a","bo_siege_large_shield_a",[(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood, construct_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("construct_spike_group_a",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible,"spike_group_a","bo_spike_group_a", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 600)]),set_material_wood, construct_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("construct_plank",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible,"plank","bo_plank", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 100)]),set_material_wood, construct_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("construct_earthwork",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible,"earthwork","bo_earthwork", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 500)]),set_material_wood, construct_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger]),
	("construct_gabion",sokf_static_movement|sokf_show_hit_point_bar|sokf_destructible,"gabion_sand","bo_gabion_sand", [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 300)]),set_material_wood, construct_prop_spawn, check_prop_hit_trigger, check_prop_destroy_trigger]),
	 
	("construct_end", 0, 0, 0, []),
#MOD end
]

#MOD begin
#0= string, 1= number of varieties, 2= number of sub-parts
objects = [
('wall_stone_simple', 2, 9),
('wall_stone_embrasure', 1, 9),
('wall_stone_entrance', 2, 7),
]
for i in range(0, 2):
	for cur_obj in range(len(objects)):
		for cur_var in range(0, objects[cur_obj][1]):
			for cur_char in range(97, 102):
				if i == 0:
					scene_props += (
						objects[cur_obj][0] + "_" + str(cur_var +1) + chr(cur_char), sokf_static_movement, objects[cur_obj][0] + "_" + str(cur_var +1) + chr(cur_char), "bo_" + objects[cur_obj][0] + "_" + str(cur_var +1), [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 20000)]),set_material_stone, check_prop_hit_trigger]),
				else:
					for cur_sub in range(0, objects[cur_obj][2]):
						scene_props += (
							objects[cur_obj][0] + "_" + str(cur_var +1) + chr(cur_char) + "_p" + str(cur_sub +1), sokf_static_movement, objects[cur_obj][0] + "_" + str(cur_var +1) + chr(cur_char) + "_p" + str(cur_sub +1), "bo_" + objects[cur_obj][0] + "_" + str(cur_var +1) + "_p" + str(cur_sub +1), [(ti_on_init_scene_prop,[(store_trigger_param_1, ":prop"),(scene_prop_set_hit_points, ":prop", 20000)]),set_material_stone, check_prop_hit_trigger]),

scene_props += ("scene_props_end", 0, 0, 0, []),

# generate dummy props to avoid item spawn issues on dedicated servers
from module_items import items
if len(scene_props) < len(items): #more items than props
	for i in range(len(scene_props), len(items)):
		scene_props += ("zyx_dummy_prop_"+str(i), 0, 0, 0, []),