from module_constants import *
from ID_factions import *
from header_items import  *
from header_operations import *
from header_triggers import *

####################################################################################################################
#  Each item record contains the following fields:
#  1) Item id: used for referencing items in other files.
#     The prefix itm_ is automatically added before each item id.
#  2) Item name. Name of item as it'll appear in inventory window
#  3) List of meshes.  Each mesh record is a tuple containing the following fields:
#    3.1) Mesh name.
#    3.2) Modifier bits that this mesh matches.
#     Note that the first mesh record is the default.
#  4) Item flags. See header_items.py for a list of available flags.
#  5) Item capabilities. Used for which animations this item is used with. See header_items.py for a list of available flags.
#  6) Item value.
#  7) Item stats: Bitwise-or of various stats about the item such as:
#      weight, abundance, difficulty, head_armor, body_armor,leg_armor, etc...
#  8) Modifier bits: Modifiers that can be applied to this item.
#  9) [Optional] Triggers: List of simple triggers to be associated with the item.
#  10) [Optional] Factions: List of factions that item can be found as merchandise.
####################################################################################################################

# Some constants for ease of use.
imodbits_none = 0
imodbits_horse_basic = imodbit_swaybacked|imodbit_lame|imodbit_spirited|imodbit_heavy|imodbit_stubborn
imodbits_cloth  = imodbit_tattered | imodbit_ragged | imodbit_sturdy | imodbit_thick | imodbit_hardened
imodbits_armor  = imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_plate  = imodbit_cracked | imodbit_rusty | imodbit_battered | imodbit_crude | imodbit_thick | imodbit_reinforced |imodbit_lordly
imodbits_polearm = imodbit_cracked | imodbit_bent | imodbit_balanced
imodbits_shield  = imodbit_cracked | imodbit_battered |imodbit_thick | imodbit_reinforced
imodbits_sword   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered
imodbits_sword_high   = imodbit_rusty | imodbit_chipped | imodbit_balanced |imodbit_tempered|imodbit_masterwork
imodbits_axe   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_mace   = imodbit_rusty | imodbit_chipped | imodbit_heavy
imodbits_pick   = imodbit_rusty | imodbit_chipped | imodbit_balanced | imodbit_heavy
imodbits_bow = imodbit_cracked | imodbit_bent | imodbit_strong |imodbit_masterwork
imodbits_crossbow = imodbit_cracked | imodbit_bent | imodbit_masterwork
imodbits_missile   = imodbit_bent | imodbit_large_bag
imodbits_thrown   = imodbit_bent | imodbit_heavy| imodbit_balanced| imodbit_large_bag
imodbits_thrown_minus_heavy = imodbit_bent | imodbit_balanced| imodbit_large_bag

imodbits_horse_good = imodbit_spirited|imodbit_heavy
imodbits_good   = imodbit_sturdy | imodbit_thick | imodbit_hardened | imodbit_reinforced
imodbits_bad    = imodbit_rusty | imodbit_chipped | imodbit_tattered | imodbit_ragged | imodbit_cracked | imodbit_bent
# Replace winged mace/spiked mace with: Flanged mace / Knobbed mace?
# Fauchard (majowski glaive) 

#MOD begin
hp_head = 150
hp_hand = 100
hp_body = 400
hp_leg = 200
hp_melee = 1000
hp_ranged = 500

weapon_attack = (ti_on_weapon_attack,	
[
    (call_script, "script_weapon_attack"),
])

#weapon_attack = (ti_on_weapon_attack,
#    [
#      (store_trigger_param_1, ":agent_id"),
#      (agent_is_active, ":agent_id"),
#      (agent_is_alive, ":agent_id"),
#	  (neg|agent_is_non_player, ":agent_id"),
#      (try_begin),
#        (multiplayer_is_dedicated_server),
#		(agent_get_player_id, ":player_id", ":agent_id"),
#        (try_begin),
#          (player_is_active, ":player_id"),
#          (multiplayer_send_message_to_player, ":player_id", multiplayer_event_client_stamina_attack),
#        (try_end),
#      (else_try),
#        (get_player_agent_no, ":player_agent"),
#        (eq, ":agent_id", ":player_agent"),
#        (call_script, "script_agent_lose_stamina", ":agent_id", stamina_attack),
#      (try_end),
#    ])

missile_hit_arrow = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit arrow"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_arrow),
	])
	
missile_hit_bolt = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit bolt"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_bolt),
	])
	
missile_hit_bullet = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit bullet"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_bullet),
	])
	
missile_hit_thrown = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit thrown"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_thrown),
	])
	
missile_hit_roundshot = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit roundshot"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_roundshot),
	])
	
missile_hit_bomb = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit bomb"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_bomb),
	])

missile_hit_fire = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit arrow"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_fire),
	])
	
missile_hit_firepot = (ti_on_missile_hit,
	[
	  #(display_message, "@missile hit firepot"),
	  # (store_trigger_param_1, ":agent_id"),
	  # (store_trigger_param_2, ":collision_type"),
	  (call_script, "script_item_missile_hit", missile_firepot),
	])
	
	

init_firepot = (ti_on_init_item,
	[
		(neg|multiplayer_is_dedicated_server),
		(set_position_delta,14,15,0),
		(particle_system_add_new,"psys_smoke_xs"),
	])

	
	
surgeon_kit_self = (ti_on_weapon_attack,
	[
		(store_trigger_param_1, ":agent_no"),
		
		# (agent_get_slot, ":hp_lost", ":agent_no", slot_agent_bleed_hp_lost),
		# (gt, ":hp_lost", 0),
		
		(store_agent_hit_points, ":agent_hp", ":agent_no", 0),
		(neq, ":agent_hp", 100),
		(agent_set_slot, ":agent_no", slot_agent_bleed_hp_lost, 0),#reset

		(val_add, ":agent_hp", score_bonus_surgeon),
		(agent_set_hit_points, ":agent_no", ":agent_hp", 0),
		
		(agent_get_wielded_item, ":item_id", ":agent_no", 0),
		(val_sub, ":item_id", 1),
		
		(assign, ":end_item_slot", ek_head),
		(try_for_range, ":cur_item_slot", ek_item_0, ":end_item_slot"),
			(agent_get_item_slot, ":cur_item", ":agent_no", ":cur_item_slot"),
			(eq, ":cur_item", ":item_id"),
			(agent_get_ammo_for_slot, ":ammo", ":agent_no", ":cur_item_slot"),
			(val_sub, ":ammo", 1),
			(agent_set_ammo, ":agent_no", ":cur_item", ":ammo"),
			(assign, ":end_item_slot", -1),#break loop
		(try_end),
		
		(agent_slot_ge, ":agent_no", slot_agent_bleed_time, 1),#stop bleeding
		(agent_set_slot, ":agent_no", slot_agent_bleed_time, 0),
		(agent_set_slot, ":agent_no", slot_agent_bleed_interval, 0),
	])
	
deploy_ladder = (ti_on_weapon_attack,
	[
		(store_trigger_param_1, ":agent_no"),

		# (copy_position, pos_spawn, pos1),
		(agent_get_position, pos_spawn, ":agent_no"),
		(position_move_y, pos_spawn, 50),
		# (position_move_x, pos_spawn, 30),
		# (position_rotate_x, pos_spawn, -90),
		(position_set_z_to_ground_level, pos_spawn),
		
		# (position_rotate_x, pos_spawn, -90),
		(assign, ":move_step", 10),
		(assign, ":end", 150),
		
		(position_rotate_x, pos_spawn, -15),
		(try_for_range, ":cur_angle", 0, ":end"),
			(copy_position, pos_calc, pos_spawn),
			(store_mul, ":rot", ":cur_angle", -1),
			(position_rotate_x, pos_calc, ":rot"),
			(position_move_z, pos_calc, 200),
			(store_div, ":range_end", 300, ":move_step"),
			(try_for_range, ":unused", 0, ":range_end"),
				(position_move_z, pos_calc, ":move_step"),
				(position_get_distance_to_ground_level, ":dist_to_ground", pos_calc),
				(le, ":dist_to_ground", 0),
				(assign, ":end", -1),
				(assign, ":range_end", -1),
				
				(agent_set_slot, ":agent_no", slot_agent_carry_slot_0_used, 0),
				(agent_unequip_item, ":agent_no", itm_ladder_5m, 0),
				(call_script, "script_agent_set_weight", ":agent_no", itm_ladder_5m, agent_sub_carry_weight),
				(call_script, "script_spawn_new_or_get_free_scene_prop", "spr_ladder_move_5m"),
				
				(position_rotate_x, pos_spawn, ":rot"),
				(store_mul, ":duration", ":cur_angle", 5),
				(prop_instance_animate_to_position, reg0, pos_spawn, ":duration"),
			(try_end),
		(try_end),
	])

on_shield_hit = (ti_on_shield_hit,
[  
    #(display_message, "@ti_on_shield_hit"),
    # (this_or_next|multiplayer_is_server),
    # (neg|game_in_multiplayer_mode),

    (store_trigger_param_1, ":victim_agent"),
	(store_trigger_param_2, ":dealer_agent"),
	(store_trigger_param_3, ":damage"),
	(store_trigger_param, ":weapon", 4),
	(store_trigger_param, ":missile", 5),
	
	(agent_get_bone_position, pos_hit, ":victim_agent", hb_item_l, 1),
	(item_get_body_armor, ":armor", ":weapon"),
	(call_script, "script_calc_attack_penetration", ":weapon", ":damage", ":armor", ":dealer_agent", ":missile"),
	(assign, ":final_damage", reg0),
	
	#upkeep
	(try_begin),
		(multiplayer_is_dedicated_server),
		(neg|agent_is_non_player, ":dealer_agent"),
		(gt, ":weapon", -1),
		(item_get_type, ":item_type", ":weapon"),
		(this_or_next|eq, ":item_type", itp_type_one_handed_wpn),
		(this_or_next|eq, ":item_type", itp_type_two_handed_wpn),
		(eq, ":item_type", itp_type_polearm),
		(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":dealer_agent", ":weapon", ":final_damage"),
	(try_end),
	
	(agent_get_troop_id, ":victim_troop", ":victim_agent"),
	(store_skill_level, ":skl", "skl_resistance", ":victim_troop"),
	(try_begin),
		(gt, ":skl", 0),
		(store_mul, ":skl_percent", ":skl", resistance_bonus),
		(store_add, ":dmg_percent", 100, ":skl_percent"),
		(val_mul, ":final_damage", 100),
		(val_div, ":final_damage", ":dmg_percent"),
		(val_max, ":final_damage", 0),
	(try_end),
	
	(try_begin),
		(gt, ":final_damage", 0),
		
		#update shield hp slots
		(agent_get_wielded_item, ":shield_item", ":victim_agent", 1),
		(try_begin),
			(item_slot_eq, ":shield_item", slot_item_carry_slot, 3),
			(assign, ":shield_hp_slot", slot_agent_cur_shield_hp),
		(else_try),
			(assign, ":shield_hp_slot", slot_agent_cur_buckler_hp),
		(try_end),
		(agent_get_slot, ":shield_hp", ":victim_agent", ":shield_hp_slot"),
		(val_sub, ":shield_hp", ":final_damage"),
		(val_max, ":shield_hp", 0),
		(agent_set_slot, ":victim_agent", ":shield_hp_slot", ":shield_hp"),
		
		(try_begin),
			(multiplayer_is_server),
			(neq, ":victim_agent", ":dealer_agent"),
			
			(agent_get_team, ":victim_team", ":victim_agent"),
			(agent_get_team, ":dealer_team", ":dealer_agent"),
			
			(assign, ":dealer_player", -1),
			(try_begin),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
			
				(neg|agent_is_non_player, ":dealer_agent"),
				(agent_get_player_id, ":dealer_player", ":dealer_agent"),
				(player_is_active, ":dealer_player"),
				
				(assign, ":damage_score", ":final_damage"),
				(agent_get_wielded_item, ":cur_item", ":victim_agent", 1),
				(try_begin),
					(gt, ":cur_item", -1),
					(item_get_body_armor, ":armor_value", ":cur_item"),
					(val_add, ":damage_score", ":armor_value"),
				(try_end),
				
				(val_div, ":damage_score", score_prop_divider),
				(try_begin),#less score on bots
					(agent_is_non_player, ":victim_agent"),
					(val_div, ":damage_score", score_victim_bot_divider),
				(try_end),
				
				(gt, ":damage_score", 0),
				
				#(player_get_score, ":player_score", ":dealer_player"),
				(player_get_slot, ":player_score", ":dealer_player", slot_player_score),
				(try_begin),
					(eq, ":victim_team", ":dealer_team"),#same team sub score
					(val_sub, ":player_score", ":damage_score"),
				(else_try),
					(val_add, ":player_score", ":damage_score"),
				(try_end),
				#(player_set_score, ":dealer_player", ":player_score"),#set score serverside
				(player_set_slot, ":dealer_player", slot_player_score, ":player_score"),
				
				(get_max_players, ":num_players"),
				(try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
					(player_is_active, ":player_no"),
					(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_score_count, ":dealer_player", ":player_score"),
				(try_end),
			(try_end),
			
			#send damage report to victim player
			(assign, reg0, ":final_damage"),
			(try_begin),
				(neg|agent_is_non_player, ":victim_agent"),
				(agent_get_player_id, ":victim_player", ":victim_agent"),
				(player_is_active, ":victim_player"),
				(player_slot_eq, ":victim_player", slot_player_show_damage_report, 1),
				(str_store_string, s0, "str_shield_received_damage"),
				(call_script, "script_multiplayer_send_message_to_player", ":victim_player", color_victim_damage),
			(try_end),

			#send damage report to dealer player
			(try_begin),
				(player_is_active, ":dealer_player"),
				(player_slot_eq, ":dealer_player", slot_player_show_damage_report, 1),
				(neq, ":dealer_agent", ":victim_agent"),
				(str_store_string, s0, "str_delivered_damage_to_shield"),
				(call_script, "script_multiplayer_send_message_to_player", ":dealer_player", color_dealer_damage),
			(try_end),
		(try_end),
	(try_end),

	#set final trigger damage
	(set_trigger_result, ":final_damage"),
  ])

#MOD end

items = []
items += ["regular_items_begin", "Regular Items Begin", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],

items += [
#head armor
["straw_hat", "Straw Hat", [("straw_hat_new",0)], itp_type_head_armor|itp_civilian, 0, 2, weight(0.10)|head_armor(5)|hit_points(241)|max_ammo(1), imodbits_none], 
["headcloth", "Headcloth", [("headcloth_a_new",0)], itp_type_head_armor|itp_civilian, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["head_wrappings", "Head Wrapping", [("head_wrapping",0)], itp_type_head_armor|itp_fit_to_head, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["common_hood", "Hood", [("hood_new",0)], itp_type_head_armor|itp_civilian, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["woolen_cap", "Woolen Cap", [("woolen_cap_new",0)], itp_type_head_armor|itp_civilian, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["felt_hat", "Felt Hat", [("felt_hat_a_new",0)], itp_type_head_armor|itp_civilian, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["felt_hat_b", "Felt Cap", [("felt_hat_b_new",0)], itp_type_head_armor|itp_civilian, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["hl_hat_1a", "Grey Hat", [("hl_hat_1a",0)], itp_type_head_armor, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["hl_hat_1b", "Brown Hat", [("hl_hat_1b",0)], itp_type_head_armor, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["hl_hat_2a", "Grey Hat with Feathers", [("hl_hat_2a",0)], itp_type_head_armor, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["hl_hat_2b", "Brown Hat with Feathers", [("hl_hat_2b",0)], itp_type_head_armor, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["hat_black", "Black Hat", [("hat_black",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["hat_blue", "Blue Hat", [("hat_blue",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["hat_brown", "Brown Hat", [("hat_brown",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["hat_grey", "Grey Hat", [("hat_grey",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["hat_noble", "Noble Hat", [("hat_noble",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["officerhat_blue", "Blue Officer Hat", [("officerhat_blue",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["officerhat_brown", "Brown Officer Hat", [("officerhat_brown",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["officerhat_grey", "Grey Officer Hat", [("officerhat_grey",0)], itp_type_head_armor, 0, 5, weight(0.30)|head_armor(16)|hit_points(242)|max_ammo(1), imodbits_none], 
["surgeon_coif", "Surgeon Coif", [("surgeon_coif",0)], itp_type_head_armor, 0, 3, weight(0.20)|head_armor(11)|hit_points(241)|max_ammo(1), imodbits_none], 
["arming_cap", "Arming Cap", [("arming_cap_a_new",0)], itp_type_head_armor, 0, 12, weight(0.30)|head_armor(23)|hit_points(242)|max_ammo(2), imodbits_none], 
["padded_coif", "Padded Coif", [("padded_coif_a_new",0)], itp_type_head_armor, 0, 15, weight(0.40)|head_armor(30)|hit_points(242)|max_ammo(2), imodbits_none], 
["fur_hat", "Fur Hat", [("fur_hat_a_new",0)], itp_type_head_armor, 0, 15, weight(0.40)|head_armor(30)|hit_points(242)|max_ammo(2), imodbits_none], 
["nomad_cap_b", "Nomad Cap", [("nomad_cap_b_new",0)], itp_type_head_armor, 0, 14, weight(0.30)|head_armor(24)|hit_points(242)|max_ammo(3), imodbits_none], 
["leather_cap", "Leather Cap", [("leather_cap_a_new",0)], itp_type_head_armor, 0, 14, weight(0.30)|head_armor(24)|hit_points(242)|max_ammo(3), imodbits_none], 
["leather_steppe_cap_a", "Padded Steppe Cap", [("leather_steppe_cap_a_new",0)], itp_type_head_armor, 0, 25, weight(0.50)|head_armor(41)|hit_points(243)|max_ammo(3), imodbits_none], 
["leather_steppe_cap_c", "Spiked Steppe Cap", [("steppe_cap_a_new",0)], itp_type_head_armor, 0, 25, weight(0.70)|head_armor(42)|hit_points(244)|max_ammo(3), imodbits_none], 
["leather_warrior_cap", "Leather Warrior Cap", [("skull_cap_new_b",0)], itp_type_head_armor, 0, 25, weight(0.50)|head_armor(41)|hit_points(243)|max_ammo(3), imodbits_none], 
["bork1", "Bork", [("bork1",0)], itp_type_head_armor, 0, 19, weight(0.50)|head_armor(38)|hit_points(243)|max_ammo(2), imodbits_none], 
["bork2", "Heavy Bork", [("bork2",0),("inv_bork2",ixmesh_inventory)], itp_type_head_armor, 0, 19, weight(0.50)|head_armor(38)|hit_points(243)|max_ammo(2), imodbits_none], 
["bork3", "Long Bork", [("bork3",0)], itp_type_head_armor, 0, 19, weight(0.50)|head_armor(38)|hit_points(243)|max_ammo(2), imodbits_none], 
["uskuf", "Uskuf", [("uskuf",0), ("inv_uskuf",ixmesh_inventory)], itp_type_head_armor, 0, 19, weight(0.50)|head_armor(38)|hit_points(243)|max_ammo(2), imodbits_none], 
["deli_cap", "Deli Cap", [("deli",0)], itp_type_head_armor, 0, 19, weight(0.50)|head_armor(38)|hit_points(243)|max_ammo(2), imodbits_none], 
["turban_1", "Red Turban", [("turban1",0), ("inv_turban1",ixmesh_inventory)], itp_type_head_armor, 0, 12, weight(0.30)|head_armor(23)|hit_points(242)|max_ammo(2), imodbits_none], 
["turban_2", "Blue Turban", [("turban2",0), ("inv_turban2",ixmesh_inventory)], itp_type_head_armor, 0, 12, weight(0.30)|head_armor(23)|hit_points(242)|max_ammo(2), imodbits_none], 
["footman_helmet", "Skullcap", [("skull_cap_new",0)], itp_type_head_armor, 0, 91, weight(2.20)|head_armor(48)|hit_points(251)|max_ammo(6), imodbits_none], 
["bascinet", "Bascinet", [("bascinet_avt_new2",0)], itp_type_head_armor, 0, 95, weight(2.30)|head_armor(50)|hit_points(252)|max_ammo(6), imodbits_none], 
["steppe_helmet", "Steppe Helmet", [("steppe_helmetW",0)], itp_type_head_armor, 0, 106, weight(2.60)|head_armor(56)|hit_points(253)|max_ammo(6), imodbits_none], 
["nomad_cap", "Nomad Cap", [("nomad_cap_a_new",0)], itp_type_head_armor|itp_civilian, 0, 70, weight(1.70)|head_armor(37)|hit_points(249)|max_ammo(6), imodbits_none], 
["chapel", "Chapel", [("chapel",0)], itp_type_head_armor, 0, 86, weight(2.10)|head_armor(45)|hit_points(251)|max_ammo(6), imodbits_none], 
["combed_morion", "Combed Morion", [("combed_morion",0)], itp_type_head_armor, 0, 99, weight(2.40)|head_armor(52)|hit_points(252)|max_ammo(6), imodbits_none], 
["visored_sallet", "Visored Sallet", [("visored_sallet",0)], itp_type_head_armor, 0, 120, weight(2.90)|head_armor(63)|hit_points(255)|max_ammo(6), imodbits_none], 
["visored_sallet_coif", "Visored Sallet with Coif", [("visored_sallet_coif",0)], itp_type_head_armor, 0, 110, weight(3.60)|head_armor(58)|hit_points(258)|max_ammo(5), imodbits_none], 
["open_sallet", "Open Sallet", [("open_sallet",0)], itp_type_head_armor, 0, 99, weight(2.40)|head_armor(52)|hit_points(252)|max_ammo(6), imodbits_none], 
["open_sallet_coif", "Open Sallet with Coif", [("open_sallet_coif",0)], itp_type_head_armor, 0, 89, weight(2.90)|head_armor(47)|hit_points(255)|max_ammo(5), imodbits_none], 
["milanese_sallet", "Milanese Sallet", [("milanese_sallet",0)], itp_type_head_armor|itp_covers_head, 0, 139, weight(3.40)|head_armor(73)|hit_points(257)|max_ammo(6), imodbits_none], 
["barbuta1", "Barbuta", [("barbuta1",0)], itp_type_head_armor, 0, 124, weight(3.00)|head_armor(65)|hit_points(255)|max_ammo(6), imodbits_none], 
["barbuta2", "Noseguard Barbuta", [("barbuta2",0)], itp_type_head_armor, 0, 127, weight(3.10)|head_armor(67)|hit_points(256)|max_ammo(6), imodbits_none], 
["greatbascinet1", "Great Bascinet", [("greatbascinet1",0)], itp_type_head_armor|itp_covers_head, 0, 152, weight(3.70)|head_armor(80)|hit_points(259)|max_ammo(6), imodbits_none], 
["burgonet_1a", "Plain Burgonet", [("sturmhaube_1",0)], itp_type_head_armor, 0, 135, weight(3.30)|head_armor(71)|hit_points(257)|max_ammo(6), imodbits_none], 
["burgonet_1b", "Black Burgonet", [("sturmhaube_1B",0)], itp_type_head_armor, 0, 135, weight(3.30)|head_armor(71)|hit_points(257)|max_ammo(6), imodbits_none], 
["burgonet_1c", "Stripped Burgonet", [("sturmhaube_1BW",0)], itp_type_head_armor, 0, 135, weight(3.30)|head_armor(71)|hit_points(257)|max_ammo(6), imodbits_none], 
["open_burgonet", "Open Burgonet", [("burgonet",0)], itp_type_head_armor, 0, 127, weight(3.10)|head_armor(67)|hit_points(256)|max_ammo(6), imodbits_none], 
["novogrod_helm", "Novogrod Helmet", [("novogrod_helm",0), ("inv_novogrod_helm",ixmesh_inventory)], itp_type_head_armor| itp_attach_armature|itp_covers_beard, 0, 114, weight(3.70)|head_armor(60)|hit_points(259)|max_ammo(5), imodbits_none], 
["chichak1", "Spiked Chichak", [("chichak1",0), ("inv_chichak1",ixmesh_inventory)], itp_type_head_armor, 0, 89, weight(2.90)|head_armor(47)|hit_points(255)|max_ammo(5), imodbits_none], 
["peyk1", "Peyk", [("peyk1",0), ("inv_peyk1",ixmesh_inventory)], itp_type_head_armor, 0, 74, weight(1.80)|head_armor(39)|hit_points(249)|max_ammo(6), imodbits_none], 

#body armor
["linen_tunic", "Linen Tunic", [("shirt_a",0)], itp_type_body_armor|itp_covers_legs, 0, 11, weight(0.70)|body_armor(5)|leg_armor(2)|hit_points(604)|max_ammo(1), imodbits_none], 
["red_shirt", "Red Shirt", [("rich_tunic_a",0)], itp_type_body_armor|itp_covers_legs, 0, 11, weight(0.70)|body_armor(5)|leg_armor(2)|hit_points(604)|max_ammo(1), imodbits_none], 
["tunic_with_green_cape", "Tunic with Green Cape", [("peasant_man_a",0)], itp_type_body_armor|itp_covers_legs, 0, 11, weight(0.70)|body_armor(5)|leg_armor(2)|hit_points(604)|max_ammo(1), imodbits_none], 
["tabard", "Tabard", [("tabard_b",0)], itp_type_body_armor|itp_covers_legs, 0, 24, weight(0.80)|body_armor(7)|leg_armor(4)|hit_points(604)|max_ammo(2), imodbits_none], 
["coarse_tunic", "Tunic with vest", [("coarse_tunic_a",0)], itp_type_body_armor|itp_covers_legs, 0, 12, weight(0.80)|body_armor(5)|leg_armor(3)|hit_points(604)|max_ammo(1), imodbits_none], 
["leather_jacket", "Leather Jacket", [("leather_jacket_new",0)], itp_type_body_armor|itp_covers_legs, 0, 38, weight(1.00)|body_armor(10)|leg_armor(5)|hit_points(605)|max_ammo(3), imodbits_none], 
["bakak", "Bakak", [("bakak",0)], itp_type_body_armor|itp_covers_legs, 0, 14, weight(0.90)|body_armor(6)|leg_armor(3)|hit_points(605)|max_ammo(1), imodbits_none], 
["surgeon_coat", "Surgeon Coat", [("surgeon_coat",0)], itp_type_body_armor|itp_covers_legs, 0, 15, weight(1.00)|body_armor(7)|leg_armor(3)|hit_points(605)|max_ammo(1), imodbits_none], 
["hl_kilt_1a", "White Shirt with Kilt", [("hl_kilt_1a",0)], itp_type_body_armor|itp_covers_legs, 0, 46, weight(1.50)|body_armor(14)|leg_armor(7)|hit_points(608)|max_ammo(2), imodbits_none], 
["hl_kilt_1b", "Black Shirt with Kilt", [("hl_kilt_1b",0)], itp_type_body_armor|itp_covers_legs, 0, 46, weight(1.50)|body_armor(14)|leg_armor(7)|hit_points(608)|max_ammo(2), imodbits_none], 
["steppe_armor", "Steppe Armor", [("lamellar_leather",0)], itp_type_body_armor|itp_covers_legs, 0, 81, weight(2.20)|body_armor(22)|leg_armor(10)|hit_points(611)|max_ammo(3), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_lamellar_leather",":agent_no",":troop_no")])]], 
["nomad_armor", "Nomad Armor", [("nomad_armor_new",0)], itp_type_body_armor|itp_covers_legs, 0, 64, weight(2.10)|body_armor(20)|leg_armor(9)|hit_points(611)|max_ammo(2), imodbits_none], 
["khergit_armor", "Khergit Armor", [("khergit_armor_new",0)], itp_type_body_armor|itp_covers_legs, 0, 64, weight(2.10)|body_armor(20)|leg_armor(9)|hit_points(611)|max_ammo(2), imodbits_none], 
["leather_vest", "Leather Vest", [("leather_vest_a",0)], itp_type_body_armor|itp_covers_legs, 0, 132, weight(3.00)|body_armor(30)|leg_armor(14)|hit_points(615)|max_ammo(3), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_leather_vest_a",":agent_no",":troop_no")])]], 
["nomad_vest", "Nomad Vest", [("nomad_vest_new",0)], itp_type_body_armor|itp_covers_legs, 0, 141, weight(3.20)|body_armor(32)|leg_armor(15)|hit_points(616)|max_ammo(3), imodbits_none], 
["fur_coat", "Fur Coat", [("fur_coat",0)], itp_type_body_armor|itp_covers_legs, 0, 118, weight(3.40)|body_armor(32)|leg_armor(15)|hit_points(617)|max_ammo(2), imodbits_none], 
["padded_cloth", "Padded Cloth", [("padded_cloth_a",0)], itp_type_body_armor|itp_covers_legs, 0, 83, weight(2.40)|body_armor(22)|leg_armor(11)|hit_points(612)|max_ammo(2), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_padded_cloth_a",":agent_no",":troop_no")])]], 
["aketon_green", "Aketon", [("padded_cloth_b",0)], itp_type_body_armor|itp_covers_legs, 0, 78, weight(2.30)|body_armor(21)|leg_armor(10)|hit_points(612)|max_ammo(2), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_padded_cloth_b",":agent_no",":troop_no")])]], 
["leather_jerkin", "Leather Jerkin", [("ragged_leather_jerkin",0)], itp_type_body_armor|itp_covers_legs, 0, 123, weight(2.80)|body_armor(28)|leg_armor(13)|hit_points(614)|max_ammo(3), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_leather_jerkin",":agent_no",":troop_no")])]], 
["padded_leather", "Padded Leather", [("leather_armor_b",0)], itp_type_body_armor|itp_covers_legs, 0, 89, weight(2.40)|body_armor(24)|leg_armor(11)|hit_points(612)|max_ammo(3), imodbits_none], 
["ragged_outfit", "Ragged Outfit", [("ragged_outfit_a_new",0)], itp_type_body_armor|itp_covers_legs, 0, 100, weight(2.90)|body_armor(27)|leg_armor(13)|hit_points(615)|max_ammo(2), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_ragged_outfit",":agent_no",":troop_no")])]], 
["tribal_warrior_outfit", "Tribal Warrior Outfit", [("tribal_warrior_outfit_a_new",0)], itp_type_body_armor|itp_covers_legs, 0, 142, weight(3.40)|body_armor(34)|leg_armor(16)|hit_points(617)|max_ammo(3), imodbits_none], 
["leather_armor", "Leather Armor", [("tattered_leather_armor_a",0)], itp_type_body_armor|itp_covers_legs, 0, 186, weight(4.20)|body_armor(42)|leg_armor(20)|hit_points(621)|max_ammo(3), imodbits_none], 
["drz_kaftan", "Kraftan", [("drz_kaftan",0)], itp_type_body_armor|itp_covers_legs, 0, 78, weight(2.20)|body_armor(21)|leg_armor(10)|hit_points(611)|max_ammo(2), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_drz_kaftan",":agent_no",":troop_no")])]], 
["padded_jack", "Padded Jack", [("gambeson",0)], itp_type_body_armor|itp_covers_legs, 0, 70, weight(2.00)|body_armor(19)|leg_armor(9)|hit_points(610)|max_ammo(2), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_gambeson",":agent_no",":troop_no")])]], 
["janichareteksiz", "Janichareteksiz", [("janichareteksiz",0)], itp_type_body_armor|itp_covers_legs, 0, 73, weight(2.10)|body_armor(20)|leg_armor(9)|hit_points(611)|max_ammo(2), imodbits_none], 
["hl_kilt_2a", "White Shirt with Vest and Kilt", [("hl_kilt_2a",0)], itp_type_body_armor|itp_covers_legs, 0, 64, weight(2.10)|body_armor(20)|leg_armor(9)|hit_points(611)|max_ammo(2), imodbits_none], 
["hl_kilt_2b", "Black Shirt with Vest and Kilt", [("hl_kilt_2b",0)], itp_type_body_armor|itp_covers_legs, 0, 64, weight(2.10)|body_armor(20)|leg_armor(9)|hit_points(611)|max_ammo(2), imodbits_none], 
["hl_kilt_3a", "Dark Leather Coat with Kilt", [("hl_kilt_3a",0)], itp_type_body_armor|itp_covers_legs, 0, 167, weight(4.50)|body_armor(45)|leg_armor(21)|hit_points(623)|max_ammo(3), imodbits_none], 
["hl_kilt_3b", "Bright Leather Coat with Kilt", [("hl_kilt_3b",0)], itp_type_body_armor|itp_covers_legs, 0, 167, weight(4.50)|body_armor(45)|leg_armor(21)|hit_points(623)|max_ammo(3), imodbits_none], 
["byrnie", "Byrnie", [("byrnie_a_new",0)], itp_type_body_armor|itp_covers_legs, 0, 313, weight(10.70)|body_armor(36)|leg_armor(17)|hit_points(654)|max_ammo(4), imodbits_none], 
["mail_shirt", "Mail Shirt", [("mail_shirt_a",0)], itp_type_body_armor|itp_covers_legs, 0, 488, weight(13.30)|body_armor(44)|leg_armor(21)|hit_points(667)|max_ammo(4), imodbits_none], 
["mail_hauberk", "Mail Hauberk", [("hauberk_a_new",0)], itp_type_body_armor|itp_covers_legs, 0, 480, weight(13.10)|body_armor(44)|leg_armor(20)|hit_points(666)|max_ammo(4), imodbits_none], 
["haubergeon", "Haubergeon", [("haubergeon_c",0)], itp_type_body_armor|itp_covers_legs, 0, 585, weight(15.90)|body_armor(53)|leg_armor(25)|hit_points(680)|max_ammo(4), imodbits_none], 
["lamellar_vest", "Lamellar Vest", [("lamellar_vest_a",0)], itp_type_body_armor|itp_covers_legs, 0, 289, weight(10.30)|body_armor(27)|leg_armor(13)|hit_points(652)|max_ammo(6), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_lamellar_vest_a",":agent_no",":troop_no")])]], 
["tasarim", "Tasarim", [("tasarim",0)], itp_type_body_armor|itp_covers_legs, 0, 495, weight(13.40)|body_armor(45)|leg_armor(21)|hit_points(667)|max_ammo(4), imodbits_none], 
["varangian_hauberk", "Varangian Hauberk", [("varangian_hauberk",0)], itp_type_body_armor|itp_covers_legs, 0, 518, weight(14.10)|body_armor(47)|leg_armor(22)|hit_points(671)|max_ammo(4), imodbits_none], 
["drz_mail_shirt", "Mail Armor", [("drz_mail_shirt",0)], itp_type_body_armor|itp_covers_legs, 0, 428, weight(11.70)|body_armor(39)|leg_armor(18)|hit_points(659)|max_ammo(4), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_drz_mail_shirt",":agent_no",":troop_no")])]], 
["kuyak_a", "Light Kuyak", [("kuyak_a",0)], itp_type_body_armor|itp_covers_legs, 0, 394, weight(18.50)|body_armor(37)|leg_armor(17)|hit_points(693)|max_ammo(5), imodbits_none], 
["kuyak_b", "Heavy Kuyak", [("kuyak_b",0)], itp_type_body_armor|itp_covers_legs, 0, 428, weight(20.00)|body_armor(40)|leg_armor(19)|hit_points(700)|max_ammo(5), imodbits_none], 
["rus_lamellar_a", "Rus Lamellar", [("rus_lamellar_a",0)], itp_type_body_armor|itp_covers_legs, 0, 459, weight(21.40)|body_armor(43)|leg_armor(20)|hit_points(707)|max_ammo(5), imodbits_none], 
["hl_kilt_4a", "Heavy Kilt with white Shirt", [("hl_kilt_4a",0)], itp_type_body_armor|itp_covers_legs, 0, 398, weight(13.70)|body_armor(46)|leg_armor(21)|hit_points(669)|max_ammo(4), imodbits_none], 
["hl_kilt_4b", "Heavy Kilt with black Shirt", [("hl_kilt_4b",0)], itp_type_body_armor|itp_covers_legs, 0, 398, weight(13.70)|body_armor(46)|leg_armor(21)|hit_points(669)|max_ammo(4), imodbits_none], 
["arabian_armor_b", "Arabian Guard Armor", [("arabian_armor_b",0)], itp_type_body_armor|itp_covers_legs, 0, 373, weight(17.50)|body_armor(35)|leg_armor(16)|hit_points(688)|max_ammo(5), imodbits_none], 
["mamluke_mail", "Mamluke Mail", [("sarranid_elite_cavalary",0)], itp_type_body_armor|itp_covers_legs, 0, 668, weight(18.20)|body_armor(61)|leg_armor(28)|hit_points(691)|max_ammo(4), imodbits_none], 
["goldsipahi", "Goldsipahi", [("goldsipahi",0)], itp_type_body_armor|itp_covers_legs, 0, 506, weight(19.60)|body_armor(39)|leg_armor(18)|hit_points(698)|max_ammo(5), imodbits_none], 
["ola_armor", "Ola Armor", [("ola",0)], itp_type_body_armor|itp_covers_legs, 0, 532, weight(20.50)|body_armor(41)|leg_armor(19)|hit_points(703)|max_ammo(5), imodbits_none], 
["bnw_armour", "Stripped Half Armour", [("bnw_armour_slashed",0)], itp_type_body_armor|itp_covers_legs, 0, 798, weight(21.20)|body_armor(57)|leg_armor(27)|hit_points(706)|max_ammo(6), imodbits_none], 
["bnw_armour_b", "Plain Half Armour", [("bnw_armour_stripes",0)], itp_type_body_armor|itp_covers_legs, 0, 798, weight(21.20)|body_armor(57)|leg_armor(27)|hit_points(706)|max_ammo(6), imodbits_none], 
["corrazina", "Corrazina", [("corrazina",0)], itp_type_body_armor|itp_covers_legs, 0, 779, weight(28.10)|body_armor(56)|leg_armor(26)|hit_points(741)|max_ammo(5), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_corrazina",":agent_no",":troop_no")])]], 
["churburg_13", "Churburg Cuirass", [("churburg_13",0)], itp_type_body_armor|itp_covers_legs, 0, 941, weight(25.30)|body_armor(67)|leg_armor(32)|hit_points(727)|max_ammo(6), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_churburg_13",":agent_no",":troop_no")])]], 
["gothic_armour", "Gothic Armour", [("gothic_armour",0)], itp_type_body_armor|itp_covers_legs, 0, 1045, weight(28.20)|body_armor(75)|leg_armor(35)|hit_points(741)|max_ammo(6), imodbits_none, [(ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_gothic_armour",":agent_no",":troop_no")])]], 
["maximilian_armour", "Maximilian Armour", [("maximilian_armour",0)], itp_type_body_armor|itp_covers_legs, 0, 1074, weight(28.90)|body_armor(77)|leg_armor(36)|hit_points(745)|max_ammo(6), imodbits_none], 
["milanese_armour", "Milanese Armour", [("milanese_armour",0)], itp_type_body_armor|itp_covers_legs, 0, 1102, weight(29.80)|body_armor(79)|leg_armor(37)|hit_points(749)|max_ammo(6), imodbits_none], 
["plate_armor_1", "Heavy Plate Armour", [("armor11",0)], itp_type_body_armor|itp_covers_legs, 0, 1093, weight(29.40)|body_armor(78)|leg_armor(37)|hit_points(747)|max_ammo(6), imodbits_none], 
["plate_armor_2", "Plate Cuirass", [("armor4",0)], itp_type_body_armor|itp_covers_legs, 0, 1074, weight(28.90)|body_armor(77)|leg_armor(36)|hit_points(745)|max_ammo(6), imodbits_none], 

#hand armor
["mail_gauntlets", "Mail Gauntlets", [("mail_gauntlets_L",0)], itp_type_hand_armor, 0, 38, weight(1.30)|body_armor(4)|hit_points(187)|max_ammo(5), imodbits_none], 
["wisby_gauntlets_black", "Black Splinted Gauntlets", [("wisby_gauntlets_black_L",0)], itp_type_hand_armor, 0, 86, weight(2.20)|body_armor(9)|hit_points(191)|max_ammo(6), imodbits_none], 
["wisby_gauntlets_red", "Red Splinted Gauntlets", [("wisby_gauntlets_red_L",0)], itp_type_hand_armor, 0, 86, weight(2.20)|body_armor(9)|hit_points(191)|max_ammo(6), imodbits_none], 
["hourglass_gauntlets", "Hourglass Gauntlets", [("hourglass_gauntlets_L",0)], itp_type_hand_armor, 0, 95, weight(2.60)|body_armor(10)|hit_points(193)|max_ammo(6), imodbits_none], 
["plate_mittens", "Plate Mittens", [("plate_mittens_L",0)], itp_type_hand_armor, 0, 114, weight(2.90)|body_armor(12)|hit_points(195)|max_ammo(6), imodbits_none], 
["bnw_gauntlets", "Stripped Gauntlets", [("bnw_gauntlet_L",0)], itp_type_hand_armor, 0, 124, weight(3.20)|body_armor(13)|hit_points(196)|max_ammo(6), imodbits_none], 

#foot armor
["wrapping_boots", "Wrapping Boots", [("wrapping_boots_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 45, weight(1.00)|leg_armor(18)|hit_points(365)|max_ammo(2), imodbits_none], 
["woolen_hose", "Woolen Hose", [("woolen_hose_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 15, weight(0.80)|leg_armor(10)|hit_points(364)|max_ammo(1), imodbits_none], 
["blue_hose", "Blue Hose", [("blue_hose_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 15, weight(0.80)|leg_armor(10)|hit_points(364)|max_ammo(1), imodbits_none], 
["ankle_boots", "Ankle Boots", [("ankle_boots_a_new",0)], itp_type_foot_armor|itp_attach_armature, 0, 45, weight(1.00)|leg_armor(18)|hit_points(365)|max_ammo(2), imodbits_none], 
["sarranid_boots_a", "Desert Shoes", [("sarranid_shoes",0)], itp_type_foot_armor|itp_attach_armature, 0, 35, weight(0.80)|leg_armor(14)|hit_points(364)|max_ammo(2), imodbits_none], 
["rus_shoes", "Rus Shoes", [("rus_shoes",0)], itp_type_foot_armor|itp_attach_armature, 0, 45, weight(1.00)|leg_armor(18)|hit_points(365)|max_ammo(2), imodbits_none], 
["bear_paw_shoes", "Bear Paw Shoes", [("bear_paw_shoes",0)], itp_type_foot_armor|itp_attach_armature, 0, 15, weight(0.80)|leg_armor(10)|hit_points(364)|max_ammo(1), imodbits_none], 
["hl_boots_1a", "Brown Highland Shoes", [("hl_boots_1a",0)], itp_type_foot_armor|itp_attach_armature, 0, 58, weight(1.30)|leg_armor(23)|hit_points(367)|max_ammo(2), imodbits_none], 
["hl_boots_1b", "Blue Highland Shoes", [("hl_boots_1b",0)], itp_type_foot_armor|itp_attach_armature, 0, 58, weight(1.30)|leg_armor(23)|hit_points(367)|max_ammo(2), imodbits_none], 
["hide_boots", "Hide Boots", [("hide_boots_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 48, weight(1.10)|leg_armor(19)|hit_points(366)|max_ammo(2), imodbits_none], 
["nomad_boots", "Nomad Boots", [("nomad_boots_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 45, weight(1.00)|leg_armor(18)|hit_points(365)|max_ammo(2), imodbits_none], 
["leather_boots", "Leather Boots", [("leather_boots_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 69, weight(1.20)|leg_armor(23)|hit_points(366)|max_ammo(3), imodbits_none], 
["khergit_leather_boots", "Black Leather Boots", [("khergit_leather_boots",0)], itp_type_foot_armor|itp_attach_armature, 0, 57, weight(1.00)|leg_armor(19)|hit_points(365)|max_ammo(3), imodbits_none], 
["sarranid_boots_b", "Desert Leather Boots", [("sarranid_boots",0)], itp_type_foot_armor|itp_attach_armature, 0, 57, weight(1.00)|leg_armor(19)|hit_points(365)|max_ammo(3), imodbits_none], 
["rus_cav_boots", "Rus Boots", [("rus_cav_boots",0)], itp_type_foot_armor|itp_attach_armature, 0, 69, weight(1.20)|leg_armor(23)|hit_points(366)|max_ammo(3), imodbits_none], 
["leather_boots_1", "Red Leather Boots", [("boot13",0)], itp_type_foot_armor|itp_attach_armature, 0, 69, weight(1.20)|leg_armor(23)|hit_points(366)|max_ammo(3), imodbits_none], 
["leather_boots_4", "Laced Boots", [("boot4",0)], itp_type_foot_armor|itp_attach_armature, 0, 69, weight(1.20)|leg_armor(23)|hit_points(366)|max_ammo(3), imodbits_none], 
["leather_boots_6", "Laced Leather Boots", [("boot6",0)], itp_type_foot_armor|itp_attach_armature, 0, 69, weight(1.20)|leg_armor(23)|hit_points(366)|max_ammo(3), imodbits_none], 
["leather_boots_7", "Black Boots", [("boot7",0)], itp_type_foot_armor|itp_attach_armature, 0, 63, weight(1.10)|leg_armor(21)|hit_points(366)|max_ammo(3), imodbits_none], 
["leather_boots_8", "Jackboots", [("boot8",0)], itp_type_foot_armor|itp_attach_armature, 0, 69, weight(1.20)|leg_armor(23)|hit_points(366)|max_ammo(3), imodbits_none], 
["hl_boots_2a", "Brown Highland Boots", [("hl_boots_2a",0)], itp_type_foot_armor|itp_attach_armature, 0, 48, weight(1.10)|leg_armor(19)|hit_points(366)|max_ammo(2), imodbits_none], 
["hl_boots_2b", "Blue Highland Boots", [("hl_boots_2b",0)], itp_type_foot_armor|itp_attach_armature, 0, 48, weight(1.10)|leg_armor(19)|hit_points(366)|max_ammo(2), imodbits_none], 
["splinted_greaves", "Splinted Greaves", [("splinted_greaves_a",0)], itp_type_foot_armor|itp_attach_armature, 0, 162, weight(3.30)|leg_armor(17)|hit_points(377)|max_ammo(6), imodbits_none], 
["rus_splint_greaves", "Splinted Greaves", [("rus_splint_greaves",0)], itp_type_foot_armor|itp_attach_armature, 0, 228, weight(4.70)|leg_armor(24)|hit_points(384)|max_ammo(6), imodbits_none], 
["splinted_greaves_spurs", "Splinted Greaves with Spurs", [("splinted_greaves_spurs",0)], itp_type_foot_armor|itp_attach_armature, 0, 228, weight(4.80)|leg_armor(24)|hit_points(384)|max_ammo(6), imodbits_none], 
["leather_shoes_15", "Shoes with Plate Greaves", [("boot15",0)], itp_type_foot_armor|itp_attach_armature, 0, 257, weight(5.30)|leg_armor(27)|hit_points(387)|max_ammo(6), imodbits_none], 
["leather_shoes_9", "Shoes with Greaves", [("boot9",0)], itp_type_foot_armor|itp_attach_armature, 0, 228, weight(4.80)|leg_armor(24)|hit_points(384)|max_ammo(6), imodbits_none], 
["shynbaulds", "Shynbaulds", [("shynbaulds",0)], itp_type_foot_armor|itp_attach_armature, 0, 285, weight(8.10)|leg_armor(30)|hit_points(401)|max_ammo(5), imodbits_none], 
["steel_greaves", "Cased Greaves", [("steel_greaves",0)], itp_type_foot_armor|itp_attach_armature, 0, 380, weight(7.90)|leg_armor(40)|hit_points(400)|max_ammo(6), imodbits_none], 

#one handed
["club", "Club", [("club_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_attack|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_mace_left_hip, 10, weight(0.63)|head_armor(48)|body_armor(75)|leg_armor(0)|hit_points(1232)|spd_rtng(90)|weapon_length(55)|shield_height(35)|max_ammo(8)|swing_damage(19,blunt)|thrust_damage(0,blunt), imodbits_none], 
["hammer", "Hammer", [("iron_hammer_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_mace_left_hip, 36, weight(1.05)|head_armor(3)|body_armor(67)|leg_armor(0)|hit_points(1733)|spd_rtng(86)|weapon_length(55)|shield_height(26)|max_ammo(50)|swing_damage(54,blunt)|thrust_damage(0,blunt), imodbits_none], 
["hammer_alt_mode", "Hammer alt mode", [("iron_hammer_new_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_scimitar|itcf_carry_mace_left_hip, 0, weight(1.05)|head_armor(2)|body_armor(14)|leg_armor(0)|hit_points(1733)|spd_rtng(86)|weapon_length(55)|shield_height(26)|max_ammo(50)|swing_damage(69,pierce)|thrust_damage(0,blunt), imodbits_none], 
["flanged_mace", "Flanged Mace", [("flanged_mace",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_mace_left_hip, 42, weight(1.05)|head_armor(32)|body_armor(28)|leg_armor(0)|hit_points(2213)|spd_rtng(86)|weapon_length(52)|shield_height(26)|max_ammo(50)|swing_damage(45,blunt)|thrust_damage(0,blunt), imodbits_none], 
["faradon_iberian_mace", "Iberian Mace", [("Faradon_IberianMace",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_mace_left_hip, 36, weight(1.02)|head_armor(24)|body_armor(42)|leg_armor(0)|hit_points(1731)|spd_rtng(86)|weapon_length(53)|shield_height(32)|max_ammo(50)|swing_damage(43,blunt)|thrust_damage(0,blunt), imodbits_none], 
["small_hammer", "War Hammer", [("small_hammar",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_longsword|itcf_carry_mace_left_hip, 36, weight(1.11)|head_armor(8)|body_armor(92)|leg_armor(4)|abundance(22)|hit_points(1736)|spd_rtng(85)|weapon_length(60)|shield_height(33)|max_ammo(50)|swing_damage(48,blunt)|thrust_damage(37,pierce), imodbits_none], 
["small_hammer_alt_mode", "War Hammer alt mode", [("small_hammar_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_longsword|itcf_carry_mace_left_hip, 0, weight(1.11)|head_armor(7)|body_armor(41)|leg_armor(4)|abundance(22)|hit_points(1736)|spd_rtng(85)|weapon_length(60)|shield_height(33)|max_ammo(50)|swing_damage(54,pierce)|thrust_damage(37,pierce), imodbits_none], 
["short_morningstar", "Short Morningstar", [("mace_short_morningstar",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unbalanced, itc_longsword|itcf_carry_mace_left_hip, 44, weight(1.24)|head_armor(2)|body_armor(8)|leg_armor(1)|abundance(18)|hit_points(2222)|spd_rtng(85)|weapon_length(41)|shield_height(21)|max_ammo(50)|swing_damage(86,pierce)|thrust_damage(50,pierce), imodbits_none], 
["short_iron_hammer", "Short Iron Hammer", [("iron_short_hammer",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unbalanced, itc_scimitar|itcf_carry_mace_left_hip, 42, weight(1.04)|head_armor(1)|body_armor(10)|leg_armor(0)|hit_points(2212)|spd_rtng(87)|weapon_length(37)|shield_height(18)|max_ammo(50)|swing_damage(80,pierce)|thrust_damage(0,blunt), imodbits_none], 
["short_iron_hammer_alt_mode", "Short Iron Hammer alt mode", [("iron_short_hammer_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_mace_left_hip, 0, weight(1.04)|head_armor(6)|body_armor(35)|leg_armor(0)|hit_points(2212)|spd_rtng(87)|weapon_length(37)|shield_height(18)|max_ammo(50)|swing_damage(54,blunt)|thrust_damage(0,blunt), imodbits_none], 
["steel_pick", "Steel Pick", [("steel_pick_new_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unbalanced, itc_scimitar|itcf_carry_mace_left_hip, 42, weight(1.08)|head_armor(2)|body_armor(5)|leg_armor(0)|hit_points(2214)|spd_rtng(86)|weapon_length(48)|shield_height(27)|max_ammo(50)|swing_damage(81,pierce)|thrust_damage(0,blunt), imodbits_none], 
["steel_pick_alt_mode", "Steel Pick alt mode", [("steel_pick_new_b_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_mace_left_hip, 0, weight(1.08)|head_armor(9)|body_armor(96)|leg_armor(0)|hit_points(2214)|spd_rtng(86)|weapon_length(48)|shield_height(27)|max_ammo(50)|swing_damage(46,blunt)|thrust_damage(0,blunt), imodbits_none], 
["dirk_dagger", "Dirk Dagger", [("dirk",0),("dirk_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_unbalanced, itc_dagger|itcf_carry_dagger_front_left|itcf_show_holster_when_drawn, 47, weight(0.45)|head_armor(18)|body_armor(59)|leg_armor(3)|abundance(15)|hit_points(2183)|spd_rtng(94)|weapon_length(46)|shield_height(12)|max_ammo(50)|swing_damage(22,cut)|thrust_damage(20,pierce), imodbits_none], 
["bollock_dagger", "Bollock Dagger", [("bollock_dagger",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_unbalanced, itc_dagger|itcf_carry_dagger_front_left, 44, weight(0.32)|head_armor(10)|body_armor(58)|leg_armor(1)|abundance(13)|hit_points(2176)|spd_rtng(95)|weapon_length(34)|shield_height(9)|max_ammo(50)|swing_damage(17,cut)|thrust_damage(17,pierce), imodbits_none], 
["rondel_dagger", "Rondel Dagger", [("rondel_dagger",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_unbalanced, itc_dagger|itcf_carry_dagger_front_left, 46, weight(0.38)|head_armor(12)|body_armor(60)|leg_armor(1)|abundance(12)|hit_points(2179)|spd_rtng(95)|weapon_length(36)|shield_height(8)|max_ammo(50)|swing_damage(20,cut)|thrust_damage(20,pierce), imodbits_none], 
["hatchet", "Hatchet", [("hatchet_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_scimitar|itcf_carry_mace_left_hip, 39, weight(0.71)|head_armor(16)|body_armor(52)|leg_armor(0)|hit_points(1716)|spd_rtng(89)|weapon_length(49)|shield_height(36)|max_ammo(50)|swing_damage(33,cut)|thrust_damage(0,blunt), imodbits_none], 
["one_handed_war_axe_a", "Short Axe", [("one_handed_war_axe_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_scimitar|itcf_carry_mace_left_hip, 40, weight(0.87)|head_armor(21)|body_armor(48)|leg_armor(0)|hit_points(1724)|spd_rtng(87)|weapon_length(54)|shield_height(39)|max_ammo(50)|swing_damage(38,cut)|thrust_damage(0,blunt), imodbits_none], 
["one_handed_war_axe_b", "Heavy Short Axe", [("one_handed_war_axe_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_scimitar|itcf_carry_mace_left_hip, 40, weight(0.91)|head_armor(23)|body_armor(47)|leg_armor(0)|hit_points(1726)|spd_rtng(86)|weapon_length(54)|shield_height(40)|max_ammo(50)|swing_damage(38,cut)|thrust_damage(0,blunt), imodbits_none], 
["italian_sword", "Italian Sword", [("italian_sword",0),("italian_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 88, weight(1.22)|head_armor(9)|body_armor(57)|leg_armor(2)|abundance(18)|hit_points(2221)|spd_rtng(86)|weapon_length(89)|shield_height(16)|max_ammo(50)|swing_damage(56,cut)|thrust_damage(47,pierce), imodbits_none], 
["italian_sword_alt_mode", "Italian Sword alt mode", [("italian_sword",0),("italian_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.22)|head_armor(9)|body_armor(57)|leg_armor(2)|abundance(18)|hit_points(2221)|spd_rtng(94)|weapon_length(89)|shield_height(16)|max_ammo(50)|swing_damage(67,cut)|thrust_damage(56,pierce), imodbits_none], 
["scottish_sword", "Scottish Sword", [("scottish_sword",0),("scottish_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 86, weight(1.13)|head_armor(7)|body_armor(31)|leg_armor(1)|abundance(18)|hit_points(2217)|spd_rtng(87)|weapon_length(86)|shield_height(15)|max_ammo(50)|swing_damage(59,cut)|thrust_damage(48,pierce), imodbits_none], 
["scottish_sword_alt_mode", "Scottish Sword alt mode", [("scottish_sword",0),("scottish_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.13)|head_armor(7)|body_armor(31)|leg_armor(1)|abundance(18)|hit_points(2217)|spd_rtng(95)|weapon_length(86)|shield_height(15)|max_ammo(50)|swing_damage(70,cut)|thrust_damage(57,pierce), imodbits_none], 
["irish_sword", "Irish Sword", [("irish_sword",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip, 85, weight(1.09)|head_armor(10)|body_armor(56)|leg_armor(1)|abundance(15)|hit_points(2215)|spd_rtng(87)|weapon_length(87)|shield_height(20)|max_ammo(50)|swing_damage(51,cut)|thrust_damage(47,pierce), imodbits_none], 
["irish_sword_alt_mode", "Irish Sword alt mode", [("irish_sword",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(1.09)|head_armor(10)|body_armor(56)|leg_armor(1)|abundance(15)|hit_points(2215)|spd_rtng(95)|weapon_length(87)|shield_height(20)|max_ammo(50)|swing_damage(60,cut)|thrust_damage(57,pierce), imodbits_none], 
["side_sword", "Side Sword", [("side_sword",0),("side_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 88, weight(1.18)|head_armor(13)|body_armor(57)|leg_armor(2)|abundance(13)|hit_points(2219)|spd_rtng(87)|weapon_length(95)|shield_height(11)|max_ammo(50)|swing_damage(53,cut)|thrust_damage(48,pierce), imodbits_none], 
["side_sword_alt_mode", "Side Sword alt mode", [("side_sword",0),("side_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.18)|head_armor(13)|body_armor(57)|leg_armor(2)|abundance(13)|hit_points(2219)|spd_rtng(95)|weapon_length(95)|shield_height(11)|max_ammo(50)|swing_damage(63,cut)|thrust_damage(57,pierce), imodbits_none], 
["rapier", "Rapier", [("rapier",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip, 90, weight(1.29)|head_armor(6)|body_armor(52)|leg_armor(2)|abundance(10)|hit_points(2225)|spd_rtng(85)|weapon_length(109)|shield_height(15)|max_ammo(50)|swing_damage(61,cut)|thrust_damage(52,pierce), imodbits_none], 
["rapier_alt_mode", "Rapier alt mode", [("rapier",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(1.29)|head_armor(6)|body_armor(52)|leg_armor(2)|abundance(10)|hit_points(2225)|spd_rtng(94)|weapon_length(109)|shield_height(15)|max_ammo(50)|swing_damage(75,cut)|thrust_damage(63,pierce), imodbits_none], 
["espada_eslavona_a", "Espada Eslavona", [("espada_eslavona_a",0),("espada_eslavona_a_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 83, weight(1.04)|head_armor(10)|body_armor(58)|leg_armor(1)|abundance(16)|hit_points(2212)|spd_rtng(87)|weapon_length(90)|shield_height(22)|max_ammo(50)|swing_damage(48,cut)|thrust_damage(45,pierce), imodbits_none], 
["espada_eslavona_a_alt_mode", "Espada Eslavona alt mode", [("espada_eslavona_a",0),("espada_eslavona_a_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.04)|head_armor(10)|body_armor(58)|leg_armor(1)|abundance(16)|hit_points(2212)|spd_rtng(95)|weapon_length(90)|shield_height(22)|max_ammo(50)|swing_damage(57,cut)|thrust_damage(54,pierce), imodbits_none], 
["espada_eslavona_b", "Long Espada Eslavona", [("espada_eslavona_b",0),("espada_eslavona_b_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 87, weight(1.19)|head_armor(20)|body_armor(67)|leg_armor(3)|abundance(18)|hit_points(2220)|spd_rtng(85)|weapon_length(103)|shield_height(24)|max_ammo(50)|swing_damage(47,cut)|thrust_damage(42,pierce), imodbits_none], 
["espada_eslavona_b_alt_mode", "Long Espada Eslavona alt mode", [("espada_eslavona_b",0),("espada_eslavona_b_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.19)|head_armor(20)|body_armor(67)|leg_armor(3)|abundance(18)|hit_points(2220)|spd_rtng(94)|weapon_length(103)|shield_height(24)|max_ammo(50)|swing_damage(58,cut)|thrust_damage(52,pierce), imodbits_none], 
["crusader_sword", "Crusader Sword", [("crusader_sword",0),("crusader_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 92, weight(1.38)|head_armor(6)|body_armor(56)|leg_armor(1)|abundance(18)|hit_points(2229)|spd_rtng(84)|weapon_length(94)|shield_height(17)|max_ammo(50)|swing_damage(64,cut)|thrust_damage(55,pierce), imodbits_none], 
["crusader_sword_alt_mode", "Crusader Sword alt mode", [("crusader_sword",0),("crusader_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.38)|head_armor(6)|body_armor(56)|leg_armor(1)|abundance(18)|hit_points(2229)|spd_rtng(93)|weapon_length(94)|shield_height(17)|max_ammo(50)|swing_damage(78,cut)|thrust_damage(67,pierce), imodbits_none], 
["longbowman_sword", "Bowman Sword", [("longbowman_sword",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip, 83, weight(1.02)|head_armor(13)|body_armor(54)|leg_armor(2)|abundance(16)|hit_points(2211)|spd_rtng(88)|weapon_length(79)|shield_height(18)|max_ammo(50)|swing_damage(47,cut)|thrust_damage(41,pierce), imodbits_none], 
["longbowman_sword_alt_mode", "Bowman Sword alt mode", [("longbowman_sword",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(1.02)|head_armor(13)|body_armor(54)|leg_armor(2)|abundance(16)|hit_points(2211)|spd_rtng(96)|weapon_length(79)|shield_height(18)|max_ammo(50)|swing_damage(56,cut)|thrust_damage(49,pierce), imodbits_none], 
["milanese_sword", "Milanese Sword", [("milanese_sword",0),("milanese_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip, 82, weight(0.94)|head_armor(16)|body_armor(58)|leg_armor(3)|abundance(15)|hit_points(2207)|spd_rtng(89)|weapon_length(74)|shield_height(12)|max_ammo(50)|swing_damage(43,cut)|thrust_damage(37,pierce), imodbits_none], 
["milanese_sword_alt_mode", "Milanese Sword alt mode", [("milanese_sword",0),("milanese_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(0.94)|head_armor(16)|body_armor(58)|leg_armor(3)|abundance(15)|hit_points(2207)|spd_rtng(97)|weapon_length(74)|shield_height(12)|max_ammo(50)|swing_damage(51,cut)|thrust_damage(44,pierce), imodbits_none], 
["grosse_messer", "Grossmesser", [("grosse_messer",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip, 81, weight(0.92)|head_armor(10)|body_armor(53)|leg_armor(2)|abundance(24)|hit_points(2206)|spd_rtng(89)|weapon_length(68)|shield_height(18)|max_ammo(50)|swing_damage(45,cut)|thrust_damage(36,pierce), imodbits_none], 
["grosse_messer_alt_mode", "Grossmesser alt mode", [("grosse_messer",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(0.92)|head_armor(10)|body_armor(53)|leg_armor(2)|abundance(24)|hit_points(2206)|spd_rtng(96)|weapon_length(68)|shield_height(18)|max_ammo(50)|swing_damage(52,cut)|thrust_damage(42,pierce), imodbits_none], 
["italian_falchion", "Italian Falchion", [("italian_falchion",0),("italian_falchion_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 92, weight(1.39)|head_armor(19)|body_armor(54)|leg_armor(3)|abundance(20)|hit_points(2230)|spd_rtng(84)|weapon_length(75)|shield_height(19)|max_ammo(50)|swing_damage(56,cut)|thrust_damage(47,pierce), imodbits_none], 
["italian_falchion_alt_mode", "Italian Falchion alt mode", [("italian_falchion",0),("italian_falchion_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.39)|head_armor(19)|body_armor(54)|leg_armor(3)|abundance(20)|hit_points(2230)|spd_rtng(93)|weapon_length(75)|shield_height(19)|max_ammo(50)|swing_damage(68,cut)|thrust_damage(58,pierce), imodbits_none], 
["scimitar", "Scimitar", [("scimitar_a",0),("scab_scimeter_a",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 78, weight(0.84)|head_armor(14)|body_armor(49)|leg_armor(3)|abundance(14)|hit_points(2202)|spd_rtng(88)|weapon_length(83)|shield_height(30)|max_ammo(50)|swing_damage(39,cut)|thrust_damage(33,cut), imodbits_none], 
["scimitar_alt_mode", "Scimitar alt mode", [("scimitar_a",0),("scab_scimeter_a",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(0.84)|head_armor(14)|body_armor(49)|leg_armor(3)|abundance(14)|hit_points(2202)|spd_rtng(96)|weapon_length(83)|shield_height(30)|max_ammo(50)|swing_damage(46,cut)|thrust_damage(39,cut), imodbits_none], 
["scimitar_b", "Shamshir", [("scimitar_b",0),("scab_scimeter_b",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 78, weight(0.85)|head_armor(14)|body_armor(50)|leg_armor(3)|abundance(14)|hit_points(2203)|spd_rtng(88)|weapon_length(86)|shield_height(28)|max_ammo(50)|swing_damage(39,cut)|thrust_damage(33,cut), imodbits_none], 
["scimitar_b_alt_mode", "Shamshir alt mode", [("scimitar_b",0),("scab_scimeter_b",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(0.85)|head_armor(14)|body_armor(50)|leg_armor(3)|abundance(14)|hit_points(2203)|spd_rtng(96)|weapon_length(86)|shield_height(28)|max_ammo(50)|swing_damage(47,cut)|thrust_damage(40,cut), imodbits_none], 
["sword_medieval_e", "Saber", [("sword_medieval_e",0),("sword_medieval_e_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_longsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 75, weight(0.69)|head_armor(13)|body_armor(64)|leg_armor(2)|abundance(15)|hit_points(2195)|spd_rtng(91)|weapon_length(87)|shield_height(17)|max_ammo(50)|swing_damage(33,cut)|thrust_damage(30,pierce), imodbits_none], 
["sword_medieval_e_alt_mode", "Saber alt mode", [("sword_medieval_e",0),("sword_medieval_e_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(0.69)|head_armor(13)|body_armor(64)|leg_armor(2)|abundance(15)|hit_points(2195)|spd_rtng(98)|weapon_length(87)|shield_height(17)|max_ammo(50)|swing_damage(38,cut)|thrust_damage(35,pierce), imodbits_none], 

#hand and a half
["cudgel", "Cudgel", [("cudgel_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_attack|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_morningstar, 12, weight(1.51)|head_armor(102)|body_armor(73)|leg_armor(0)|hit_points(1276)|spd_rtng(93)|weapon_length(62)|shield_height(43)|max_ammo(8)|swing_damage(46,blunt)|thrust_damage(0,blunt), imodbits_none], 
["mace_1", "Spiked Club", [("spiked_club_swup",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_thrust_polearm, 14, weight(1.43)|head_armor(5)|body_armor(19)|leg_armor(1)|abundance(19)|hit_points(1752)|spd_rtng(94)|weapon_length(67)|shield_height(41)|max_ammo(50)|swing_damage(98,pierce)|thrust_damage(71,pierce), imodbits_none], 
["mace_4", "Heavy Flanged Mace", [("mace_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_morningstar|itcf_carry_mace_left_hip, 42, weight(1.38)|head_armor(8)|body_armor(86)|leg_armor(0)|hit_points(1749)|spd_rtng(94)|weapon_length(65)|shield_height(46)|max_ammo(50)|swing_damage(73,blunt)|thrust_damage(0,blunt), imodbits_none], 
["sarranid_mace_1", "Ribbed Mace", [("mace_small_d",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_unbalanced|itp_can_knock_down, itc_morningstar|itcf_carry_mace_left_hip, 56, weight(1.98)|head_armor(64)|body_armor(68)|leg_armor(0)|hit_points(2259)|spd_rtng(89)|weapon_length(73)|shield_height(42)|max_ammo(50)|swing_damage(75,blunt)|thrust_damage(0,blunt), imodbits_none], 
["faradon_warhammer", "Warhammer", [("Faradon_warhammer",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_morningstar|itcf_thrust_polearm|itcf_carry_mace_left_hip, 43, weight(1.52)|head_armor(16)|body_armor(65)|leg_armor(9)|abundance(13)|hit_points(1756)|spd_rtng(93)|weapon_length(76)|shield_height(48)|max_ammo(50)|swing_damage(75,blunt)|thrust_damage(59,pierce), imodbits_none], 
["faradon_warhammer_alt_mode", "Warhammer alt mode", [("Faradon_warhammer_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_thrust_polearm|itcf_carry_mace_left_hip, 0, weight(1.52)|head_armor(11)|body_armor(17)|leg_armor(9)|abundance(13)|hit_points(1756)|spd_rtng(93)|weapon_length(76)|shield_height(48)|max_ammo(50)|swing_damage(93,pierce)|thrust_damage(59,pierce), imodbits_none], 
["military_hammer", "Military Hammer", [("military_hammer",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_morningstar|itcf_carry_mace_left_hip, 41, weight(1.33)|head_armor(28)|body_armor(76)|leg_armor(0)|hit_points(1747)|spd_rtng(93)|weapon_length(70)|shield_height(55)|max_ammo(50)|swing_damage(60,blunt)|thrust_damage(0,blunt), imodbits_none], 
["military_hammer_alt_mode", "Military Hammer alt mode", [("military_hammer_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_mace_left_hip, 0, weight(1.33)|head_armor(6)|body_armor(10)|leg_armor(0)|hit_points(1747)|spd_rtng(93)|weapon_length(70)|shield_height(55)|max_ammo(50)|swing_damage(94,pierce)|thrust_damage(0,blunt), imodbits_none], 
["fighting_pick", "Fighting Pick", [("fighting_pick_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_mace_left_hip, 39, weight(1.07)|head_armor(2)|body_armor(8)|leg_armor(0)|hit_points(1734)|spd_rtng(96)|weapon_length(70)|shield_height(45)|max_ammo(50)|swing_damage(95,pierce)|thrust_damage(0,blunt), imodbits_none], 
["fighting_pick_alt_mode", "Fighting Pick alt mode", [("fighting_pick_new_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_morningstar|itcf_carry_mace_left_hip, 0, weight(1.07)|head_armor(4)|body_armor(71)|leg_armor(0)|hit_points(1734)|spd_rtng(96)|weapon_length(70)|shield_height(45)|max_ammo(50)|swing_damage(66,blunt)|thrust_damage(0,blunt), imodbits_none], 
["one_handed_battle_axe_a", "Fighting Axe", [("one_handed_battle_axe_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_carry_mace_left_hip, 48, weight(1.38)|head_armor(11)|body_armor(49)|leg_armor(0)|hit_points(1749)|spd_rtng(93)|weapon_length(73)|shield_height(50)|max_ammo(50)|swing_damage(74,cut)|thrust_damage(0,blunt), imodbits_none], 
["one_handed_battle_axe_b", "Battle Axe", [("one_handed_battle_axe_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_thrust_polearm|itcf_carry_mace_left_hip, 49, weight(1.43)|head_armor(12)|body_armor(48)|leg_armor(2)|abundance(11)|hit_points(1752)|spd_rtng(93)|weapon_length(76)|shield_height(51)|max_ammo(50)|swing_damage(76,cut)|thrust_damage(68,pierce), imodbits_none], 
["one_handed_battle_axe_c", "Spiked Battle Axe", [("one_handed_battle_axe_c",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_thrust_polearm|itcf_carry_mace_left_hip, 50, weight(1.57)|head_armor(13)|body_armor(45)|leg_armor(3)|abundance(13)|hit_points(1759)|spd_rtng(92)|weapon_length(73)|shield_height(53)|max_ammo(50)|swing_damage(82,cut)|thrust_damage(68,pierce), imodbits_none], 
["one_handed_battle_axe_c_alt_mode", "Spiked Battle Axe alt mode", [("one_handed_battle_axe_c_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_wooden_parry|itp_unbalanced, itc_morningstar|itcf_thrust_polearm|itcf_carry_mace_left_hip, 0, weight(1.57)|head_armor(2)|body_armor(6)|leg_armor(3)|abundance(13)|hit_points(1759)|spd_rtng(92)|weapon_length(73)|shield_height(53)|max_ammo(50)|swing_damage(133,pierce)|thrust_damage(68,pierce), imodbits_none], 
["grosse_messer_b", "Langmesser", [("grosse_messer_b",0),("grosse_messer_b_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 72, weight(1.74)|head_armor(14)|body_armor(54)|leg_armor(2)|abundance(13)|hit_points(2247)|spd_rtng(94)|weapon_length(87)|shield_height(19)|max_ammo(50)|swing_damage(91,cut)|thrust_damage(83,pierce), imodbits_none], 
["grosse_messer_b_alt_mode", "Langmesser alt mode", [("grosse_messer_b",0),("grosse_messer_b_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.74)|head_armor(14)|body_armor(54)|leg_armor(2)|abundance(13)|hit_points(2247)|spd_rtng(90)|weapon_length(87)|shield_height(19)|max_ammo(50)|swing_damage(83,cut)|thrust_damage(76,pierce), imodbits_none], 
["german_bastard_sword", "German Bastard Sword", [("german_bastard_sword",0),("german_bastard_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 102, weight(1.47)|head_armor(15)|body_armor(58)|leg_armor(2)|abundance(26)|hit_points(2234)|spd_rtng(96)|weapon_length(100)|shield_height(20)|max_ammo(50)|swing_damage(79,cut)|thrust_damage(67,pierce), imodbits_none], 
["german_bastard_sword_alt_mode", "German Bastard Sword alt mode", [("german_bastard_sword",0),("german_bastard_sword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.47)|head_armor(15)|body_armor(58)|leg_armor(2)|abundance(26)|hit_points(2234)|spd_rtng(92)|weapon_length(100)|shield_height(20)|max_ammo(50)|swing_damage(72,cut)|thrust_damage(61,pierce), imodbits_none], 
["english_longsword", "English Longsword", [("english_longsword",0),("english_longsword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 96, weight(1.27)|head_armor(15)|body_armor(56)|leg_armor(2)|abundance(28)|hit_points(2224)|spd_rtng(97)|weapon_length(93)|shield_height(21)|max_ammo(50)|swing_damage(70,cut)|thrust_damage(58,pierce), imodbits_none], 
["english_longsword_alt_mode", "English Longsword alt mode", [("english_longsword",0),("english_longsword_scabbard",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.27)|head_armor(15)|body_armor(56)|leg_armor(2)|abundance(28)|hit_points(2224)|spd_rtng(94)|weapon_length(93)|shield_height(21)|max_ammo(50)|swing_damage(65,cut)|thrust_damage(55,pierce), imodbits_none], 
["longsword_b", "Longsword", [("longsword_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_bastardsword|itcf_carry_sword_left_hip, 103, weight(1.54)|head_armor(14)|body_armor(57)|leg_armor(2)|abundance(21)|hit_points(2237)|spd_rtng(95)|weapon_length(99)|shield_height(17)|max_ammo(50)|swing_damage(82,cut)|thrust_damage(71,pierce), imodbits_none], 
["longsword_b_alt_mode", "Longsword alt mode", [("longsword_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(1.54)|head_armor(14)|body_armor(57)|leg_armor(2)|abundance(21)|hit_points(2237)|spd_rtng(92)|weapon_length(99)|shield_height(17)|max_ammo(50)|swing_damage(77,cut)|thrust_damage(66,pierce), imodbits_none], 
["sword_repent", "Repent Sword", [("sword_repent",0),("sword_repent_scab",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_bastardsword|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 99, weight(1.35)|head_armor(14)|body_armor(61)|leg_armor(2)|abundance(15)|hit_points(2228)|spd_rtng(97)|weapon_length(105)|shield_height(19)|max_ammo(50)|swing_damage(74,cut)|thrust_damage(67,pierce), imodbits_none], 
["sword_repent_alt_mode", "Repent Sword alt mode", [("sword_repent",0),("sword_repent_scab",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip|itcf_show_holster_when_drawn, 0, weight(1.35)|head_armor(14)|body_armor(61)|leg_armor(2)|abundance(15)|hit_points(2228)|spd_rtng(93)|weapon_length(105)|shield_height(19)|max_ammo(50)|swing_damage(68,cut)|thrust_damage(62,pierce), imodbits_none], 
["estoc", "Estoc", [("estoc",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_next_item_as_melee|itp_unbalanced, itc_bastardsword|itcf_carry_sword_left_hip, 91, weight(1.05)|head_armor(22)|body_armor(68)|leg_armor(2)|abundance(14)|hit_points(2213)|spd_rtng(99)|weapon_length(114)|shield_height(19)|max_ammo(50)|swing_damage(56,cut)|thrust_damage(55,pierce), imodbits_none], 
["estoc_alt_mode", "Estoc alt mode", [("estoc",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_unbalanced, itc_spear_horse|itcf_carry_sword_left_hip, 0, weight(1.05)|head_armor(22)|body_armor(68)|leg_armor(2)|abundance(14)|hit_points(2213)|spd_rtng(95)|weapon_length(114)|shield_height(19)|max_ammo(50)|swing_damage(51,cut)|thrust_damage(50,pierce), imodbits_none], 

#two handed
["largeclub", "Warclub", [("Faradon_LargeClub",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_wooden_attack|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_nodachi_horse, 36, weight(2.36)|head_armor(128)|body_armor(74)|leg_armor(0)|hit_points(1318)|spd_rtng(85)|weapon_length(92)|shield_height(52)|max_ammo(8)|swing_damage(59,blunt)|thrust_damage(0,blunt), imodbits_none], 
["ironclub", "Bar Mace", [("Faradon_IronClub",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_nodachi_horse, 75, weight(3.45)|head_armor(10)|body_armor(100)|leg_armor(0)|hit_points(2333)|spd_rtng(77)|weapon_length(96)|shield_height(50)|max_ammo(50)|swing_damage(118,blunt)|thrust_damage(0,blunt), imodbits_none], 
["ravensbeak", "Raven's Beak", [("ravensbeak",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_nodachi_horse|itcf_thrust_polearm, 44, weight(1.75)|head_armor(12)|body_armor(67)|leg_armor(3)|abundance(18)|hit_points(1768)|spd_rtng(90)|weapon_length(90)|shield_height(54)|max_ammo(50)|swing_damage(84,blunt)|thrust_damage(70,pierce), imodbits_none], 
["ravensbeak_alt_mode", "Raven's Beak alt mode", [("ravensbeak_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_wooden_parry|itp_unbalanced, itc_nodachi_horse|itcf_thrust_polearm, 0, weight(1.75)|head_armor(4)|body_armor(11)|leg_armor(3)|abundance(18)|hit_points(1768)|spd_rtng(90)|weapon_length(90)|shield_height(54)|max_ammo(50)|swing_damage(121,pierce)|thrust_damage(70,pierce), imodbits_none], 
["long_fighting_axe", "Long Fighting Axe", [("fighting_axe_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_unbalanced, itc_nodachi_horse|itcf_carry_mace_left_hip, 49, weight(1.54)|head_armor(16)|body_armor(47)|leg_armor(0)|hit_points(1757)|spd_rtng(91)|weapon_length(84)|shield_height(61)|max_ammo(50)|swing_damage(76,pierce)|thrust_damage(0,pierce), imodbits_none], 
["faradon_twohanded1", "German Great Sword", [("Faradon_twohanded1",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_next_item_as_melee|itp_unbalanced, itc_greatsword, 111, weight(1.83)|head_armor(19)|body_armor(61)|leg_armor(3)|abundance(16)|hit_points(2252)|spd_rtng(93)|weapon_length(123)|shield_height(20)|max_ammo(50)|swing_damage(89,cut)|thrust_damage(79,pierce), imodbits_none], 
["faradon_twohanded1_alt_mode", "German Great Sword alt mode", [("Faradon_twohanded1",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(1.83)|head_armor(19)|body_armor(61)|leg_armor(3)|abundance(16)|hit_points(2252)|spd_rtng(90)|weapon_length(123)|shield_height(20)|max_ammo(50)|swing_damage(83,cut)|thrust_damage(74,pierce), imodbits_none], 
["faradon_twohanded2", "Danish Great Sword", [("Faradon_twohanded2",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_next_item_as_melee|itp_unbalanced, itc_greatsword, 113, weight(1.92)|head_armor(17)|body_armor(62)|leg_armor(2)|abundance(20)|hit_points(2256)|spd_rtng(92)|weapon_length(124)|shield_height(23)|max_ammo(50)|swing_damage(92,cut)|thrust_damage(83,pierce), imodbits_none], 
["faradon_twohanded2_alt_mode", "Danish Great Sword alt mode", [("Faradon_twohanded2",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(1.92)|head_armor(17)|body_armor(62)|leg_armor(2)|abundance(20)|hit_points(2256)|spd_rtng(89)|weapon_length(124)|shield_height(23)|max_ammo(50)|swing_damage(86,cut)|thrust_damage(78,pierce), imodbits_none], 
["lowlander_sword", "Lowlander Sword", [("lowlander_sword",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_next_item_as_melee|itp_unbalanced, itc_greatsword, 143, weight(3.12)|head_armor(10)|body_armor(54)|leg_armor(2)|abundance(16)|hit_points(2316)|spd_rtng(82)|weapon_length(131)|shield_height(28)|max_ammo(50)|swing_damage(130,cut)|thrust_damage(110,pierce), imodbits_none], 
["lowlander_sword_alt_mode", "Lowlander Sword alt mode", [("lowlander_sword",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(3.12)|head_armor(10)|body_armor(54)|leg_armor(2)|abundance(16)|hit_points(2316)|spd_rtng(80)|weapon_length(131)|shield_height(28)|max_ammo(50)|swing_damage(124,cut)|thrust_damage(105,pierce), imodbits_none], 
["scottish_claymore", "Claymore", [("scottish_claymore",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_next_item_as_melee|itp_unbalanced, itc_greatsword, 123, weight(2.35)|head_armor(9)|body_armor(54)|leg_armor(1)|abundance(15)|hit_points(2278)|spd_rtng(88)|weapon_length(113)|shield_height(24)|max_ammo(50)|swing_damage(114,cut)|thrust_damage(105,pierce), imodbits_none], 
["scottish_claymore_alt_mode", "Claymore alt mode", [("scottish_claymore",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(2.35)|head_armor(9)|body_armor(54)|leg_armor(1)|abundance(15)|hit_points(2278)|spd_rtng(86)|weapon_length(113)|shield_height(24)|max_ammo(50)|swing_damage(109,cut)|thrust_damage(101,pierce), imodbits_none], 
["flamberge", "Flamberge", [("flamberge",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_next_item_as_melee|itp_unbalanced, itc_greatsword, 171, weight(4.23)|head_armor(15)|body_armor(53)|leg_armor(2)|abundance(18)|hit_points(2372)|spd_rtng(73)|weapon_length(139)|shield_height(23)|max_ammo(50)|swing_damage(133,cut)|thrust_damage(117,pierce), imodbits_none], 
["flamberge_alt_mode", "Flamberge alt mode", [("flamberge",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(4.23)|head_armor(15)|body_armor(53)|leg_armor(2)|abundance(18)|hit_points(2372)|spd_rtng(73)|weapon_length(139)|shield_height(23)|max_ammo(50)|swing_damage(133,cut)|thrust_damage(117,pierce), imodbits_none], 

#polearms
["quarter_staff", "Quarter Staff", [("quarter_staff",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_attack|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_staff_horse, 20, weight(1.20)|head_armor(45)|body_armor(75)|leg_armor(8)|abundance(82)|hit_points(1260)|spd_rtng(90)|weapon_length(167)|shield_height(62)|max_ammo(8)|swing_damage(38,blunt)|thrust_damage(28,blunt), imodbits_none], 
["war_mallet", "War Mallet", [("maul_c",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_attack|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_poleaxe, 81, weight(4.00)|head_armor(176)|body_armor(94)|leg_armor(0)|hit_points(1880)|spd_rtng(70)|weapon_length(86)|shield_height(70)|max_ammo(50)|swing_damage(80,blunt)|thrust_damage(0,blunt), imodbits_none], 
["military_fork", "Military Fork", [("military_fork_swup",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced, itc_cutting_spear, 59, weight(2.00)|head_armor(0)|leg_armor(2)|abundance(12)|hit_points(1300)|spd_rtng(83)|weapon_length(165)|shield_height(74)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(60,pierce), imodbits_none], 
["shortened_spear", "Shortened Spear", [("spear_g_1-9m",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_spear_horse, 60, weight(1.50)|head_armor(56)|body_armor(56)|leg_armor(8)|abundance(28)|hit_points(1275)|spd_rtng(88)|weapon_length(140)|shield_height(57)|max_ammo(8)|swing_damage(46,blunt)|thrust_damage(38,pierce), imodbits_none], 
["spear", "Spear", [("spear_h_2-15m",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_spear_horse, 60, weight(1.80)|head_armor(42)|body_armor(56)|leg_armor(9)|abundance(28)|hit_points(1290)|spd_rtng(86)|weapon_length(155)|shield_height(60)|max_ammo(8)|swing_damage(55,blunt)|thrust_damage(43,pierce), imodbits_none], 
["war_spear", "War Spear", [("spear_i_2-3m",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_spear_horse, 60, weight(2.10)|head_armor(42)|body_armor(58)|leg_armor(9)|abundance(24)|hit_points(1305)|spd_rtng(84)|weapon_length(170)|shield_height(62)|max_ammo(8)|swing_damage(61,blunt)|thrust_damage(49,pierce), imodbits_none], 
["german_hunting_spear", "German Hunting Spear", [("german_hunting_spear",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_spear_horse, 57, weight(1.80)|head_armor(32)|body_armor(54)|leg_armor(4)|abundance(23)|hit_points(1290)|spd_rtng(82)|weapon_length(158)|shield_height(96)|max_ammo(8)|swing_damage(52,blunt)|thrust_damage(44,pierce), imodbits_none], 
["partisan", "Partisan", [("partisan",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced, itc_spear_horse, 83, weight(2.40)|head_armor(9)|body_armor(53)|leg_armor(2)|abundance(27)|hit_points(2280)|spd_rtng(79)|weapon_length(158)|shield_height(83)|max_ammo(50)|swing_damage(95,cut)|thrust_damage(74,pierce), imodbits_none], 
["ashwood_pike", "Ashwood Pike", [("pike_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced, itc_cutting_spear, 58, weight(2.40)|head_armor(0)|leg_armor(8)|abundance(36)|hit_points(1320)|spd_rtng(78)|weapon_length(189)|shield_height(95)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(46,pierce), imodbits_none], 
["awlpike", "Awlpike", [("awl_pike_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_unbalanced, itc_cutting_spear, 60, weight(2.50)|head_armor(0)|leg_armor(5)|abundance(9)|hit_points(1325)|spd_rtng(82)|weapon_length(165)|shield_height(48)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(67,pierce), imodbits_none], 
["awlpike_long", "Long Awlpike", [("awl_pike_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_cutting_spear, 57, weight(2.90)|head_armor(0)|leg_armor(5)|abundance(11)|hit_points(1345)|spd_rtng(74)|weapon_length(205)|shield_height(102)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(62,pierce), imodbits_none], 
["pike_a", "Pike", [("pike_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_no_parry|itp_is_pike|itp_unbalanced, itc_pike, 58, weight(3.40)|head_armor(0)|leg_armor(5)|abundance(22)|hit_points(1370)|spd_rtng(71)|weapon_length(304)|shield_height(95)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(70,pierce), imodbits_none], 
["pike_b", "Half Pike", [("pike_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_no_parry|itp_is_pike|itp_unbalanced, itc_pike, 58, weight(3.20)|head_armor(0)|leg_armor(6)|abundance(31)|hit_points(1360)|spd_rtng(72)|weapon_length(291)|shield_height(101)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(62,pierce), imodbits_none], 
["light_lance", "Light Lance", [("spear_b_2-75m",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_offset_lance|itp_couchable|itp_unbalanced, itc_cutting_spear, 58, weight(2.30)|head_armor(0)|leg_armor(12)|abundance(36)|hit_points(1315)|spd_rtng(80)|weapon_length(195)|shield_height(88)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(44,pierce), imodbits_none], 
["heavy_lance", "Heavy Lance", [("spear_f_2-9m",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_offset_lance|itp_couchable|itp_unbalanced, itc_cutting_spear, 58, weight(2.60)|head_armor(0)|leg_armor(6)|abundance(26)|hit_points(1330)|spd_rtng(77)|weapon_length(210)|shield_height(90)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(53,pierce), imodbits_none], 
["great_lance", "Great Lance", [("heavy_lance_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_couchable, itc_cutting_spear, 63, weight(3.30)|head_armor(0)|leg_armor(0)|hit_points(1365)|spd_rtng(81)|weapon_length(229)|shield_height(0)|max_ammo(8)|swing_damage(0,blunt)|thrust_damage(140,pierce), imodbits_none], 
["bec_de_corbin", "Bec De Corbin", [("bec_de_corbin",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff_horse, 70, weight(2.40)|head_armor(15)|body_armor(74)|leg_armor(9)|abundance(25)|hit_points(1800)|spd_rtng(78)|weapon_length(148)|shield_height(97)|max_ammo(50)|swing_damage(83,pierce)|thrust_damage(60,pierce), imodbits_none], 
["bec_de_corbin_alt_mode", "Bec De Corbin alt mode", [("bec_de_corbin_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_spear_horse, 0, weight(2.40)|head_armor(13)|body_armor(38)|leg_armor(9)|abundance(25)|hit_points(1800)|spd_rtng(78)|weapon_length(148)|shield_height(97)|max_ammo(50)|swing_damage(92,blunt)|thrust_damage(60,pierce), imodbits_none], 
["dane_axe", "Dane Axe", [("daneaxe",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_poleaxe_horse, 81, weight(1.80)|head_armor(7)|body_armor(48)|leg_armor(0)|hit_points(1770)|spd_rtng(86)|weapon_length(93)|shield_height(61)|max_ammo(50)|swing_damage(88,cut)|thrust_damage(0,blunt), imodbits_none], 
["voulge", "Voulge", [("voulge_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 92, weight(3.90)|head_armor(0)|body_armor(48)|leg_armor(0)|abundance(20)|hit_points(1875)|spd_rtng(68)|weapon_length(124)|shield_height(95)|max_ammo(50)|swing_damage(153,cut)|thrust_damage(101,cut), imodbits_none], 
["bardiche", "Bardiche", [("two_handed_battle_long_axe_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 83, weight(2.70)|head_armor(0)|body_armor(48)|leg_armor(0)|abundance(20)|hit_points(1815)|spd_rtng(75)|weapon_length(140)|shield_height(103)|max_ammo(50)|swing_damage(129,cut)|thrust_damage(85,pierce), imodbits_none], 
["great_bardiche", "Great Bardiche", [("two_handed_battle_long_axe_c",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 85, weight(3.00)|head_armor(0)|body_armor(48)|leg_armor(0)|abundance(20)|hit_points(1830)|spd_rtng(73)|weapon_length(155)|shield_height(110)|max_ammo(50)|swing_damage(136,cut)|thrust_damage(89,pierce), imodbits_none], 
["war_scythe", "War Scythe", [("scythe_new",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 24, weight(2.80)|head_armor(8)|body_armor(49)|leg_armor(4)|abundance(20)|hit_points(1820)|spd_rtng(76)|weapon_length(223)|shield_height(93)|max_ammo(50)|swing_damage(105,cut)|thrust_damage(76,blunt), imodbits_none], 
["glaive_1", "Glaive", [("glaive1",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear_horse, 80, weight(1.90)|head_armor(9)|body_armor(51)|leg_armor(4)|abundance(19)|hit_points(1775)|spd_rtng(84)|weapon_length(148)|shield_height(74)|max_ammo(50)|swing_damage(85,cut)|thrust_damage(63,pierce), imodbits_none], 
["glaive_2", "Crossguard Glaive", [("glaive2",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear_horse, 81, weight(2.10)|head_armor(9)|body_armor(52)|leg_armor(4)|abundance(19)|hit_points(1785)|spd_rtng(82)|weapon_length(157)|shield_height(77)|max_ammo(50)|swing_damage(90,cut)|thrust_damage(67,pierce), imodbits_none], 
["poleaxe", "Poleaxe", [("poleaxe_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 83, weight(2.60)|head_armor(8)|body_armor(55)|leg_armor(3)|abundance(15)|hit_points(1810)|spd_rtng(76)|weapon_length(166)|shield_height(104)|max_ammo(50)|swing_damage(96,cut)|thrust_damage(76,pierce), imodbits_none], 
["poleaxe_alt_mode", "Poleaxe alt mode", [("poleaxe_a_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_spear, 0, weight(2.60)|head_armor(0)|body_armor(37)|leg_armor(3)|abundance(15)|hit_points(1810)|spd_rtng(76)|weapon_length(166)|shield_height(104)|max_ammo(50)|swing_damage(132,blunt)|thrust_damage(76,pierce), imodbits_none], 
["simple_poleaxe", "Burgundian Poleaxe", [("simple_poleaxe",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 84, weight(2.50)|head_armor(8)|body_armor(42)|leg_armor(4)|abundance(16)|hit_points(1805)|spd_rtng(79)|weapon_length(157)|shield_height(78)|max_ammo(50)|swing_damage(103,cut)|thrust_damage(75,pierce), imodbits_none], 
["simple_poleaxe_alt_mode", "Burgundian Poleaxe alt mode", [("simple_poleaxe_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_spear, 0, weight(2.50)|head_armor(12)|body_armor(54)|leg_armor(4)|abundance(16)|hit_points(1805)|spd_rtng(79)|weapon_length(157)|shield_height(78)|max_ammo(50)|swing_damage(95,blunt)|thrust_damage(75,pierce), imodbits_none], 
["elegant_poleaxe", "Elegant Poleaxe", [("elegant_poleaxe",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 82, weight(2.50)|head_armor(8)|body_armor(51)|leg_armor(4)|abundance(29)|hit_points(1805)|spd_rtng(77)|weapon_length(155)|shield_height(96)|max_ammo(50)|swing_damage(96,cut)|thrust_damage(66,pierce), imodbits_none], 
["elegant_poleaxe_alt_mode", "Elegant Poleaxe alt mode", [("elegant_poleaxe_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced|itp_can_knock_down, itc_spear, 0, weight(2.50)|head_armor(37)|body_armor(87)|leg_armor(4)|abundance(29)|hit_points(1805)|spd_rtng(77)|weapon_length(155)|shield_height(96)|max_ammo(50)|swing_damage(74,blunt)|thrust_damage(66,pierce), imodbits_none], 
["german_poleaxe", "German Poleaxe", [("german_poleaxe",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 82, weight(2.60)|head_armor(6)|body_armor(47)|leg_armor(5)|abundance(17)|hit_points(1810)|spd_rtng(75)|weapon_length(160)|shield_height(112)|max_ammo(50)|swing_damage(99,cut)|thrust_damage(68,pierce), imodbits_none], 
["german_poleaxe_alt_mode", "German Poleaxe alt mode", [("german_poleaxe_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(2.60)|head_armor(4)|body_armor(25)|leg_armor(5)|abundance(17)|hit_points(1810)|spd_rtng(75)|weapon_length(160)|shield_height(112)|max_ammo(50)|swing_damage(113,pierce)|thrust_damage(68,pierce), imodbits_none], 
["halbert_1", "Halbert", [("halbert_1",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 85, weight(3.00)|head_armor(8)|body_armor(49)|leg_armor(6)|abundance(18)|hit_points(1830)|spd_rtng(73)|weapon_length(184)|shield_height(110)|max_ammo(50)|swing_damage(104,cut)|thrust_damage(72,pierce), imodbits_none], 
["halbert_1_alt_mode", "Halbert alt mode", [("halbert_1_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(3.00)|head_armor(7)|body_armor(35)|leg_armor(6)|abundance(18)|hit_points(1830)|spd_rtng(73)|weapon_length(184)|shield_height(110)|max_ammo(50)|swing_damage(111,pierce)|thrust_damage(72,pierce), imodbits_none], 
["halbert_3", "Curved Halbert", [("halbert_3",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 85, weight(2.90)|head_armor(8)|body_armor(42)|leg_armor(3)|abundance(15)|hit_points(1825)|spd_rtng(74)|weapon_length(174)|shield_height(103)|max_ammo(50)|swing_damage(105,cut)|thrust_damage(80,pierce), imodbits_none], 
["halbert_3_alt_mode", "Curved Halbert alt mode", [("halbert_3_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(2.90)|head_armor(4)|body_armor(32)|leg_armor(3)|abundance(15)|hit_points(1825)|spd_rtng(74)|weapon_length(174)|shield_height(103)|max_ammo(50)|swing_damage(119,pierce)|thrust_damage(80,pierce), imodbits_none], 
["swiss_halberd", "Swiss Halberd", [("swiss_halberd",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 84, weight(2.80)|head_armor(8)|body_armor(48)|leg_armor(4)|abundance(23)|hit_points(1820)|spd_rtng(75)|weapon_length(199)|shield_height(104)|max_ammo(50)|swing_damage(103,cut)|thrust_damage(72,pierce), imodbits_none], 
["swiss_halberd_alt_mode", "Swiss Halberd alt mode", [("swiss_halberd_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(2.80)|head_armor(4)|body_armor(25)|leg_armor(4)|abundance(23)|hit_points(1820)|spd_rtng(75)|weapon_length(199)|shield_height(104)|max_ammo(50)|swing_damage(122,pierce)|thrust_damage(72,pierce), imodbits_none], 
["guisarme_a", "Guisarme", [("guisarme_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 85, weight(2.90)|head_armor(8)|body_armor(45)|leg_armor(3)|abundance(16)|hit_points(1825)|spd_rtng(74)|weapon_length(196)|shield_height(98)|max_ammo(50)|swing_damage(105,pierce)|thrust_damage(79,pierce), imodbits_none], 
["english_bill", "English Bill", [("english_bill",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_staff, 85, weight(3.30)|head_armor(9)|body_armor(41)|leg_armor(5)|abundance(25)|hit_points(1845)|spd_rtng(70)|weapon_length(206)|shield_height(138)|max_ammo(50)|swing_damage(100,cut)|thrust_damage(68,pierce), imodbits_none], 
["english_bill_alt_mode", "English Bill alt mode", [("english_bill_alt",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_spear, 0, weight(3.30)|head_armor(5)|body_armor(34)|leg_armor(5)|abundance(25)|hit_points(1845)|spd_rtng(70)|weapon_length(206)|shield_height(138)|max_ammo(50)|swing_damage(111,pierce)|thrust_damage(68,pierce), imodbits_none], 

#shields
["steel_shield", "Steel Shield", [("shield_dragon",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 165, weight(3.50)|body_armor(57)|leg_armor(255)|hit_points(192)|spd_rtng(92)|shield_width(32)|shield_height(0)|max_ammo(50)|thrust_damage(65,blunt), imodbits_none, [on_shield_hit]], 
["steel_buckler1", "Round Steel Buckler", [("steel_buckler1",0)], itp_type_shield|itp_merchandise, itcf_carry_buckler_left, 180, weight(2.00)|body_armor(52)|leg_armor(78)|hit_points(126)|spd_rtng(100)|shield_width(1)|shield_height(0)|max_ammo(50)|thrust_damage(80,blunt), imodbits_none, [on_shield_hit]], 
["steel_buckler2", "Steel Buckler", [("steel_buckler2",0)], itp_type_shield|itp_merchandise, itcf_carry_buckler_left, 215, weight(2.10)|body_armor(54)|leg_armor(78)|hit_points(126)|spd_rtng(99)|shield_width(1)|shield_height(0)|max_ammo(70)|thrust_damage(97,blunt), imodbits_none, [on_shield_hit]], 
["tab_shield_small_round_a", "Plain Cavalry Shield", [("tableau_shield_small_round_3",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 45, weight(2.20)|body_armor(20)|leg_armor(255)|hit_points(198)|spd_rtng(99)|shield_width(33)|shield_height(0)|max_ammo(8)|thrust_damage(19,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_small_round_shield_3",":agent_no",":troop_no")])]], 
["tab_shield_small_round_b", "Round Cavalry Shield", [("tableau_shield_small_round_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 47, weight(2.40)|body_armor(22)|leg_armor(255)|hit_points(198)|spd_rtng(98)|shield_width(33)|shield_height(0)|max_ammo(8)|thrust_damage(20,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_small_round_shield_1",":agent_no",":troop_no")])]], 
["tab_shield_small_round_c", "Elite Cavalry Shield", [("tableau_shield_small_round_2",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 48, weight(2.50)|body_armor(23)|leg_armor(255)|hit_points(198)|spd_rtng(97)|shield_width(33)|shield_height(0)|max_ammo(8)|thrust_damage(20,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_small_round_shield_2",":agent_no",":troop_no")])]], 
["tab_shield_round_a", "Cheap Round Shield", [("tableau_shield_round_5",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 42, weight(2.10)|body_armor(19)|leg_armor(255)|hit_points(210)|spd_rtng(99)|shield_width(35)|shield_height(0)|max_ammo(8)|thrust_damage(18,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_round_shield_5",":agent_no",":troop_no")])]], 
["tab_shield_round_b", "Round Shield", [("tableau_shield_round_3",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 47, weight(2.40)|body_armor(22)|leg_armor(255)|hit_points(240)|spd_rtng(98)|shield_width(40)|shield_height(0)|max_ammo(8)|thrust_damage(20,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_round_shield_3",":agent_no",":troop_no")])]], 
["tab_shield_round_c", "Round Shield with Buckler", [("tableau_shield_round_3",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 50, weight(2.60)|body_armor(25)|leg_armor(255)|hit_points(240)|spd_rtng(97)|shield_width(40)|shield_height(0)|max_ammo(8)|thrust_damage(21,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_round_shield_2",":agent_no",":troop_no")])]], 
["tab_shield_round_d", "Heavy Round Shield", [("tableau_shield_round_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_round_shield, 52, weight(2.80)|body_armor(27)|leg_armor(255)|hit_points(234)|spd_rtng(96)|shield_width(39)|shield_height(0)|max_ammo(8)|thrust_damage(22,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_round_shield_1",":agent_no",":troop_no")])]], 
["tab_shield_round_e", "Huscarl's Round Shield", [("tableau_shield_round_4",0)], itp_type_shield|itp_merchandise, itcf_carry_round_shield, 55, weight(3.00)|body_armor(18)|leg_armor(255)|hit_points(252)|spd_rtng(95)|shield_width(42)|shield_height(0)|max_ammo(8)|thrust_damage(23,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_round_shield_4",":agent_no",":troop_no")])]], 
["tab_shield_kite_cav_a", "Horseman's Kite Shield", [("tableau_shield_kite_4",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 40, weight(2.00)|body_armor(27)|leg_armor(255)|hit_points(234)|spd_rtng(100)|shield_width(23)|shield_height(55)|max_ammo(8)|thrust_damage(17,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_kite_shield_4",":agent_no",":troop_no")])]], 
["tab_shield_heater_cav_a", "Horseman's Heater Shield", [("tableau_shield_heater_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 45, weight(2.30)|body_armor(21)|leg_armor(255)|hit_points(225)|spd_rtng(98)|shield_width(25)|shield_height(50)|max_ammo(8)|thrust_damage(19,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_heater_shield_2",":agent_no",":troop_no")])]], 
["tab_shield_pavise_b", "Pavise", [("tableau_shield_pavise_2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_board_shield, 57, weight(3.10)|body_armor(28)|leg_armor(255)|hit_points(363)|spd_rtng(94)|shield_width(27)|shield_height(94)|max_ammo(8)|thrust_damage(24,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_pavise_shield_2",":agent_no",":troop_no")])]], 
["tab_shield_pavise_c", "Heavy Pavise", [("tableau_shield_pavise_1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_board_shield, 63, weight(3.50)|body_armor(32)|leg_armor(255)|hit_points(366)|spd_rtng(92)|shield_width(28)|shield_height(94)|max_ammo(8)|thrust_damage(26,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_pavise_shield_1",":agent_no",":troop_no")])]], 
["tab_shield_otto_1", "Wing Shield", [("tableau_shield_otto1",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 50, weight(2.60)|body_armor(23)|leg_armor(255)|hit_points(225)|spd_rtng(97)|shield_width(25)|shield_height(50)|max_ammo(8)|thrust_damage(21,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_otto_shield1",":agent_no",":troop_no")])]], 
["tab_shield_otto_2", "Heavy Wing Shield", [("tableau_shield_otto2",0)], itp_type_shield|itp_merchandise|itp_wooden_parry, itcf_carry_kite_shield, 55, weight(2.90)|body_armor(26)|leg_armor(255)|hit_points(225)|spd_rtng(95)|shield_width(25)|shield_height(50)|max_ammo(8)|thrust_damage(23,blunt), imodbits_none, [on_shield_hit, (ti_on_init_item,[(store_trigger_param_1,":agent_no"),(store_trigger_param_2,":troop_no"),(call_script,"script_item_init_banner","tableau_otto_shield2",":agent_no",":troop_no")])]], 

#thrown
["stones", "Stones", [("throwing_stone",0),("throwing_stone_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_stone|itcf_carry_quiver_right_vertical, 30, weight(4.00)|head_armor(38)|abundance(50)|hit_points(908)|spd_rtng(71)|shoot_speed(18)|weapon_length(6)|max_ammo(10)|swing_damage(2,blunt)|thrust_damage(40,blunt)|accuracy(106), imodbits_none], 
["stones_alt_mode", "Stones alt mode", [("throwing_stone",0),("throwing_stone_missile",ixmesh_flying_ammo)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_can_knock_down, itc_cleaver|itcf_carry_quiver_right_vertical, 0, weight(0.40)|head_armor(3)|body_armor(50)|leg_armor(38)|hit_points(908)|spd_rtng(96)|weapon_length(6)|max_ammo(2)|swing_damage(16,blunt)|thrust_damage(0,blunt), imodbits_none], 
["darts", "Darts", [("dart_b",0),("dart_b_missile",ixmesh_flying_ammo),("dart_b_bag",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_right_vertical|itcf_show_holster_when_drawn, 54, weight(1.80)|head_armor(3)|body_armor(14)|abundance(13)|hit_points(846)|spd_rtng(72)|shoot_speed(19)|weapon_length(19)|max_ammo(6)|swing_damage(50,pierce)|thrust_damage(84,pierce)|accuracy(107), imodbits_none], 
["darts_alt_mode", "Darts alt mode", [("dart_b",0),("dart_b_missile",ixmesh_flying_ammo),("dart_b_bag",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_has_bayonet, itc_dagger_thrust|itcf_carry_quiver_right_vertical|itcf_show_holster_when_drawn|itp_has_bayonet, 0, weight(0.30)|head_armor(3)|leg_armor(3)|abundance(13)|hit_points(846)|spd_rtng(97)|weapon_length(29)|max_ammo(50)|swing_damage(0,blunt)|thrust_damage(14,pierce), imodbits_none], 
["war_darts", "War Darts", [("dart_a",0),("dart_a_missile",ixmesh_flying_ammo),("dart_a_bag",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 54, weight(2.40)|head_armor(3)|body_armor(20)|abundance(8)|hit_points(848)|spd_rtng(71)|shoot_speed(18)|weapon_length(34)|max_ammo(6)|swing_damage(50,pierce)|thrust_damage(103,pierce)|accuracy(106), imodbits_none], 
["war_darts_alt_mode", "War Darts alt mode", [("dart_a",0),("dart_a_missile",ixmesh_flying_ammo),("dart_a_bag",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_has_bayonet, itc_dagger_thrust|itcf_carry_quiver_back|itcf_show_holster_when_drawn|itp_has_bayonet, 0, weight(0.40)|head_armor(2)|leg_armor(3)|abundance(8)|hit_points(848)|spd_rtng(96)|weapon_length(44)|max_ammo(50)|swing_damage(0,blunt)|thrust_damage(20,pierce), imodbits_none], 
["javelins", "Javelins", [("javelin",0),("javelin_missile",ixmesh_flying_ammo),("javelins_quiver_new",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 60, weight(4.80)|head_armor(3)|body_armor(42)|abundance(9)|hit_points(856)|spd_rtng(67)|shoot_speed(15)|weapon_length(67)|max_ammo(6)|swing_damage(50,pierce)|thrust_damage(144,pierce)|accuracy(105), imodbits_none], 
["javelins_alt_mode", "Javelins alt mode", [("javelin",0),("javelin_missile",ixmesh_flying_ammo),("javelins_quiver_new",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_has_bayonet|itp_can_knock_down, itc_spear_horse|itcf_carry_quiver_back|itcf_show_holster_when_drawn|itp_has_bayonet, 0, weight(0.80)|head_armor(2)|body_armor(73)|leg_armor(3)|abundance(9)|hit_points(856)|spd_rtng(99)|weapon_length(77)|max_ammo(50)|swing_damage(54,blunt)|thrust_damage(42,pierce), imodbits_none], 
["jarids", "Jarids", [("jarid_new",0),("jarid_new_missile",ixmesh_flying_ammo),("jarid_quiver",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 30, weight(2.40)|head_armor(7)|body_armor(43)|abundance(3)|hit_points(856)|spd_rtng(67)|shoot_speed(15)|weapon_length(71)|max_ammo(3)|swing_damage(50,pierce)|thrust_damage(149,pierce)|accuracy(105), imodbits_none], 
["jarids_alt_mode", "Jarids alt mode", [("jarid_new",0),("jarid_new_missile",ixmesh_flying_ammo),("jarid_quiver",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_has_bayonet|itp_can_knock_down, itc_spear_horse|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 0, weight(0.80)|head_armor(9)|body_armor(73)|leg_armor(7)|abundance(3)|hit_points(856)|spd_rtng(99)|weapon_length(81)|max_ammo(50)|swing_damage(44,blunt)|thrust_damage(43,pierce), imodbits_none], 
["throwing_spears", "Throwing Spears", [("jarid_new_b",0),("jarid_new_b_missile",ixmesh_flying_ammo),("jarid_new_b_bag",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_javelin|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 60, weight(5.40)|head_armor(2)|body_armor(52)|abundance(5)|hit_points(858)|spd_rtng(66)|shoot_speed(14)|weapon_length(71)|max_ammo(6)|swing_damage(50,pierce)|thrust_damage(174,pierce)|accuracy(105), imodbits_none], 
["throwing_spears_alt_mode", "Throwing Spears alt mode", [("jarid_new_b",0),("jarid_new_b_missile",ixmesh_flying_ammo),("jarid_new_b_bag",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_has_bayonet|itp_can_knock_down, itc_spear_horse|itcf_carry_quiver_back|itcf_show_holster_when_drawn|itp_has_bayonet, 0, weight(0.90)|head_armor(8)|body_armor(73)|leg_armor(2)|abundance(5)|hit_points(858)|spd_rtng(98)|weapon_length(81)|max_ammo(50)|swing_damage(50,blunt)|thrust_damage(52,pierce), imodbits_none], 
["throwing_knives", "Throwing Knives", [("throwing_knife",0),("throwing_knife_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_axe|itcf_carry_quiver_front_right, 72, weight(1.10)|head_armor(1)|body_armor(47)|abundance(6)|hit_points(842)|spd_rtng(73)|shoot_speed(24)|weapon_length(15)|max_ammo(9)|swing_damage(50,cut)|thrust_damage(67,cut)|accuracy(108), imodbits_none], 
["throwing_knives_alt_mode", "Throwing Knives alt mode", [("throwing_knife",0),("throwing_knife_missile",ixmesh_flying_ammo)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry, itc_dagger|itcf_carry_quiver_front_right, 0, weight(0.10)|head_armor(8)|body_armor(47)|leg_armor(1)|abundance(6)|hit_points(842)|spd_rtng(99)|weapon_length(25)|max_ammo(50)|swing_damage(6,cut)|thrust_damage(6,pierce), imodbits_none], 
["throwing_daggers", "Throwing Daggers", [("throwing_dagger",0),("throwing_dagger_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_axe|itcf_carry_quiver_front_right, 56, weight(1.10)|head_armor(2)|body_armor(59)|abundance(8)|hit_points(843)|spd_rtng(73)|shoot_speed(23)|weapon_length(19)|max_ammo(7)|swing_damage(50,cut)|thrust_damage(66,cut)|accuracy(108), imodbits_none], 
["throwing_daggers_alt_mode", "Throwing Daggers alt mode", [("throwing_dagger",0),("throwing_dagger_missile",ixmesh_flying_ammo)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry, itc_dagger|itcf_carry_quiver_front_right, 0, weight(0.20)|head_armor(11)|body_armor(59)|leg_armor(2)|abundance(8)|hit_points(843)|spd_rtng(98)|weapon_length(29)|max_ammo(50)|swing_damage(11,cut)|thrust_damage(11,pierce), imodbits_none], 
["throwing_axes", "Throwing Axes", [("throwing_axe_a",0),("throwing_axe_a_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_axe|itcf_carry_quiver_front_right, 30, weight(2.40)|head_armor(7)|abundance(48)|hit_points(856)|spd_rtng(67)|shoot_speed(15)|weapon_length(44)|max_ammo(3)|swing_damage(50,cut)|thrust_damage(105,cut)|accuracy(105), imodbits_none], 
["throwing_axes_alt_mode", "Throwing Axes alt mode", [("throwing_axe_a",0),("throwing_axe_a_missile",ixmesh_flying_ammo)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry, itc_scimitar|itcf_carry_quiver_front_right, 0, weight(0.80)|body_armor(48)|leg_armor(7)|hit_points(856)|spd_rtng(92)|weapon_length(54)|max_ammo(50)|swing_damage(53,cut)|thrust_damage(0,blunt), imodbits_none], 
["heavy_throwing_axes", "Heavy Throwing Axes", [("throwing_axe_b",0),("throwing_axe_b_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_axe|itcf_carry_quiver_front_right, 30, weight(2.70)|head_armor(9)|abundance(44)|hit_points(858)|spd_rtng(66)|shoot_speed(14)|weapon_length(47)|max_ammo(3)|swing_damage(50,cut)|thrust_damage(109,cut)|accuracy(105), imodbits_none], 
["heavy_throwing_axes_alt_mode", "Heavy Throwing Axes alt mode", [("throwing_axe_b",0),("throwing_axe_b_missile",ixmesh_flying_ammo)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry, itc_scimitar|itcf_carry_quiver_front_right, 0, weight(0.90)|body_armor(44)|leg_armor(9)|hit_points(858)|spd_rtng(91)|weapon_length(57)|max_ammo(50)|swing_damage(59,cut)|thrust_damage(0,blunt), imodbits_none], 

#bows
["short_bow", "Short Bow", [("short_bow_new",0),("short_bow_new_carry",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bow_back, 46, weight(0.50)|hit_points(610)|spd_rtng(64)|shoot_speed(32)|thrust_damage(56,cut)|accuracy(93), imodbits_none], 
["short_bow_alt_mode", "Short Bow alt mode", [("short_bow_new",0),("short_bow_new_carry",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bow_back, 0, weight(0.50)|head_armor(48)|body_armor(100)|hit_points(610)|spd_rtng(99)|weapon_length(74)|shield_height(0)|max_ammo(8)|swing_damage(17,blunt)|thrust_damage(0,blunt), imodbits_none], 
["recurve_bow", "Recurve Bow", [("nomad_bow_new",0),("nomad_bow_new_case",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 57, weight(0.40)|hit_points(608)|spd_rtng(63)|shoot_speed(36)|thrust_damage(71,cut)|accuracy(102), imodbits_none], 
["recurve_bow_alt_mode", "Recurve Bow alt mode", [("nomad_bow_new",0),("nomad_bow_new_case",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 0, weight(0.40)|head_armor(48)|body_armor(100)|hit_points(608)|spd_rtng(97)|weapon_length(49)|shield_height(0)|max_ammo(8)|swing_damage(13,blunt)|thrust_damage(0,blunt), imodbits_none], 
["flat_bow", "Flat Bow", [("hunting_bow_new",0),("hunting_bow_new_carry",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bow_back, 57, weight(0.70)|hit_points(614)|spd_rtng(62)|shoot_speed(36)|thrust_damage(70,cut)|accuracy(101), imodbits_none], 
["flat_bow_alt_mode", "Flat Bow alt mode", [("hunting_bow_new",0),("hunting_bow_new_carry",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bow_back, 0, weight(0.70)|head_armor(48)|body_armor(100)|hit_points(614)|spd_rtng(92)|weapon_length(86)|shield_height(0)|max_ammo(8)|swing_damage(21,blunt)|thrust_damage(0,blunt), imodbits_none], 
["tatar_bow", "Tatar Bow", [("khergit_bow_new",0),("khergit_bow_new_case",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 72, weight(0.50)|hit_points(670)|spd_rtng(61)|shoot_speed(40)|thrust_damage(88,cut)|accuracy(111), imodbits_none], 
["tatar_bow_alt_mode", "Tatar Bow alt mode", [("khergit_bow_new",0),("khergit_bow_new_case",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 0, weight(0.50)|head_armor(48)|body_armor(100)|hit_points(670)|spd_rtng(96)|weapon_length(57)|shield_height(0)|max_ammo(3)|swing_damage(14,blunt)|thrust_damage(0,blunt), imodbits_none], 
["horn_bow", "Horn Bow", [("strong_bow_new",0),("strong_bow_new_case",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 78, weight(0.60)|hit_points(732)|spd_rtng(60)|shoot_speed(42)|thrust_damage(95,cut)|accuracy(115), imodbits_none], 
["horn_bow_alt_mode", "Horn Bow alt mode", [("strong_bow_new",0),("strong_bow_new_case",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bowcase_left|itcf_show_holster_when_drawn, 0, weight(0.60)|head_armor(48)|body_armor(100)|hit_points(732)|spd_rtng(95)|weapon_length(55)|shield_height(0)|max_ammo(3)|swing_damage(17,blunt)|thrust_damage(0,blunt), imodbits_none], 
["reflex_bow", "Reflex Bow", [("war_bow_new",0),("war_bow_new_carry",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bow_back, 95, weight(0.80)|hit_points(676)|spd_rtng(57)|shoot_speed(46)|thrust_damage(116,cut)|accuracy(126), imodbits_none], 
["reflex_bow_alt_mode", "Reflex Bow alt mode", [("war_bow_new",0),("war_bow_new_carry",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bow_back, 0, weight(0.80)|head_armor(48)|body_armor(100)|hit_points(676)|spd_rtng(90)|weapon_length(94)|shield_height(0)|max_ammo(3)|swing_damage(20,blunt)|thrust_damage(0,blunt), imodbits_none], 
["long_bow", "Long Bow", [("long_bow_new",0),("long_bow_new_carry",ixmesh_carry)], itp_primary|itp_can_penetrate_shield|itp_type_bow|itp_two_handed|itp_next_item_as_melee, itcf_shoot_bow|itcf_carry_bow_back, 106, weight(0.90)|hit_points(618)|spd_rtng(54)|shoot_speed(49)|thrust_damage(131,cut)|accuracy(135), imodbits_none], 
["long_bow_alt_mode", "Long Bow alt mode", [("long_bow_new",0),("long_bow_new_carry",ixmesh_carry)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_no_parry|itp_can_knock_down, itc_bow_melee|itcf_carry_bow_back, 0, weight(0.90)|head_armor(48)|body_armor(100)|hit_points(618)|spd_rtng(89)|weapon_length(95)|shield_height(0)|max_ammo(8)|swing_damage(25,blunt)|thrust_damage(0,blunt), imodbits_none], 

#crossbows
["wooden_crossbow", "Wooden Crossbow", [("crossbow_d",0)], itp_primary|itp_can_penetrate_shield|itp_type_crossbow|itp_two_handed|itp_cant_reload_while_moving|itp_next_item_as_melee, itcf_shoot_crossbow|itcf_carry_crossbow_back, 73, weight(4.00)|hit_points(680)|spd_rtng(52)|shoot_speed(34)|max_ammo(1)|thrust_damage(67,cut)|accuracy(112), imodbits_none], 
["wooden_crossbow_alt_mode", "Wooden Crossbow alt mode", [("crossbow_d",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_crossbow_back, 0, weight(4.00)|head_armor(48)|body_armor(100)|leg_armor(16)|abundance(100)|hit_points(680)|spd_rtng(77)|weapon_length(90)|max_ammo(8)|swing_damage(84,blunt)|thrust_damage(61,blunt), imodbits_none], 
["light_horn_crossbow", "Light Horn Crossbow", [("crossbow_b",0)], itp_primary|itp_can_penetrate_shield|itp_type_crossbow|itp_two_handed|itp_cant_reload_while_moving|itp_next_item_as_melee, itcf_shoot_crossbow|itcf_carry_crossbow_back, 148, weight(4.50)|hit_points(690)|spd_rtng(46)|shoot_speed(49)|max_ammo(1)|thrust_damage(141,cut)|accuracy(128), imodbits_none], 
["light_horn_crossbow_alt_mode", "Light Horn Crossbow alt mode", [("crossbow_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_crossbow_back, 0, weight(4.50)|head_armor(48)|body_armor(100)|leg_armor(16)|abundance(100)|hit_points(690)|spd_rtng(73)|weapon_length(90)|max_ammo(8)|swing_damage(85,blunt)|thrust_damage(61,blunt), imodbits_none], 
["heavy_horn_crossbow", "Heavy Horn Crossbow", [("crossbow_a",0)], itp_primary|itp_can_penetrate_shield|itp_type_crossbow|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_crossbow|itcf_carry_crossbow_back, 171, weight(4.60)|hit_points(692)|spd_rtng(45)|shoot_speed(53)|max_ammo(1)|thrust_damage(164,cut)|accuracy(133), imodbits_none], 
["heavy_horn_crossbow_alt_mode", "Heavy Horn Crossbow alt mode", [("crossbow_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_crossbow_back, 0, weight(4.60)|head_armor(48)|body_armor(100)|leg_armor(16)|abundance(100)|hit_points(692)|spd_rtng(72)|weapon_length(90)|max_ammo(8)|swing_damage(84,blunt)|thrust_damage(61,blunt), imodbits_none], 
["light_steel_crossbow", "Light Arbalest", [("crossbow_e",0)], itp_primary|itp_can_penetrate_shield|itp_type_crossbow|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_crossbow|itcf_carry_crossbow_back, 163, weight(5.00)|hit_points(700)|spd_rtng(38)|shoot_speed(52)|max_ammo(1)|thrust_damage(155,cut)|accuracy(131), imodbits_none], 
["light_steel_crossbow_alt_mode", "Light Arbalest alt mode", [("crossbow_e",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_crossbow_back, 0, weight(5.00)|head_armor(48)|body_armor(100)|leg_armor(16)|abundance(100)|hit_points(700)|spd_rtng(70)|weapon_length(90)|max_ammo(8)|swing_damage(86,blunt)|thrust_damage(63,blunt), imodbits_none], 
["heavy_steel_crossbow", "Heavy Arbalest", [("crossbow_c",0)], itp_primary|itp_can_penetrate_shield|itp_type_crossbow|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_crossbow|itcf_carry_crossbow_back, 189, weight(5.20)|hit_points(704)|spd_rtng(35)|shoot_speed(56)|max_ammo(1)|thrust_damage(181,cut)|accuracy(137), imodbits_none], 
["heavy_steel_crossbow_alt_mode", "Heavy Arbalest alt mode", [("crossbow_c",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_crossbow_back, 0, weight(5.20)|head_armor(48)|body_armor(100)|leg_armor(16)|abundance(100)|hit_points(704)|spd_rtng(68)|weapon_length(90)|max_ammo(8)|swing_damage(85,blunt)|thrust_damage(61,blunt), imodbits_none], 

#muskets
["handgonne_a", "Compact Handgonne", [("handgonne_a",0)], itp_primary|itp_can_penetrate_shield|itp_type_musket|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_musket|itcf_reload_musket|itcf_carry_spear, 155, weight(4.00)|hit_points(680)|spd_rtng(25)|shoot_speed(75)|weapon_length(21)|max_ammo(1)|thrust_damage(149,blunt)|accuracy(92), imodbits_none], 
["handgonne_a_alt_mode", "Compact Handgonne alt mode", [("handgonne_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_two_handed_wpn|itp_two_handed|itp_two_handed|itp_has_bayonet|itp_wooden_parry, itc_nodachi|itcf_carry_spear, 0, weight(4.00)|head_armor(4)|body_armor(100)|hit_points(680)|spd_rtng(77)|weapon_length(61)|max_ammo(8)|swing_damage(114,pierce)|thrust_damage(0,blunt), imodbits_none], 
["handgonne_b", "Handgonne", [("handgonne_b",0)], itp_primary|itp_can_penetrate_shield|itp_type_musket|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_musket|itcf_reload_musket|itcf_carry_spear, 175, weight(3.60)|hit_points(672)|spd_rtng(24)|shoot_speed(80)|weapon_length(19)|max_ammo(1)|thrust_damage(170,blunt)|accuracy(86), imodbits_none], 
["handgonne_b_alt_mode", "Handgonne alt mode", [("handgonne_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_parry, itc_poleaxe|itcf_carry_spear, 0, weight(3.60)|head_armor(3)|body_armor(100)|hit_points(672)|spd_rtng(79)|weapon_length(80)|max_ammo(8)|swing_damage(112,pierce)|thrust_damage(0,blunt), imodbits_none], 
["matchlock_arquebus_1", "Guarded Arquebus", [("arquebus",0)], itp_primary|itp_can_penetrate_shield|itp_type_musket|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_musket|itcf_reload_musket|itcf_carry_spear, 235, weight(5.00)|hit_points(700)|spd_rtng(22)|shoot_speed(92)|weapon_length(89)|max_ammo(1)|thrust_damage(227,blunt)|accuracy(136), imodbits_none], 
["matchlock_arquebus_1_alt_mode", "Guarded Arquebus alt mode", [("arquebus",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_spear, 0, weight(5.00)|head_armor(48)|body_armor(100)|leg_armor(24)|abundance(100)|hit_points(700)|spd_rtng(70)|weapon_length(100)|max_ammo(8)|swing_damage(86,blunt)|thrust_damage(60,blunt), imodbits_none], 
["matchlock_arquebus_2", "Arquebus", [("matchlock_1",0)], itp_primary|itp_can_penetrate_shield|itp_type_musket|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_musket|itcf_reload_musket|itcf_carry_spear, 252, weight(5.00)|hit_points(700)|spd_rtng(21)|shoot_speed(95)|weapon_length(91)|max_ammo(1)|thrust_damage(244,blunt)|accuracy(136), imodbits_none], 
["matchlock_arquebus_2_alt_mode", "Arquebus alt mode", [("matchlock_1",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_spear, 0, weight(5.00)|head_armor(48)|body_armor(100)|leg_armor(24)|abundance(100)|hit_points(700)|spd_rtng(70)|weapon_length(100)|max_ammo(8)|swing_damage(86,blunt)|thrust_damage(60,blunt), imodbits_none], 
["matchlock_arquebus_3", "Long Arquebus", [("matchlock_2",0)], itp_primary|itp_can_penetrate_shield|itp_type_musket|itp_two_handed|itp_cant_reload_while_moving|itp_cant_reload_on_horseback|itp_next_item_as_melee, itcf_shoot_musket|itcf_reload_musket|itcf_carry_spear, 256, weight(5.10)|hit_points(702)|spd_rtng(20)|shoot_speed(96)|weapon_length(98)|max_ammo(1)|thrust_damage(248,blunt)|accuracy(137), imodbits_none], 
["matchlock_arquebus_3_alt_mode", "Long Arquebus alt mode", [("matchlock_2",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_two_handed|itp_has_bayonet|itp_wooden_attack|itp_wooden_parry|itp_offset_musket|itp_can_knock_down, itc_staff|itcf_carry_spear, 0, weight(5.10)|head_armor(48)|body_armor(100)|leg_armor(44)|abundance(100)|hit_points(702)|spd_rtng(69)|weapon_length(100)|max_ammo(8)|swing_damage(86,blunt)|thrust_damage(55,blunt), imodbits_none], 

#pistols
["wheellock_pistol_1", "Pistol", [("wheellock_pistol",0)], itp_primary|itp_can_penetrate_shield|itp_type_pistol|itp_two_handed|itp_next_item_as_melee, itcf_shoot_pistol|itcf_carry_pistol_front_left|itcf_reload_pistol, 203, weight(2.50)|hit_points(650)|spd_rtng(26)|shoot_speed(85)|weapon_length(39)|max_ammo(1)|thrust_damage(133,blunt)|accuracy(89), imodbits_none], 
["wheellock_pistol_1_alt_mode", "Pistol alt mode", [("wheellock_pistol",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_has_bayonet|itp_offset_musket|itp_can_knock_down, itc_cleaver|itcf_carry_pistol_front_left, 0, weight(2.50)|head_armor(50)|body_armor(100)|hit_points(650)|spd_rtng(75)|weapon_length(30)|max_ammo(8)|swing_damage(49,blunt)|thrust_damage(0,blunt), imodbits_none], 

#arrows
["barbed_arrows", "Barbed Arrows", [("arrow_barbed",0),("arrow_barbed_flying",ixmesh_flying_ammo),("quiver_arrow_a",ixmesh_carry)], itp_type_arrows|itp_can_penetrate_shield|itp_default_ammo, itcf_carry_quiver_back_right, 120, weight(1.80)|leg_armor(2)|abundance(11)|weapon_length(80)|max_ammo(24)|swing_damage(50,cut), imodbits_none, [missile_hit_arrow]], 
["barbed_arrows_alt_mode", "Flaming Barbed Arrows", [("arrow_barbed",0),("arrow_barbed_flying",ixmesh_flying_ammo),("quiver_arrow_a",ixmesh_carry)], itp_type_arrows|itp_can_penetrate_shield, itcf_carry_quiver_back_right, 0, weight(1.80)|leg_armor(2)|abundance(11)|weapon_length(80)|max_ammo(24)|swing_damage(50,cut), imodbits_none, [missile_hit_fire]], 
["bodkin_arrows", "Bodkin Arrows", [("arrow_bodkin",0),("arrow_bodkin_flying",ixmesh_flying_ammo),("quiver_arrow_a",ixmesh_carry)], itp_type_arrows|itp_can_penetrate_shield, itcf_carry_quiver_back_right, 144, weight(1.80)|leg_armor(1)|abundance(13)|weapon_length(80)|max_ammo(24)|swing_damage(50,cut)|thrust_damage(1,cut), imodbits_none, [missile_hit_arrow]], 
["bodkin_arrows_alt_mode", "Flaming Bodkin Arrows", [("arrow_bodkin",0),("arrow_bodkin_flying",ixmesh_flying_ammo),("quiver_arrow_a",ixmesh_carry)], itp_type_arrows|itp_can_penetrate_shield, itcf_carry_quiver_back_right, 0, weight(1.80)|leg_armor(1)|abundance(13)|weapon_length(80)|max_ammo(24)|swing_damage(50,cut), imodbits_none, [missile_hit_fire]], 

#bolts
["barbed_bolts", "Barbed Bolts", [("bolt_barbed",0),("bolt_barbed_flying",ixmesh_flying_ammo),("quiver_bolt_a",ixmesh_carry)], itp_type_bolts|itp_can_penetrate_shield|itp_default_ammo, itcf_carry_quiver_right_vertical, 144, weight(1.40)|leg_armor(2)|abundance(11)|weapon_length(65)|max_ammo(18)|swing_damage(50,cut), imodbits_none, [missile_hit_bolt]], 
["barbed_bolts_alt_mode", "Flaming Barbed Bolts", [("bolt_barbed",0),("bolt_barbed_flying",ixmesh_flying_ammo),("quiver_bolt_a",ixmesh_carry)], itp_type_bolts|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 0, weight(1.40)|leg_armor(2)|abundance(11)|weapon_length(65)|max_ammo(18)|swing_damage(50,cut), imodbits_none, [missile_hit_fire]], 
["bodkin_bolts", "Bodkin Bolts", [("bolt_bodkin",0),("bolt_bodkin_flying",ixmesh_flying_ammo),("quiver_bolt_a",ixmesh_carry)], itp_type_bolts|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 162, weight(1.40)|leg_armor(1)|abundance(13)|weapon_length(65)|max_ammo(18)|swing_damage(50,cut)|thrust_damage(1,cut), imodbits_none, [missile_hit_bolt]], 
["bodkin_bolts_alt_mode", "Flaming Bodkin Bolts", [("bolt_bodkin",0),("bolt_bodkin_flying",ixmesh_flying_ammo),("quiver_bolt_a",ixmesh_carry)], itp_type_bolts|itp_can_penetrate_shield, itcf_carry_quiver_right_vertical, 0, weight(1.40)|leg_armor(1)|abundance(13)|weapon_length(65)|max_ammo(18)|swing_damage(50,cut), imodbits_none, [missile_hit_fire]], 

#bullets
["stone_bullets", "Stone Bullets", [("bullet_stone",0),("bullet_stone_flying",ixmesh_flying_ammo),("cartridge_bandolier",ixmesh_carry)], itp_type_bullets|itp_can_penetrate_shield|itp_default_ammo, itcf_carry_bow_back, 24, weight(0.40)|leg_armor(2)|abundance(45)|weapon_length(2)|max_ammo(12)|swing_damage(2,cut), imodbits_none, [missile_hit_bullet]], 
["lead_bullets", "Lead Bullets", [("bullet_lead",0),("bullet_lead_flying",ixmesh_flying_ammo),("cartridge_bandolier",ixmesh_carry)], itp_type_bullets|itp_can_penetrate_shield, itcf_carry_bow_back, 72, weight(0.40)|leg_armor(2)|abundance(43)|weapon_length(2)|max_ammo(12)|swing_damage(50,cut)|thrust_damage(4,blunt), imodbits_none, [missile_hit_bullet]], 

#horses
["horse_dark", "Dark Horse", [("horse_a_new",0)], itp_type_horse, 0, 214, weight(400.00)|body_armor(8)|hit_points(190)|horse_scale(95)|horse_speed(46)|horse_maneuver(35)|horse_charge(70), imodbits_none], 
["horse", "Horse", [("horse_b_new",0)], itp_type_horse, 0, 221, weight(427.00)|body_armor(9)|hit_points(194)|horse_scale(97)|horse_speed(47)|horse_maneuver(35)|horse_charge(74), imodbits_none], 
["sumpter_horse", "Sumpter Horse", [("sumpter_horse",0)], itp_type_horse, 0, 228, weight(456.00)|body_armor(10)|hit_points(198)|horse_scale(99)|horse_speed(48)|horse_maneuver(36)|horse_charge(82), imodbits_none], 
["saddle_horse", "Saddle Horse", [("saddle_horse",0)], itp_type_horse, 0, 228, weight(495.00)|body_armor(8)|hit_points(204)|horse_scale(102)|horse_speed(50)|horse_maneuver(38)|horse_charge(94), imodbits_none], 
["steppe_horse", "Steppe Horse", [("steppe_horse",0)], itp_type_horse, 0, 235, weight(526.00)|body_armor(9)|hit_points(208)|horse_scale(104)|horse_speed(51)|horse_maneuver(38)|horse_charge(99), imodbits_none], 
["arabian_horse_a", "Desert Horse", [("arabian_horse_a",0)], itp_type_horse, 0, 226, weight(480.00)|body_armor(8)|hit_points(202)|horse_scale(101)|horse_speed(49)|horse_maneuver(37)|horse_charge(88), imodbits_none], 
["courser", "Courser", [("courser",0)], itp_type_horse, 0, 240, weight(544.00)|body_armor(10)|hit_points(210)|horse_scale(105)|horse_speed(51)|horse_maneuver(38)|horse_charge(103), imodbits_none], 
["arabian_horse_b", "Arabian Horse", [("arabian_horse_b",0)], itp_type_horse, 0, 230, weight(509.00)|body_armor(8)|hit_points(206)|horse_scale(103)|horse_speed(50)|horse_maneuver(38)|horse_charge(96), imodbits_none], 
["hunter", "Hunter", [("hunting_horse",0)], itp_type_horse, 0, 237, weight(542.00)|body_armor(9)|hit_points(210)|horse_scale(105)|horse_speed(51)|horse_maneuver(38)|horse_charge(102), imodbits_none], 
["charger", "Charger", [("charger_new",0)], itp_type_horse, 0, 819, weight(565.00)|body_armor(41)|hit_points(204)|horse_scale(102)|horse_speed(45)|horse_maneuver(34)|horse_charge(96), imodbits_none], 
["warhorse_steppe", "Steppe Charger", [("warhorse_steppe",0)], itp_type_horse, 0, 810, weight(614.00)|body_armor(40)|hit_points(210)|horse_scale(105)|horse_speed(46)|horse_maneuver(35)|horse_charge(107), imodbits_none], 
["kher_warhorse", "Crimean Charger", [("kher_warhorse",0)], itp_type_horse, 0, 834, weight(567.00)|body_armor(42)|hit_points(204)|horse_scale(102)|horse_speed(45)|horse_maneuver(34)|horse_charge(96), imodbits_none], 
["sar_warhorse", "Desert War Horse", [("sar_warhorse",0)], itp_type_horse, 0, 812, weight(631.00)|body_armor(40)|hit_points(212)|horse_scale(106)|horse_speed(47)|horse_maneuver(35)|horse_charge(110), imodbits_none], 
["plated_charger", "Plated Charger", [("charger_plate",0)], itp_type_horse, 0, 1356, weight(718.00)|body_armor(60)|hit_points(216)|horse_scale(108)|horse_speed(44)|horse_maneuver(33)|horse_charge(118), imodbits_none], 
["camel", "Camel", [("camel",0)], itp_type_horse, 0, 240, weight(778.00)|body_armor(0)|hit_points(240)|horse_scale(120)|horse_speed(45)|horse_maneuver(34)|horse_charge(132), imodbits_none], 

]

# weight = 0
# head_armor = 1
# body_armor = 2
# leg_armor = 3
# difficulty = 4
# hit_points = 5
# speed_rating = 6
# missile_speed = 7
# weapon_length = 8
# max_ammo = 9
# swing_damage = 10
# swing_type = 11
# thrust_damage = 12
# thrust_type = 13
# abundance = 14

#generate item modifiers
for i_mod in range (1, 4):
	items += ["mod_"+str(i_mod)+"_items_begin", "Mod "+str(i_mod)+" Items Begin", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],

	for i_item in range (itm_regular_items_begin+1, itm_mod_1_items_begin):
		values = [ #get base values
			get_weight(items[i_item][6]),#0
			get_head_armor(items[i_item][6]),#1
			get_body_armor(items[i_item][6]),#2
			get_leg_armor(items[i_item][6]),#3
			get_difficulty(items[i_item][6]),#4
			get_hit_points(items[i_item][6]),#5
			get_speed_rating(items[i_item][6]),#6
			get_missile_speed(items[i_item][6]),#7
			get_weapon_length(items[i_item][6]),#8
			get_max_ammo(items[i_item][6]),#9
			get_swing_damage(items[i_item][6])&0xff,#10 #damage
			get_swing_damage(items[i_item][6])/0x100,#11 #type
			get_thrust_damage(items[i_item][6])&0xff,#12 #damage
			get_thrust_damage(items[i_item][6])/0x100,#13 #type
			get_abundance(items[i_item][6])#14
			]

		item_type = items[i_item][3]&0x000000ff
		
		if item_type >= itp_type_one_handed_wpn and item_type <= itp_type_polearm:
			if values[10] > 0:
				values[10] += i_mod
			if values[12] > 0:
				values[12] += i_mod
		
		elif item_type == itp_type_shield:
			values[2] += i_mod
			values[5] += i_mod*10
		
		elif item_type >= itp_type_bow and item_type <= itp_type_thrown or item_type == itp_type_pistol or item_type == itp_type_musket:
			values[12] += i_mod
		
		elif item_type == itp_type_arrows or item_type == itp_type_bolts or item_type == itp_type_bullets:
			values[14] += i_mod
		
		elif item_type >= itp_type_head_armor and item_type <= itp_type_hand_armor:
			for i in range(1, 4):
				if values[i] > 0:
					values[i] += i_mod
		
		elif item_type == itp_type_horse:
			values[2] += i_mod
			values[5] += i_mod*10
			values[6] += i_mod
			values[7] += i_mod

		trigger_list = [] #check for triggers
		if (len(items[i_item]) > 8):
			trigger_list = items[i_item][8]

		#faction_list = [] #check for factions #NOT USED
		#if (len(items[i_item]) > 9):
			#faction_list = items[i_item][9]

		items += [
			str(i_mod)+"_"+ items[i_item][0],#Item id
			"+" +str(i_mod)+ " "+ items[i_item][1],#Item name
			items[i_item][2],#List of meshes
			items[i_item][3],#Item flags
			items[i_item][4],#Item capabilities
			items[i_item][5] * i_mod * 1.5,#Item value
			weight(values[0])|
			head_armor(values[1])|
			body_armor(values[2])|
			leg_armor(values[3])|
			difficulty(values[4])|
			hit_points(values[5])|
			spd_rtng(values[6])|
			shoot_speed(values[7])|
			weapon_length(values[8])|
			max_ammo(values[9])|
			swing_damage(values[10],values[11])|
			thrust_damage(values[12],values[13])|
			abundance(values[14]),
			items[i_item][7],#Modifier bits
			trigger_list
			#faction_list #NOT USED
		],

items += ["special_items_begin", "Special Items Begin", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],
items += [
#special
["grenade", "Grenade", [("grenade",0),("grenade_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown, itcf_throw_stone, 413, weight(1.20)|head_armor(255)|abundance(100)|hit_points(1104)|spd_rtng(63)|shoot_speed(13)|weapon_length(8)|max_ammo(1)|swing_damage(50,blunt)|thrust_damage(75,blunt)|accuracy(104), imodbits_none, [missile_hit_bomb]], 
["firepot", "Firepot", [("firepot",0),("firepot_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown, itcf_throw_stone, 472, weight(1.60)|head_armor(255)|abundance(100)|hit_points(632)|spd_rtng(59)|shoot_speed(12)|weapon_length(15)|max_ammo(1)|swing_damage(8,blunt)|thrust_damage(69,blunt)|accuracy(103), imodbits_none, [init_firepot, missile_hit_firepot]], 

["linstock", "Linstock", [("linstock",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_dagger_front_right, 10, weight(0.70)|hit_points(1235)|spd_rtng(91)|weapon_length(56)|swing_damage(17, blunt)|thrust_damage(0, blunt), imodbits_none], 
["refiller", "Refiller", [("ramrod_a",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed, itc_cutting_spear|itcf_carry_axe_back, 64, weight(3.80)|hit_points(1390)|spd_rtng(78)|weapon_length(138)|swing_damage(0, blunt)|thrust_damage(42, blunt), imodbits_none], 
["refiller_alt_mode", "Ramrod", [("ramrod_b",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed, itc_cutting_spear|itcf_carry_axe_back, 0, weight(3.80)|hit_points(1390)|spd_rtng(78)|weapon_length(127)|swing_damage(0, blunt)|thrust_damage(42, blunt), imodbits_none], 

["engineer_hammer", "Engineer Hammer", [("engineer_hammer",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_unbalanced|itp_can_knock_down, itc_cleaver|itcf_carry_dagger_front_right, 41, weight(1.00)|hit_points(2210)|spd_rtng(85)|weapon_length(50)|swing_damage(27, blunt)|thrust_damage(0, blunt), imodbits_none], 
["engineer_shovel", "Engineer Shovel", [("engineer_shovel",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_unbalanced, itc_cutting_spear|itcf_carry_spear, 59, weight(2.00)|hit_points(1300)|spd_rtng(84)|weapon_length(100)|swing_damage(0, blunt)|thrust_damage(25, cut), imodbits_none], 
["engineer_axe", "Engineer Axe", [("engineer_axe",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_bonus_against_shield|itp_unbalanced, itc_poleaxe|itcf_carry_mace_left_hip, 104, weight(2.90)|hit_points(2305)|spd_rtng(76)|weapon_length(86)|swing_damage(64, cut)|thrust_damage(0, blunt), imodbits_none], 
["engineer_material", "Engineer Material", [("engineer_material_single",0),("engineer_material",ixmesh_carry),("engineer_material",ixmesh_inventory)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_axe|itcf_carry_board_shield|itcf_show_holster_when_drawn, 0, weight(15.00)|head_armor(255)|abundance(100)|hit_points(610)|spd_rtng(70)|shoot_speed(17)|weapon_length(60)|max_ammo(60)|swing_damage(8,blunt)|thrust_damage(38,blunt)|food_quality(0)|accuracy(106), imodbits_none, [missile_hit_thrown]], 
["engineer_material_alt_mode", "Engineer Material alt mode", [("engineer_material_single",0),("engineer_material",ixmesh_carry),("engineer_material",ixmesh_inventory)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_attack|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_scimitar|itcf_carry_board_shield|itcf_show_holster_when_drawn, 0, weight(0.50)|hit_points(610)|spd_rtng(88)|weapon_length(70)|swing_damage(11, blunt)|thrust_damage(0, blunt), imodbits_none], 

["surgeon_kit", "Surgeon Kit", [("surgeon_dressing",0),("surgeon_dressing_missile",ixmesh_flying_ammo),("surgeon_box",ixmesh_carry),("surgeon_box",ixmesh_inventory)], itp_primary|itp_can_penetrate_shield|itp_type_thrown|itp_next_item_as_melee, itcf_throw_axe|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 260, weight(4.00)|head_armor(255)|abundance(100)|hit_points(604)|spd_rtng(73)|shoot_speed(21)|weapon_length(6)|max_ammo(20)|thrust_damage(0,blunt)|food_quality(0)|accuracy(107), imodbits_none, [missile_hit_thrown]], 
["surgeon_kit_alt_mode", "Surgeon Kit alt mode", [("surgeon_dressing",0),("surgeon_dressing_missile",ixmesh_flying_ammo),("surgeon_box",ixmesh_carry),("surgeon_box",ixmesh_inventory)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_can_knock_down, itc_cleaver|itcf_carry_quiver_back|itcf_show_holster_when_drawn, 0, weight(0.20)|hit_points(604)|spd_rtng(98)|weapon_length(6)|swing_damage(0, blunt)|thrust_damage(0, blunt), imodbits_none, [surgeon_kit_self]], 

["missile_ignition_kit", "Missile Ignition Kit", [("surgeon_dressing",0),("surgeon_dressing_missile",ixmesh_flying_ammo)], itp_primary|itp_can_penetrate_shield|itp_type_thrown, itcf_throw_stone|itcf_carry_dagger_front_right, 78, weight(1.20)|hit_points(604)|spd_rtng(75)|shoot_speed(0)|weapon_length(6)|max_ammo(6)|thrust_damage(0, blunt)|food_quality(0)|accuracy(107), imodbits_none, [missile_hit_thrown]], 

["torch", "Torch", [("torch",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_wooden_attack|itp_wooden_parry|itp_unbalanced|itp_can_knock_down, itc_scimitar, 10, weight(0.90)|hit_points(1245)|spd_rtng(87)|weapon_length(54)|swing_damage(20, blunt)|thrust_damage(0, blunt), imodbits_none, [(ti_on_init_item,[(neg|multiplayer_is_dedicated_server),(set_position_delta,0,50,0),(particle_system_add_new,"psys_fire_xs"),(particle_system_add_new,"psys_smoke_xs")])]], 

["ladder_5m", "Ladder", [("ladder_5m",0)], itp_primary|itp_no_blur|itp_crush_through|itp_type_one_handed_wpn|itp_no_parry|itp_wooden_attack|itp_two_handed, itcf_thrust_polearm, 40, weight(20.00)|hit_points(2200)|spd_rtng(50)|weapon_length(138)|swing_damage(0, blunt)|thrust_damage(0, blunt), imodbits_none, [deploy_ladder]], 

]

items += [
	#Admin Items
	["admin_items_begin", "{!}no name", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],
	["big_head", "Big Head", [("big_head",0)], itp_type_head_armor|itp_covers_head, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none ],

	["ghost_head", "Ghost Head", [("invalid_item",0)], itp_type_head_armor|itp_covers_head, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none ],
	["ghost_body", "Ghost Body", [("invalid_item",0)], itp_type_body_armor, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none ],
	["ghost_hands", "Ghost Hands", [("invalid_item",0)], itp_type_hand_armor, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none ],
	["ghost_legs", "Ghost Legs", [("invalid_item",0)], itp_type_foot_armor, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none ],

	["bush_body", "Bush Body", [("body_bush",0)], itp_type_body_armor, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(0)|leg_armor(0)|difficulty(0), imodbits_none ],
	["rock_body", "Rock Body", [("body_rock",0)], itp_type_body_armor, 0, 0, weight(0)|abundance(0)|head_armor(255)|body_armor(255)|leg_armor(255)|difficulty(0), imodbits_none ],
	["barrel_body", "Barrel Body", [("body_barrel",0)], itp_type_body_armor, 0, 0, weight(0)|abundance(0)|head_armor(0)|body_armor(50)|leg_armor(25)|difficulty(0), imodbits_none ],

	["big_rocks", "Big Rocks", [("big_throw_rock",0)],
	 itp_type_thrown|itp_primary|itp_can_penetrate_shield|itp_next_item_as_melee,
	 itcf_throw_stone|itcf_carry_board_shield,
	 0, weight(0)|difficulty(0)|hit_points(100)|spd_rtng(75)|shoot_speed(20)|thrust_damage(255,blunt)|accuracy(255)|max_ammo(100)|weapon_length(50), 
	 imodbits_none ],
	 
	["big_rocks_alt_mode", "Big Rocks alt mode", [("big_throw_rock",0)], 
	 itp_type_two_handed_wpn|itp_primary|itp_crush_through|itp_no_blur,
	 itc_greatsword|itcf_carry_board_shield,
	 0, weight(0)|difficulty(0)|hit_points(100)|spd_rtng(75)|weapon_length(100)|swing_damage(255,blunt)|thrust_damage(255,blunt), 
	 imodbits_none ],	
	 
	["flaming_arrows", "Flaming Arrows", [("arrow_bodkin",0),("arrow_bodkin_flying",ixmesh_flying_ammo),("quiver_arrow_a",ixmesh_carry)], itp_type_arrows|itp_can_penetrate_shield, itcf_carry_quiver_back_right, 0, weight(0)|weapon_length(80)|max_ammo(255)|thrust_damage(0, pierce), imodbits_none, [missile_hit_fire]], 

	["autofire_musket", "Autofire Musket", [("arquebus",0)], itp_type_musket|itp_merchandise|itp_two_handed|itp_primary|itp_can_penetrate_shield|itp_next_item_as_melee, itcf_shoot_musket|itcf_carry_spear|itcf_reload_musket, 0, weight(0)|difficulty(0)|spd_rtng(100)|shoot_speed(100)|thrust_damage(100,blunt)|max_ammo(-1)|accuracy(100), imodbits_none ],
	["autofire_musket_melee", "{!}no name", [("arquebus",0)], itp_type_polearm|itp_wooden_parry|itp_two_handed|itp_primary|itp_cant_use_on_horseback|itp_has_bayonet|itp_no_blur|itp_offset_musket, itc_staff|itcf_carry_spear, 0, weight(0)|difficulty(0)|spd_rtng(75)|weapon_length(100)|swing_damage(11,blunt)|thrust_damage(6,blunt), imodbits_none ],
	
	["ultimate_pike", "Ultimate Pike", [("admin_pike",0)], itp_type_polearm|itp_merchandise|itp_wooden_parry|itp_primary|itp_bonus_against_shield|itp_offset_lance|itp_couchable|itp_crush_through|itp_can_knock_down|itp_no_blur, itc_spear, 0, weight(0)|difficulty(0)|spd_rtng(100)|weapon_length(718)|swing_damage(200,blunt)|thrust_damage(200,pierce), imodbits_none ],
	["spam_grenade", "Spam Grenade", [("grenade",0),("grenade_missile",ixmesh_flying_ammo)], itp_type_thrown|itp_merchandise|itp_primary|itp_can_penetrate_shield|custom_kill_info(4), itcf_throw_stone|itcf_carry_quiver_right_vertical, 0, weight(0)|difficulty(0)|spd_rtng(100)|shoot_speed(50)|thrust_damage(0,blunt)|max_ammo(100)|weapon_length(10)|accuracy(100), imodbits_none, [missile_hit_bomb] ],
	["horse_small", "My Little Pony", [("horse_a_new",0)], itp_type_horse|itp_merchandise, 0, 0, abundance(0)|hit_points(1000)|body_armor(50)|difficulty(0)|horse_speed(60)|horse_maneuver(100)|horse_charge(30)|horse_scale(50), imodbits_none ],
	["horse_big", "My Big Pony", [("charger_plate",0)], itp_type_horse|itp_merchandise, 0, 0, abundance(0)|hit_points(1000)|body_armor(100)|difficulty(0)|horse_speed(100)|horse_maneuver(70)|horse_charge(150)|horse_scale(300), imodbits_none ],

	###Artillery###
	["cannon_ammo_begin", "{!}no name", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],
	["round_shot_32lb", "Round Shot", [("round_shot_32lb",0)], itp_type_one_handed_wpn|itp_primary, 0, 0, weight(14.5), imodbits_none ],
	["canister_shot_32lb", "Grape Shot", [("grape_shot_32lb",0)], itp_type_one_handed_wpn|itp_primary, 0, 0, weight(14.5), imodbits_none ],

	["cannon_missile_begin", "{!}no name", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],
	["round_shot_32lb_missile", "Round Shot", [("invalid_item",0),("round_shot_32lb",ixmesh_flying_ammo)], itp_type_bullets|itp_can_penetrate_shield, 0, 0, max_ammo(1)|weapon_length(8)|thrust_damage(0,blunt), imodbits_none, [missile_hit_roundshot] ],
	["canister_shot_32lb_missile", "Grape Shot", [("invalid_item",0),("bullet_stone_flying",ixmesh_flying_ammo)], itp_type_bullets|itp_can_penetrate_shield, 0, 0, max_ammo(100)|weapon_length(2)|thrust_damage(0,blunt), imodbits_none, [missile_hit_bullet] ],

	["cannon_barrel_1_begin", "{!}no name", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],
	["barrel_1_round_shot", "{!}no name", [("invalid_item",0)], itp_type_musket|itp_primary|itp_can_penetrate_shield|custom_kill_info(6), 0, 0, shoot_speed(204)|spd_rtng(100)|thrust_damage(255,blunt)|accuracy(1), imodbits_none ],
	["barrel_1_canister_shot", "{!}no name", [("invalid_item",0)], itp_type_musket|itp_primary|itp_can_penetrate_shield|custom_kill_info(5), 0, 0, shoot_speed(204)|spd_rtng(75)|thrust_damage(255,blunt)|accuracy(5), imodbits_none ],

	#other shit
	["ground_sml", "{!}no name", [("ground_sml",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield, itcf_throw_stone, 0, weight(0.4)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(5,blunt)|accuracy(100)|max_ammo(1)|weapon_length(5), imodbits_none, [missile_hit_bullet]],
	["ground_med", "{!}no name", [("ground_med",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield, itcf_throw_stone, 0, weight(1.6)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(20,blunt)|accuracy(100)|max_ammo(1)|weapon_length(10), imodbits_none, [missile_hit_bullet]],
	["ground_big", "{!}no name", [("ground_big",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield, itcf_throw_stone, 0, weight(6.4)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(80,blunt)|accuracy(100)|max_ammo(1)|weapon_length(20), imodbits_none, [missile_hit_bullet]],
	
	["stone_sml", "{!}no name", [("stone_sml",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield, itcf_throw_stone, 0, weight(0.4)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(5,blunt)|accuracy(100)|max_ammo(1)|weapon_length(5), imodbits_none, [missile_hit_bullet]],
	["stone_med", "{!}no name", [("stone_med",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield, itcf_throw_stone, 0, weight(1.6)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(20,blunt)|accuracy(100)|max_ammo(1)|weapon_length(10), imodbits_none, [missile_hit_bullet]],
	["stone_big", "{!}no name", [("stone_big",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield, itcf_throw_stone, 0, weight(6.4)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(80,blunt)|accuracy(100)|max_ammo(1)|weapon_length(20), imodbits_none, [missile_hit_bullet]],
	
	["wood_sml", "{!}no name", [("wood_sml",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield|itp_no_pick_up_from_ground, itcf_throw_axe, 0, weight(0.5)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(6,blunt)|accuracy(100)|max_ammo(1)|weapon_length(10), imodbits_none, [missile_hit_bullet]],
	["wood_med", "{!}no name", [("wood_med",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield|itp_no_pick_up_from_ground, itcf_throw_axe, 0, weight(1.5)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(18,blunt)|accuracy(100)|max_ammo(1)|weapon_length(20), imodbits_none, [missile_hit_bullet]],
	["wood_big", "{!}no name", [("wood_big",0)], itp_type_thrown|itp_primary|itp_can_penetrate_shield|itp_no_pick_up_from_ground, itcf_throw_axe, 0, weight(2.5)|difficulty(0)|hit_points(0)|spd_rtng(100)|shoot_speed(10)|thrust_damage(30,blunt)|accuracy(100)|max_ammo(1)|weapon_length(30), imodbits_none, [missile_hit_bullet]],
	
	["human_to_animal", "{!}", [("invalid_item",0)], itp_type_crossbow, 0, 0, 0, imodbits_none],
	["animal_to_human", "{!}", [("invalid_item",0)], itp_type_crossbow, 0, 0, 0, imodbits_none],

	#symbols
	["weapon_round_shot", "{!}", [("invalid_item",0)], itp_type_crossbow|custom_kill_info(6), 0, 0, 0, imodbits_none],
	["weapon_grape_shot", "{!}", [("invalid_item",0)], itp_type_crossbow|custom_kill_info(5), 0, 0, 0, imodbits_none],
	["weapon_explosion", "{!}", [("invalid_item",0)], itp_type_crossbow|custom_kill_info(4), 0, 0, 0, imodbits_none],
	["weapon_fire", "{!}", [("invalid_item",0)], itp_type_crossbow|custom_kill_info(3), 0, 0, 0, imodbits_none],
	["weapon_drown", "{!}", [("invalid_item",0)], itp_type_crossbow|custom_kill_info(2), 0, 0, 0, imodbits_none],
	["weapon_bleed", "{!}", [("invalid_item",0)], itp_type_crossbow|custom_kill_info(1), 0, 0, 0, imodbits_none],
]

items += [
	["practice_arrows_2", "Blunt Arrows", [("invalid_item",0),("invalid_item",ixmesh_flying_ammo),("invalid_item",ixmesh_carry)],
	 itp_type_arrows|itp_merchandise|itp_can_penetrate_shield, 
	 itcf_carry_quiver_back, 
	 72, weight(1.7)|weapon_length(82)|thrust_damage(0,blunt)|max_ammo(24),
	 imodbits_none, [missile_hit_arrow] ],
]

items += ["items_end", "Items End", [("invalid_item",0)], itp_type_horse, 0, 0, 0, imodbits_none ],
#MOD end