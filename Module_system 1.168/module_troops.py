import random

from header_common import *
from header_items import *
from header_troops import *
from header_skills import *
from ID_factions import *
from ID_items import *
from ID_scenes import *

####################################################################################################################
#  Each troop contains the following fields:
#  1) Troop id (string): used for referencing troops in other files. The prefix trp_ is automatically added before each troop-id .
#  2) Troop name (string).
#  3) Plural troop name (string).
#  4) Troop flags (int). See header_troops.py for a list of available flags
#  5) Scene (int) (only applicable to heroes) For example: scn_reyvadin_castle|entry(1) puts troop in reyvadin castle's first entry point
#  6) Reserved (int). Put constant "0" or 0.
#  7) Faction (int)
#  8) Inventory (list): Must be a list of items
#  9) Attributes (int): Example usage:
#           str_6|agi_6|int_4|cha_5|level(5)
# 10) Weapon proficiencies (int): Example usage:
#           wp_one_handed(55)|wp_two_handed(90)|wp_polearm(36)|wp_archery(80)|wp_crossbow(24)|wp_throwing(45)
#     The function wp(x) will create random weapon proficiencies close to value x.
#     To make an expert archer with other weapon proficiencies close to 60 you can use something like:
#           wp_archery(160) | wp(60)
# 11) Skills (int): See header_skills.py to see a list of skills. Example:
#           knows_ironflesh_3|knows_power_strike_2|knows_athletics_2|knows_riding_2
# 12) Face code (int): You can obtain the face code by pressing ctrl+E in face generator screen
# 13) Face code (int)(2) (only applicable to regular troops, can be omitted for heroes):
#     The game will create random faces between Face code 1 and face code 2 for generated troops
# 14) Troop image (string): If this variable is set, the troop will use an image rather than its 3D visual during the conversations
####################################################################################################################

def wp(x):
  n = 0
  n |= wp_one_handed(x)
  n |= wp_two_handed(x)
  n |= wp_polearm(x)
  n |= wp_archery(x)
  n |= wp_crossbow(x)
  n |= wp_throwing(x)
  n |= wp_firearm(x)
  return n

def wpe(m,a,c,t,f):
   n = 0
   n |= wp_one_handed(m)
   n |= wp_two_handed(m)
   n |= wp_polearm(m)
   n |= wp_archery(a)
   n |= wp_crossbow(c)
   n |= wp_throwing(t)
   n |= wp_firearm(f)
   return n

def wpex(o,w,p,a,c,t,f):
   n = 0
   n |= wp_one_handed(o)
   n |= wp_two_handed(w)
   n |= wp_polearm(p)
   n |= wp_archery(a)
   n |= wp_crossbow(c)
   n |= wp_throwing(t)
   n |= wp_firearm(f)
   return n
   
def wp_melee(x):
  n = 0
  n |= wp_one_handed(x + 20)
  n |= wp_two_handed(x)
  n |= wp_polearm(x + 10)
  return n

#Skills
#MOD begin
wp0 = 100

base_att = str_45|agi_45
base_skl = knows_trade_10|knows_horse_archery_5
base_wpt = wpex(wp0,wp0,wp0,600,wp0,wp0,wp0)

def_attrib = str_0 | agi_0 | int_0 | cha_0
def_attrib_multiplayer = str_0 | agi_0 | int_0 | cha_0

special_att = base_att
special_skl = base_skl
special_wpt = base_wpt

pikeman_att = base_att
pikeman_skl = base_skl
pikeman_wpt = base_wpt

infantry_att = base_att
infantry_skl = base_skl
infantry_wpt = base_wpt

cavalry_att = base_att
cavalry_skl = base_skl
cavalry_wpt = base_wpt

archer_att = base_att
archer_skl = base_skl
archer_wpt = base_wpt

horse_archer_att = base_att
horse_archer_skl = base_skl
horse_archer_wpt = base_wpt

crossbowman_att = base_att
crossbowman_skl = base_skl
crossbowman_wpt = base_wpt

mounted_crossbowman_att = base_att
mounted_crossbowman_skl = base_skl
mounted_crossbowman_wpt = base_wpt

musketeer_att = base_att
musketeer_skl = base_skl
musketeer_wpt = base_wpt

mounted_musketeer_att = base_att
mounted_musketeer_skl = base_skl
mounted_musketeer_wpt = base_wpt

# special_att = str_12|agi_18
# special_skl = knows_power_strike_3|knows_athletics_6
# special_wpt = wpex(wp0,wp0,wp0,wp0,wp0,wp0,wp0)

# pikeman_att = str_12|agi_18
# pikeman_skl = knows_power_strike_4|knows_power_throw_3|knows_athletics_6
# pikeman_wpt = wpex(wp0 +75,wp0 +75,wp0 +150,wp0 +0,wp0 +0,wp0 +50,wp0 +0)

# infantry_att = str_15|agi_15
# infantry_skl = knows_power_strike_5|knows_power_throw_5|knows_athletics_5
# infantry_wpt = wpex(wp0 +150,wp0 +150,wp0 +150,wp0 +0,wp0 +0,wp0 +150,wp0 +0)

# cavalry_att = str_12|agi_12
# cavalry_skl = knows_power_strike_4|knows_power_throw_3|knows_riding_4
# cavalry_wpt = wpex(wp0 +100,wp0 +100,wp0 +100,wp0 +0,wp0 +0,wp0 +100,wp0 +0)

# archer_att = str_18|agi_12
# archer_skl = knows_power_pull_6|knows_power_strike_3|knows_athletics_4
# archer_wpt = wpex(wp0 +50,wp0 +50,wp0 +50,wp0 +350,wp0 +0,wp0 +0,wp0 +0)

# horse_archer_att = str_18|agi_12
# horse_archer_skl = knows_power_pull_6|knows_power_strike_3|knows_riding_2|knows_horse_archery_5
# horse_archer_wpt = wpex(wp0 +50,wp0 +50,wp0 +50,wp0 +350,wp0 +0,wp0 +0,wp0 +0)

# crossbowman_att = str_15|agi_12
# crossbowman_skl = knows_power_pull_4|knows_power_strike_3|knows_athletics_4
# crossbowman_wpt = wpex(wp0 +50,wp0 +50,wp0 +50,wp0 +0,wp0 +150,wp0 +0,wp0 +0)

# mounted_crossbowman_att = str_12|agi_15
# mounted_crossbowman_skl = knows_power_pull_3|knows_power_strike_3|knows_riding_2|knows_horse_archery_5
# mounted_crossbowman_wpt = wpex(wp0 +50,wp0 +50,wp0 +50,wp0 +0,wp0 +150,wp0 +0,wp0 +0)

# musketeer_att = str_15|agi_12
# musketeer_skl = knows_power_reload_3|knows_power_strike_3|knows_athletics_4
# musketeer_wpt = wpex(wp0 +50,wp0 +50,wp0 +50,wp0 +0,wp0 +0,wp0 +0,wp0 +125)

# mounted_musketeer_att = str_12|agi_15
# mounted_musketeer_skl = knows_power_reload_2|knows_power_strike_3|knows_riding_2|knows_horse_archery_5
# mounted_musketeer_wpt = wpex(wp0 +50,wp0 +50,wp0 +50,wp0 +0,wp0 +0,wp0 +0,wp0 +125)
#MOD end

kingdom_1_face_young = 0x0000000000000001124000000020000000000000001c00800000000000000000
kingdom_1_face_middle  = 0x0000000800000001124000000020000000000000001c00800000000000000000
kingdom_1_face_old   = 0x0000000ff10023084deeffffffffffff00000000001efff90000000000000000

kingdom_2_face_young = 0x0000000000000001124000000020000000000000001c00800000000000000000
kingdom_2_face_middle  = 0x0000000800000001124000000020000000000000001c00800000000000000000
kingdom_2_face_old   = 0x0000000ff100230c4deeffffffffffff00000000001efff90000000000000000

kingdom_3_face_young = 0x0000000009003109207000000000000000000000001c80470000000000000000
kingdom_3_face_middle  = 0x00000007c9003109207000000000000000000000001c80470000000000000000
kingdom_3_face_old   = 0x0000000fff0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000

kingdom_4_face_young = 0x0000000009002003140000000000000000000000001c80400000000000000000
kingdom_4_face_middle  = 0x0000000849002003140000000000000000000000001c80400000000000000000
kingdom_4_face_old   = 0x0000000fc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

kingdom_5_face_young   = 0x0000000400000001124000000020000000000000001c00800000000000000000
kingdom_5_face_middle  = 0x00000008310023084deeffffffffffff00000000001efff90000000000000000
kingdom_5_face_old     = 0x0000000c710023084deeffffffffffff00000000001efff90000000000000000

kingdom_6_face_young = 0x0000000000000001124000000020000000000000001c00800000000000000000
kingdom_6_face_middle  = 0x0000000800000001124000000020000000000000001c00800000000000000000
kingdom_6_face_old   = 0x0000000fc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

kingdom_7_face_young   = 0x0000000400000001124000000020000000000000001c00800000000000000000
kingdom_7_face_middle  = 0x00000007c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
kingdom_7_face_old     = 0x0000000bc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

kingdom_8_face_young = 0x00000000000062c76ddcdf7feefbffff00000000001efdbc0000000000000000
kingdom_8_face_middle  = 0x00000007c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
kingdom_8_face_old   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

kingdom_9_face_young = 0x00000000310023084deeffffffffffff00000000001efff90000000000000000
kingdom_9_face_middle  = 0x00000008310023084deeffffffffffff00000000001efff90000000000000000
kingdom_9_face_old   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

kingdom_10_face_young = 0x000000003f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
kingdom_10_face_middle  = 0x000000077f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
kingdom_10_face_old     = 0x0000000b3f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000

kingdom_11_face_young = 0x0000000000000001124000000020000000000000001c00800000000000000000
kingdom_11_face_middle  = 0x0000000800000001124000000020000000000000001c00800000000000000000
kingdom_11_face_old   = 0x0000000ff10023084deeffffffffffff00000000001efff90000000000000000

kingdom_12_face_young = 0x0000000000000001124000000020000000000000001c00800000000000000000
kingdom_12_face_middle  = 0x0000000800000001124000000020000000000000001c00800000000000000000
kingdom_12_face_old   = 0x0000000ff100230c4deeffffffffffff00000000001efff90000000000000000

kingdom_13_face_young = 0x0000000009003109207000000000000000000000001c80470000000000000000
kingdom_13_face_middle  = 0x00000007c9003109207000000000000000000000001c80470000000000000000
kingdom_13_face_old   = 0x0000000fff0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000

kingdom_14_face_young = 0x0000000009002003140000000000000000000000001c80400000000000000000
kingdom_14_face_middle  = 0x0000000849002003140000000000000000000000001c80400000000000000000
kingdom_14_face_old   = 0x0000000fc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

kingdom_15_face_young   = 0x0000000400000001124000000020000000000000001c00800000000000000000
kingdom_15_face_middle  = 0x00000008310023084deeffffffffffff00000000001efff90000000000000000
kingdom_15_face_old     = 0x0000000c710023084deeffffffffffff00000000001efff90000000000000000

kingdom_16_face_young = 0x0000000000000001124000000020000000000000001c00800000000000000000
kingdom_16_face_middle  = 0x0000000800000001124000000020000000000000001c00800000000000000000
kingdom_16_face_old   = 0x0000000fc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

kingdom_17_face_young   = 0x0000000400000001124000000020000000000000001c00800000000000000000
kingdom_17_face_middle  = 0x00000007c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
kingdom_17_face_old     = 0x0000000bc00062c76ddcdf7feefbffff00000000001efdbc0000000000000000

kingdom_18_face_young = 0x00000000000062c76ddcdf7feefbffff00000000001efdbc0000000000000000
kingdom_18_face_middle  = 0x00000007c00062c76ddcdf7feefbffff00000000001efdbc0000000000000000
kingdom_18_face_old   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

kingdom_19_face_young = 0x00000000310023084deeffffffffffff00000000001efff90000000000000000
kingdom_19_face_middle  = 0x00000008310023084deeffffffffffff00000000001efff90000000000000000
kingdom_19_face_old   = 0x0000000fc0000001124000000020000000000000001c00800000000000000000

kingdom_20_face_young = 0x000000003f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
kingdom_20_face_middle  = 0x000000077f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000
kingdom_20_face_old     = 0x0000000b3f0061cd6d7ffbdf9df6ebee00000000001ffb7f0000000000000000

tf_guarantee_all = tf_guarantee_boots|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_helmet|tf_guarantee_horse|tf_guarantee_shield|tf_guarantee_ranged
tf_guarantee_all_wo_ranged = tf_guarantee_boots|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_helmet|tf_guarantee_horse|tf_guarantee_shield

troops = [
["player", "Player", "Player", tf_hero|tf_unmoveable_in_party_window, 0, 0, fac_commoners, [], 0, 0, 0, 0x000000018000000136db6db6db6db6db00000000001db6db0000000000000000 ],
["multiplayer_profile_troop_male","multiplayer_profile_troop_male","multiplayer_profile_troop_male", tf_hero|tf_guarantee_all, 0, 0,fac_commoners,[],0,0,0,0x000000018000000136db6db6db6db6db00000000001db6db0000000000000000],
["multiplayer_profile_troop_female","multiplayer_profile_troop_female","multiplayer_profile_troop_female", tf_hero|tf_female|tf_guarantee_all, 0, 0,fac_commoners,[],0,0,0,0x000000018000000136db6db6db6db6db00000000001db6db0000000000000000],
["temp_troop","Temp Troop","Temp Troop",tf_hero,0,0,fac_commoners,[],0,0,0,0],
####################################################################################################################
# Troops before this point are hardwired into the game and their order should not be changed!
####################################################################################################################

["temp_array_a","0","0",0,0,0,0,[],0,0,0,0],
["temp_array_b","0","0",0,0,0,0,[],0,0,0,0],
["temp_array_c","0","0",0,0,0,0,[],0,0,0,0],
["banner_background_color_array","0","0",0,0,0,0,[],0,0,0,0],
["multiplayer_data","0","0",0,0,0,0,[],0,0,0,0],


#MOD begin
##########################################
#Kingdom of England
["kingdom_1_inf_melee_1","Billman","Billmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,22,fac_kingdom_1,[itm_chapel, itm_bascinet, itm_open_sallet_coif, itm_open_sallet, itm_arming_cap, itm_burgonet_1a ,itm_haubergeon, itm_aketon_green, itm_leather_jerkin, itm_mail_hauberk, itm_ankle_boots, itm_leather_boots, itm_leather_boots_6,  itm_leather_boots_7, itm_leather_boots_8, itm_splinted_greaves, itm_wisby_gauntlets_black, itm_mail_gauntlets, itm_english_bill, itm_guisarme_a, itm_bollock_dagger, itm_grosse_messer, itm_italian_falchion, itm_milanese_sword, itm_ravensbeak, itm_faradon_warhammer, itm_short_iron_hammer, itm_corrazina, itm_padded_jack],infantry_att,infantry_wpt,infantry_skl,kingdom_1_face_young,kingdom_1_face_old],
["kingdom_1_inf_melee_2","Pikeman","Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,8,fac_kingdom_1,[itm_leather_armor,itm_splinted_greaves,itm_pike_a,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_1_face_young,kingdom_1_face_old],
["kingdom_1_inf_melee_3","Gallowglass","Gallowglasses",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,5,fac_kingdom_1,[itm_ragged_outfit, itm_wrapping_boots, itm_leather_boots_1, itm_leather_boots_4, itm_leather_boots_6, itm_leather_boots_7, itm_dane_axe, itm_irish_sword],infantry_att,infantry_wpt,infantry_skl,kingdom_1_face_young,kingdom_1_face_old],
["kingdom_1_inf_missile_1","Longbowman","Longbowmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,43,fac_kingdom_1,[itm_open_sallet,  itm_open_sallet_coif, itm_bascinet, itm_chapel, itm_arming_cap, itm_corrazina, itm_aketon_green, itm_padded_cloth, itm_leather_vest, itm_padded_jack,itm_wrapping_boots, itm_splinted_greaves, itm_leather_boots, itm_leather_boots_6,  itm_leather_boots_7, itm_leather_boots_8, itm_leather_shoes_9, itm_long_bow,itm_bodkin_arrows, itm_barbed_arrows, itm_one_handed_war_axe_a,itm_bollock_dagger, itm_grosse_messer, itm_italian_falchion, itm_milanese_sword, itm_faradon_warhammer, itm_longbowman_sword, itm_short_iron_hammer, itm_steel_buckler1, itm_steel_buckler2, itm_missile_ignition_kit],archer_att,archer_wpt,archer_skl,kingdom_1_face_young,kingdom_1_face_old],
["kingdom_1_inf_missile_2","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,10,fac_kingdom_1,[itm_chapel,itm_aketon_green,itm_ankle_boots,itm_ankle_boots,itm_matchlock_arquebus_1,itm_stone_bullets,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_1_face_young,kingdom_1_face_old],
["kingdom_1_cav_melee_1","English Knight","English Knights",tf_mounted|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_horse|tf_guarantee_ranged|tf_guarantee_shield,0,12,fac_kingdom_1,[itm_greatbascinet1,  itm_milanese_sallet, itm_burgonet_1a, itm_burgonet_1b, itm_burgonet_1c, itm_open_burgonet, itm_barbuta1, itm_open_sallet, itm_milanese_armour, itm_maximilian_armour, itm_plate_armor_2, itm_gothic_armour, itm_shynbaulds, itm_splinted_greaves_spurs, itm_splinted_greaves, itm_leather_boots_6, itm_hourglass_gauntlets, itm_mail_gauntlets, itm_plate_mittens, itm_wisby_gauntlets_black, itm_bnw_gauntlets, itm_plated_charger, itm_saddle_horse, itm_hunter, itm_courser, itm_charger, itm_horse_dark, itm_tab_shield_heater_cav_a, itm_tab_shield_kite_cav_a,  itm_english_longsword, itm_german_bastard_sword, itm_longsword_b, itm_grosse_messer_b, itm_faradon_warhammer, itm_short_iron_hammer, itm_short_morningstar, itm_steel_pick, itm_ravensbeak, itm_light_lance, itm_heavy_lance, itm_great_lance],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_1_face_young,kingdom_1_face_old],

#Polish Lithuanian Union
["kingdom_2_inf_melee_1","Militia","Militias",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,4,fac_kingdom_2,[itm_fur_coat,itm_hide_boots,itm_one_handed_battle_axe_a,itm_javelins,],infantry_att,infantry_wpt,infantry_skl,kingdom_2_face_young,kingdom_2_face_old],
["kingdom_2_inf_melee_2","Drab","Drabs",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,5,fac_kingdom_2,[itm_leather_armor,itm_rus_shoes,itm_halbert_3,],infantry_att,infantry_wpt,infantry_skl,kingdom_2_face_young,kingdom_2_face_old],
["kingdom_2_inf_missile_1","Cossack","Cossacks",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,16,fac_kingdom_2,[itm_fur_hat,itm_nomad_vest,itm_wrapping_boots,itm_recurve_bow,itm_barbed_arrows,itm_hatchet,],archer_att,archer_wpt,archer_skl,kingdom_2_face_young,kingdom_2_face_old],
["kingdom_2_cav_melee_1","Lancer","Lancers",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,22,fac_kingdom_2,[itm_leather_vest,itm_wrapping_boots,itm_heavy_lance,itm_scimitar,itm_saddle_horse,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_2_face_young,kingdom_2_face_old],
["kingdom_2_cav_melee_2","Hussar","Hussars",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,15,fac_kingdom_2,[itm_leather_jerkin,itm_rus_shoes,itm_heavy_lance,itm_scimitar,itm_hunter,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_2_face_young,kingdom_2_face_old],
["kingdom_2_cav_missile_1","Lipka Tatar","Lipka Tatars",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,38,fac_kingdom_2,[itm_nomad_cap_b,itm_nomad_armor,itm_wrapping_boots,itm_tatar_bow,itm_barbed_arrows,itm_hatchet,itm_steppe_horse,],horse_archer_att,horse_archer_wpt,horse_archer_skl,kingdom_2_face_young,kingdom_2_face_old],

#Ottoman Empire
["kingdom_3_inf_melee_1","Janissary Footman","Janissary Footmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,12,fac_kingdom_3,[itm_bork2,itm_janichareteksiz,itm_ankle_boots,itm_scimitar_b,itm_tab_shield_small_round_a,],infantry_att,infantry_wpt,infantry_skl,kingdom_3_face_young,kingdom_3_face_old],
["kingdom_3_inf_missile_1","Janissary Arquebusier","Janissary Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,19,fac_kingdom_3,[itm_bork1,itm_bakak,itm_wrapping_boots,itm_matchlock_arquebus_3,itm_stone_bullets,itm_scimitar,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_3_face_young,kingdom_3_face_old],
["kingdom_3_inf_missile_2","Azab","Azabs",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,9,fac_kingdom_3,[itm_turban_1,itm_steppe_armor,itm_ankle_boots,itm_horn_bow,itm_barbed_arrows,itm_scimitar,],archer_att,archer_wpt,archer_skl,kingdom_3_face_young,kingdom_3_face_old],
["kingdom_3_cav_melee_1","Deli","Delis",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,18,fac_kingdom_3,[itm_deli_cap,itm_steppe_armor,itm_woolen_hose,itm_sword_medieval_e,itm_tab_shield_otto_1,itm_horse,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_3_face_young,kingdom_3_face_old],
["kingdom_3_cav_melee_2","Spahi","Spahis",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,25,fac_kingdom_3,[itm_chichak1,itm_tasarim,itm_rus_shoes,itm_scimitar,itm_tab_shield_small_round_b,itm_arabian_horse_b,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_3_face_young,kingdom_3_face_old],
["kingdom_3_cav_missile_1","Tatar","Tatars",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,17,fac_kingdom_3,[itm_steppe_armor,itm_wrapping_boots,itm_tatar_bow,itm_barbed_arrows,itm_scimitar,itm_horse_dark,],horse_archer_att,horse_archer_wpt,horse_archer_skl,kingdom_3_face_young,kingdom_3_face_old],

#Kingdom of France
["kingdom_4_inf_melee_1","Pikeman","Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,21,fac_kingdom_4,[itm_padded_leather,itm_wrapping_boots,itm_pike_b,itm_side_sword,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_4_face_young,kingdom_4_face_old],
["kingdom_4_inf_melee_2","Halberdier","Halberdiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,10,fac_kingdom_4,[itm_padded_jack,itm_ankle_boots,itm_guisarme_a,itm_side_sword,],infantry_att,infantry_wpt,infantry_skl,kingdom_4_face_young,kingdom_4_face_old],
["kingdom_4_inf_missile_1","Arbalestier","Arbalestiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged|tf_guarantee_shield,0,27,fac_kingdom_4,[itm_padded_cloth,itm_wrapping_boots,itm_heavy_steel_crossbow,itm_bodkin_bolts,itm_tab_shield_pavise_b,itm_side_sword,],crossbowman_att,crossbowman_wpt,crossbowman_skl,kingdom_4_face_young,kingdom_4_face_old],
["kingdom_4_inf_missile_2","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,12,fac_kingdom_4,[itm_combed_morion,itm_aketon_green,itm_ankle_boots,itm_matchlock_arquebus_1,itm_stone_bullets,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_4_face_young,kingdom_4_face_old],
["kingdom_4_cav_melee_1","Chevaux Leger","Chevaux Legers",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,19,fac_kingdom_4,[itm_chapel,itm_leather_armor,itm_ankle_boots,itm_light_lance,itm_side_sword,itm_courser,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_4_face_young,kingdom_4_face_old],
["kingdom_4_cav_melee_2","Gendarme","Gendarmes",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_boots,0,11,fac_kingdom_4,[itm_visored_sallet,itm_aketon_green,itm_wisby_gauntlets_black,itm_splinted_greaves_spurs,itm_great_lance,itm_milanese_sword,itm_charger,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_4_face_young,kingdom_4_face_old],

#Kingdom of Scotland
["kingdom_5_inf_melee_1","Lowland Pikeman","Lowland Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,62,fac_kingdom_5,[itm_hl_kilt_3a, itm_hl_kilt_1a, itm_hl_kilt_2a, itm_padded_leather, itm_aketon_green, itm_leather_jerkin, itm_footman_helmet, itm_padded_cloth, itm_common_hood, itm_hl_hat_1a, itm_hl_hat_2a, itm_leather_boots_1, itm_leather_boots_4, itm_leather_boots_6, itm_leather_boots_7, itm_leather_boots_8, itm_ankle_boots, itm_hl_boots_1a, itm_ashwood_pike, itm_pike_a, itm_pike_b, itm_one_handed_battle_axe_b, itm_faradon_warhammer, itm_steel_pick, itm_fighting_pick, itm_dirk_dagger, itm_scottish_sword, itm_ravensbeak],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_5_face_young,kingdom_5_face_old],
["kingdom_5_inf_melee_2","Lowland Halberdier","Lowland Halberdiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,10,fac_kingdom_5,[itm_haubergeon, itm_corrazina, itm_hl_kilt_3a, itm_hl_kilt_2a, itm_hl_kilt_1a, itm_padded_jack, itm_aketon_green, itm_leather_jerkin, itm_visored_sallet, itm_visored_sallet_coif, itm_chapel, itm_open_sallet, itm_open_sallet_coif, itm_common_hood, itm_hl_hat_1a, itm_hl_hat_1b, itm_halbert_3, itm_halbert_1, itm_swiss_halberd, itm_scottish_sword, itm_faradon_warhammer, itm_steel_pick, itm_one_handed_battle_axe_a, itm_hourglass_gauntlets, itm_plate_mittens, itm_shynbaulds, itm_splinted_greaves, itm_splinted_greaves_spurs, itm_leather_boots_1, itm_leather_boots_4, itm_leather_boots_6, itm_dirk_dagger, itm_fighting_pick],infantry_att,infantry_wpt,infantry_skl,kingdom_5_face_young,kingdom_5_face_old],
["kingdom_5_inf_melee_3","Highlander","Highlanders",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,8,fac_kingdom_5,[itm_scottish_claymore, itm_hl_kilt_3a, itm_hl_kilt_3b, itm_scottish_sword, itm_irish_sword, itm_one_handed_war_axe_b, itm_one_handed_war_axe_a, itm_leather_warrior_cap, itm_footman_helmet, itm_hl_hat_1a, itm_hl_hat_1b, itm_hl_hat_2a, itm_hl_hat_2b, itm_hl_kilt_1b, itm_hl_boots_2a, itm_hl_boots_2b, itm_leather_boots_6, itm_leather_boots_7, itm_leather_boots_8, itm_mail_gauntlets, itm_wisby_gauntlets_black],infantry_att,infantry_wpt,infantry_skl,kingdom_5_face_young,kingdom_5_face_old],
["kingdom_5_inf_missile_1","Highland Archer","Highland Archers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,7,fac_kingdom_5,[itm_reflex_bow, itm_flat_bow, itm_short_bow, itm_barbed_arrows, itm_bodkin_arrows, itm_scottish_sword, itm_longbowman_sword, itm_one_handed_war_axe_a, itm_dirk_dagger, itm_hl_kilt_2a, itm_hl_kilt_2b, itm_hl_kilt_1a, itm_hl_kilt_1b, itm_tunic_with_green_cape, itm_ragged_outfit, itm_leather_cap, itm_padded_coif, itm_hl_hat_1a, itm_hl_hat_1b, itm_hl_hat_2a, itm_hl_hat_2b, itm_common_hood, itm_leather_boots_4, itm_leather_boots_6, itm_leather_boots_7, itm_leather_boots_8, itm_hl_boots_1a, itm_hl_boots_1b, itm_hl_boots_2a, itm_hl_boots_2b, itm_missile_ignition_kit, itm_steel_buckler1, itm_steel_buckler2],archer_att,archer_wpt,archer_skl,kingdom_5_face_young,kingdom_5_face_old],
["kingdom_5_inf_missile_2","Lowland Crossbowman","Lowland Crossbowmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,5,fac_kingdom_5,[itm_aketon_green, itm_padded_jack, itm_padded_leather, itm_mail_shirt, itm_ankle_boots, itm_hl_kilt_2a, itm_hl_kilt_2b, itm_hl_kilt_1a, itm_hl_kilt_1b, itm_tunic_with_green_cape, itm_plate_mittens, itm_hl_boots_1a, itm_hl_boots_1b, itm_hl_boots_2a, itm_hl_boots_2a, itm_hl_boots_2b, itm_hl_hat_1a, itm_hl_hat_2a, itm_heavy_horn_crossbow, itm_light_steel_crossbow, itm_heavy_steel_crossbow, itm_barbed_bolts, itm_bodkin_bolts, itm_scottish_sword, itm_faradon_warhammer, itm_tab_shield_pavise_b, itm_steel_buckler1, itm_steel_buckler2, itm_irish_sword, itm_italian_falchion, itm_bollock_dagger],crossbowman_att,crossbowman_wpt,crossbowman_skl,kingdom_5_face_young,kingdom_5_face_old],
["kingdom_5_cav_melee_1","Border Horseman","Border Horsemen",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,8,fac_kingdom_5,[itm_burgonet_1a, itm_burgonet_1b, itm_burgonet_1c, itm_bascinet, itm_open_burgonet, itm_mail_shirt, itm_drz_mail_shirt, itm_mail_hauberk, itm_byrnie, itm_hl_boots_1a, itm_hl_boots_1b, itm_leather_boots_1, itm_leather_boots_4, itm_leather_boots_6, itm_leather_boots_7, itm_leather_boots_8, itm_splinted_greaves, itm_splinted_greaves_spurs, itm_plate_mittens, itm_hourglass_gauntlets, itm_mail_gauntlets, itm_war_darts, itm_javelins, itm_throwing_spears, itm_lowlander_sword, itm_long_fighting_axe, itm_irish_sword, itm_ravensbeak, itm_tab_shield_kite_cav_a, itm_steel_pick, itm_grosse_messer, itm_milanese_sword, itm_horse_dark, itm_saddle_horse, itm_courser, itm_hunter, itm_tab_shield_kite_cav_a],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_5_face_young,kingdom_5_face_old],

#Holy Roman Empire
["kingdom_6_inf_melee_1","Swiss Pikeman","Swiss Pikeman",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,12,fac_kingdom_6,[itm_hl_hat_2a,itm_padded_jack,itm_ankle_boots,itm_ankle_boots,itm_pike_a,itm_side_sword,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_6_face_young,kingdom_6_face_old],
["kingdom_6_inf_melee_2","Landsknecht Halberdier","Landsknecht Halberdiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,11,fac_kingdom_6,[itm_hl_hat_2b,itm_padded_jack,itm_ankle_boots,itm_swiss_halberd,],infantry_att,infantry_wpt,infantry_skl,kingdom_6_face_young,kingdom_6_face_old],
["kingdom_6_inf_melee_3","Landsknecht Swordsman","Landsknecht Swordsmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,8,fac_kingdom_6,[itm_open_sallet,itm_leather_armor,itm_leather_boots,itm_leather_boots,itm_faradon_twohanded1,itm_side_sword,],infantry_att,infantry_wpt,infantry_skl,kingdom_6_face_young,kingdom_6_face_old],
["kingdom_6_inf_missile_1","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,35,fac_kingdom_6,[itm_hl_hat_2b,itm_padded_jack,itm_ankle_boots,itm_matchlock_arquebus_1,itm_stone_bullets,itm_side_sword,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_6_face_young,kingdom_6_face_old],
["kingdom_6_inf_missile_2","Crossbowman","Crossbowmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged|tf_guarantee_shield,0,9,fac_kingdom_6,[itm_padded_leather,itm_ankle_boots,itm_heavy_horn_crossbow,itm_barbed_bolts,itm_side_sword,itm_tab_shield_pavise_b,],crossbowman_att,crossbowman_wpt,crossbowman_skl,kingdom_6_face_young,kingdom_6_face_old],
["kingdom_6_cav_melee_1","Reiter","Reiters",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_boots,0,25,fac_kingdom_6,[itm_visored_sallet_coif,itm_bnw_armour,itm_wisby_gauntlets_black,itm_leather_shoes_9,itm_heavy_lance,itm_sword_repent,itm_courser,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_6_face_young,kingdom_6_face_old],

#Portuguese Empire
["kingdom_7_inf_melee_1","Pikeman","Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,24,fac_kingdom_7,[itm_combed_morion,itm_aketon_green,itm_ankle_boots,itm_pike_b,itm_side_sword,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_7_face_young,kingdom_7_face_old],
["kingdom_7_inf_melee_2","Footman","Footmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,15,fac_kingdom_7,[itm_hl_hat_2a,itm_padded_jack,itm_leather_boots,itm_side_sword,itm_steel_shield,],infantry_att,infantry_wpt,infantry_skl,kingdom_7_face_young,kingdom_7_face_old],
["kingdom_7_inf_melee_3","Spearman","Spearmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,21,fac_kingdom_7,[itm_padded_leather,itm_leather_boots,itm_war_spear,itm_tab_shield_pavise_b,],infantry_att,infantry_wpt,infantry_skl,kingdom_7_face_young,kingdom_7_face_old],
["kingdom_7_inf_missile_1","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,26,fac_kingdom_7,[itm_combed_morion,itm_padded_cloth,itm_leather_boots,itm_matchlock_arquebus_3,itm_stone_bullets,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_7_face_young,kingdom_7_face_old],
["kingdom_7_cav_melee_1","Lancer","Lancers",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_boots,0,9,fac_kingdom_7,[itm_burgonet_1a,itm_bnw_armour_b,itm_wisby_gauntlets_black,itm_leather_shoes_9,itm_heavy_lance,itm_sword_repent,itm_courser,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_7_face_young,kingdom_7_face_old],
["kingdom_7_cav_melee_2","Man at Arms","Men at Arms",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,5,fac_kingdom_7,[itm_milanese_sallet,itm_aketon_green,itm_wisby_gauntlets_black,itm_shynbaulds,itm_great_lance,itm_sword_repent,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_7_face_young,kingdom_7_face_old],

#Spanish Empire
["kingdom_8_inf_melee_1","Pikeman","Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,29,fac_kingdom_8,[itm_hl_hat_2b,itm_padded_jack,itm_ankle_boots,itm_pike_b,itm_side_sword,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_8_face_young,kingdom_8_face_old],
["kingdom_8_inf_melee_2","Aragonese Footman","Aragonese Footmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_gloves|tf_guarantee_shield,0,21,fac_kingdom_8,[itm_drz_mail_shirt,itm_wisby_gauntlets_black,itm_leather_shoes_9,itm_espada_eslavona_a,itm_steel_shield,],infantry_att,infantry_wpt,infantry_skl,kingdom_8_face_young,kingdom_8_face_old],
["kingdom_8_inf_missile_1","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,28,fac_kingdom_8,[itm_combed_morion,itm_padded_jack,itm_ankle_boots,itm_matchlock_arquebus_3,itm_stone_bullets,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_8_face_young,kingdom_8_face_old],
["kingdom_8_inf_missile_2","Crossbowman","Crossbowmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,11,fac_kingdom_8,[itm_hl_hat_2b,itm_padded_jack,itm_ankle_boots,itm_heavy_steel_crossbow,itm_bodkin_bolts,itm_side_sword,],crossbowman_att,crossbowman_wpt,crossbowman_skl,kingdom_8_face_young,kingdom_8_face_old],
["kingdom_8_cav_melee_1","Lancer","Lancers",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_boots|tf_guarantee_shield,0,7,fac_kingdom_8,[itm_visored_sallet,itm_bnw_armour,itm_wisby_gauntlets_black,itm_leather_shoes_9,itm_heavy_lance,itm_sword_repent,itm_courser,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_8_face_young,kingdom_8_face_old],
["kingdom_8_cav_missile_1","Ginete","Ginetes",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,4,fac_kingdom_8,[itm_combed_morion,itm_drz_mail_shirt,itm_leather_boots,itm_light_horn_crossbow,itm_barbed_bolts,itm_espada_eslavona_a,itm_courser,],mounted_crossbowman_att,mounted_crossbowman_wpt,mounted_crossbowman_skl,kingdom_8_face_young,kingdom_8_face_old],

#Union of Kalmar
["kingdom_9_inf_melee_1","Pikeman","Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,39,fac_kingdom_9,[itm_open_sallet,itm_leather_jerkin,itm_leather_boots,itm_pike_a,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_9_face_young,kingdom_9_face_old],
["kingdom_9_inf_melee_2","Halberdier","Halberdiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,5,fac_kingdom_9,[itm_visored_sallet,itm_padded_leather,itm_leather_boots,itm_halbert_1,],infantry_att,infantry_wpt,infantry_skl,kingdom_9_face_young,kingdom_9_face_old],
["kingdom_9_inf_missile_1","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,34,fac_kingdom_9,[itm_combed_morion,itm_padded_jack,itm_ankle_boots,itm_matchlock_arquebus_3,itm_stone_bullets,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_9_face_young,kingdom_9_face_old],
["kingdom_9_inf_missile_2","Crossbowman","Crossbowmen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,9,fac_kingdom_9,[itm_open_sallet,itm_aketon_green,itm_leather_boots,itm_heavy_steel_crossbow,itm_bodkin_bolts,itm_side_sword,],crossbowman_att,crossbowman_wpt,crossbowman_skl,kingdom_9_face_young,kingdom_9_face_old],
["kingdom_9_cav_melee_1","Horseman","Horsemen",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_gloves|tf_guarantee_boots|tf_guarantee_shield,0,6,fac_kingdom_9,[itm_open_sallet_coif,itm_bnw_armour_b,itm_wisby_gauntlets_black,itm_leather_shoes_9,itm_sword_repent,itm_hunter,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_9_face_young,kingdom_9_face_old],
["kingdom_9_cav_melee_2","Light Lancer","Light Lancers",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,7,fac_kingdom_9,[itm_byrnie,itm_leather_boots,itm_heavy_lance,itm_sword_repent,itm_hunter,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_9_face_young,kingdom_9_face_old],

#Kingdom of Hungary
["kingdom_10_inf_melee_1","Levy","Levys",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,7,fac_kingdom_10,[itm_common_hood,itm_tunic_with_green_cape,itm_ankle_boots,itm_hammer,itm_tab_shield_round_a,],infantry_att,infantry_wpt,infantry_skl,kingdom_10_face_young,kingdom_10_face_old],
["kingdom_10_inf_melee_2","Pikeman","Pikemen",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots,0,6,fac_kingdom_10,[itm_hl_hat_2b,itm_padded_jack,itm_ankle_boots,itm_pike_b,],pikeman_att,pikeman_wpt,pikeman_skl,kingdom_10_face_young,kingdom_10_face_old],
["kingdom_10_inf_missile_1","Archer","Archer",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged|tf_guarantee_shield,0,15,fac_kingdom_10,[itm_padded_leather,itm_leather_boots,itm_reflex_bow,itm_barbed_arrows,itm_faradon_warhammer,itm_tab_shield_otto_1,],archer_att,archer_wpt,archer_skl,kingdom_10_face_young,kingdom_10_face_old],
["kingdom_10_inf_missile_2","Arquebusier","Arquebusiers",tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged,0,13,fac_kingdom_10,[itm_hl_hat_2b,itm_padded_jack,itm_ankle_boots,itm_matchlock_arquebus_1,itm_stone_bullets,itm_side_sword,],musketeer_att,musketeer_wpt,musketeer_skl,kingdom_10_face_young,kingdom_10_face_old],
["kingdom_10_cav_melee_1","Hussar","Hussars",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_shield,0,27,fac_kingdom_10,[itm_leather_jerkin,itm_leather_boots,itm_heavy_lance,itm_tab_shield_otto_2,itm_courser,],cavalry_att,cavalry_wpt,cavalry_skl,kingdom_10_face_young,kingdom_10_face_old],
["kingdom_10_cav_missile_1","Horse Archer","Horse Archers",tf_mounted|tf_guarantee_horse|tf_guarantee_helmet|tf_guarantee_armor|tf_guarantee_boots|tf_guarantee_ranged|tf_guarantee_shield,0,32,fac_kingdom_10,[itm_leather_jerkin,itm_leather_boots,itm_tatar_bow,itm_barbed_arrows,itm_tab_shield_otto_1,],horse_archer_att,horse_archer_wpt,horse_archer_skl,kingdom_10_face_young,kingdom_10_face_old],

["multiplayer_surgeon","Surgeon","Surgeons",
  tf_guarantee_all,0,0,0,
  [
    itm_surgeon_coif,
    itm_surgeon_coat,
    itm_ankle_boots,
		itm_hatchet,
		itm_steel_pick,
		itm_italian_falchion,
		itm_one_handed_battle_axe_a,
		itm_surgeon_kit,
    itm_tab_shield_pavise_b,
  ],
  special_att,special_wpt,special_skl,0,0
],
["multiplayer_engineer","Engineer","Engineers",
  tf_guarantee_all,0,0,0,
  [
    itm_arming_cap,
	itm_padded_coif, 
    itm_aketon_green,
    itm_wrapping_boots,
	itm_leather_boots_4,
	itm_leather_boots_6,
	itm_leather_boots_7,
	itm_leather_boots_8,
    itm_engineer_hammer,
		itm_engineer_shovel,
		itm_engineer_axe,
		itm_engineer_material,
    itm_ladder_5m,
    #itm_grenade,
    #itm_firepot,
  ],
  special_att,special_wpt,special_skl,0,0
],
["multiplayer_gunner","Gunner","Gunners",
  tf_guarantee_all,0,0,0,
  [
    itm_arming_cap,
    itm_tabard,
    itm_ankle_boots,
	itm_leather_boots_4,
	itm_leather_boots_6,
	itm_leather_boots_7,
	itm_leather_boots_8,
    itm_hatchet,
	itm_steel_pick,
	itm_italian_falchion,
	itm_one_handed_battle_axe_a,
		itm_linstock,
		itm_refiller,
  ],
  special_att,special_wpt,special_skl,0,0
],
]

#client online troop
troops += ["online_character","Online Character" ,"Online Character",tf_guarantee_all, 0, 0, 0,[],cha_30,wp(wp0),knows_trade_10,0],

troops += ["multiplayer_troop_end","0","0",0,0,0,0,[],0,0,0,0,0],

for i in range (0, multiplayer_max_possible_player_id):
  troops += ["player_troop_"+str(i),"0","0", 0, 0, 0, 0,[],str_3|agi_3|cha_30,wp(wp0),knows_trade_10,0],
troops += ["multiplayer_player_troop_end","0","0",0,0,0,0,[],0,0,0,0,0],

for i in range (0, multiplayer_max_possible_player_id):
  troops += ["player_troop_online_"+str(i),"0","0", 0, 0, 0, 0,[],str_3|agi_3|cha_30,wp(wp0),knows_trade_10,0],
troops += ["multiplayer_player_troop_online_end","0","0",0,0,0,0,[],0,0,0,0,0],

for i in range (0, multiplayer_max_possible_player_id):
	for j in range (0, 9):
		troops += ["player_"+str(i)+"_life_inv_slot_"+str(j)+"_item","0","0",0,0,0,0,[],0,0,0,0,0],

for i in range (0, multiplayer_max_possible_player_id):
	for j in range (0, 9):
		troops += ["player_"+str(i)+"_life_inv_slot_"+str(j)+"_usage","0","0",0,0,0,0,[],0,0,0,0,0],

troops += ["player_item_total_usage","0","0",0,0,0,0,[],0,0,0,0],

troops += ["online_level_xp","0","0",0,0,0,0,[],0,0,0,0],

#max slot numbers 1048576
for i in range (0, 8):
	troops += ["unquie_id_round_bonus_"+str(i),"0","0",0,0,0,0,[],0,0,0,0],
for i in range (0, 8):
	troops += ["unquie_id_team_hits_"+str(i),"0","0",0,0,0,0,[],0,0,0,0],
for i in range (0, 8):
	troops += ["unquie_id_team_hit_kicks_"+str(i),"0","0",0,0,0,0,[],0,0,0,0],

#positional weapon weight
# for i in range (0, itm_items_end):
#     troops += ["item_"+str(i)+"_pos_weight","0","0",0,0,0,0,[],0,0,0,0],

#day time fog colors
troops += ["fog_color","0","0",0,0,0,0,[],0,0,0,0],
	
#props
troops += ["prop_destructible","0","0",0,0,0,0,[],0,0,0,0],

#construct props
troops += ["construct_prop_cost","0","0",0,0,0,0,[],0,0,0,0],

#advanced orders
troops += ["team_next_order_time","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_next_action_time","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_action_time_number","0","0",0,0,0,0,[],0,0,0,0],

troops += ["team_volley_check_time_bow","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_volley_check_time_crossbow","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_volley_check_time_musket","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_volley_shot_time_bow","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_volley_shot_time_crossbow","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_volley_shot_time_musket","0","0",0,0,0,0,[],0,0,0,0],

troops += ["team_archer_amount","0","0",0,0,0,0,[],0,0,0,0],
troops += ["team_archer_target_in_range_amount","0","0",0,0,0,0,[],0,0,0,0],

objects = [
('_order_run'),
('_order_volley'),
('_order_fire'),
]
for i in range (len(objects)):
	for j in range (0, 12):
		troops += ["group_" +str(j) +objects[i],"0","0",0,0,0,0,[],0,0,0,0],
troops += ["advanced_order_arrays_end","0","0",0,0,0,0,[],0,0,0,0],
		
for i in range (0, 512):
	troops += ["sound_channel_"+str(i),"0","0",0,0,0,0,[],0,0,0,0],
troops += ["sound_channels_end","0","0",0,0,0,0,[],0,0,0,0],

troops += ["cannon_1_childs","0","0",0,0,0,0,[],0,0,0,0],
for i in range (1, 21):
	troops += ["cannon_1_child_"+str(i)+"_pos","0","0",0,0,0,0,[],0,0,0,0],
#MOD end