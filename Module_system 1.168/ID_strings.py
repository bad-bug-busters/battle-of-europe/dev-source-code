str_no_string = 0
str_empty_string = 1
str_yes = 2
str_no = 3
str_s0 = 4
str_reg1 = 5
str_credits_1 = 6
str_credits_2 = 7
str_credits_3 = 8
str_credits_4 = 9
str_credits_5 = 10
str_credits_6 = 11
str_credits_7 = 12
str_credits_8 = 13
str_credits_9 = 14
str_credits_10 = 15
str_credits_11 = 16
str_credits_12 = 17
str_multi_game_type_1 = 18
str_multi_game_type_2 = 19
str_multi_game_type_3 = 20
str_multi_game_type_7 = 21
str_multi_game_type_8 = 22
str_multi_game_types_end = 23
str_poll_kick_player_s1_by_s0 = 24
str_poll_ban_player_s1_by_s0 = 25
str_poll_change_map_to_s1_by_s0 = 26
str_poll_change_map_to_s1_and_factions_to_s2_and_s3_by_s0 = 27
str_poll_change_number_of_bots_to_reg0_and_reg1_by_s0 = 28
str_poll_kick_player = 29
str_poll_ban_player = 30
str_poll_change_map = 31
str_poll_change_map_with_faction = 32
str_poll_change_number_of_bots = 33
str_poll_time_left = 34
str_poll_result_yes = 35
str_poll_result_no = 36
str_server_name = 37
str_game_password = 38
str_map = 39
str_game_type = 40
str_max_number_of_players = 41
str_number_of_bots_in_team_reg1 = 42
str_team_reg1_faction = 43
str_spectator_camera = 44
str_control_block_direction = 45
str_round_time_limit = 46
str_players_take_control_of_a_bot_after_death = 47
str_team_points_limit = 48
str_point_gained_from_flags = 49
str_point_gained_from_capturing_flag = 50
str_respawn_period = 51
str_add_to_official_game_servers_list = 52
str_combat_speed = 53
str_combat_speed_0 = 54
str_combat_speed_1 = 55
str_combat_speed_2 = 56
str_combat_speed_3 = 57
str_combat_speed_4 = 58
str_off = 59
str_on = 60
str_defender_spawn_count_limit = 61
str_unlimited = 62
str_automatic = 63
str_by_mouse_movement = 64
str_free = 65
str_stick_to_any_player = 66
str_stick_to_team_members = 67
str_stick_to_team_members_view = 68
str_make_factions_voteable = 69
str_make_kick_voteable = 70
str_make_ban_voteable = 71
str_bots_upper_limit_for_votes = 72
str_make_maps_voteable = 73
str_auto_team_balance_limit = 74
str_welcome_message = 75
str_allow_player_banners = 76
str_reg0 = 77
str_s0_reg0 = 78
str_s0_s1 = 79
str_reg0_dd_reg1reg2 = 80
str_s0_dd_reg0 = 81
str_respawning_in_reg0_seconds = 82
str_no_more_respawns_remained_this_round = 83
str_reg0_respawns_remained = 84
str_this_is_your_last_respawn = 85
str_wait_next_round = 86
str_yes_wo_dot = 87
str_no_wo_dot = 88
str_s1_returned_flag = 89
str_s1_auto_returned_flag = 90
str_s1_captured_flag = 91
str_s1_taken_flag = 92
str_s1_neutralized_flag_reg0 = 93
str_s1_captured_flag_reg0 = 94
str_s1_pulling_flag_reg0 = 95
str_s1_destroyed_target_0 = 96
str_s1_destroyed_target_1 = 97
str_s1_destroyed_catapult = 98
str_s1_destroyed_trebuchet = 99
str_s1_destroyed_all_targets = 100
str_s1_defended_castle = 101
str_s1_captured_castle = 102
str_auto_team_balance_in_20_seconds = 103
str_auto_team_balance_next_round = 104
str_auto_team_balance_done = 105
str_s1_won_round = 106
str_round_draw = 107
str_round_draw_no_one_remained = 108
str_reset_to_default = 109
str_done = 110
str_player_name = 111
str_kills = 112
str_deaths = 113
str_ping = 114
str_dead = 115
str_reg0_dead = 116
str_bots_reg0_agents = 117
str_bot_1_agent = 118
str_score_reg0 = 119
str_flags_reg0 = 120
str_reg0_players = 121
str_reg0_player = 122
str_open_gate = 123
str_close_gate = 124
str_open_door = 125
str_close_door = 126
str_raise_ladder = 127
str_drop_ladder = 128
str_back = 129
str_start_map = 130
str_choose_an_option = 131
str_choose_a_poll_type = 132
str_choose_faction = 133
str_choose_a_faction = 134
str_choose_troop = 135
str_choose_a_troop = 136
str_choose_items = 137
str_options = 138
str_redefine_keys = 139
str_submit_a_poll = 140
str_administrator_panel = 141
str_kick_player = 142
str_ban_player = 143
str_mute_player = 144
str_unmute_player = 145
str_quit = 146
str_poll_for_changing_the_map = 147
str_poll_for_changing_the_map_and_factions = 148
str_poll_for_changing_number_of_bots = 149
str_poll_for_kicking_a_player = 150
str_poll_for_banning_a_player = 151
str_choose_a_player = 152
str_choose_a_map = 153
str_choose_a_faction_for_team_reg0 = 154
str_choose_number_of_bots_for_team_reg0 = 155
str_spectator = 156
str_spectators = 157
str_score = 158
str_command = 159
str_use_default_banner = 160
str_team_reg0_bot_count_is_reg1 = 161
str_input_is_not_correct_for_the_command_type_help_for_more_information = 162
str_maximum_seconds_for_round_is_reg0 = 163
str_respawn_period_is_reg0_seconds = 164
str_bots_upper_limit_for_votes_is_reg0 = 165
str_map_is_voteable = 166
str_map_is_not_voteable = 167
str_factions_are_voteable = 168
str_factions_are_not_voteable = 169
str_players_respawn_as_bot = 170
str_players_do_not_respawn_as_bot = 171
str_kicking_a_player_is_voteable = 172
str_kicking_a_player_is_not_voteable = 173
str_banning_a_player_is_voteable = 174
str_banning_a_player_is_not_voteable = 175
str_player_banners_are_allowed = 176
str_player_banners_are_not_allowed = 177
str_default_armor_is_forced = 178
str_default_armor_is_not_forced = 179
str_auto_team_balance_threshold_is_reg0 = 180
str_starting_gold_ratio_is_reg0 = 181
str_combat_gold_bonus_ratio_is_reg0 = 182
str_round_gold_bonus_ratio_is_reg0 = 183
str_team_points_limit_is_reg0 = 184
str_defender_spawn_count_limit_is_s1 = 185
str_system_error = 186
str_s2_s3 = 187
str_server_name_s0 = 188
str_map_name_s0 = 189
str_game_type_s0 = 190
str_a_duel_request_is_sent_to_s0 = 191
str_s0_offers_a_duel_with_you = 192
str_your_duel_with_s0_is_cancelled = 193
str_a_duel_between_you_and_s0_will_start_in_3_seconds = 194
str_you_have_lost_a_duel = 195
str_you_have_won_a_duel = 196
str_server_s0 = 197
str_duel_starts_in_reg0_seconds = 198
str_banner_selection_text = 199
str_profile_banner_selection_text = 200
str_delivered_damage_to_player = 201
str_horse_delivered_damage = 202
str_delivered_damage_to_horse = 203
str_received_damage = 204
str_horse_received_damage = 205
str_delivered_damage_to_shield = 206
str_shield_received_damage = 207
str_delivered_damage_to_object = 208
str_equip = 209
str_choose_ammo = 210
str_carry_slot_display_s1 = 211
str_carry_slot_0 = 212
str_carry_slot_1 = 213
str_carry_slot_2 = 214
str_carry_slot_3 = 215
str_carry_slot_4 = 216
str_carry_slot_5 = 217
str_carry_slot_6 = 218
str_carry_slot_7 = 219
str_carry_slot_8 = 220
str_carry_slot_9 = 221
str_carry_slot_10 = 222
str_carry_slot_11 = 223
str_shield_damage_reg1_s1 = 224
str_info_grenade = 225
str_info_firepot = 226
str_info_engineer_hammer = 227
str_info_surgeon_kit = 228
str_info_missile_ignition_kit = 229
str_info_ladder_5m = 230
str_order_movement_run = 231
str_order_movement_walk = 232
str_order_shot_volley = 233
str_order_shot_fire = 234
str_order_shot_normal = 235
str_ui_order_movement_run = 236
str_ui_order_movement_walk = 237
str_ui_order_shot_volley = 238
str_ui_order_shot_fire = 239
str_ui_order_shot_normal = 240
str_update = 241
str_reset_password = 242
str_mod_menu = 243
str_show_damage_report = 244
str_show_shot_distance = 245
str_show_movement_speed = 246
str_refresh = 247
str_cloud_amount = 248
str_precipitation_strength = 249
str_fog_distance = 250
str_thunderstorm = 251
str_wind_strength = 252
str_wind_direction = 253
str_day_time = 254
str_server_announcement = 255
str_main_info = 256
str_manage_players = 257
str_clan_chat = 258
str_admin_chat = 259
str_admin_message = 260
str_muted = 261
str_mute = 262
str_unmute = 263
str_kick = 264
str_ban = 265
str_temp_ban = 266
str_swap_team = 267
str_swap_spec = 268
str_slay = 269
str_restore = 270
str_tp_to_me = 271
str_tp_to_player = 272
str_tp_me = 273
str_website_url_1 = 274
str_service_url_1 = 275
str_service_action_1 = 276
str_service_action_2 = 277
str_service_action_3 = 278
str_service_action_4 = 279
str_service_action_5 = 280
str_server_security_token = 281
str_att_0_name = 282
str_att_1_name = 283
str_att_2_name = 284
str_att_3_name = 285
str_att_0_info = 286
str_att_1_info = 287
str_att_2_info = 288
str_att_3_info = 289
str_wpf_0_name = 290
str_wpf_1_name = 291
str_wpf_2_name = 292
str_wpf_3_name = 293
str_wpf_4_name = 294
str_wpf_5_name = 295
str_wpf_6_name = 296
str_wpf_0_info = 297
str_wpf_1_info = 298
str_wpf_2_info = 299
str_wpf_3_info = 300
str_wpf_4_info = 301
str_wpf_5_info = 302
str_wpf_6_info = 303
str_construct_prop_0 = 304
str_construct_prop_1 = 305
str_construct_prop_2 = 306
str_construct_prop_3 = 307
str_construct_prop_4 = 308
str_construct_prop_5 = 309
str_skl_0_name = 310
str_skl_1_name = 311
str_skl_2_name = 312
str_skl_3_name = 313
str_skl_4_name = 314
str_skl_5_name = 315
str_skl_6_name = 316
str_skl_7_name = 317
str_skl_8_name = 318
str_skl_9_name = 319
str_skl_10_name = 320
str_skl_11_name = 321
str_skl_12_name = 322
str_skl_13_name = 323
str_skl_14_name = 324
str_skl_15_name = 325
str_skl_16_name = 326
str_skl_17_name = 327
str_skl_18_name = 328
str_skl_19_name = 329
str_skl_20_name = 330
str_skl_21_name = 331
str_skl_22_name = 332
str_skl_23_name = 333
str_skl_24_name = 334
str_skl_25_name = 335
str_skl_26_name = 336
str_skl_27_name = 337
str_skl_28_name = 338
str_skl_29_name = 339
str_skl_30_name = 340
str_skl_31_name = 341
str_skl_32_name = 342
str_skl_33_name = 343
str_skl_34_name = 344
str_skl_35_name = 345
str_skl_36_name = 346
str_skl_37_name = 347
str_skl_38_name = 348
str_skl_39_name = 349
str_skl_40_name = 350
str_skl_41_name = 351
str_skl_0_info = 352
str_skl_1_info = 353
str_skl_2_info = 354
str_skl_3_info = 355
str_skl_4_info = 356
str_skl_5_info = 357
str_skl_6_info = 358
str_skl_7_info = 359
str_skl_8_info = 360
str_skl_9_info = 361
str_skl_10_info = 362
str_skl_11_info = 363
str_skl_12_info = 364
str_skl_13_info = 365
str_skl_14_info = 366
str_skl_15_info = 367
str_skl_16_info = 368
str_skl_17_info = 369
str_skl_18_info = 370
str_skl_19_info = 371
str_skl_20_info = 372
str_skl_21_info = 373
str_skl_22_info = 374
str_skl_23_info = 375
str_skl_24_info = 376
str_skl_25_info = 377
str_skl_26_info = 378
str_skl_27_info = 379
str_skl_28_info = 380
str_skl_29_info = 381
str_skl_30_info = 382
str_skl_31_info = 383
str_skl_32_info = 384
str_skl_33_info = 385
str_skl_34_info = 386
str_skl_35_info = 387
str_skl_36_info = 388
str_skl_37_info = 389
str_skl_38_info = 390
str_skl_39_info = 391
str_skl_40_info = 392
str_skl_41_info = 393
str_key_0 = 394
str_key_1 = 395
str_key_2 = 396
str_key_3 = 397
str_key_4 = 398
str_key_5 = 399
str_key_6 = 400
str_key_7 = 401
str_key_8 = 402
str_key_9 = 403
str_key_10 = 404
str_key_11 = 405
str_key_12 = 406
str_key_13 = 407
str_key_14 = 408
str_key_15 = 409
str_key_16 = 410
str_key_17 = 411
str_key_18 = 412
str_key_19 = 413
str_key_20 = 414
str_key_21 = 415
str_key_22 = 416
str_key_23 = 417
str_key_24 = 418
str_key_25 = 419
str_key_26 = 420
str_key_27 = 421
str_key_28 = 422
str_key_29 = 423
str_key_30 = 424
str_key_31 = 425
str_key_32 = 426
str_key_33 = 427
str_key_34 = 428
str_key_35 = 429
str_key_36 = 430
str_key_37 = 431
str_key_38 = 432
str_key_39 = 433
str_key_40 = 434
str_key_41 = 435
str_key_42 = 436
str_key_43 = 437
str_key_44 = 438
str_key_45 = 439
str_key_46 = 440
str_key_47 = 441
str_key_48 = 442
str_key_49 = 443
str_key_50 = 444
str_key_51 = 445
str_key_52 = 446
str_key_53 = 447
str_key_54 = 448
str_key_55 = 449
str_key_56 = 450
str_key_57 = 451
str_key_58 = 452
str_key_59 = 453
str_key_60 = 454
str_key_61 = 455
str_key_62 = 456
str_key_63 = 457
str_key_64 = 458
str_key_65 = 459
str_key_66 = 460
str_key_67 = 461
str_key_68 = 462
str_key_69 = 463
str_key_70 = 464
str_key_71 = 465
str_key_72 = 466
str_key_73 = 467
str_key_74 = 468
str_key_75 = 469
str_key_76 = 470
str_key_77 = 471
str_key_78 = 472
str_key_79 = 473
str_key_80 = 474
str_key_81 = 475
str_key_82 = 476
str_key_83 = 477
str_key_84 = 478
str_key_85 = 479
str_key_86 = 480
str_key_87 = 481
str_key_88 = 482
str_key_89 = 483
str_key_90 = 484
str_key_91 = 485
str_key_92 = 486
str_key_93 = 487
str_key_94 = 488
str_key_95 = 489
str_key_96 = 490
str_key_97 = 491
str_key_98 = 492
str_key_99 = 493
str_key_100 = 494
str_key_101 = 495
str_key_102 = 496
str_key_103 = 497
str_key_104 = 498
str_key_105 = 499
str_key_106 = 500
str_key_107 = 501
str_key_108 = 502
str_key_109 = 503
str_key_110 = 504
str_key_111 = 505
str_key_112 = 506
str_key_113 = 507
str_key_114 = 508
str_key_115 = 509
str_key_116 = 510
str_key_117 = 511
str_key_118 = 512
str_key_119 = 513
str_key_120 = 514
str_key_121 = 515
str_key_122 = 516
str_key_123 = 517
str_key_124 = 518
str_key_125 = 519
str_key_126 = 520
str_key_127 = 521
str_key_128 = 522
str_key_129 = 523
str_key_130 = 524
str_key_131 = 525
str_key_132 = 526
str_key_133 = 527
str_key_134 = 528
str_key_135 = 529
str_key_136 = 530
str_key_137 = 531
str_key_138 = 532
str_key_139 = 533
str_key_140 = 534
str_key_141 = 535
str_key_142 = 536
str_key_143 = 537
str_key_144 = 538
str_key_145 = 539
str_key_146 = 540
str_key_147 = 541
str_key_148 = 542
str_key_149 = 543
str_key_150 = 544
str_key_151 = 545
str_key_152 = 546
str_key_153 = 547
str_key_154 = 548
str_key_155 = 549
str_key_156 = 550
str_key_157 = 551
str_key_158 = 552
str_key_159 = 553
str_key_160 = 554
str_key_161 = 555
str_key_162 = 556
str_key_163 = 557
str_key_164 = 558
str_key_165 = 559
str_key_166 = 560
str_key_167 = 561
str_key_168 = 562
str_key_169 = 563
str_key_170 = 564
str_key_171 = 565
str_key_172 = 566
str_key_173 = 567
str_key_174 = 568
str_key_175 = 569
str_key_176 = 570
str_key_177 = 571
str_key_178 = 572
str_key_179 = 573
str_key_180 = 574
str_key_181 = 575
str_key_182 = 576
str_key_183 = 577
str_key_184 = 578
str_key_185 = 579
str_key_186 = 580
str_key_187 = 581
str_key_188 = 582
str_key_189 = 583
str_key_190 = 584
str_key_191 = 585
str_key_192 = 586
str_key_193 = 587
str_key_194 = 588
str_key_195 = 589
str_key_196 = 590
str_key_197 = 591
str_key_198 = 592
str_key_199 = 593
str_key_200 = 594
str_key_201 = 595
str_key_202 = 596
str_key_203 = 597
str_key_204 = 598
str_key_205 = 599
str_key_206 = 600
str_key_207 = 601
str_key_208 = 602
str_key_209 = 603
str_key_210 = 604
str_key_211 = 605
str_key_212 = 606
str_key_213 = 607
str_key_214 = 608
str_key_215 = 609
str_key_216 = 610
str_key_217 = 611
str_key_218 = 612
str_key_219 = 613
str_key_220 = 614
str_key_221 = 615
str_key_222 = 616
str_key_223 = 617
str_key_224 = 618
str_key_225 = 619
str_key_226 = 620
str_key_227 = 621
str_key_228 = 622
str_key_229 = 623
str_key_230 = 624
str_key_231 = 625
str_key_232 = 626
str_key_233 = 627
str_key_234 = 628
str_key_235 = 629
str_key_236 = 630
str_key_237 = 631
str_key_238 = 632
str_key_239 = 633
str_key_240 = 634
str_key_241 = 635
str_key_242 = 636
str_key_243 = 637
str_key_244 = 638
str_key_245 = 639
str_key_246 = 640
str_key_247 = 641
str_key_248 = 642
str_key_249 = 643
str_key_250 = 644
str_key_251 = 645
str_key_252 = 646
str_key_253 = 647
str_key_254 = 648
str_key_255 = 649
str_scene_0 = 650
str_scene_1 = 651
str_scene_2 = 652
str_scene_3 = 653
str_scene_4 = 654
str_scene_5 = 655
str_scene_6 = 656
str_scene_7 = 657
str_scene_8 = 658
str_scene_9 = 659
str_scene_10 = 660
str_scene_11 = 661
str_scene_12 = 662
str_scene_13 = 663
str_scene_14 = 664
str_scene_15 = 665
str_scene_16 = 666
str_scene_17 = 667
str_scene_18 = 668
str_scene_19 = 669
str_scene_20 = 670
str_scene_21 = 671
str_scene_22 = 672
str_scene_23 = 673
str_scene_24 = 674
str_scene_25 = 675
str_scene_26 = 676
str_scene_27 = 677
str_scene_28 = 678
str_scene_29 = 679
str_scene_30 = 680
str_scene_31 = 681
str_scene_32 = 682
str_scene_33 = 683
str_scene_34 = 684
str_scene_35 = 685
str_scene_36 = 686
str_scene_37 = 687
str_scene_38 = 688
str_scene_39 = 689
str_scene_40 = 690
str_scene_41 = 691
str_scene_42 = 692
str_scene_43 = 693
str_scene_44 = 694
str_scene_45 = 695
str_scene_46 = 696
str_scene_47 = 697


