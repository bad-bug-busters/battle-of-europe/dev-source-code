from header_common import *
from ID_animations import *
from header_mission_templates import *
from header_tableau_materials import *
from header_items import *
from module_constants import *

####################################################################################################################
#  Each tableau material contains the following fields:
#  1) Tableau id (string): used for referencing tableaux in other files. The prefix tab_ is automatically added before each tableau-id.
#  2) Tableau flags (int). See header_tableau_materials.py for a list of available flags
#  3) Tableau sample material name (string).
#  4) Tableau width (int).
#  5) Tableau height (int).
#  6) Tableau mesh min x (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  7) Tableau mesh min y (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  8) Tableau mesh max x (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  9) Tableau mesh max y (int): divided by 1000 and used when a mesh is auto-generated using the tableau material
#  10) Operations block (list): A list of operations. See header_operations.py for reference.
#     The operations block is executed when the tableau is activated.
# 
####################################################################################################################

#banner height = 200, width = 85 with wood, 75 without wood

tableaus = [
  ("game_character_sheet", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 266, 532,
	[
       # (store_script_param, ":troop_no", 1),
	]),

  ("game_inventory_window", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 180, 270,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
			 (cur_tableau_add_sun_light, pos1, 255, 255, 255),
			 
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_inventory_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_inventory_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       ]),

  ("game_profile_window", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 320, 480, [
    (store_script_param, ":profile_no", 1),
    (assign, ":gender", ":profile_no"),
    (val_mod, ":gender", 2),
    (try_begin),
      (eq, ":gender", 0),
      (assign, ":troop_no", "trp_multiplayer_profile_troop_male"),
    (else_try),
      (assign, ":troop_no", "trp_multiplayer_profile_troop_female"),
    (try_end),
    (troop_set_face_key_from_current_profile, ":troop_no"),
    (cur_tableau_set_background_color, 0xFF888888),
    (cur_tableau_set_ambient_light, 65, 75, 85),
    (set_fixed_point_multiplier, 100),
    (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

    (init_position, pos1),
    (position_set_z, pos1, 100),
    (position_set_x, pos1, -20),
    (position_set_y, pos1, -20),
    (cur_tableau_add_tableau_mesh, "tableau_troop_profile_color", ":troop_no", pos1, 0, 0),
    (position_set_z, pos1, 200),
    (cur_tableau_add_tableau_mesh, "tableau_troop_profile_alpha_mask", ":troop_no", pos1, 0, 0),
    ]),

  ("game_party_window", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 300, 300,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_party_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_party_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       ]),

  ("game_troop_label_banner", 0, "tableau_with_transparency", 256, 256, -128, 0, 128, 256,
   [
       (store_script_param, ":banner_mesh", 1),

       # (cur_tableau_set_background_color, 0xFF888888),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 100, 100, 0, 100000),

       (init_position, pos1),
       # (position_set_y, pos1, 120),
       (cur_tableau_add_mesh, ":banner_mesh", pos1, 100, 0),
	   
       #(init_position, pos1),
       #(position_set_z, pos1, 10),
       #(cur_tableau_add_mesh, "mesh_troop_label_banner", pos1, 112, 0),
       ]),

#MOD begin
  ("round_shield_1", 0, "sample_shield_round_1", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_round_1", 240, 60, -50, 0, 0),
	]),
	
  ("round_shield_2", 0, "sample_shield_matte", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_round_2", 240, 60, -50, 0, 0),
	]),
	
  ("round_shield_3", 0, "sample_shield_matte", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_round_3", 240, 60, -50, 0, 0),
	]),
  
	("round_shield_4", 0, "sample_shield_matte", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_round_4", 240, 60, -50, 0, 0),
	]),
  
	("round_shield_5", 0, "sample_shield_matte", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_round_5", 240, 60, -50, 0, 0),
	]),
	
	("small_round_shield_1", 0, "sample_shield_small_round_1", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_small_round_1", 240, 60, -50, 0, 0),
	]),
  
	("small_round_shield_2", 0, "sample_shield_small_round_2", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_small_round_2", 240, 60, -50, 0, 0),
	]),
	
	("small_round_shield_3", 0, "sample_shield_matte", 512, 256, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_small_round_3", 240, 60, -50, 0, 0),
	]),
	
	("kite_shield_4", 0, "sample_tableau_shield_kite_4", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_kite_4", 240, 60, -50, 40, 40),
	]),
	
	("heater_shield_2", 0, "sample_tableau_shield_heater_2", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_heater_2", 240, 60, -50, 25, 25),
	]),
	
	("pavise_shield_1", 0, "sample_tableau_shield_pavise_1", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_pavise_1", 240, 60, -54, 0, 0),
	]),
	
	("pavise_shield_2", 0, "sample_shield_matte", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_pavise_2", 240, 60, -54, 0, 0),
	]),

	("otto_shield1", 0, "sample_shield_matte", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_otto1", 280, 70, -53, -10, -10),
	]),

	("otto_shield2", 0, "sample_shield_matte", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_shield_otto2", 280, 70, -53, -10, -10),
	]),


	("gambeson", 0, "sample_gambeson", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_gambeson", -1, -1, 0, 0, 0),
	]),

	("drz_kaftan", 0, "sample_drz_kaftan", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_drz_kaftan", -1, -1, 0, 0, 0),
	]),

	("gothic_armour", 0, "sample_gothic_armour", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_gothic_armour", -1, -1, 0, 0, 0),
	]),

	("churburg_13", 0, "sample_churburg_13", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_churburg_13", -1, -1, 0, 0, 0),
	]),

	("corrazina", 0, "sample_corrazina", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_corrazina", 200, 50, 19, 54, 71),
	]),

	("drz_mail_shirt", 0, "sample_drz_mail_shirt", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_drz_mail_shirt", -1, -1, 0, 0, 0),
	]),

	("leather_jerkin", 0, "sample_leather_jerkin", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_leather_jerkin", -1, -1, 0, 0, 0),
	]),

	("ragged_outfit", 0, "sample_ragged_outfit", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_ragged_outfit", -1, -1, 0, 0, 0),
	]),

	("leather_vest_a", 0, "sample_leather_vest_a", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_leather_vest_a", -1, -1, 0, 0, 0),
	]),

	("lamellar_leather", 0, "sample_lamellar_leather", 512, 512, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_lamellar_leather", -1, -1, 0, 0, 0),
	]),

	("padded_cloth_a", 0, "sample_padded_cloth_a", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_padded_cloth_a", -1, -1, 0, 0, 0),
	]),

	("padded_cloth_b", 0, "sample_padded_cloth_b", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_padded_cloth_b", -1, -1, 0, 0, 0),
	]),

	("lamellar_vest_a", 0, "sample_lamellar_vest_a", 1024, 1024, 0, 0, 0, 0,
	[
		(store_script_param, ":value", 1),
		(call_script, "script_item_set_banner", ":value", "mesh_tableau_mesh_lamellar_vest_a", -1, -1, 0, 0, 0),
	]),
#MOD end

  ("troop_note_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau", ":troop_no"),
       ]),

  ("troop_note_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFC6BB94),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (call_script, "script_add_troop_to_cur_tableau", ":troop_no"),
       ]),
  
  ("troop_inventory_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_inventory", ":troop_no"),
       ]),

  ("troop_inventory_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF6A583A),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (call_script, "script_add_troop_to_cur_tableau_for_inventory", ":troop_no"),
       ]),

  ("troop_profile_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_profile", ":troop_no"),
       ]),

  ("troop_profile_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFF9E7A8),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (call_script, "script_add_troop_to_cur_tableau_for_profile", ":troop_no"),
       ]),


  ("troop_party_alpha_mask", 0, "mat_troop_portrait_mask", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0x00888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (cur_tableau_render_as_alpha_mask),
       (call_script, "script_add_troop_to_cur_tableau_for_party", ":troop_no"),
       ]),

  ("troop_party_color", 0, "mat_troop_portrait_color", 1024, 1024, 0, 0, 400, 400,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFFBE9C72),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (call_script, "script_add_troop_to_cur_tableau_for_party", ":troop_no"),
       ]),

  ("troop_note_mesh", 0, "tableau_with_transparency", 1024, 1024, 0, 0, 350, 350,
   [
       (store_script_param, ":troop_no", 1),
       (cur_tableau_set_background_color, 0xFF888888),
       (cur_tableau_set_ambient_light, 65, 75, 85),
       (set_fixed_point_multiplier, 100),
       (cur_tableau_set_camera_parameters, 0, 40, 40, 0, 100000),

       (init_position, pos1),
       (position_set_z, pos1, 100),
       (position_set_x, pos1, -20),
       (position_set_y, pos1, -20),
       (cur_tableau_add_tableau_mesh, "tableau_troop_note_color", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 200),
       (cur_tableau_add_tableau_mesh, "tableau_troop_note_alpha_mask", ":troop_no", pos1, 0, 0),
       (position_set_z, pos1, 300),
       (cur_tableau_add_mesh, "mesh_portrait_blend_out", pos1, 0, 0),
       ]),
]
