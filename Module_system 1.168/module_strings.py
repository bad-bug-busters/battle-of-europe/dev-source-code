#MOD begin
from ID_scenes import *
from ID_parties import *
from header_triggers import *
from module_scenes import scenes
from module_parties import parties
from module_skills import skills
#MOD end

# -*- coding: cp1254 -*-
strings = [
  ("no_string", "NO STRING!"),
  ("empty_string", " "),
  ("yes", "Yes."),
  ("no", "No."),
# Strings before this point are hardwired. 
 
  ("s0", "{!}{s0}"),
  ("reg1", "{!}{reg1}"),

  ("credits_1", "Mount&Blade: Warband Copyright 2008-2010 Taleworlds Entertainment"),
  ("credits_2", "Game design:^Armagan Yavuz^Steve Negus^Cem Cimenbicer"),
  ("credits_3", "Programming:^Armagan Yavuz^Cem Cimenbicer^Serdar Kocdemir^Ozan Gumus"),
  ("credits_4", "CG Artists:^Ozgur Saral^Mustafa Ozturk^Pinar Cekic^Ozan Unlu^Yigit Savtur^Umit Singil"),
  ("credits_5", "Concept Artist:^Ganbat Badamkhand"),
  ("credits_6", "Writing:^Steve Negus^Armagan Yavuz^Ryan A. Span"),
  ("credits_7", "Original Music:^Jesse Hopkins"),
  ("credits_8", "Voice Talent:^Tassilo Egloffstein"),
  ("credits_9", "This game has been supported by The Scientific and Technological Research Council of Turkey.^^\
Tutorial written by:^Steve Negus^Armagan Yavuz^Edward Spoerl^^\
Horse Motion Capture Animation Supplied by:^Richard Widgery & Kinetic Impulse^^\
Physics:^Havok^^\
Sound and Music Program Library:^FMODex Sound System by Firelight Technologies^^\
Skybox Textures:^Jay Weston^^\
Chinese Translation:^Hetairoi; Gaodatailang; silentjealousy; Ginn; fallout13; James; D.Kaede; Kan2; alixyang; muyiboy^^\
TaleWorlds Director of Communications:^Ali Erkin^^\
TaleWorlds Forum Programming:^Brett Flannigan ^^^\
TaleWorlds.com Forum Administrators and Moderators:^\
Janus^\
Archonsod^\
Narcissus^\
Nairagorn^\
Lost Lamb^\
Deus Ex^\
Merentha^\
Volkier^\
Instag0^\
Ativan^\
ego^\
Guspav^\
Hallequin^\
Invictus^\
okiN^\
Raz^\
rejenorst^\
Skyrage^\
ThVaz^^^\
Mount&Blade Community Suggestions and Feedback:^\
A_Mustang^\
adamlug^\
Adorno^\
alden^\
Alhanalem^\
amade^\
Anthallas^\
Alkhadias Master^\
Arch3r^\
Archevious^\
Arcas Nebun^\
Arcon^\
Arcturus^\
ares007^\
Arjihad^\
BadabombadaBang^\
Badun^\
BaronAsh^\
Berserker Pride^\
bgfan^\
bierdopjeee^\
Big_Mac^\
Binboy^\
blink180heights^\
BlodsHammar^\
Bloid^\
Brandon^\
Brego^\
chenjielian^\
cifre^\
COGlory^\
Corinthian Hoplite^\
Crazed Rabbit^\
CryptoCactus^\
CtrlAltDe1337^\
Cuther^\
Da-V-Man^\
dimitrischris^\
dstemmer^\
EasyCo506^\
Egbert^\
ethneldryt^\
eudaimondaimon^\
Faranox^\
Fawzia dokhtar-i-Sanjar^\
Fei Dao^\
Gabeed^\
GeN76^\
General_Hospital^\
GhosTR^\
glustrod^\
guspav^\
Halcyon^\
Harn^\
Hethwill^\
Highelfwarrior^\
HULKSMASH^\
Iberon^\
ignoble^\
Jack_Merchantson^\
JoG^\
Jov^\
Kazzan^\
King Jonathan the Great^\
Kleidophoros^\
knight^\
Kong Burger^\
Kristiania^\
l3asu^\
Larkraxm^\
Leandro1021DX^\
lighthaze^\
Llew2^\
Lord Rich^\
lordum_ediz^\
Lucke189^\
Mabons^\
MacPharlan^\
Madnes5^\
MagicMaster^\
Makh^\
ManiK^\
Manitas^\
Marin Peace Bringer^\
Martinet^\
MAXHARDMAN^\
Merlkir^\
miguel8500^\
Mithras^\
Moddan^\
Nate^\
Nemeo^\
Nite/m4re^\
noobalicous^\
Nord Champion^\
okiN^\
Orion^\
OTuphlos^\
Papa Lazarou^\
Phallas^\
Plazek^\
Prcin^\
PSYCHO78^\
PsykoOps^\
Reapy^\
Red River^\
Rhizobium^\
Riggea^\
Rongar^\
Ros^\
sadnhappy^\
Sarejo^\
ScientiaExcelsa^\
Scorch!^\
Seawied86^\
sebal87^\
shikamaru 1993^\
Shun^\
silentdawn^\
Sir Gowe^\
Skyrage^\
Slawomir of Aaarrghh^\
SoloSebo^\
SovietSoldier^\
Stabbing Hobo^\
Stratigos001^\
Styo^\
TalonAquila^\
test^\
The Yogi^\
Thundertrod^\
Thyr^\
Tim^\
Titanshoe^\
tmos^\
Toffey^\
Tonttu^\
Trenalok^\
Tronde^\
UberWiggett^\
Urist^\
Ursca^\
urtzi^\
Vermin^\
Viajero^\
Vincenzo^\
Vulkan^\
Warcat92^\
Welcome_To_Hell^\
Wheem^\
Wu-long^\
Yellonet^\
Yobbo^\
Yoshi Murasaki^\
Yoshiboy^\
Zyconnic^^^\
Special Thanks to Toby Lee for his ideas and in depth feedback on the combat system.^\
...and many many other wonderful Mount&Blade players!^^\
(This is only a small sample of all the players who have contributed to the game by providing suggestions and feedback.^\
This list has been compiled by sampling only a few threads in the Taleworlds Forums.^\
Unfortunately compiling an exhaustive list is almost impossible.^\
We apologize sincerely if you contributed your suggestions and feedback but were not listed here, and please know that we are grateful to you all the same...)\
"),
  ("credits_10", "Paradox Interactive^^President and CEO:^Theodore Bergqvist^^Executive Vice President:^Fredrik Wester\
^^Chief Financial Officer:^Lena Eriksson^^Finance & Accounting:^Annlouise Larsson^^VP Sales & Marketing US:^Reena M. Miranda\
^^VP Sales & Marketing EU:^Martin Sirc^^Distribution Manager Nordic:^Erik Helmfridsson^^Director of PR & Marketing:^Susana Meza\
^^PR & Marketing:^Sofia Forsgren^^Product Manager:^Boel Bermann\
"),
  ("credits_11", "Logotype:^Jason Brown^^Cover Art:^Piotr Fox Wysocki\
^^Layout:^Christian Sabe^Melina Grundel^^Poster:^Piotr Fox Wysocki^^Map & Concept Art:^Ganbat Badamkhand\
^^Manual Editing:^Digital Wordsmithing: Ryan Newman, Nick Stewart^^Web:^Martin Ericsson^^Marketing Assets:^2Coats\
^^Localization:^S&H Entertainment Localization^^GamersGate:^Ulf Hedblom^Andreas Pousette^Martin Ericson^Christoffer Lindberg\
"),
  ("credits_12", "Thanks to all of our partners worldwide, in particular long-term partners:\
^Koch Media (Germany & UK)^Blue Label (Italy & France)^Friendware (Spain)^New Era Interactive Media Co. Ltd. (Asia)\
^Snowball (Russia)^Pinnacle (UK)^Porto Editora (Portugal)^Hell-Tech (Greece)^CD Projekt (Poland, Czech Republic, Slovakia & Hungary)\
^Paradox Scandinavian Distribution (Scandinavia)\
"),

#### Warband added texts

#multiplayer game type names
  ("multi_game_type_1", "Deathmatch"),
  ("multi_game_type_2", "Team Deathmatch"),
  ("multi_game_type_3", "Battle"),
  ("multi_game_type_7", "Siege"),
  ("multi_game_type_8", "Duel"),
  ("multi_game_types_end", "multi_game_types_end"),

  ("poll_kick_player_s1_by_s0", "{s0} started a poll to kick player {s1}."),
  ("poll_ban_player_s1_by_s0", "{s0} started a poll to ban player {s1}."),
  ("poll_change_map_to_s1_by_s0", "{s0} started a poll to change map to {s1}."),
  ("poll_change_map_to_s1_and_factions_to_s2_and_s3_by_s0", "{s0} started a poll to change map to {s1} and factions to {s2} and {s3}."),
  ("poll_change_number_of_bots_to_reg0_and_reg1_by_s0", "{s0} started a poll to change bot counts to {reg0} and {reg1}."),

  ("poll_kick_player", "Poll to kick player {s0}: 1 = Accept, 2 = Decline"),
  ("poll_ban_player", "Poll to ban player {s0}: 1 = Accept, 2 = Decline"),
  ("poll_change_map", "Poll to change map to {s0}: 1 = Accept, 2 = Decline"),
  ("poll_change_map_with_faction", "Poll to change map to {s0} and factions to {s1} versus {s2}: 1 = Accept, 2 = Decline"),
  ("poll_change_number_of_bots", "Poll to change number of bots to {reg0} for {s0} and {reg1} for {s1}: 1 = Accept, 2 = Decline"),
  ("poll_time_left", "({reg0} seconds left)"),
  ("poll_result_yes", "The poll is accepted by the majority."),
  ("poll_result_no", "The poll is rejected by the majority."),

  # ("total_item_cost_reg0", "Total cost: {reg0}"),

  ("server_name", "Server name:"),
  ("game_password", "Game password:"),
  ("map", "Map:"),
  ("game_type", "Game type:"),
  ("max_number_of_players", "Maximum number of players:"),
  ("number_of_bots_in_team_reg1", "Number of bots in team {reg1}:"), 
  ("team_reg1_faction", "Team {reg1} faction:"),
  # ("enable_valve_anti_cheat", "Enable Valve Anti-cheat (Requires valid Steam account)"),
  # ("allow_friendly_fire", "Allow ranged friendly fire"),
  # ("allow_melee_friendly_fire", "Allow melee friendly fire"),
  # ("allow_horse_friendly_fire", "Allow horse friendly fire"),#MOD
  # ("friendly_fire_damage_self_ratio", "Friendly fire damage self (%):"),
  # ("friendly_fire_damage_friend_ratio", "Friendly fire damage friend (%):"),
  ("spectator_camera", "Spectator camera:"),
  ("control_block_direction", "Control block direction:"),
  # ("map_time_limit", "Map time limit (minutes):"),
  ("round_time_limit", "Round time limit (seconds):"),
  ("players_take_control_of_a_bot_after_death", "Switch to bot on death:"),
  ("team_points_limit", "Team point limit:"),
  ("point_gained_from_flags", "Team points gained for flags (%):"),
  ("point_gained_from_capturing_flag", "Points gained for capturing flags:"),
  ("respawn_period", "Respawn period (seconds):"),
  ("add_to_official_game_servers_list", "Add to official game servers list"),
  ("combat_speed", "Combat_speed:"),
  ("combat_speed_0", "Slowest"),
  ("combat_speed_1", "Slower"),
  ("combat_speed_2", "Medium"),
  ("combat_speed_3", "Faster"),
  ("combat_speed_4", "Fastest"),
  ("off", "Off"),
  ("on", "On"),
  ("defender_spawn_count_limit", "Defender spawn count:"),
  ("unlimited", "Unlimited"),
  ("automatic", "Automatic"),
  ("by_mouse_movement", "By mouse movement"),
  ("free", "Free"),
  ("stick_to_any_player", "Lock to any player"),
  ("stick_to_team_members", "Lock to team members"),
  ("stick_to_team_members_view", "Lock to team members' view"),
  ("make_factions_voteable", "Allow polls to change factions"),
  ("make_kick_voteable", "Allow polls to kick players"),
  ("make_ban_voteable", "Allow polls to ban players"),
  ("bots_upper_limit_for_votes", "Bot count limit for polls:"),
  ("make_maps_voteable", "Allow polls to change maps"),
  # ("valid_vote_ratio", "Poll accept threshold (%):"),
  ("auto_team_balance_limit", "Auto team balance threshold (diff.):"),
  ("welcome_message", "Welcome message:"),
  # ("initial_gold_multiplier", "Starting gold (%):"),
  # ("battle_earnings_multiplier", "Combat gold bonus (%):"),
  # ("round_earnings_multiplier", "Round gold bonus (%):"),
  ("allow_player_banners", "Allow individual banners"),
  # ("force_default_armor", "Force minimum armor"),#MOD disable
  
  ("reg0", "{!}{reg0}"),
  ("s0_reg0", "{!}{s0} {reg0}"),
  ("s0_s1", "{!}{s0} {s1}"),
  ("reg0_dd_reg1reg2", "{!}{reg0}:{reg1}{reg2}"),
  ("s0_dd_reg0", "{!}{s0}: {reg0}"),
  ("respawning_in_reg0_seconds", "Respawning in {reg0} seconds..."),
  ("no_more_respawns_remained_this_round", "No lives left for this round"),
  ("reg0_respawns_remained", "({reg0} lives remaining)"),
  ("this_is_your_last_respawn", "(This is your last life)"),
  ("wait_next_round", "(Wait for the next round)"),

  ("yes_wo_dot", "Yes"),
  ("no_wo_dot", "No"),

  ("s1_returned_flag", "{s1} has returned their flag to their base!"),
  ("s1_auto_returned_flag", "{s1} flag automatically returned to their base!"),
  ("s1_captured_flag", "{s1} has captured the enemy flag!"),
  ("s1_taken_flag", "{s1} has taken the enemy flag!"),
  ("s1_neutralized_flag_reg0", "{s1} has neutralized flag {reg0}."),
  ("s1_captured_flag_reg0", "{s1} has captured flag {reg0}!"),
  ("s1_pulling_flag_reg0", "{s1} has started pulling flag {reg0}."),

  ("s1_destroyed_target_0", "{s1} destroyed target A!"),
  ("s1_destroyed_target_1", "{s1} destroyed target B!"),
  ("s1_destroyed_catapult", "{s1} destroyed the catapult!"),
  ("s1_destroyed_trebuchet", "{s1} destroyed the trebuchet!"),
  ("s1_destroyed_all_targets", "{s1} destroyed all targets!"),
  
  ("s1_defended_castle", "{s1} defended their castle!"),
  ("s1_captured_castle", "{s1} captured the castle!"),
  
  ("auto_team_balance_in_20_seconds", "Auto-balance will be done in 20 seconds."),
  ("auto_team_balance_next_round", "Auto-balance will be done next round."),
  ("auto_team_balance_done", "Teams have been auto-balanced."),
  ("s1_won_round", "{s1} has won the round!"),
  ("round_draw", "Time is up. Round draw."),
  ("round_draw_no_one_remained", "No one left. Round draw."),

  ("reset_to_default", "Reset to Default"),
  ("done", "Done"),
  ("player_name", "Player Name"),
  ("kills", "Kills"),
  ("deaths", "Deaths"),
  ("ping", "Ping"),
  ("dead", "Dead"),
  ("reg0_dead", "{reg0} Dead"),
  ("bots_reg0_agents", "Bots ({reg0} agents)"),
  ("bot_1_agent", "Bot (1 agent)"),
  ("score_reg0", "Score: {reg0}"),
  ("flags_reg0", "(Flags: {reg0})"),
  ("reg0_players", "({reg0} players)"),
  ("reg0_player", "({reg0} player)"),

  ("open_gate", "Open Gate"),
  ("close_gate", "Close Gate"),
  ("open_door", "Open Door"),
  ("close_door", "Close Door"),
  ("raise_ladder", "Raise Ladder"),
  ("drop_ladder", "Drop Ladder"),

  ("back", "Back"),
  ("start_map", "Start Map"),

  ("choose_an_option", "Choose an option:"),
  ("choose_a_poll_type", "Choose a poll type:"),
  ("choose_faction", "Choose Faction"),
  ("choose_a_faction", "Choose a faction:"),
  ("choose_troop", "Choose Troop"),
  ("choose_a_troop", "Choose a troop class:"),
  ("choose_items", "Choose Equipment"),
  ("options", "Options"),
  ("redefine_keys", "Redefine Keys"),
  ("submit_a_poll", "Submit a Poll"),
  ("administrator_panel", "Administrator Panel"),
  ("kick_player", "Kick Player"),
  ("ban_player", "Ban Player"),
  ("mute_player", "Mute Player"),
  ("unmute_player", "Unmute Player"),
  ("quit", "Quit"),
  ("poll_for_changing_the_map", "Change the map"),
  ("poll_for_changing_the_map_and_factions", "Change the map and factions"),
  ("poll_for_changing_number_of_bots", "Change number of bots in teams"),
  ("poll_for_kicking_a_player", "Kick a player"),
  ("poll_for_banning_a_player", "Ban a player"),
  ("choose_a_player", "Choose a player:"),
  ("choose_a_map", "Choose a map:"),
  ("choose_a_faction_for_team_reg0", "Choose a faction for team {reg0}:"),
  ("choose_number_of_bots_for_team_reg0", "Choose number of bots for team {reg0}:"),
  ("spectator", "Spectator"),
  ("spectators", "Spectators"),
  ("score", "Score"),
  ("command", "Command:"),
  ("use_default_banner", "Use Faction's Banner"),
	
  ("team_reg0_bot_count_is_reg1", "{!}Team {reg0} bot count is {reg1}."),
  ("input_is_not_correct_for_the_command_type_help_for_more_information", "{!}Input is not correct for the command. Type 'help' for more information."),
  ("maximum_seconds_for_round_is_reg0", "Maximum seconds for round is {reg0}."),
  ("respawn_period_is_reg0_seconds", "Respawn period is {reg0} seconds."),
  ("bots_upper_limit_for_votes_is_reg0", "Bots upper limit for votes is {reg0}."),
  ("map_is_voteable", "Map is voteable."),
  ("map_is_not_voteable", "Map is not voteable."),
  ("factions_are_voteable", "Factions are voteable."),
  ("factions_are_not_voteable", "Factions are not voteable."),
  ("players_respawn_as_bot", "Players respawn as bot."),
  ("players_do_not_respawn_as_bot", "Players do not respawn as bot."),
  ("kicking_a_player_is_voteable", "Kicking a player is voteable."),
  ("kicking_a_player_is_not_voteable", "Kicking a player is not voteable."),
  ("banning_a_player_is_voteable", "Banning a player is voteable."),
  ("banning_a_player_is_not_voteable", "Banning a player is not voteable."),
  ("player_banners_are_allowed", "Player banners are allowed."),
  ("player_banners_are_not_allowed", "Player banners are not allowed."),
  ("default_armor_is_forced", "All items for faction troops are allowed."),#MOD
  ("default_armor_is_not_forced", "Only faction troop items are allowed."),#MOD
  # ("percentage_of_yes_votes_required_for_a_poll_to_get_accepted_is_reg0", "Percentage of yes votes required for a poll to get accepted is {reg0}%."),
  ("auto_team_balance_threshold_is_reg0", "Auto team balance threshold is {reg0}."),
  ("starting_gold_ratio_is_reg0", "Starting gold is {reg0}."),#MOD
  ("combat_gold_bonus_ratio_is_reg0", "Score gold bonus is {reg0}."),#MOD
  ("round_gold_bonus_ratio_is_reg0", "Tick gold bonus is {reg0}."),#MOD
  # ("point_gained_from_flags_is_reg0", "Team points gained for flags is {reg0}%."),
  # ("point_gained_from_capturing_flag_is_reg0", "Points gained for capturing flags is {reg0}%."),
  # ("map_time_limit_is_reg0", "Map time limit is {reg0} minutes."),
  ("team_points_limit_is_reg0", "Team point limit is {reg0}."),
  ("defender_spawn_count_limit_is_s1", "Defender spawn count is {s1}."),
  ("system_error", "SYSTEM ERROR!"),
 
  ("s2_s3", "{!}{s2}^{s3}"),

  ("server_name_s0", "Server Name: {s0}"),
  ("map_name_s0", "Map Name: {s0}"),
  ("game_type_s0", "Game Type: {s0}"),  

	("a_duel_request_is_sent_to_s0", "A duel offer is sent to {s0}."),
  ("s0_offers_a_duel_with_you", "{s0} offers a duel with you."),
  ("your_duel_with_s0_is_cancelled", "Your duel with {s0} is cancelled."),
  ("a_duel_between_you_and_s0_will_start_in_3_seconds", "A duel between you and {s0} will start in 3 seconds."),
  ("you_have_lost_a_duel", "You have lost a duel."),
  ("you_have_won_a_duel", "You have won a duel!"),
  ("server_s0", "[!]: {s0}"),
  # ("disallow_ranged_weapons", "Disallow ranged weapons"),
  # ("ranged_weapons_are_disallowed", "Ranged weapons are disallowed."),
  # ("ranged_weapons_are_allowed", "Ranged weapons are allowed."),
  ("duel_starts_in_reg0_seconds", "Duel starts in {reg0} seconds..."),

  ("banner_selection_text", "You have been awarded the right to carry a banner.\
 Your banner will signify your status and bring you honour. Which banner do you want to choose?"),
  ("profile_banner_selection_text", "Choose a banner for your profile:"),

#MOD begin
  ("delivered_damage_to_player", "Delivered {reg0} damage."),
  ("horse_delivered_damage", "Horse delivered {reg0} damage."),
  ("delivered_damage_to_horse", "Delivered {reg0} damage to horse."),
  ("received_damage", "Received {reg0} damage."),
  ("horse_received_damage", "Horse received {reg0} damage."),
	("delivered_damage_to_shield", "Delivered {reg0} damage to shield."),
	("shield_received_damage", "Shield received {reg0} damage."),
	("delivered_damage_to_object", "Delivered {reg0} damage to object."),
  
  ("equip", "Equip"),
  # ("cant_sprint", "You cant sprint while attacking/blocking or with low stamina."),

  ("choose_ammo", "Choose Ammunition"),
  
  #carry slots
  ("carry_slot_display_s1", "Carry slot: {s1}"),
  
  ("carry_slot_0", "hands(can't sheath)"),
  ("carry_slot_1", "back 1"),
  ("carry_slot_2", "back 2"),
  ("carry_slot_3", "back 3"),
  ("carry_slot_4", "left stomach"),
  ("carry_slot_5", "right stomach"),
  ("carry_slot_6", "left hip 1"),
  ("carry_slot_7", "left hip 2"),
  ("carry_slot_8", "left hip 3"),
  ("carry_slot_9", "left hip 4"),
  ("carry_slot_10", "right hip"),
  ("carry_slot_11", "right upper leg"),
	
	("shield_damage_reg1_s1", "Bash: {reg1}{s1}"),
	
	("info_grenade", "Explodes and deals area damage"),
	("info_firepot", "Sets stuff on fire"),
  ("info_engineer_hammer", "Used to build^and repair structures"),
  ("info_surgeon_kit", "Stops bleeding and restores^the health lost due to it"),
	("info_missile_ignition_kit", "Ignites arrows and bolts"),
	("info_ladder_5m", "Can be deployed"),

	("order_movement_run", "{s10} - Always run"),
	("order_movement_walk", "{s10} - Run when necessary"),
  ("order_shot_volley", "{s10} - Shoot in volleys"),
	("order_shot_fire", "{s10} - Ignite missiles"),
	("order_shot_normal", "{s10} - Quench missiles"),
	
	("ui_order_movement_run", "[{s1}]: {s2}, always run!"),
	("ui_order_movement_walk", "[{s1}]: {s2}, run when necessary!"),
  ("ui_order_shot_volley", "[{s1}]: {s2}, shoot in volleys!"),
	("ui_order_shot_fire", "[{s1}]: {s2}, ignite missiles!"),
	("ui_order_shot_normal", "[{s1}]: {s2}, quench missiles!"),
	
	
	("update", "Update"),
	("reset_password", "Reset Password"),
  
  ("mod_menu", "Mod Menu"),
  ("show_damage_report", "Show damage report"),
  ("show_shot_distance", "Show shot distance"),
  ("show_movement_speed", "Show movement speed"),
  ("refresh", "Refresh"),

  # ("disallow_melee_weapons", "Disallow meele weapons"),
  # ("disallow_horses", "Disallow horses"),
  # ("disallow_armors", "Disallow armors"),

  ("cloud_amount", "Cloud Amount"),
	("precipitation_strength", "Precipitation Strength"),
	("fog_distance", "Fog Distance"),
	("thunderstorm", "Thunderstorm"),
	("wind_strength", "Wind Strength"),
	("wind_direction", "Wind Direction"),
  ("day_time", "Day Time"),
  
	# ("server_announcement", "Join our Events: Saturday(Team Deathmatch)/Sunday(Siege) at 6pm GMT(NA:-6)"),
	("server_announcement", "Join our Discord for more info, updates and discussions: discord.gg/z2EMg89"),
  ("main_info", "Discord: discord.gg/z2EMg89"),
	
	("manage_players", "Manage_Players"),

	("clan_chat", "Send Message to Clan/Regiment"),
	("admin_chat", "Send Message to Admins"),
  ("admin_message", "Send Admin Message"),

	("muted", "Muted"),

	("mute", "Mute"),
	("unmute", "Unmute"),
	("kick", "Kick"),
	("ban", "Ban"),
	("temp_ban", "Temp Ban"),
	("swap_team", "Swap Team"),
	("swap_spec", "Swap to Spectators"),
	("slay", "Slay"),
	("restore", "Restore"),
	("tp_to_me", "Teleport to me"),
	("tp_to_player", "Teleport to Player"),
	
  ("tp_me", "Teleport Forward"),
  
	("website_url_1", "battleofeurope.net"),
	("service_url_1", "http://battleofeurope.net/api/Game/{s10}"),
	("service_action_1", "UpdateServerStatus?players={s4}&token={s1}&server={s2}&reqId={reg2}"),
	("service_action_2", "GetChar?id={reg1}&name={s3}&withStats={reg3}&token={s1}&server={s2}&reqId={reg2}"),
	("service_action_3", "DoTick?ids={s3}&token={s1}&server={s2}&reqId={reg2}"),
	("service_action_4", "Upkeep?usages={s3}&token={s1}&server={s2}&reqId={reg2}"),
	("service_action_5", "ResetPassword?id={reg1}&token={s1}&server={s2}&reqId={reg2}"),
	("server_security_token", "token"),

	("att_0_name", "Strength"),
	("att_1_name", "Agility"),
	("att_2_name", "Intelligence"),
	("att_3_name", "Charisma"),

	("att_0_info", "0"),
	("att_1_info", "0"),
	("att_2_info", "0"),
	("att_3_info", "0"),

	("wpf_0_name", "One Handed"),
	("wpf_1_name", "Two Handed"),
	("wpf_2_name", "Polearm"),
	("wpf_3_name", "Archery"),
	("wpf_4_name", "Crossbow"),
	("wpf_5_name", "Throwing"),
	("wpf_6_name", "Firearm"),

	("wpf_0_info", "0"),
	("wpf_1_info", "0"),
	("wpf_2_info", "0"),
	("wpf_3_info", "0"),
	("wpf_4_info", "0"),
	("wpf_5_info", "0"),
	("wpf_6_info", "0"),
	
	("construct_prop_0", "Box"),
	("construct_prop_1", "Large Shield"),
	("construct_prop_2", "Spikes"),
	("construct_prop_3", "Plank"),
	("construct_prop_4", "Earthwork"),
	("construct_prop_5", "Gabion"),
]

for i in range(len(skills)):
	strings +=("skl_" +str(i)+ "_name", str(skills[i][1])),

for i in range(len(skills)):
	strings +=("skl_" +str(i)+ "_info", str(skills[i][4])),

for i in range(0x00, 0xff +1):
	if i == key_1:
		str_key = '1'
	elif i == key_2:
		str_key = '2'
	elif i == key_3:
		str_key = '3'
	elif i == key_4:
		str_key = '4'
	elif i == key_5:
		str_key = '5'
	elif i == key_6:
		str_key = '6'
	elif i == key_7:
		str_key = '7'
	elif i == key_8:
		str_key = '8'
	elif i == key_9:
		str_key = '9'
	elif i == key_0:
		str_key = '0'
	elif i == key_a:
		str_key = 'A'
	elif i == key_b:
		str_key = 'B'
	elif i == key_c:
		str_key = 'C'
	elif i == key_d:
		str_key = 'D'
	elif i == key_e:
		str_key = 'E'
	elif i == key_f:
		str_key = 'F'
	elif i == key_g:
		str_key = 'G'
	elif i == key_h:
		str_key = 'H'
	elif i == key_i:
		str_key = 'I'
	elif i == key_j:
		str_key = 'J'
	elif i == key_k:
		str_key = 'K'
	elif i == key_l:
		str_key = 'L'
	elif i == key_m:
		str_key = 'M'
	elif i == key_n:
		str_key = 'N'
	elif i == key_o:
		str_key = 'O'
	elif i == key_p:
		str_key = 'P'
	elif i == key_q:
		str_key = 'Q'
	elif i == key_r:
		str_key = 'R'
	elif i == key_s:
		str_key = 'S'
	elif i == key_t:
		str_key = 'T'
	elif i == key_u:
		str_key = 'U'
	elif i == key_v:
		str_key = 'V'
	elif i == key_w:
		str_key = 'W'
	elif i == key_x:
		str_key = 'X'
	elif i == key_y:
		str_key = 'Y'
	elif i == key_z:
		str_key = 'Z'
	elif i == key_numpad_0:
		str_key = 'Numpad 0'
	elif i == key_numpad_1:
		str_key = 'Numpad 1'
	elif i == key_numpad_2:
		str_key = 'Numpad 2'
	elif i == key_numpad_3:
		str_key = 'Numpad 3'
	elif i == key_numpad_4:
		str_key = 'Numpad 4'
	elif i == key_numpad_5:
		str_key = 'Numpad 5'
	elif i == key_numpad_6:
		str_key = 'Numpad 6'
	elif i == key_numpad_7:
		str_key = 'Numpad 7'
	elif i == key_numpad_8:
		str_key = 'Numpad 8'
	elif i == key_numpad_9:
		str_key = 'Numpad 9'
	elif i == key_num_lock:
		str_key = 'Num Lock'
	elif i == key_numpad_slash:
		str_key = 'Numpad DIV'
	elif i == key_numpad_multiply:
		str_key = 'Numpad MUL'
	elif i == key_numpad_minus:
		str_key = 'Numpad MIN'
	elif i == key_numpad_plus:
		str_key = 'Numpad PLUS'
	elif i == key_numpad_enter:
		str_key = 'Numpad ENTER'
	elif i == key_numpad_period:
		str_key = 'Numpad DEL'
	elif i == key_insert:
		str_key = 'Insert'
	elif i == key_delete:
		str_key = 'Delete'
	elif i == key_home:
		str_key = 'Home'
	elif i == key_end:
		str_key = 'End'
	elif i == key_page_up:
		str_key = 'Page Up'
	elif i == key_page_down:
		str_key = 'Page Down'
	elif i == key_up:
		str_key = 'Up'
	elif i == key_down:
		str_key = 'Down'
	elif i == key_left:
		str_key = 'Left'
	elif i == key_right:
		str_key = 'Right'
	elif i == key_f1:
		str_key = 'F1'
	elif i == key_f2:
		str_key = 'F2'
	elif i == key_f3:
		str_key = 'F3'
	elif i == key_f4:
		str_key = 'F4'
	elif i == key_f5:
		str_key = 'F5'
	elif i == key_f6:
		str_key = 'F6'
	elif i == key_f7:
		str_key = 'F7'
	elif i == key_f8:
		str_key = 'F8'
	elif i == key_f9:
		str_key = 'F9'
	elif i == key_f10:
		str_key = 'F10'
	elif i == key_f11:
		str_key = 'F11'
	elif i == key_f12:
		str_key = 'F12'
	elif i == key_space:
		str_key = 'Space Bar'
	elif i == key_enter:
		str_key = 'Enter'
	elif i == key_tab:
		str_key = 'Tab'
	elif i == key_back_space:
		str_key = 'Backspace'
	elif i == key_open_braces:
		str_key = ' [ '
	elif i == key_close_braces:
		str_key = ' ] '
	elif i == key_comma:
		str_key = ' , '
	elif i == key_period:
		str_key = ' > '
	elif i == key_slash:
		str_key = ' / '
	elif i == key_back_slash:
		str_key = ' \ '
	elif i == key_equals:
		str_key = ' = '
	elif i == key_minus:
		str_key = ' - '
	elif i == key_semicolon:
		str_key = 'Semicolon'
	elif i == key_apostrophe:
		str_key = 'Apostrophe'
	elif i == key_tilde:
		str_key = 'Tilde'
	elif i == key_caps_lock:
		str_key = 'Caps Lock'
	elif i == key_left_shift:
		str_key = 'Left Shift'
	elif i == key_right_shift:
		str_key = 'Right Shift'
	elif i == key_left_control:
		str_key = 'Left Ctrl'
	elif i == key_right_control:
		str_key = 'Right Ctrl'
	elif i == key_left_alt:
		str_key = 'Left Alt'
	elif i == key_right_alt:
		str_key = 'Right Alt'
	elif i == key_left_mouse_button:
		str_key = 'Left Click'
	elif i == key_right_mouse_button:
		str_key = 'Right Click'
	elif i == key_middle_mouse_button:
		str_key = 'Middle Mouse Button'
	elif i == key_mouse_button_4:
		str_key = 'Mouse Button 4'
	elif i == key_mouse_button_5:
		str_key = 'Mouse Button 5'
	elif i == key_mouse_button_6:
		str_key = 'Mouse Button 6'
	elif i == key_mouse_button_7:
		str_key = 'Mouse Button 7'
	elif i == key_mouse_button_8:
		str_key = 'Mouse Button 8'
	elif i == key_mouse_scroll_up:
		str_key = 'Mouse Scroll Up'
	elif i == key_mouse_scroll_down:
		str_key = 'Mouse Scroll Down'
	else:
		str_key = str(hex(i))
	strings +=("key_"+str(i), str_key),


for i in range(len(scenes)):
	if i == scn_random_scene_steppe:
		str_scene = 'Random Steppe'
	elif i == scn_random_scene_plain:
		str_scene = 'Random Plains'
	elif i == scn_random_scene_snow:
		str_scene = 'Random Snowy Plains'
	elif i == scn_random_scene_desert:
		str_scene = 'Random Desert'
	elif i == scn_random_scene_steppe_forest:
		str_scene = 'Random Steppe Forest'
	elif i == scn_random_scene_plain_forest:
		str_scene = 'Random Forest'
	elif i == scn_random_scene_snow_forest:
		str_scene = 'Random Snowy Forest'
	elif i == scn_random_scene_desert_forest:
		str_scene = 'Random Desert Forest'
		
	elif i == scn_quick_battle_1:
		str_scene = 'Outpost Farm'
	elif i == scn_quick_battle_4:
		str_scene = 'Forest Fort'
		
	elif i == scn_multi_scene_1:
		str_scene = 'Ruins'
	elif i == scn_multi_scene_2:
		str_scene = 'Village'
	elif i == scn_multi_scene_3:
		str_scene = 'Hailes Castle'
	elif i == scn_multi_scene_4:
		str_scene = 'Ruined Fort'
	elif i == scn_multi_scene_5:
		str_scene = 'Hill Town'
	elif i == scn_multi_scene_6:
		str_scene = 'Abandoned Village'
	elif i == scn_multi_scene_7:
		str_scene = 'Field by the River'
	elif i == scn_multi_scene_8:
		str_scene = 'Rudkhan Castle'
	elif i == scn_multi_scene_10:
		str_scene = 'Turin Castle'
	elif i == scn_multi_scene_15:
		str_scene = 'Mahdaar Castle'
	elif i == scn_multi_scene_16:
		str_scene = 'Jameyyed Castle'
	elif i == scn_multi_scene_17:
		str_scene = 'The Arena'
	elif i == scn_multi_scene_19:
		str_scene = 'Canyon'
	elif i == scn_multi_scene_20:
		str_scene = 'Desert Town'
	elif i == scn_multi_scene_22:
		str_scene = 'Flodden Field'
	elif i == scn_multi_scene_23:
		str_scene = 'River Fort'
		
	elif i == scn_quick_battle_scene_1:
		str_scene = 'Farmhouse'
	elif i == scn_quick_battle_scene_2:
		str_scene = 'Oasis'
	elif i == scn_quick_battle_scene_3:
		str_scene = 'Tulbuk Pass'
	elif i == scn_quick_battle_scene_4:
		str_scene = 'Haima Castle'
	elif i == scn_quick_battle_scene_5:
		str_scene = 'Ulbas Castle'
		
	elif i == scn_boe_map_1:
		str_scene = 'Azincourt'
	elif i == scn_multi_scene_21:
		str_scene = 'Test Area'
		
	elif i == scn_multi_outer_1:
		str_scene = 'Old Citadel'
	elif i == scn_multi_outer_2:
		str_scene = 'Irish Cliffs'
	elif i == scn_multi_outer_3:
		str_scene = 'Burned Village'
	elif i == scn_multi_outer_4:
		str_scene = 'River Crossing'
	elif i == scn_multi_outer_5:
		str_scene = 'Forest Island'
	elif i == scn_multi_outer_6:
		str_scene = 'Sandy Island'
		
	elif i == scn_boe_castle_1:
		str_scene = 'Small Fort'
	elif i == scn_boe_castle_2:
		str_scene = 'Swamp Castle'
		
	else:
		str_scene = scenes[i][0]
	strings +=("scene_"+str(i), str_scene),
#MOD end