from header_common import *
from header_operations import *
from header_triggers import *
from header_scenes import *
from module_constants import *

####################################################################################################################
#  Each scene record contains the following fields:
#  1) Scene id {string}: used for referencing scenes in other files. The prefix scn_ is automatically added before each scene-id.
#  2) Scene flags {int}. See header_scenes.py for a list of available flags
#  3) Mesh name {string}: This is used for indoor scenes only. Use the keyword "none" for outdoor scenes.
#  4) Body name {string}: This is used for indoor scenes only. Use the keyword "none" for outdoor scenes.
#  5) Min-pos {(float,float)}: minimum (x,y) coordinate. Player can't move beyond this limit.
#  6) Max-pos {(float,float)}: maximum (x,y) coordinate. Player can't move beyond this limit.
#  7) Water-level {float}. 
#  8) Terrain code {string}: You can obtain the terrain code by copying it from the terrain generator screen
#  9) List of other scenes accessible from this scene {list of strings}.
#     (deprecated. This will probably be removed in future versions of the module system)
#     (In the new system passages are used to travel between scenes and
#     the passage's variation-no is used to select the game menu item that the passage leads to.)
# 10) List of chest-troops used in this scene {list of strings}. You can access chests by placing them in edit mode.
#     The chest's variation-no is used with this list for selecting which troop's inventory it will access.
####################################################################################################################

scenes = [
  ("water",0,"none", "none", (0,0),(0,0),0,"0",
    [],[]),
  ("random_scene_steppe",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000232642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_steppe"),
  ("random_scene_plain",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000332642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_plain"),
  ("random_scene_snow",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000432642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_snow"),
  ("random_scene_desert",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000532642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_desert"),
  ("random_scene_steppe_forest",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000a32642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_steppe"),
  ("random_scene_plain_forest",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000b32642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_plain"),
  ("random_scene_snow_forest",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000cfe642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_snow"),
  ("random_scene_desert_forest",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x00000000dfe642800007d1f4000000000000000000000000",
    [],[], "outer_terrain_desert"),
  ("quick_battle_1",sf_generate,"none", "none", (0,0),(0,0),0,"0x30401ee300059966000001bf0000299a0000638f", 
    [],[], "outer_terrain_plain"),
  ("quick_battle_4",sf_generate,"none", "none", (0,0),(0,0),0,"0x00001d63c005114300006228000053bf00004eb9", 
    [],[], "outer_terrain_plain"),

# multiplayer
#MOD begin
  ("multi_scene_1",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000001300389800003a4ea000058340000637a0000399b",
    [],[],"outer_terrain_plain"),# Ruins
  ("multi_scene_2",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000012002a0b20004992700006e54000007fe00001fd2",
    [],[],"outer_terrain_steppe"),# Village
  ("multi_scene_3",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000013002e0b20005154500006e540000235600007b55",
    [],[],"outer_terrain_plain"),# Hailes Castle
  ("multi_scene_4",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000001300659630003c8f300003ca000006a8900003c89",
    [],[],"outer_terrain_plain"),# Ruined Fort
  ("multi_scene_5",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000023002a1ba0004210900003ca000006a8900007a7b",
    [],[],"outer_terrain_plain"),# Hill Town
  ("multi_scene_6",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000002300494b200048524000059e80000453300001d32",
    [],[],"outer_terrain_plain"),# Ruins Field
  ("multi_scene_7",sf_generate,"none", "none", (0,0),(0,0),0,"0x0000000130010e0e0005fd84000011c60000285b00005cbe",
    [],[],"outer_terrain_plain"),# Field by the River
  ("multi_scene_8",sf_generate,"none", "none", (0,0),(0,0),0,"0x0000000020004db18004611400005c918000397b00004c2e",
    [],[],"outer_terrain_steppe"),# Rudkhan Castle
  ("multi_scene_10",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000003009cde1000599630000423b00005756000000af",
    [],[],"outer_terrain_plain"),# Turin Castle
  ("multi_scene_15",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000500b1d158005394c00001230800072880000018f",
    [],[],"outer_terrain_desert"),# Mahdaar Castle
  ("multi_scene_16",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000d007abd20002c8b1000050c50000752a0000788c",
    [],[],"outer_terrain_desert"),# Jameyyed Castle
  ("multi_scene_17",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000002200005000005f57b00005885000046bd00006d9c",
    [],[],"outer_terrain_steppe"),# The Arena
  # ("multi_scene_18",sf_generate|sf_muddy_water,"none", "none", (0,0),(100,100),-100,"0x00000000b00037630002308c00000c9400005d4c00000f3a",
    # [],[],"outer_terrain_plain"),# Forest Hideout
  ("multi_scene_19",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000001300389800003a4ea000058340000637a0000399b",
    [],[],"outer_terrain_steppe"),# Canyon
  ("multi_scene_20",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000013002ab630004651800000d7a00007f3100002701",
    [],[],"outer_terrain_desert"),# Desert Town
  ("multi_scene_22",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000 ",
    [],[],"outer_terrain_plain"),# Flodden Field
  ("multi_scene_23",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain"),# River Fort
  ("boe_map_1",sf_generate,"none", "none", (0,0),(0,0),0,"0x0000000230000000000fa3e8000000000000000000000000",
    [],[], "outer_terrain_plain"),# Azincourt
  
  ("multi_scene_21",sf_generate|sf_auto_entry_points,"none", "none", (0,0),(0,0),0,"0x0000000330000000000ff3fc000000000000000000000000",
    [],[],"outer_terrain_plain"),# Test Area
		
  ("multi_outer_1",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain_beach"),
	("multi_outer_2",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain_beach"),
	("multi_outer_3",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000200000000004b12c000000000000000000000000",
    [],[],"outer_terrain_steppe"),
	("multi_outer_4",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000003e8fa000000000000000000000000",
    [],[],"outer_terrain_plain"),
	("multi_outer_5",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_ocean"),
  ("multi_outer_6",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000500000000004b12c000000000000000000000000",
    [],[],"outer_terrain_ocean"),
	("multi_outer_7",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain"),
	("multi_outer_8",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain"),
	("multi_outer_9",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain"),
	("multi_outer_10",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[],"outer_terrain_plain"),	

  ("boe_castle_1",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[], "outer_terrain_plain"),
  ("boe_castle_2",sf_generate|sf_muddy_water,"none", "none", (0,0),(0,0),0,"0x00000000300000000007d1f4000000000000000000000000",
    [],[], "outer_terrain_plain"),
#MOD end

  ("quick_battle_scene_1",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000023002dee300045d1d000001bf0000299a0000638f", 
    [],[], "outer_terrain_plain"),#Farmhouse
  ("quick_battle_scene_2",sf_generate,"none", "none", (0,0),(0,0),0,"0x0000000250001d630005114300006228000053bf00004eb9", 
    [],[], "outer_terrain_desert"),#Oasis
  ("quick_battle_scene_3",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000023002b76300046d2400000190000076300000692a", 
    [],[], "outer_terrain_plain"),#Tulbuk's Pass
  ("quick_battle_scene_4",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000025a00f23700057d5f00006d6a000050ba000036df", 
    [],[], "outer_terrain_desert"),#Haima Castle
  ("quick_battle_scene_5",sf_generate,"none", "none", (0,0),(0,0),0,"0x000000012007985300055550000064d500005c060000759e",
    [],[],"outer_terrain_plain"),#Ulbas Castle
  ("quick_battle_maps_end",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000001300389800003a4ea000058340000637a0000399b",
    [],[],"outer_terrain_plain"),
]
scenes += ("maps_end",sf_generate,"none", "none", (0,0),(0,0),0,"0x00000001300389800003a4ea000058340000637a0000399b",[],[]),	
#MOD end