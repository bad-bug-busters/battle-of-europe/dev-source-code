gtf_overlay    = 0x00000001 #deprecated
gtf_dusty      = 0x00000002 #controls dustiness of the ground for foot dust particle systems 
gtf_has_color  = 0x00000004 #you can overwrite the ambient color of the ground spec (default: 0.61, 0.72, 0.15)

#IMPORTANT NOTE: Ground_specs have dependency on module system and the engine c++ code!
#                You cannot add new ground types as they are hardcoded in the engine code.
#                Make sure you have updated your module's header_ground_types.py file 

uv_scale = 8.0 #how often it tiles ... 1.0 = 40 meters

#arguments:
#spec_name, flags, material, uv_scale, multitex_material_name, gtf_has_color->color
ground_specs = [
    ("gray_stone", gtf_overlay|gtf_has_color,"ground_stone_a",uv_scale,"none",(1.0,1.0,1.0)),
    ("brown_stone",gtf_overlay|gtf_has_color,"ground_patch_rock",uv_scale,"none",(1.0,1.0,1.0)),
    ("turf",       gtf_overlay|gtf_has_color,"ground_grassy",uv_scale,"none",(1.0,1.0,1.0)),
    ("steppe",     gtf_overlay|gtf_has_color|gtf_dusty,"ground_steppe",uv_scale,"none",(1.0,1.0,1.0)),
    ("snow",       gtf_overlay|gtf_has_color,"ground_snow",uv_scale,"none",(1.0,1.0,1.0)),
    ("earth",      gtf_overlay|gtf_has_color|gtf_dusty,"ground_earth",uv_scale,"none",(1.0,1.0,1.0)),
    ("desert",     gtf_overlay|gtf_has_color|gtf_dusty,"ground_desert", uv_scale,"none",(1.0,1.0,1.0)),
    ("forest",     gtf_overlay|gtf_has_color,"ground_forest",uv_scale,"none",(1.0,1.0,1.0)),
    ("pebbles",    gtf_overlay|gtf_has_color,"ground_pebbles",uv_scale,"none",(1.0,1.0,1.0)),
    ("village",    gtf_overlay|gtf_has_color,"ground_village",uv_scale,"none",(1.0,1.0,1.0)),
    ("path",       gtf_overlay|gtf_has_color|gtf_dusty,"ground_path",uv_scale,"none",(1.0,1.0,1.0)),
]

def write_vec(file,vec):
  file.write(" %f %f %f "%vec)
  
def save_ground_specs():
  file = open("./ground_specs.txt","w")
  for ground_spec in ground_specs:
    file.write(" %s %d %s %f %s"%(ground_spec[0],ground_spec[1],ground_spec[2],ground_spec[3],ground_spec[4]))
    if (ground_spec[1] & gtf_has_color):
      file.write(" %f %f %f"%ground_spec[5])
    file.write("\n")
  file.close()

def save_c_header():
  file = open("./ground_spec_codes.h","w")
  file.write("#ifndef _GROUND_SPEC_CODES_H\n")
  file.write("#define _GROUND_SPEC_CODES_H\n\n")
  file.write("typedef enum {\n")
  for ground_spec in ground_specs:
    file.write("  ground_%s,\n"%ground_spec[0])
  file.write("}Ground_spec_codes;\n")
  file.write("const int num_ground_specs = %d;\n"%(len(ground_specs)))
  file.write("\n\n")
  file.write("\n#endif\n")
  file.close()
  
def save_python_header():
  file = open("./header_ground_types.py","w")
  for ig in xrange(len(ground_specs)):
    ground_spec = ground_specs[ig]
    file.write("ground_%s = %d\n"%(ground_spec[0], ig))
  file.write("\n\n")
  file.close()

print "Exporting ground_spec data..."
save_ground_specs()
save_c_header()
save_python_header()
#print "Finished."
