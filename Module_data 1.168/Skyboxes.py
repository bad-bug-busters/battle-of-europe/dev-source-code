import math #MOD

sf_day        = 0x00000000
sf_dawn       = 0x00000001
sf_night      = 0x00000002

sf_clouds_0   = 0x00000000
sf_clouds_1   = 0x00000010
sf_clouds_2   = 0x00000020
sf_clouds_3   = 0x00000030

sf_no_shadows = 0x10000000
sf_HDR        = 0x20000000  # this will generate HDR-shaded skyboxes, you should make a LDR version of your skybox for compatibility

# mesh_name, flags, sun_heading, sun_altitude, flare_strength, postfx, sun_color, hemi_color, ambient_color, (fog_start, fog_color),

# to generate new skybox textures with hdr option:
# hdr images are required to generate our RGBE coded skybox images


# To add a skybox, you should first edit this file and put new skyboxes.txt file into the "Data/" folder of your module. 
# The first "mesh_name" parameter is the name of the skybox mesh to be used for that entry. 
# You can check our meshes from the "skyboxes.brf" file with OpenBRF and copy them, 
# just replace the material's textures with yours. And you will also have to change the 
# specular color parameters for correct hdr rendering. of course specular color does not 
# corresponds to specular lighting, those parameters are used as compression values 
# for RGBE decoding and should be generated while you generate RGBE textures. 
# (specular.red = Scale component, specular.green = Bias component) 
# You can check our materials for the instances of this usage. 
#
# For skybox textures, we are using uncompressed *.hdr files to generate *_rgb.dds and *_exp.dds files. 
# its just a RGBE encoding of the skybox for hdr lighting. here is an example: 
# "skybox.dds" -> simple non-hdr (LDR) image, used when you dont use hdr (DXT1 format is good)
# "skybox_rgb.dds" -> RGB components of the HDR image (DXT1 format is preffered)
# "skybox_exp.dds" -> E (exponent) component of the HDR image (L16 format is good, you can use half resolution for this texture)
# We are using our own command line tool to generete those files from "skybox.hdr" image. 
# But you can generate them with some of the hdr-image editors too. The images should be gamma corrected and should not have mipmaps. 
# You can use Photoshop with DDS plug-ins or DirectX Texture Viewer to see the contents of our dds images. 
# 
# ..visit Taleworlds Forums for more information..


# skyboxes = [
  # ("skybox_cloud_1", sf_day|sf_clouds_1, 179.0, 52.0, 0.85, "pfx_sunny", (1.39*1.32,1.39*1.21,1.39*1.08), (0.0, 0.0, 0.0), (0.96*16.0/255,0.96*23.5/255,0.96*44.5/255), (300, 0xFF8CA2AD)),
  # ("skybox_cloud_1", sf_day|sf_clouds_1|sf_HDR, 179.0, 52.0, 0.85, "pfx_sunny", (1.39*1.32,1.39*1.21,1.39*1.08), (0.0, 0.0, 0.0), (0.86*16.0/255,0.86*23.5/255,0.86*44.5/255), (300, 0xFF8CA2AD)),

  # ("skybox_night_1", sf_night|sf_clouds_1, 152.0, 38.0, 0.0, "pfx_night", (1.0*17.0/255,1.0*21.0/255,1.0*27.0/255),(0.0,0.0,0.0), (0.9*5.0/255,0.9*5.0/255,0.9*15.0/255), (500, 0xFF152035)),
  # ("skybox_night_1", sf_night|sf_clouds_1|sf_HDR, 152.0, 38.0, 0.0, "pfx_night", (1.0*17.0/255,1.0*21.0/255,1.0*27.0/255),(0.0,0.0,0.0), (0.9*5.0/255,0.9*5.0/255,0.9*15.0/255), (500, 0xFF152035)),
  # ("skybox_night_2", sf_night|sf_clouds_3, 152.0, 38.0, 0.0, "pfx_night", (1.0*17.0/255,1.0*21.0/255,1.0*27.0/255),(0.0,0.0,0.0), (0.9*5.0/255,0.9*5.0/255,0.9*15.0/255), (500, 0xFF152035)),
  # ("skybox_night_2", sf_night|sf_clouds_3|sf_HDR, 152.0, 38.0, 0.0, "pfx_night", (1.0*17.0/255,1.0*21.0/255,1.0*27.0/255),(0.0,0.0,0.0), (0.9*5.0/255,0.9*5.0/255,0.9*15.0/255), (500, 0xFF152035)),
  
  # ("skybox_sunset_1", sf_dawn|sf_clouds_1, 180.0, 9.146, 0.7, "pfx_sunset", (230.0/220,120.0/220,37.0/220),(0.0,0.0,0.0), (14.5/210,21.0/210,40.0/210), (150, 0xFF897262)),
  # ("skybox_sunset_1", sf_dawn|sf_clouds_1|sf_HDR, 180.0, 9.146, 0.7, "pfx_sunset", (230.0/220,120.0/220,37.0/220),(0.0,0.0,0.0), (14.5/210,21.0/210,40.0/210), (150, 0xFF897262)),
  
  # ("skybox_cloud_2", sf_day|sf_clouds_2, 180.0, 19.17, 0.4, "pfx_cloudy", (0.8*0.9,0.8*0.85,0.8*0.75),(0.0,0.0,0.0), (0.8*40.0/255,0.8*46.5/255,0.8*77.0/255), (120, 0xFF607090)),
  # ("skybox_cloud_2", sf_day|sf_clouds_2|sf_HDR, 180.0, 19.17, 0.4, "pfx_cloudy", (0.8*0.9,0.85*0.8,0.8*0.75),(0.0,0.0,0.0), (0.8*40.0/255,0.8*46.5/255,0.8*77.0/255), (120, 0xFF607090)),
  
  # ("skybox_cloud_2", sf_day|sf_clouds_3|sf_no_shadows, 180.0, 19.17, 0.4, "pfx_overcast", (0.4,0.35,0.31),(0.0,0.0,0.0), (50.0/255,60.0/255,103.0/255), (120, 0xFF607090)),
  # ("skybox_cloud_2", sf_day|sf_clouds_3|sf_no_shadows|sf_HDR, 180.0, 19.17, 0.4, "pfx_overcast", (0.4,0.35,0.31),(0.0,0.0,0.0), (50.0/255,60.0/255,103.0/255), (120, 0xFF607090)),
  
  # ("skybox_clearday", sf_day|sf_clouds_0, 179.0, 80.0, 0.95, "pfx_sunny", (0.99*1.32,0.99*1.21,0.99*1.08), (0.0, 0.0, 0.0), (0.96*16.0/255,0.96*23.5/255,0.96*44.5/255), (300, 0xFF8CA2AD)),
  # ("skybox_clearday", sf_day|sf_clouds_0|sf_HDR, 179.0, 80.0, 0.95, "pfx_sunny", (0.99*1.32,0.99*1.21,0.99*1.08), (0.0, 0.0, 0.0), (0.86*16.0/255,0.86*23.5/255,0.86*44.5/255), (300, 0xFF8CA2AD)),
# ]

#MOD begin


arrays = [
# [-16.55, 11, 15, 22],
# [-15.33, 11, 15, 21],
# [-11.84, 11, 15, 21],
# [-6.38, 10, 14, 19],
# [0.64, 51, 61, 62],
# [8.84, 74, 102, 129],
# [17.83, 79, 111, 150],
# [27.31, 82, 115, 158],
# [36.94, 84, 118, 163],
# [46.29, 87, 121, 166],
# [54.71, 89, 123, 168],
# [61.02, 91, 124, 170],
# [63.45, 91, 124, 170],
# [61.02, 91, 124, 170],
# [54.71, 89, 123, 168],
# [46.29, 87, 121, 166],
# [36.94, 84, 118, 163],
# [27.31, 82, 115, 158],
# [17.83, 79, 111, 150],
# [8.84, 74, 102, 129],
# [0.64, 51, 61, 62],
# [-6.38, 10, 14, 19],
# [-11.84, 11, 15, 21],
# [-15.33, 11, 15, 21],

[-16.55, 16*2, 18*2, 22*2],

[-15.33, 16*2, 18*2, 21*2],
[-11.84, 16*2, 18*2, 21*2],
[-6.38, 15*2, 17*2, 19*2],
[0.64, 56, 61, 62],
[8.84, 102, 116, 129],
[17.83, 116, 131, 150],
[27.31, 120, 137, 158],
[36.94, 124, 141, 163],
[46.29, 129, 145, 166],
[54.71, 129, 146, 168],
[61.02, 131, 147, 170],

[63.45, 131, 147, 170],

[61.02, 131, 147, 170],
[54.71, 129, 146, 168],
[46.29, 129, 145, 166],
[36.94, 124, 141, 163],
[27.31, 120, 137, 158],
[17.83, 116, 131, 150],
[8.84, 102, 116, 129],
[0.64, 56, 61, 62],
[-6.38, 15*2, 17*2, 19*2],
[-11.84, 16*2, 18*2, 21*2],
[-15.33, 16*2, 18*2, 21*2],

]

#sun_color  0 = (255, 204, 51)
#sun_color 90 = (255, 255, 255)
sun_g_min = 127;
sun_b_min = 63;
sun_g_mod = 255 - sun_g_min;
sun_b_mod = 255 - sun_b_min;

angle_mod = 1.0 / 90;

skyboxes = [
	("skydome", 0, 180, 63.45, 0.0, "pfx_new", (1.0,1.0,1.0), (0.0, 0.0, 0.0), (0.25, 0.25, 0.25), (0, 0)),
]

for cur_box in range(len(arrays)):
	if arrays[cur_box][0] < 0:
		altitude = arrays[cur_box][0] * -3;
		sun_r = 127;
		sun_g = 127;
		sun_b = 255;
	else:
		altitude = arrays[cur_box][0];
		sun_r = 255;
		sun_g = (sun_g_min + math.pow(altitude * angle_mod, 0.5) * sun_g_mod);
		sun_b = (sun_b_min + math.pow(altitude * angle_mod, 0.5) * sun_b_mod);

	amb_r = arrays[cur_box][1];
	amb_g = arrays[cur_box][2];
	amb_b = arrays[cur_box][3];
	
	print format(min(int(amb_r),255), '02x') + format(min(int(amb_g),255), '02x') + format(min(int(amb_b),255), '02x')
	skyboxes += (
		"skydome",#mesh_name
		0,#flags
		0,#sun_heading
		altitude,#sun_altitude
		0.0,#flare_strength
		"pfx_new",#postfx
		(sun_r/255.0, sun_g/255.0, sun_b/255.0),#sun_color RGB
		(0.0, 0.0, 0.0),#hemi_color RGB
		(amb_r/255.0, amb_g/255.0, amb_b/255.0),#ambient_color RGB
		(0, 0)#fog_start,fog_color
	),
#MOD end



def save_skyboxes():
  file = open("./skyboxes.txt","w")
  file.write("%d\n"%len(skyboxes))
  for skybox in  skyboxes:
    file.write("%s %d %f %f %f %s\n"%(skybox[0],skybox[1],skybox[2],skybox[3],skybox[4],skybox[5]))
    file.write(" %f %f %f "%skybox[6])
    file.write(" %f %f %f "%skybox[7])
    file.write(" %f %f %f "%skybox[8])
    file.write(" %f %d\n"%skybox[9])
  file.close()

print ("Exporting skyboxes...")
save_skyboxes()
print ("Finished.")
  
